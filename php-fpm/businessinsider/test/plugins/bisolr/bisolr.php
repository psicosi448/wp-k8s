<?php
/*
Plugin Name: BiSolr
Description: Sostituisce la ricerca wp con solr
Version: 1
Author: Matteo Barale
*/

function bi_search_result($max = 10, $page = 1){
	if(get_bloginfo('url') == 'https://it.businessinsider.com'){
		$bisolar_url =	'http://solrcloud.am.kataweb.it:8888/solr/businessinsider-wp/select';
	}else{
		$bisolar_url =	'http://test-solrcloud.am.kataweb.it:8888/solr/businessinsider-wp/select';
	}
	if(isset($_GET['s']) && !empty($_GET['s'])){
		$query = stripslashes($_GET['s']);
	}else{
		$query = '*';
	}
	if(isset($_GET['categories']) && !empty($_GET['categories'])){
		$query .= ' AND categories:"' . $_GET['categories'] . '" ';
	}

	$skip = $max * ($page -1);
	$search_options = [
		'q' => '(' . $query .')',
	    'rows' => $max,
		'start' => $skip,
	];

	if(isset($_GET['sort']) && !empty($_GET['sort'])){
		$search_options['sort'] = 'pubdate desc';
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $bisolar_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $search_options);
	$head = curl_exec($ch);
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	$xml_snippet = new SimpleXMLElement( $head );
	return $xml_snippet;
}