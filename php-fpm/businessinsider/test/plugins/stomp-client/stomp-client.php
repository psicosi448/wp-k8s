<?php
/*
Plugin Name: ClientStomp
Description: Plugin che invia un messaggio in coda per ogni azione sui post/page/ters
Author: Manafactory
Version: 1.2 Beta
*/

class ClientStomp{
    private $hosts;
    private $stomp;
    private $queue;
    private static $POST_STATUS_DISABLES = array('attachment','inherit');
    private static $MSG_POST_PUBLISHED = 'post-published';
    private static $MSG_TERM_PUBLISHED = 'term-published';
    private static $MSG_POST_DELETED = 'post-deleted';
    private static $MSG_TERM_DELETED = 'term-deleted';

    private static $ACTIONID_KEY = 'action';
    private static $BLOGID_KEY = 'blogId';
    private static $CONTENTID_KEY = 'contentId';
    private static $PERSISTENT = 'persistent';
    private static $SITEURL_KEY = 'siteUrl';
    private static $BLOGNAME_KEY = 'blogName';
    private static $OBJECT_TYPE = 'objectType';
    private static $CONTENT_TYPE = 'contentType';
    private static $DEBUG = false;

    public function __construct($hosts){
        // include a library
        $queue = get_option('queue');

        require_once("Stomp.php");
        $this->queue = $queue;

        if(!isset($this->stomp)){
            // make a connection
            $this->hosts = $hosts;
            $this->stomp = new Stomp($this->hosts);
        }
    }

    public function init(){
        add_action('transition_post_status', array(&$this, 'transition_post_status'), 1, 3);

        add_action('create_term',  array(&$this, 'create_term'), 1, 3);
        add_action('edit_terms',  array(&$this, 'update_term'), 1, 2);
        add_action('delete_term',  array(&$this, 'delete_term'), 1, 3);

        add_action( 'admin_menu', array(&$this, 'stomp_menu') );
        //call register settings function
        add_action( 'admin_init',  array(&$this,'register_stomp_plugin_settings'));
    }



    function register_stomp_plugin_settings() {
        //register our settings
        register_setting( 'stomp-settings-group', 'queue' );

    }

    public function stomp_menu(){
        add_options_page(
            'Stomp',
            'Stomp Config',
            'manage_options',
            'options_page_stomp',
            array(
                $this,
                'settings_page'
            )
        );

    }

	public function  settings_page() {
?>
<div class="wrap">
    <h2>Stomp Configuration</h2>

    <form method="post" action="options.php">
        <?php settings_fields( 'stomp-settings-group' ); ?>
        <?php do_settings_sections( 'stomp-settings-group' ); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Queue</th>
                <td><input type="text" name="queue" value="<?php echo esc_attr( get_option('queue') ); ?>" /></td>
            </tr>
        </table>

        <?php submit_button(); ?>

    </form>
</div>
    <?php
	}
    public function transition_post_status($new_status, $old_status, $post=null){
        if($new_status=='publish' and !in_array($post->post_status, self::$POST_STATUS_DISABLES)){
            self::do_send(self::$MSG_POST_PUBLISHED, $post->ID, "post", $post->post_type);
        }elseif($old_status=='publish' and ($new_status=='draft' or $new_status=='trash' or $new_status=='pending') and !in_array($post->post_status, self::$POST_STATUS_DISABLES)){
            self::do_send(self::$MSG_POST_DELETED, $post->ID, "post", $post->post_type);
        }
    }

    public function create_term( $term_id, $tt_id, $taxonomy){
            self::do_send(self::$MSG_TERM_PUBLISHED, $term_id, "tax", $taxonomy);
    }

    public function delete_term( $term_id, $tt_id, $taxonomy){
        self::do_send(self::$MSG_TERM_DELETED, $term_id, "tax", $taxonomy);
    }

    public function update_term( $term_id, $taxonomy){
        self::do_send(self::$MSG_TERM_PUBLISHED, $term_id, "tax", $taxonomy);
    }

    private function debug($msg){
    if(!self::$DEBUG)
        return;

        if(is_array($msg))
            $msg = print_r($msg, true);
        error_log($msg);
    }


    private function do_send($msg, $object_id, $object_type = "post", $content_type = "post"){
        global $blog_id;

        if(self::connect()){
            $args = array(
                self::$PERSISTENT => 'true',
                self::$ACTIONID_KEY => $msg,
                self::$OBJECT_TYPE => $object_type,
                self::$CONTENT_TYPE => $content_type,
                self::$CONTENTID_KEY => $object_id,
                self::$SITEURL_KEY => get_bloginfo('url'),
                self::$BLOGID_KEY => $blog_id,
                self::$BLOGNAME_KEY => get_option('blogname')
            );

            // in debug non invio in coda
            self::debug($args);
            if(self::$DEBUG)
                return;

            self::send("", $args);
            self::disconnect();
        }
    }

    private function connect(){

        if(self::$DEBUG)
            return true;

        try{
            $this->stomp->connect();
            return $this->stomp->isConnected();
        } catch (StompException $e) {
            echo $e->getMessage() . "\n";
            echo $e->getDetails() . "\n\n\n";
            return false;
        }
    }

    private function disconnect(){
        // disconnect
        $this->stomp->disconnect();
    }

    private function send($msg, $properties=array(), $sync=null){
        try{
            $this->stomp->send($this->queue, $msg, $properties, $sync);
            return true;
        } catch (StompException $e) {
            return false;
        }
    }
}
$cs = new ClientStomp("failover://(tcp://back195.mi.kataweb.it:61613,tcp://back196.mi.kataweb.it:61613)?randomize=false");
$cs->init();
?>