<?php
if(!class_exists ('mf_ue_user_data')) {
    class mf_ue_user_data {
        private $col;
        private $row;
        public $roles;

        public function __construct($roles = []) {
            $this->col = 0;
            $this->row = 0;
            $this->roles = $roles;

            $this->mf_ue_geta_user_data();
        }

        private function setHeader($filename) {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");;
            header("Content-Disposition: attachment;filename=$filename");
            header("Content-Transfer-Encoding: binary ");
        }

        private function addCol($amount = 1) {
            $this->col += $amount;
            return $this->col;
        }

        private function addRow($amount = 1) {
            $this->row += $amount;
            return $this->row;
        }

        private function xlsBOF() {
            echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
        }

        private function xlsEOF() {
            echo pack("ss", 0x0A, 0x00);
        }

        private function xlsWriteNumber($Value) {
            echo pack("sssss", 0x203, 14, $this->row, $this->col, 0x0);
            echo pack("d", $Value);
        }

        private function xlsWriteLabel($Value) {
            $L = strlen($Value);
            echo pack("ssssss", 0x204, 8 + $L, $this->row, $this->col, 0x0190, $L);
            echo $Value;
        }

        private function cleanData(&$str) {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            return $str;
        }

        public function mf_ue_geta_user_data() {
            $args= array(
                'role__in' => $this->roles,
                'orderby' => 'user_nicename',
                'order' => 'ASC'
            );
            $users = get_users($args);

            if(($users)) {
                $filename = 'estrazione ruoli '.get_bloginfo('name');

                $this->setHeader(sanitize_title($filename) . ".xls");

                // Inizio esportazione
                $this->xlsBOF();
                // Intestazione
                $this->xlsWriteLabel("SSO User");
                $this->addCol();
                $this->xlsWriteLabel("Wp User");
                $this->addCol();
                $this->xlsWriteLabel("Nome");
                $this->addCol();
                $this->xlsWriteLabel("Cognome");
                $this->addCol();
                $this->xlsWriteLabel("Ruolo");
                $this->addRow();

                foreach($users as $row) {
                    $this->col = 0;
                    $user_sso = get_user_meta($row->ID, 'sso_user', true);
                    $user_sso = $user_sso ? $user_sso : 'non specificata';
                    $user_firstname = get_user_meta($row->ID, 'first_name', true);
                    $user_sso = $user_sso ? $user_sso : 'non specificato';
                    $user_lastname = get_user_meta($row->ID, 'last_name', true);
                    $user_sso = $user_sso ? $user_sso : 'non specificato';
                    
                    $role = [];
                    if(!empty($row->roles)) {
                        $role = $row->roles;
                    } else {
                        $role[] = 'undefined';
                    }

                    $this->xlsWriteLabel($this->cleanData($user_sso));
                    $this->addCol();
                    $this->xlsWriteLabel($this->cleanData($row->data->user_login));
                    $this->addCol();
                    $this->xlsWriteLabel($this->cleanData($user_firstname));
                    $this->addCol();
                    $this->xlsWriteLabel($this->cleanData($user_lastname));
                    $this->addCol();
                    $this->xlsWriteLabel($this->cleanData(implode(', ', $role)));
                    $this->addRow();
                }

                $this->xlsEOF();
            }
        }
    }
}
//add_action('init', function() { $user_data = new mf_ue_user_data(); });