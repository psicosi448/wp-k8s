<?php
/**
 * Plugin Name: MF Users Extractor
 * Plugin URI: http://www.kataweb.it
 * Description: Esegue un'estrazione di utenti in base al ruolo e permette di salvarla su file.
 * Version: 0.1
 * Author: Manafactory
 * Author URI: http://www.manafactory.it
 * Requires at least: 4.1
 * Tested up to: 4.8
 *
 * @package Manafactory
 * @category Plugin
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

require_once __DIR__ .'/mf_download.php';

if (!class_exists("MFUserExtractor")) {
    class MFUserExtractor
    {
        public $roles = null;

        public function __construct()
        {
            if($_GET['page'] == 'sub_page_user_extractor' && (isset($_POST['action']) && $_POST['action'] == 'download') && (isset($_POST['role']) && $_POST['role'])) {
                $roles = [];
                foreach ($_POST['role'] as $key => $value) $roles[] = $key;
                new mf_ue_user_data($roles);
                die();
            }
            add_action('admin_init', array(&$this, 'load_settings'));
            add_action('admin_menu', array(&$this, 'mf_ue_menu'), 99);
        }

        function load_settings()
        {
            wp_enqueue_style("mf_ue", plugins_url('assets/css/mf_ue_css.css', __FILE__), false, "1.0", "all");
            wp_enqueue_script("mf_ue", plugins_url( 'assets/js/mf_ue_js.js', __FILE__ ), array( 'jquery' ), "1.0", true);
            wp_localize_script('mf_ue', 'mf_ue_admin', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'admin_url' => admin_url('/tools.php?page=sub_page_user_extractor')));
        }

        public function mf_ue_menu()
        {
            if (function_exists('add_submenu_page')) {
                add_submenu_page('tools.php', 'Estrai utenti', 'Estrai utenti', 'manage_options', 'sub_page_user_extractor', array(&$this, 'iml_ue_page'));
            }
        }

        public function iml_ue_page()
        {
            $this->get_user_roles();
            ?>
            <div class="wrap options_wrap mf_ue">
                <div id="icon-options-general"></div>
                <h2>Estrai set di utenti</h2>
                <form method="post" id="mf_ue_form">
                    <div class="input_section">
                        <div class="input_title">
                            <h3><span class="dashicons dashicons-id-alt"></span>&nbsp;Opzioni estrazione</h3>
                            <span class="submit"><input type="button" class="button-primary" id="download" value="Estrai utenti"/></span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="all_options">
                            <div class="option_input option_text">
                                <label for="user_role"> Ruolo utente:
                                    <!--<small>Seleziona il ruolo utente per visualizzare le capabilities.</small>-->
                                </label>
                                <div class="option-container">
                                    <?php
                                    foreach($this->roles as $key => $value) {
                                       // if ($key != 'administrator') {
                                            $translated_name = esc_html__($value['name'], 'user-role-editor');  // get translation from URE language file, if exists
                                            if ($translated_name === $value['name']) { // get WordPress internal translation
                                                $translated_name = translate_user_role($translated_name);
                                            }
                                            echo '<label class="checkbox-select"><input id="role' . $key . '" name="role[' . $key . ']" data-value="' . $key . '" class="checkbox roles" type="checkbox" />' . $translated_name . '</label>';
                                      //  }
                                    }
                                    ?>
                                </div>
                                <span id="error_description" class="error_msg"></span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                </form>
            </div>
            <?php
        }

        public function get_user_roles()
        {
            if(function_exists('wp_get_current_user')) {
                global $wp_roles;

                if (!isset($wp_roles)) {
                    $wp_roles = new WP_Roles();
                }

                $this->roles = $wp_roles->roles;

                if (is_array($this->roles) && count($this->roles) > 0) {
                    asort($this->roles);
                }

                $this->roles = apply_filters('editable_roles', $this->roles);

                return $this->roles;
            }
        }
    }
}

add_action( 'wp_ajax_mf_ue_get_count', 'mf_ue_get_count' );
add_action( 'wp_ajax_nopriv_mf_ue_get_count', 'mf_ue_get_count' );
function mf_ue_get_count() {
    $users = count_users();
    extract($_POST);

    $count = 0;
    if(!empty($roles) && $users['total_users'] > 0) {
        foreach ($users['avail_roles'] as $key => $value) {
            if(in_array($key, $roles)) $count += $value;
        }
    }
    echo json_encode(['users_count' => $count]);
    die();
}

// Instantiate the class
if (class_exists("MFUserExtractor")) {
    $dl_pluginMFUserExtractor = new MFUserExtractor();
}