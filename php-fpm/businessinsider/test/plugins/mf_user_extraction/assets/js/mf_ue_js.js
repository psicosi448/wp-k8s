/**
 * Created by marcotron on 13/10/2017.
 */

(function($){
    $('#download').click(function(){
        var user_roles = [];
        $('.roles').each(function(i) {
            var chekbox = $(this);
            if (chekbox.is(':checked')) {
                user_roles.push(chekbox.data('value'));
            }
        });

        if(user_roles.length){
            $.ajax({
                url: mf_ue_admin.ajax_url,
                data: {
                    'action': 'mf_ue_get_count',
                    'roles': user_roles
                },
                type: "post",
                dataType: 'json',
                beforeSend: function () {
                    $("body").append('<div id="overloader"></div>');
                },
                success: function (response) {
                    if(response.users_count > 100) {
                        $('#overloader').remove();
                        alert('Attenzione, l\'estrazione richiesta prevede più di ' + response.users_count+' utenti.');
                    } else if(response.users_count <=0) {
                        $('#overloader').remove();
                        alert('Non esiste nessun utente per i ruoli selezionati');
                    } else {
                        var action = $('<input>').attr({
                            type: 'hidden',
                            id: 'action',
                            name: 'action',
                            value: 'download'
                        }).appendTo('form');

                        $('#mf_ue_form').append(action).attr('action', mf_ue_admin.admin_url+'&action=download').submit();
                        $('#overloader').remove();
                    }
                },
                error: function(xhr, type, exception) {
                    $('#overloader').remove();
                    var msg = '';
                    if (xhr.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (xhr.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (xhr.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (type === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (type === 'timeout') {
                        msg = 'Time out error.';
                    } else if (type === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + xhr.responseText;
                    }
                    alert(msg);
                    return false;
                }
            });
        } else {
            alert('Selezionare un ruolo');
        }
        /*var user_role = $('#user_role').find(":selected").val();
        if(user_role=='none') {
            alert('Selezionare un ruolo');
        } else {
            location.href = mf_ue_admin.admin_url+'&action=download&role='+user_role;
        }*/
    });
})(jQuery);


