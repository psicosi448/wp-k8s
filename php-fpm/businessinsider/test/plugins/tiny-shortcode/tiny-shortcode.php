<?php
/*
Plugin Name: tiny-shortcode
Plugin URI: http://marbu.org
Description: Aggiunge tasti custom all'editor
Version: 1.0
Author: Marco Buttarini
Author URI: http://marbu.org
*/
// adding all modules

include('modules/shortcodes.php');
include('modules/functions.php');
include('modules/hooks.php');
include('modules/scripts.php');
include('modules/ajax.php');
