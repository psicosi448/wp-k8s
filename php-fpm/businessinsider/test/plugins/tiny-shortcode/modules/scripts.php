<?php 
// adding styles and scripts
add_action('wp_print_scripts', 'ts_add_script_fn');
function ts_add_script_fn(){


   if(is_admin()){
     wp_enqueue_style('ts_bootsrap_css', plugins_url('/inc/assets/css/boot-cont.css', __FILE__ ) ) ;
     wp_enqueue_style('ts_jquery-ui-1.8.24.custom.css', plugins_url( '/css/jquery-ui.css' , __FILE__ )) ;
	wp_enqueue_script('ts_admin_js', plugins_url('/js/admin.js', __FILE__ ), array('jquery', 'jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-dialog', 'jquery-ui-autocomplete', 'jquery-ui-position' , 'jquery-effects-fade', 'jquery-ui-draggable', 'jquery-ui-resizable', 'jquery-ui-button', 'jquery-effects-core' ), '2.8' ) ;	
	wp_enqueue_style('ts_admin_css', plugins_url('/css/admin.css', __FILE__ ) ) ;	
	
  }else{

  }
}
?>