jQuery(document).ready(function($){
		$('#add_gallery_shortcode').click(function(){
                    selected = tinyMCE.activeEditor.selection.getContent();

                    if( selected ){
                        //If text is selected when button is clicked
                        //Wrap shortcode around it.
                        content =  '[tvzap-gallery]'+selected+'[/tvzap-gallery]';
                    }else{
                        content =  '[tvzap-gallery id="'+$('#post_id').val()+'" title="'+$('#gallery_input').val()+'"]';
                    }

		    if($('#post_id').val() != ""){
			tinymce.execCommand('mceInsertContent', false, content);
			$('#post_id').val('');
			$('#gallery_input').val('');
			$( ".modal_box" ).dialog( "destroy" );
		    }
		});

    $('#reset_gallery_shortcode').click(function(){
             		$('#post_id').val('');
			$('#gallery_input').val('');
	
    })

	
	
}); // main jquery container



jQuery(document).ready(function($){
		$('#add_twitter_shortcode').click(function(){
                    selected = tinyMCE.activeEditor.selection.getContent();

                    if( selected ){
                        //If text is selected when button is clicked
                        //Wrap shortcode around it.
                        content =  '[tvzap-twitter]'+selected+'[/tvzap-twitter]';
                    }else{
			var contenuto = $('#twitter_input').val();
			var match = contenuto.match(/\<p\>(.*?)(?=\<\/p\>)/i);
			if(!match)
			    $("#errore_twitter").html("Inserisci un codice embed valido");
			else
			    var tweet = match[1];

			var statuses = contenuto.match(/status\/(.*?)(?=\")/i)
			if(!statuses)
			    $("#errore_twitter").html("Inserisci un codice embed valido");
			else
			    var id=statuses[1];

		//	var statuses = contenuto.match(/https:\/\/twitter\.com\/(.*?)(?=\")/i)
		//	var statuses = contenuto.match(/https:\/\/twitter\.com\/([A-Za-z0-9]*)\/statuses\/(.*)(?=\")/i)
			//var statuses = contenuto.match(/https:\/\/twitter\.com\/(?=)([_A-Za-z0-9]*)\/statuses\/(.*)\"/i);
                        var statuses = contenuto.match(/https:\/\/twitter\.com\/(?=)([_A-Za-z0-9]*)\/status\/(.*)\"/i);
			if(!statuses)
			    $("#errore_twitter").html("Inserisci qui un codice embed valido");
			else
			    var user = statuses[statuses.length-2];
			
//			alert(statuses.toSource());
			
		        content =  '[tvzap-twitter id="'+id+'"  user="'+user+'"]'+tweet+'[/tvzap-twitter]';
                    }
		    
		    if(($('#twitter_input').val() != "") && (tweet != undefined) && (id != undefined) && (user != undefined)){
			tinymce.execCommand('mceInsertContent', false, content);
			$('#twitter_input').val('');
			$( ".twitter_box" ).dialog( "destroy" );
			$("#errore_twitter").html("");
		    }
		})

	
	
}); // main jquery container




jQuery(document).ready(function($){
		$('#add_facebook_shortcode').click(function(){
                    selected = tinyMCE.activeEditor.selection.getContent();

                    if( selected ){
                        //If text is selected when button is clicked
                        //Wrap shortcode around it.
                        content =  '[tvzap-facebook]'+selected+'[/tvzap-facebook]';
                    }else{
			var contenuto = $('#facebook_input').val();
			
			// recupero i dati che mi servono per ricostruire l'embed
			var match = contenuto.match(/data-href=\"(.*?)(?=\")/i);
			if(!match)
			    $("#errore_facebook").html("Inserisci un codice embed valido");
			else
			    var fburl = match[1];

                        content =  '[tvzap-facebook url="'+ fburl +'"]';
                    }

		    if(($('#facebook_input').val() != "")  && (fburl != undefined)) {
			tinymce.execCommand('mceInsertContent', false, content);
			$('#facebook_input').val('');
			$( ".facebook_box" ).dialog( "destroy" );
			$("#errore_facebook").html("");
		    }
		})

	




	
}); // main jquery container


function receiveMessage(event){
	//alert(event.data);
	//alert(event.origin);
	
    for ( var dataInd in event.data) {
	var data = event.data[dataInd];
//	  alert(data['summary']);
	var datasummary = data['summary'];
	var summary = addslashes(datasummary);
        selected = tinyMCE.activeEditor.selection.getContent();
		var canonical = data["url"];
        if( selected ){
            //If text is selected when button is clicked
            //Wrap shortcode around it.
            content =  '[tvzap-video]'+selected+'[/tvzap-video]';
        }else{

			var arrid = data['id'].split(":");
			// chiamo il fast search per il recupero della canonical

			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				dataType: "json",
				async: false,
				data: {
					action: "fastsearch_action",
					videodomain:  arrid[0] ,
					videoid: arrid[1]
				},
				success: function( fsdata ) {
					console.log(fsdata);
					if(fsdata.asset != null){
						canonical = fsdata.asset.url;
						console.log(canonical);
					}
				}
			});



			if(data['id'] != null)
				content =  '[tvzap-video id="'+data['id']+'" title="'+data['title']+'" url="'+canonical+'" summary="'+summary+'"  width="640" height="360"]';
        }

	tinymce.execCommand('mceInsertContent', false, content);
	jQuery( ".video_box" ).dialog( "destroy" );

    }
    
}
window.addEventListener("message", receiveMessage, false);




function tvzap_normalizer(str){
    str = str.replace(/(\r\n|\n|\r)/gm,"");
    return str;
}



function changeslashes(str) {
str=str.replace('"',"'");
return str;
}

function addslashes(str) {
    if(!str) 
	return "";
    str=str.replace(/\\/g,'\\\\');
    str=str.replace(/\'/g,'\\\'');
    str=str.replace(/\"/g,'\\"');
    str=str.replace(/\0/g,'\\0');
    return str;
}
function stripslashes(str) {
str=str.replace(/\\'/g,'\'');
str=str.replace(/\\"/g,'"');
str=str.replace(/\\0/g,'\0');
str=str.replace(/\\\\/g,'\\');
return str;
}
