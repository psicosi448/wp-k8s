<?php 

// process shortcode funxtionality
add_shortcode( 'tvzap-gallery', 'kw_gallery_shortcode_handler' );
function kw_gallery_shortcode_handler( $atts, $content = null ) {
	$this_post = get_post( $atts['id'] );
	/*
       	$out .= '
	<h3>'.$this_post->post_title.'</h3><div class="embedded">'.$this_post->post_content .'</div>
	';
	*/
	
	$out .= '<div class="entry-photogallery-container is-dimmer gs-gallery"><div class="entry-photogallery">
				<div class="photogallery-hd">
					<h3 class="photogallery-title"><a href="'.get_permalink($this_post->ID).'">'.$this_post->post_title.'</a></h3>
				</div><!-- /photogallery-hd -->                        
				<div class="flexslider" data-id="'.$atts['id'].'" data-seo="'.$this_post->post_name.'">
					<ul class="slides">';
	$gall = get_field("fotogallery", $this_post->ID);

	//		print_r($gall);
	$conta=0;
	foreach($gall as $item){

	  $out .= '<li><div class="slide-img-container"><div class="slide-img"><img src="'.get_bloginfo('template_url').'/assets/img/placeholder.png" data-lazy="'.$item['immagine']['sizes']['inside-gallery'].'" alt="'.esc_attr($item['immagine']['didascalia']).'" /></div></div><div class="caption"><p class="caption-content">'.$item['didascalia'].'</p></div></li>';

	}
	$out .= '</ul>
                        </div>';
	
	$out .= '<div class="entry-photogallery-footer"><div class="slides-count">';

	$out.= '<span class="current-slide"></span> di <span class="total-slides"></span>
		
			</div>';
	if(function_exists('share_gallery_popup')){
		$out.= share_gallery_popup(get_permalink(),array("gsTitle"=>get_the_title(), "gsSummary"=>$this_post->post_title));
	}
                $out.='  </div>
               </div>
                <div class="caption-sliding"></div>
            </div>';


	  return $out; 
}



// process shortcode funxtionality
add_shortcode( 'tvzap-virtual', 'kw_virtual_shortcode_handler' );
function kw_virtual_shortcode_handler( $atts, $content = null ) {
	$this_post = get_post( $atts['id'] );
	
	/*
       	$out .= '
	<h3>'.$this_post->post_title.'</h3><div class="embedded">'.$this_post->post_content .'</div>
	';
	*/
	$ct = getCtFromCpt($this_post->ID, $this_post->post_type);


	if($this_post->post_type == "personaggi_cpt")
	  $ptype = "personaggi_ct";
	else if($this_post->post_type == "programmi_cpt")
	  $ptype = "programmi_ct";

    $args = array('posts_per_page'   => -1,
		  'offset'           => 0,
		  'post_type'           => "attachment",
		  'post_status' => 'inherit', 
		  $ptype => $ct->slug,
		 'orderby' => 'post_date',
		 'order' => 'DESC'
		  );


$list_pic = get_posts($args);
$contami=0;
	
	$out .= '<div class="entry-photogallery-container is-dimmer gs-gallery"><div class="entry-photogallery">
                        <h3><a href="'.get_permalink($this_post->ID).'">'.$this_post->post_title.'</a></h3>
                        <div class="flexslider" data-id="'.$atts['id'].'" data-seo="'.$this_post->post_name.'">
                            <ul class="slides">';

	
	//	$gall = get_field("fotogallery", $this_post->ID);
	$conta=0;
	foreach($list_pic as $pic){
	  
	  $image = $pic->guid;
	  $conta++;
	  //	  $out .= '<li><img src="'.get_bloginfo('template_url').'/assets/img/placeholder.png" data-lazy="'.$image.'" alt="Immagine '.$conta.'" /><div class="caption"><p class="caption-content"> Immagine '.$conta.'</p></div></li>';
	  
	  $out .= '<li><img src="'.get_bloginfo('template_url').'/assets/img/placeholder.png" data-lazy="'.$image.'" alt="Immagine '.$conta.'" /></li>';
	}
	
	//		print_r($gall);
	$out .= '</ul>
                        </div>';
	
	$out .= '<div class="entry-photogallery-footer"><div class="slides-count">';
	
	$out.= '<span class="current-slide"></span> di <span class="total-slides"></span>
	
			</div>';
	if(function_exists('share_gallery_popup')){
		$out.= share_gallery_popup(get_permalink(),array("gsTitle"=>get_the_title(), "gsSummary"=>$this_post->post_title));
	}
                $out.='  </div>
               </div>
            </div>';

	
	return $out; 
}



add_shortcode( 'tvzap-video', 'kw_video_shortcode_handler' );
function kw_video_shortcode_handler( $atts, $content = null ) {
    global $post;
    $posturl=get_permalink($post->ID);
  // sostituisco la url con l'embed
    $newurl = str_replace("video.repubblica.it", "video.repubblica.it/embed", $atts['url']);
    $newurl = str_replace("video.tvzap.kataweb.it", "video.tvzap.kataweb.it/embed", $newurl);
    $newurl = str_replace("video.d.repubblica.it", "video.d.repubblica.it/embed", $newurl);
    $newurl = str_replace("video.espresso.repubblica.it", "video.espresso.repubblica.it/embed", $newurl);
    $newurl = str_replace("video.3nz.it", "video.3nz.it/embed", $newurl);
    $newurl = str_replace("video.it.businessinsider.com", "video.it.businessinsider.com/embed", $newurl);

	$newurl = str_replace('http://', '//', $newurl);
	$newurl = str_replace('https://', '//', $newurl);


  $width=  $atts['width'];
  $height=  $atts['height'];
  $containerID = preg_split('/:/',$atts['id']);
  $containerID = $containerID[2];

  $out .= '<div class="entry-video"><div id='.$containerID.' class="video-container noscript"><iframe class="video-embed" src="'.$newurl.'?adref='.$posturl.'&responsive=true';
  if(function_exists("is_amp_endpoint")){
  	if(is_amp_endpoint()){
	    $out .= '&_wt_visit_from=webapp.amp';
    }
  }
	if (is_feed( )){
  	$out .= '&_wt_visit_from=webapp.fbia';
  }

	$out .= '" width="'.$width.'" height="'.$height.'" ></iframe></div>';

  if(function_exists('share_popup')){
  	$out.= share_popup(get_permalink($post->ID)."#".$containerID);
  }
  $out .="</div>";
  //  $out .= '<iframe class="gele-video-embed"  frameborder="0" marginwidth="0" marginheight="0"  style="border: 0px; width: '.intval($width+13).'px;height: '.intval($height+27).'px;" src="'.$newurl.'&width='.$width.'&height='.$height.'"></iframe>';
  return $out; 
}


add_shortcode( 'tvzap-facebook', 'kw_facebook_shortcode_handler' );
function kw_facebook_shortcode_handler( $atts, $content = null ) {
  $out .= '<div class="fb-post" data-href="'.$atts['url'].'" data-width="550"></div>
<script>$(function(){ $().widgetWrap({ provider:\'fb\' }) });</script>';

  return $out; 
}


add_shortcode( 'tvzap-twitter', 'kw_twitter_shortcode_handler' );
function kw_twitter_shortcode_handler( $atts, $content = null ) {
  

$out .='<blockquote class="twitter-tweet"><p></p><a href="https://twitter.com/'.$atts['user'].'/statuses/'.$atts['id'].'"></a></blockquote>
<script>$(function(){ $().widgetWrap({ provider:\'tw\' }) });</script>';

  return $out;
}

?>