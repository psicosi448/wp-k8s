<?php 
	require_once('../../../../wp-load.php');
	require_once('../../../../wp-admin/includes/admin.php');
	do_action('admin_init');
 
	if ( ! is_user_logged_in() )
		die('You must be logged in to access this script.');
 

?>

jQuery(document).ready(function($) {	

    tinymce.create('tinymce.plugins.tvzap_gallery_plugin', {
        init : function(ed, url) {
                // Register command for when button is clicked
                ed.addCommand('tvzap_gallery_insert_shortcode', function() {			
		    $( ".modal_box" ).dialog({
		      width: 700,
			  height: 160

		      });
		    jQuery( "#gallery_input" ).autocomplete("option", "appendTo", "#copy_dialog");
		    
                });
            ed.addButton('tvzap_gallery_button', {title : 'Inserisci Gallery', cmd : 'tvzap_gallery_insert_shortcode', image: url + '/img/gallery.png' });
        },   
    });
    tinymce.PluginManager.add('tvzap_gallery_button', tinymce.plugins.tvzap_gallery_plugin);



});
