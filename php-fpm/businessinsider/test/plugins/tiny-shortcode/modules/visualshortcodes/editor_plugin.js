(function(){
	/*
	 * Create the TinyMCE plugin object.
	 * 
	 * See http://www.tinymce.com/wiki.php/Creating_a_plugin for more information
	 * on how to create TinyMCE plugins.
	 */
    tinymce.create( 'tinymce.plugins.visualShortcodes', {
		
		/*
		 * Create the function to initialize our plugin. We're going to set up all the
		 * properties necessary to function and then set some event handlers to make everything
		 * work properly.
		 * 
		 * This function takes two arguments:
		 * 
		 * ed: the Editor object (this code will run once for each editor on the page)
		 * url: the URL of the directory that this file resides in
		 */
//        init : function( ed, url ){
        visualShortcodes : function( ed, url ){
	    //alert("init1");
			
			// A counter will help us assign unique ids to each shortcode in this editor.
            this.counter = 0;
			
			// Set up some variables
            var t = this,
                i,
                shortcode,
                names = [];
			
			// Save the url in the object so that it's accessible elsewhere
            t.url = url;

			// Pull in the shortcodes object that we stored in the internationalization object earlier
//            t._shortcodes = tinymce.i18n['visualShortcode.shortcodes'];
//	    alert(t._shortcodes);	
	    //alert("forseforse");
	    //alert(myshort);
            t._shortcodes = myshort;
            if( !t._shortcodes || undefined === t._shortcodes.length || 0 == t._shortcodes.length ){
				// If we don't have any shortcodes, we don't need to do anything else. Bail immediately.
                return;
            }
			
			// Set up the shortcodes object and fill it with the shortcodes
            t.shortcodes = {};
            for( i = 0, shortcode = t._shortcodes[i]; i < t._shortcodes.length; shortcode = t._shortcodes[++i]){
                if(undefined === shortcode.shortcode || '' == shortcode.shortcode || undefined === shortcode.image || '' == shortcode.image){
					/*
					 * All shortcodes must have a non-empty string for the shortcode and image properties.
					 * If those conditions are not met, skip to the next one.
					 */
                    continue;
                }
                t.shortcodes[shortcode.shortcode] = shortcode;
                names.push(shortcode.shortcode);
            }
	    //alert(names);
            if( names.length < 1 ){
				// Again, if we don't have any valid shortcodes to work with, bail.
                return;
            }

            t._buildRegex( names );

            t._createButtons();

			/*
			 * Add an event handler for our plugin on the 'mousedown' event. This sets up
			 * the handler to show our control buttons if the user clicks an image that
			 * represents a shortcode.
			 */
            //ed.onMouseDown.add(function( ed, e ){
	    ed.on("mousedown", function( ed, e ){
				// We're only interested in images that have the right class
                if( e.target.nodeName == 'IMG' && ed.dom.hasClass(e.target, 'kw_galleryVisualShortcode')){
					// Get the name of the shortcode from the ID
                    var imgID = e.target.id.replace( /^vscImage\d+-(.+)$/, '$1' );
					// Check if the shortcode has a command defined. If so...
                    if( undefined !== t.shortcodes[imgID] && undefined !== t.shortcodes[imgID].command ){
						// Show both the delete and edit buttons. Otherwise...
                        ed.plugins.wordpress._showButtons(e.target, 'kw_gallery_vscbuttons');
                    } else {
						// Only show the delete button
                        ed.plugins.wordpress._showButtons(e.target, 'kw_gallery_vscbutton');
                    }
                } else {
					// If we're not clicking the right kind of image, hide the buttons just in case
                    t._hideButtons();
                }
            });

			/*
			 * Add an event handler for our plugin on the editor's 'change' event. This function
			 * replaces the shortcodes with their images and updates the content of the editor as
			 * the contents of the editor are being changed.
			 * 
			 * The 'change' event fires each time there is an 'undo-able' block change made.
			 */
			//ed.onChange.add(function(ed, o){
	    ed.on('change',function(ed){
				if( !t.regex.test(ed.content)){
					/*
					 * We shouldn't bother with changing anything and repainting the editor if we
					 * don't even have a regex match on our shortcodes.
					 */
					return;
				}
				
				// Get the updated content
				ed.content = t._doScImage( ed.content );
				// Set the new content
				ed.setContent(ed.content);
				// Repaint the editor
				ed.execCommand('mceRepaint');
			});

			/*
			 * Add an event handler for our plugin on the editor's 'beforesetcontent' event. This
			 * will swap the shortcode out for its image when the editor is initialized, or
			 * whenever switching from HTML to Visual mode.
			 */
            //ed.onBeforeSetContent.add(function(ed, o){
	    ed.on("BeforeSetContent", function(ed){
				if( !t.regex.test(ed.content)){
					/*
					 * We shouldn't bother with changing anything and repainting the editor if we
					 * don't even have a regex match on our shortcodes.
					 */
					return;
				}
				
				/*
				 * Honestly, I'm not sure why/how this works. We don't return anything and are
				 * making the change directly on the object passed in as the second argument. How
				 * does this change the content of the editor? I don't know. But it seems to work.
				 * 
				 * For whatever reason, this does not require a full setting / repainting of the
				 * editor's content like the function above.
				 * 
				 * This code was borrowed from the WordPress gallery TinyMCE plugin.
				 */
                ed.content = t._doScImage( ed.content );
            });

			/*
			 * Add an event handler for our plugin on the editor's 'postprocess' event. This
			 * changes the images back to shortcodes before saving the content to the form field
			 * and when switching from Visual mode to HTML mode.
			 * 
			 * This code was borrowed from the WordPress gallery TinyMCE plugin.
			 */
            //ed.onPostProcess.add(function(ed, o) {
		  ed.on("PostProcess", function(ed) {
                if( ed.get ){
                    ed.content = t._getScImage( ed.content );
				}
            });

			/*
			 * Add an event handler for the plugin on the editor's initialization event. This
			 * sets up some global event handlers to hide the buttons if the user scrolls or
			 * if they drag something with their mouse.
			 * 
			 * This code was borrowed from the WordPress gallery TinyMCE plugin.
			 */
            //ed.onInit.add(function(ed) {
		  ed.on("init", function() {
				// Hide the buttons if the user scrolls
//                tinymce.dom.Event.add(ed.getWin(), 'scroll', function(e) {
                tinymce.DOM.bind(ed.getWin(), 'scroll', function(e) {
                    t._hideButtons();
                });
				// Hide the buttons if the user drags something
                //tinymce.dom.Event.add(ed.getBody(), 'dragstart', function(e) {
		tinymce.DOM.bind(ed.getBody(), 'dragstart', function(e) {                
		  t._hideButtons();
                 });
            });

        },

		/*
		 * Replace shortcodes with their respective images.
		 * 
		 * For each match, the function will replace it with an image. The arguments correspond,
		 * respectively, to:
		 *  - the whole matched string (the whole shortcode, possibly wrapped in <p> tags)
		 *  - the name of the shortcode
		 *  - the arguments of the shortcode (could be an empty string)
		 * 
		 * The id of the shortcode image will start with 'vscImage', followed by the current counter
		 * value (which is incremented as it's used, so next time it will be different), a hyphen, and 
		 * the name of the shortcode.
		 * 
		 * The class 'mceItem' prevents WordPress's normal image management icons from showing up when
		 * the image is clicked.
		 * 
		 * The arguments of the shortcode are encoded and stored in the 'title' attribute of the image.
		 * 
		 * This code is based largely on the WordPress gallery TinyMCE plugin.
		 */
        _doScImage: function( co ){
            var t = this;
            return co.replace( t.regex, function(a,b,c){
		
		var part = a.split(c +']');
		var inizio = part[1];
		var insider = inizio.split('[/'+ b);
		var contenuto = insider[0];
		//alert(contenuto);

		
                return '<img src="'+t.shortcodes[b].transparent+'"  style="background:url('+t.shortcodes[b].image+') no-repeat center center;border: 1px dashed #888; width: 99%; height: 250px; margin: 10px 0px;" id="vscImage'+(t.counter++)+'-'+b+'" class="mceItem wp-gallery1 kw_galleryVisualShortcode" title="' + b + tinymce.DOM.encode(c) + '" alt="' + tinymce.DOM.encode(contenuto) + '"/>' ;
            });
        },

		/*
		 * Replace images with their respective shortcodes.
		 * 
		 * This code is based mostly on the WordPress gallery TinyMCE plugin.
		 */
        _getScImage: function( co ){

			// Used to grab the title/class attributes and decode them
            function getAttr(s, n) {
                n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
                return n ? tinymce.DOM.decode(n[1]) : '';
            };

            return co.replace(/(?:<p[^>]*>)*(<img[^>]+>)(?:<\/p>)*/g, function(a,im) {
                var cls = getAttr(im, 'class');
//		alert(a);
//		alert(im);
		var kw_title = getAttr(im, 'title');
		var kw_content = getAttr(im, 'alt');
		var kw_nome = kw_title.split(' ');
//		alert()
                if ( cls.indexOf('kw_galleryVisualShortcode') != -1 )
                    return '<p>['+tinymce.trim(kw_title)+']'+tinymce.trim(kw_content)+'[/'+tinymce.trim(kw_nome[0])+']</p>';

                return a;
            });
        },

		/*
		 * Builds the plugin's shortcode for finding registered shortcodes
		 * 
		 * The regex is global and case insensitive, and only searches for self-closing
		 * shortcodes.
		 */
        _buildRegex: function( names ){
            var t = this,
            //reString = '';
	    
//            reString = '\\\[(' + names.join('|') + ')(( [^\\\]]+)*)\\\]';
// \[(tvzap-gallery|tvzap-video|tvzap-twitter|tvzap-facebook)(( [^\]]+)*)\]

 //           reString = '\\\[(' + names.join('|') + ')(( [^\\\]]+)*)\\\]';
//            reString = '\\\[(' + names.join('|') + ')(.*?)?\\\](?:(.+?)?\\\[\\/(' + names.join('|') + ')\\\])?';
	    reString = '\\\[(' + names.join('|') + ')(( [^\\\]]+)*)?\\\](?:(.+?)?\\\[\\/(' + names.join('|') + ')\\\])?';

//	    alert(reString);
	    t.regex = new RegExp( reString, 'gi' );
        },

		/* 
		 * Hide the buttons from the user!
		 */
        _hideButtons: function(){
            tinymce.DOM.hide('kw_gallery_vscbuttons');
            tinymce.DOM.hide('kw_gallery_vscbutton');
        },

		/* 
		 * Creates the action buttons
		 * 
		 * We need two sets of buttons: one for shortcodes that only get a delete button
		 * and one for shortcodes that get both a delete and an edit button. Set up the
		 * event handlers for all three of the buttons here too.
		 */
        _createButtons: function(){
	    //alert("createButtons");	
			// Initialize the variables we need/want
            var t = this,
                ed = tinyMCE.activeEditor,
                DOM = tinyMCE.DOM,

                delbutton,
                delbutton2;
				
			// Remove extra copies of our buttons (in case we have multiple editors on the page)
            DOM.remove( 'kw_gallery_vscbuttons' );
            DOM.remove( 'kw_gallery_vscbutton' );

			/*
			 * Add the divs to hold the buttons and make sure they start their lives hidden.
			 * We have two button holder divs; the one with the id 'kw_gallery_vscbuttons' will have
			 * both an edit and a delete button, whereas the one with the id 'kw_gallery_vscbutton'
			 * will have only a delete button.
			 */
            DOM.add( document.body, 'div', {
                id: 'kw_gallery_vscbuttons',
                style: 'display:none;'
            });
            DOM.add( document.body, 'div', {
                id: 'kw_gallery_vscbutton',
                style: 'display:none;'
            });

			

			// Add the 'delete' button (to go with the 'edit' button)
            delbutton = DOM.add( 'kw_gallery_vscbuttons', 'img', {
                src: t.url + '/img/delete.png',
                id: 'kw_gallery_delshortcode',
                width: '24',
                height: '24',
                style: 'margin:2px;'
            });
/*
			// Add the 'delete' button (to go by itself)
            delbutton2 = DOM.add( 'kw_gallery_vscbutton', 'img', {
                src: t.url + '/img/delete.png',
                id: 'kw_gallery_delshortcode2',
                width: '24',
                height: '24',
                style: 'margin:2px;'
            });
*/


			// Add an event handler for both delete buttons to delete the image on click
//			tinymce.dom.Event.add( [ delbutton, delbutton2 ], 'mousedown', function(e){
tinymce.DOM.bind( [ delbutton, delbutton2 ], 'mousedown', function(e){
				var ed = tinyMCE.activeEditor, el = ed.selection.getNode(), el2;
				if( el.nodeName == 'IMG' && ed.dom.hasClass( el, 'kw_galleryVisualShortcode' ) ){
					// If we have the right kind of image selected, go about deleting it.
					// Grab the parent node ahead of time.
					el2 = el.parentNode;
					// Get rid of the element
					ed.dom.remove( el );
					// Repaint the editor, just in case.
					ed.execCommand( 'mceRepaint' );
					// Hide the buttons
					t._hideButtons();
					// Select the parent element
					ed.selection.select(el2);
					// Prevent bubbling.
					return false;
				}
			});
            
        }
    });

	// Add the plugin object to the TinyMCE plugin manager
    tinymce.PluginManager.add( 'visualshortcodes', tinymce.plugins.visualShortcodes );
	
})();
