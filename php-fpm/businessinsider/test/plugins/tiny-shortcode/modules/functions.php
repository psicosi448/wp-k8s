<?php 
//Class to replace shortcode with image in visual editor
class tvzap_Visual_Shortcodes {
	function __construct() {
		add_filter('mce_external_plugins', array($this, 'plugins'));
		add_filter('mce_external_languages', array($this, 'lang'));
		add_action('admin_print_styles', array($this, 'styles'));
	}
	function styles() {
		wp_enqueue_style('kw_gallery_visualshortcodes', plugins_url('/css/buttons.css', __FILE__));
	}
	function plugins($plugins) {
	  $plugins['visualshortcodes'] = plugins_url('visualshortcodes/editor_plugin.js', __FILE__);
		return $plugins;
	}
	function lang($langs) {
		
		$langs['visualshortcodes'] = dirname(__FILE__) . '/visualshortcodes/langs.php';
		return $langs;
	}

}
new tvzap_Visual_Shortcodes();



?>