<?php 
	
add_action('wp_ajax_post_search_action', 'kw_gallery_search_action');
add_action('wp_ajax_nopriv_post_search_action', 'kw_gallery_search_action');

function kw_gallery_search_action(){
	global $wpdb;

  if( check_ajax_referer( 'security_nonce', 'security') ){

	$search_line = '%'.$_GET['name_startsWith'].'%';


	$res = $wpdb->get_results( $wpdb->prepare( "SELECT ID, post_title, post_date FROM ".$wpdb->prefix."posts WHERE post_title LIKE '%s' AND post_type = 'gallery' ORDER BY post_date DESC", $search_line ) );

	foreach( $res as $single_res ){
	  $out[] = array( 'id' => $single_res->ID, 'title' => $single_res->post_title." (". date_i18n(get_option( 'date_format' ),strtotime($single_res->post_date)).")" );
	}
	echo json_encode( $out );
  }
  die();
}




add_action('wp_ajax_fastsearch_action', 'kw_fastsearch_action');
add_action('wp_ajax_nopriv_fastsearch_action', 'kw_fastsearch_action');

function kw_fastsearch_action(){
    global $wpdb;

    if(is_admin()){
        $fsurl = "https://video.repubblica.it/php/services/pri/fast_search.php?domain=" . $_GET['videodomain'] . "&id=" . $_GET['videoid'] ;
        $response = wp_remote_get($fsurl);

        if ( is_array( $response ) && ! is_wp_error( $response ) ) {
            $headers = $response['headers']; // array of http header lines
            $body    = $response['body']; // use the content

            $xml = simplexml_load_string($body, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);

            echo json_encode( $array );
        }else{
            echo false;
        }

    }
    die();
}




?>