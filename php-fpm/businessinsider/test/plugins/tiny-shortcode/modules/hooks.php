<?php 

function strbtwn($s,$start,$end){
    $i = strpos($s,$start);
    $j = strpos($s,$end,$i);
    return $i===false||$j===false? false: substr(substr($s,$i,$j-$i),strlen($start));
}

if (!function_exists('json_last_error_msg')) {
    function json_last_error_msg() {
        static $errors = array(
            JSON_ERROR_NONE             => null,
            JSON_ERROR_DEPTH            => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH   => 'Underflow or the modes mismatch',
            JSON_ERROR_CTRL_CHAR        => 'Unexpected control character found',
            JSON_ERROR_SYNTAX           => 'Syntax error, malformed JSON',
            JSON_ERROR_UTF8             => 'Malformed UTF-8 characters, possibly incorrectly encoded'
        );
        $error = json_last_error();
        return array_key_exists($error, $errors) ? $errors[$error] : "Unknown error ({$error})";
    }
} 

// check shortcode on save post
add_action("save_post","tvzap_save_shortcode_as_postmeta");
function tvzap_save_shortcode_as_postmeta($post_id){
  if ( !wp_is_post_revision( $post_id ) ) {
    $post = get_post( $post_id );
    $pattern = get_shortcode_regex();
    if(preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches )&& array_key_exists( 2, $matches )){
      $found = false;
      $counter=0;
      // ciclo sugli shortcode per individuare il primo video embeddato
      foreach($matches[2] as $short){
	if($short == "tvzap-video"){
	  $index = $counter;
	  break;
	}
	$counter++;
      }
      // 
      $idvideo = strbtwn($matches[3][$index], 'id="', '" ');
      if($idvideo){
	$expvid = explode(":", $idvideo); 
	$idv = $expvid[2];
	if($idv){
	  // ho individuato l'id del primo video tvzap embeddato, interrogo il fast search per recuperare i tag og video
	  //	  if (strpos(get_bloginfo("url"), 'test') !== false)
	  //  $fsurl =  'http://test.video.repubblica.it/php/services/fast_search.php';
	  //else
	    $fsurl =  'http://video.repubblica.it/php/services/fast_search.php';

	  $fsurl .= "?id=".$idv."&output=json";
	  //	  echo $fsurl;
	  $json = file_get_contents($fsurl);
	  // rimuovo i commenti html
	  $json = preg_replace('/<!--(.*)-->/Uis', '', $json);

	  if($json){

	    $obj = json_decode($json);
	    /*
	    print_r($obj);
	    echo   json_last_error_msg();*/
	    $type = $obj[0]->type;
	    $ogvideo = $obj[0]->ogvideo;
	    $ogvideo_width = $obj[0]->ogvideo_width;
	    $ogvideo_height = $obj[0]->ogvideo_height;
	    //	    echo "-".$type."--".$ogvideo;
	    if($ogvideo){
	      update_post_meta($post_id, "ogvideo", $ogvideo);
	      update_post_meta($post_id, "ogvideo_width", $ogvideo_width);
	      update_post_meta($post_id, "ogvideo_height", $ogvideo_height);
	      return;
	    }
	    
	  }
	}
      }
      //   exit;

      
      
      $alls=$matches['2'];
      if($alls){
	// salvo il dato come postmeta
	update_post_meta($post_id, "content_shortcode", "alls");
      }
    }

    // se non sono uscito prima cancello tutti gli eventuali ogvideo inseriti
    $oldog=get_post_meta($post_id, "ogvideo", true);
    if($oldog){
      delete_post_meta($post_id, "ogvideo");
      delete_post_meta($post_id, "ogvideo_width");
      delete_post_meta($post_id, "ogvideo_height");

    }
  }
}



add_action("social_properties", "ogvideo_meta", 11);
function ogvideo_meta(){
  if(is_single()){
    global $post;
    
    $ogvideo=get_post_meta($post->ID, "ogvideo", true);
    $ogvideo_width=get_post_meta($post->ID, "ogvideo_width", true);
    $ogvideo_height=get_post_meta($post->ID, "ogvideo_height", true);
    if($ogvideo){
      echo '<meta property="og:video" content="'.$ogvideo.'">'."\n";
      echo '<meta property="og:video:width" content="'.$ogvideo_width.'">'."\n";
      echo '<meta property="og:video:height" content="'.$ogvideo_height.'">'."\n";
    }
  }
}

 // init process for registering our button
 add_action('init', 'tvzap_gallery_shortcode_button_init');
 function tvzap_gallery_shortcode_button_init() {

      //Abort early if the user will never see TinyMCE
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;

      //Add a callback to regiser our tinymce plugin   
      add_filter("mce_external_plugins", "tvzap_register_tinymce_plugin"); 

      // Add a callback to add our button to the TinyMCE toolbar
      add_filter('mce_buttons', 'tvzap_gallery_add_tinymce_button');
}


//This callback registers our plug-in
function tvzap_register_tinymce_plugin($plugin_array) {
  //  $plugin_array['tvzap_gallery_button'] = plugins_url( '/editor_plugin.js.php', __FILE__ );
    $plugin_array['tvzap_video_button'] = plugins_url( '/video_plugin.js.php', __FILE__ );
//    $plugin_array['tvzap_twitter_button'] = plugins_url( '/twitter_plugin.js.php', __FILE__ );
    //   $plugin_array['tvzap_facebook_button'] = plugins_url( '/facebook_plugin.js.php', __FILE__ );
    return $plugin_array;
}

//This callback adds our button to the toolbar
function tvzap_gallery_add_tinymce_button($buttons) {
            //Add the button ID to the $button array
//    $buttons[] = "tvzap_gallery_button";
    $buttons[] = "tvzap_video_button";
//    $buttons[] = "tvzap_twitter_button";
//    $buttons[] = "tvzap_facebook_button";
    return $buttons;
}

// inserting autosugges and modal widow functionality
add_action('admin_footer', 'tvzap_footer_data');
function tvzap_footer_data(){
		echo '
	<p class="hidden modal_box" title="Inserisci Gallery" >
<small>Cerca il titolo di una gallery</small><br /><br />

<input type="text" class="wide_input" id="gallery_input" name="gallery_input" value="" />
<input type="hidden"  id="post_id" />
<input type="button" name="add_gallery_shortcode" id="add_gallery_shortcode" class="button button-primary button-large" value="Inserisci" >
<input type="button" name="reset_gallery_shortcode" id="reset_gallery_shortcode" class="button button-large" value="Annulla" >
</p>

<script>

    jQuery( "#gallery_input" ).autocomplete({
	source: function( request, response ) {
        jQuery.ajax({
          url: \''.get_option('home').'/wp-admin/admin-ajax.php\',
          dataType: "json",
          data: {
            action: "post_search_action",
			name_startsWith: request.term,
  			security: \''.wp_create_nonce("security_nonce").'\'
          },
          success: function( data ) {
            response( jQuery.map( data, function( item ) {
              return {
                label: item.title ,
                value: item.id
              }
            }));
          }
        });
      },
        minLength: 3,
		width: 600,

        focus: function( event, ui ) {
            jQuery( "#gallery_input" ).val( ui.item.label );
            return false;
        },
        select: function( event, ui ) {
			jQuery("#gallery_input").val( ui.item.label );
			jQuery("#post_id").val( ui.item.value );
			console.debug(  ui.item );
            return false;
    }
});

</script>
';

/*
		echo '	<p class="hidden twitter_box" title="Twitter" >
<small>Inserisci il codice del Tweet</small><br>
<textarea style="width:98%; height: 230px;" id="twitter_input" name="twitter_input"></textarea><br>
<span class="errore" id="errore_twitter"></span>
<input type="button" name="add_twitter_shortcode" id="add_twitter_shortcode" class="button button-primary button-large" value="Inserisci" >
</p>';


		echo '	<p class="hidden facebook_box" title="Facebook" >
<small>Inserisci il codice di Facebook</small><br>
<textarea style="width:98%; height: 230px;" id="facebook_input" name="facebook_input"></textarea><br>
<span class="errore" id="errore_facebook"></span>
<input type="button" name="add_facebook_shortcode" id="add_facebook_shortcode" class="button button-primary button-large" value="Inserisci" >


</p>';
*/


		//		echo '	<p class="hidden video_box" title="Inserisci Video" ><iframe style="width: 98%; height:98%;" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen src="http://data.kataweb.it/content-finder/find?sourceSystemEnable=repubblica-reptv&from='.get_bloginfo('url').'"></iframe></p>';

}


add_filter('tvzap_visual_shortcodes','add_visual_shortcode_image_tvzap');
function add_visual_shortcode_image_tvzap($shortcodes){

	$shortcodes[] = array(
		'shortcode' => 'tvzap-gallery',
		'image' => plugins_url( '/img/camera.png', __FILE__ ),
		'transparent' => plugins_url( '/img/transparent.png', __FILE__ ),
		'command' => NULL,
	);


	$shortcodes[] = array(
		'shortcode' => 'tvzap-video',
		'image' => plugins_url( '/img/film.png', __FILE__ ),
		'transparent' => plugins_url( '/img/transparent.png', __FILE__ ),
		'command' => NULL,
	);


	$shortcodes[] = array(
		'shortcode' => 'tvzap-twitter',
		'image' => plugins_url( '/img/tweet.png', __FILE__ ),
		'transparent' => plugins_url( '/img/transparent.png', __FILE__ ),
		'command' => NULL,
	);


	$shortcodes[] = array(
		'shortcode' => 'tvzap-facebook',
		'image' => plugins_url( '/img/post.png', __FILE__ ),
		'transparent' => plugins_url( '/img/transparent.png', __FILE__ ),
		'command' => NULL,
	);


	return $shortcodes;
}



?>