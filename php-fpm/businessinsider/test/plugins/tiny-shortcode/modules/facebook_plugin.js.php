<?php 
	require_once('../../../../wp-load.php');
	require_once('../../../../wp-admin/includes/admin.php');
	do_action('admin_init');
 
	if ( ! is_user_logged_in() )
		die('You must be logged in to access this script.');
 

?>

jQuery(document).ready(function($) {	

    tinymce.create('tinymce.plugins.tvzap_facebook_plugin', {
        init : function(ed, url) {
                // Register command for when button is clicked
                ed.addCommand('tvzap_facebook_insert_shortcode', function() {			
		    $( ".facebook_box" ).dialog({
		      width: 500,
			  height: 390
			  });
		    
                });
            ed.addButton('tvzap_facebook_button', {title : 'Facebook', cmd : 'tvzap_facebook_insert_shortcode', image: url + '/img/facebook.png' });
        },   
    });
    tinymce.PluginManager.add('tvzap_facebook_button', tinymce.plugins.tvzap_facebook_plugin);



});
