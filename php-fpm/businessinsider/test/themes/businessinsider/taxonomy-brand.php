<?php
get_header();  $post_not_in = [];
$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;
$acf_key = $taxonomy . '_' . $term_id;
$wide= get_field('wide', $acf_key);
if($wide)
	get_template_part("template/taxonomy-brand-wide");
else
	get_template_part("template/taxonomy-brand-standard");

?>

<?php get_footer(); ?>