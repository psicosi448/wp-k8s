<?php get_header(); ?>

<!-- MAIN -->
<main style="min-height: 100vh;">
	<div class="quicklook-wrapper wrapper">
		<div class="wrapper">
			<div class="group-primary article-wrapper">
				<?php if(have_posts()): while (have_posts()): the_post(); ?>
					<article id="page-<?php echo get_the_ID(); ?>" class="article-full">
						<?php
						$social_desk = social_share($post, 'social-share');
						if($social_desk) echo $social_desk;
						?>
						<h1 class="article-title">
							<?php the_title(); ?>
						</h1>
						<div class="article-body">
							<?php the_content(); ?>
                            <div id='gedi-newsletter-2'> </div>

                            <?php
							$social_mobile = social_share($post, 'social-share-mobile end-article');
							if($social_mobile) echo $social_mobile;
							?>
							<?php //Social
							$facebook = get_field('facebook', 'option');
							$twitter = get_field('twitter', 'option');
							?>
							<?php if($facebook || $twitter):  ?>
							<div class="social-follow">
								<div class="follow-pretext"></div>
								<?php if($twitter): ?>
									<a href="<?php echo $twitter; ?>" class="twitter-follow-button" data-show-count="false">Follow @BIItaly</a>
								<?php endif; ?>
								<?php if($facebook): ?>
									<div class="fb-like" data-href="<?php echo $facebook; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
								<?php endif; ?>
							</div>
							<?php endif; ?>
							<!-- BANNER -->
							<!-- BANNER -->
						</div>
					</article>
				<?php endwhile; endif; ?>
			</div>
			<div class="sidebar-sito-common">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</main>
<!-- END MAIN -->
<?php get_footer(); ?>
