<?php
// Includo i Vari file per le configurazioni
define("RELATED_WEEKS_AGO_HOME", 2);
define("RELATED_WEEKS_AGO_LIST", 4);


function kill_404_redirect_bi() {
  if (is_404()) {
   add_action('redirect_canonical','__return_false');
  }
}
add_action('template_redirect','kill_404_redirect_bi',1);

require_once('inc/feed-newsstand.php');


require('inc/instant-articles.php');

// Ho spacchettato le funzioni in maniera da non creare un unico file difficle da manutenere
require('inc/front/header_footer.php');

//File per la gestione delle funzioni dei post
require('inc/front/postUtils.php');

//File per la gestione delle funzioni condivise
require('inc/front/global_functions.php');

//File per la gestione di acf
require('inc/admin/acf.php');

// amp
require('inc/amp.php');

// wp-cli
require('inc/wp-cli.php');
require('inc/importer.php');

require('inc/msnfeed.php');


require('inc/options_outplay.php');

//Impostazioni di base
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails', array( 'post' ) );
add_theme_support( 'post-formats', array( 'video', 'gallery' ) );
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'single', 619, 368, true );
	add_image_size( 'single-result', 323, 174, true );
	add_image_size( 'strillo_grande', 780, 400, true );
	add_image_size( 'teaser_regular', 504, 299, true );
	add_image_size( 'teaser_small', 151, 113, true );
	add_image_size( 'teaser_small_box', 256, 170, true );
	add_image_size( 'teaser_menu', 433, 200, true );
    add_image_size( 'apertura_home', 640, 427, true );
    add_image_size( 'user_profile', 300, 389, true );

}

add_action( 'after_setup_theme', 'bi_register_speciali_menu' );
function bi_register_speciali_menu() {
    register_nav_menu( 'speciali', "Menu speciali" );
}

/*
|--------------------------------------------------------------------------
| Funzioni Admin
|--------------------------------------------------------------------------
|
| Di seguito tutte le funzioni per la gestione del Backend.
*/

if(is_admin()){
	require ('inc/admin/rssParse/rssParse.php');
	require ('inc/admin/media.php');
	require ('inc/admin/admin_global.php');
}


function add_alt_tags($content)
{
    global $post;
    if(is_feed() && $_GET["type"] == "cust"){
        preg_match_all('/<img (.*?)\/?>/', $content, $images);
        if(!is_null($images))
        {
            foreach($images[1] as $index => $value)
            {
                if(!preg_match('/alt=/', $value))
                {
                    $new_img = str_replace('<img', '<img alt="'.$post->post_title.'" data-portal-copyright="Business Insider Italia" data-has-syndication-rights="0" ', $images[0][$index]);
                    $content = str_replace($images[0][$index], $new_img, $content);
                }else{
                    $new_img = str_replace('<img', '<img data-portal-copyright="Business Insider Italia" data-has-syndication-rights="0" ', $images[0][$index]);
                    $content = str_replace($images[0][$index], $new_img, $content);
                }
            }
        }

    }
    return $content;
}
add_filter('the_content', 'add_alt_tags', 99999);

function bi_get_main_video($post) {
    $pattern = get_shortcode_regex();

    if ( preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches )
        && array_key_exists( 2, $matches )
        && in_array( 'tvzap-video', $matches[2] )
    ) {
        return $matches[0][0];
    }
    return false;
}

// Do this only once. Can go anywhere inside your functions.php file

//$role_object = get_role( 'editor' );
//$role_object->add_cap( 'edit_theme_options' );

function bi_hide_menu() {

    if (current_user_can('editor')) {

        remove_submenu_page( 'themes.php', 'themes.php' ); // hide the theme selection submenu
        remove_submenu_page( 'themes.php', 'widgets.php' ); // hide the widgets submenu
        remove_submenu_page( 'themes.php', 'customize.php' ); // hide the customizer submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2F' ); // hide the customizer submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Ftools.php' ); // hide the customizer submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Ftools.php&#038;autofocus%5Bcontrol%5D=background_image' ); // hide the background submenu
        remove_submenu_page( 'themes.php', 'customize.php?autofocus[panel]=amp_panel&return='.urlencode(home_url()).'%2Fwp-admin%2F&customize_amp=1' ); // hide the background submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Fnav-menus.php' );
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Findex.php' ); // hide the customizer submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Fedit.php' ); // hide the customizer submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Fprofile.php' ); // hide the customizer submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Fadmin.php%3Fpage%3Dacf-options-impostazioni-business-insider' ); // hide the customizer submenu
        remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Fupload.php' ); // hide the customizer submenu

    }
}

add_action('admin_menu', 'bi_hide_menu', 999);
