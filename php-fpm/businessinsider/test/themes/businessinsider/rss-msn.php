<?php
/**
 * Template Name: Custom RSS Template - Feedname
 */

$postCount = 10; // The number of posts to show in the feed
$post_formats = get_theme_support( 'post-formats' );


if($_GET["type"] == "gallery")
    $operator = "IN";
else
	$operator = "NOT IN";

//print_r($post_formats);
$args = [
	'posts_per_page' => $postCount,
	'meta_query' => [
		[
			'key' => 'msn_feed',
			'value' => 1,
			'compare' => '==',
		]
	],
	'tax_query' => [
		'relation' => 'AND',
		[
			'taxonomy' => 'post_format',
			'field'    => 'slug',
			'terms'    => array( 'post-format-gallery' ),
			'operator' => $operator,
        ]
	]
];
//$posts = query_posts($args);
$wpb_all_query = new WP_Query($args);

header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:media="http://search.yahoo.com/mrss/" xmlns:mi="http://schemas.ingestion.microsoft.com/common/" version="2.0">
    <channel>
        <title><?php bloginfo_rss('name'); ?> - Feed</title>
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss('description') ?></description>
	    <?php while($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); setup_postdata($wpb_all_query->post);
		    $media_gallery = get_field("media_gallery", $post->ID, true);
	    ?>
            <item>
                <title><?php the_title_rss(); ?></title>
                <link><?php the_permalink_rss(); ?></link>
                <description><![CDATA[<?php

	                if($media_gallery[0]["acf_fc_layout"] == "blocco_testo"){
		                echo apply_filters("the_content", $media_gallery[0]["area_di_testo"]);
	                }else {
		                the_content();
	                }
                    //                    the_excerpt_rss()
                    ?>]]></description>
                <pubDate><?php echo mysql2date('c', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
                <guid isPermaLink="false"><?php the_guid(); ?></guid>
                <author><?php the_author(); ?></author>
                <dc:abstract><?php



	                if($media_gallery[0]["acf_fc_layout"] == "blocco_testo"){
		                $postcontent = $media_gallery[0]["area_di_testo"];
                    }else{
		                $postcontent = $post->post_content;
                    }
	                $postcontent = do_shortcode($postcontent);
	                $postcontent = strip_tags($postcontent);
	                $postcontent = str_replace("\n", " ", $postcontent);
	                $postcontent = str_replace("&nbsp;", " ", $postcontent);
	                $postcontent = substr($postcontent, 0, strpos(wordwrap($postcontent, 447), "\n"))."...";

	                echo $postcontent;
                    ?></dc:abstract>
                <dc:date><?php echo date_i18n('c', strtotime($post->post_date)); ?></dc:date>
                <dc:modified><?php echo date_i18n('c', strtotime($post->post_modified)); ?></dc:modified>
                <mi:dateTimeWritten><?php echo date_i18n('c', strtotime($post->post_date)); ?></mi:dateTimeWritten>
                <?php
                if( have_rows('media_gallery') ){
                    ?>
                <media:group>
                <?php
	                // Ciclo su tutti i capi
                    $m=0;
	                while ( have_rows('media_gallery') ) : the_row();
		                if ( get_row_layout() == 'immagine' ):
			                $idimage  = get_sub_field('immagine');
			                $image = wp_get_attachment_image_src($idimage, "full");
		                    if($image) {
			                    $excerpt = get_post_field( 'post_excerpt', $idimage );

			                    if($media_gallery[$m-1]["acf_fc_layout"] == "blocco_testo")
    			                    $title   = $media_gallery[$m-1]["area_di_testo"];
			                    else
				                    $title   = get_post_field( 'post_title', $idimage );

			                    $title = str_replace("\n", " ", $title);
			                    $title = str_replace("&nbsp;", " ", $title);

			                    if($media_gallery[$m+1]["acf_fc_layout"] == "blocco_testo")
				                    $content   = $media_gallery[$m+1]["area_di_testo"];
			                    else
			                        $content = get_post_field( 'post_content', $idimage );

			                    //$url     = wp_get_attachment_image_src( $id, "full" );
			                    $caption = get_sub_field( 'didascalia' );

			                    ?>
                                <media:content height="<?php echo $image[2]; ?>" type="image/jpeg" width="<?php echo $image[1]; ?>" url="<?php echo $image[0]; ?>">
                                    <media:credit scheme="urn:ebu">Fornito da Business Insider Italia</media:credit>
                                    <media:title><?php echo strip_tags($title); ?></media:title>
                                    <media:text><![CDATA[<?php echo $content; ?>]]></media:text>

                                    <?php
                                    $getty = false;
                                    $articolo_importato = get_post_meta($post->ID, "articolo_importato", true);
                                    if($articolo_importato == 1) {
	                                    $sindacation = 1;
                                    }else{
	                                    if(strpos(strtolower($excerpt), "getty") === false){
		                                    $sindacation = 0;
                                        }else{
		                                    $sindacation = 1;
		                                    $getty = true;
                                        }

                                    }
				                    ?>
                                    <mi:hasSyndicationRights><?php echo $sindacation; ?></mi:hasSyndicationRights>
				                    <?php
				                    /**
				                     * se il valore del tag media:text fà match con Getty --> <mi:LicensorName>Getty</mi:LicensorName>
				                     * altrimenti il campo va valorizzato come segue:
				                     * se media:text contiene almeno un punto "." --> substring del valore di media:text a partire dall'ultima occorrenza del punto ".";
				                     * se nel valore di media:text non compare il punto "." --> mi:LicensorName viene valorizzato con media:text
				                     */
				                    $licensor = "";
				                    if($getty)
					                    $licensor = "Getty";
				                    else{
					                    if(strpos(strtolower($excerpt), ".") !== false){
						                    $arr_excerpt = explode(".", $excerpt);
						                    $licensor = $arr_excerpt[count($arr_excerpt)-1];
					                    }else{
						                    $licensor = $excerpt;
					                    }
				                    }
				                    if($licensor){
					                    ?>
                                        <mi:LicensorName><?php echo htmlspecialchars($licensor); ?></mi:LicensorName>
					                    <?php
				                    }
				                    ?>

                                </media:content>
			                    <?php
		                    }
		                endif;
		                $c++;
		                $m++;

	                endwhile;
                ?>
                </media:group>
                <?php
                }else{

                if(has_post_thumbnail()){
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id(), "full");
                    if($image) {
                        $excerpt = get_post_field('post_excerpt', get_post_thumbnail_id());
                        $title = get_post_field('post_title', get_post_thumbnail_id());
                        $content = get_post_field('post_content', get_post_thumbnail_id());
                        ?>
                        <media:content height="<?php echo $image[2]; ?>" type="image/jpeg" width="<?php echo $image[1]; ?>" url="<?php echo $image[0]; ?>">
                            <?php
                                $getty = false;
	                            $articolo_importato = get_post_meta($post->ID, "articolo_importato", true);
	                            if($articolo_importato == 1) {
		                           $sindacation = 1;
        	                    }else {

		                            if ( strpos( strtolower( $excerpt ), "getty" ) === false ) {
			                            $sindacation = 0;
		                             } else {
			                            $sindacation = 1;
			                            $getty = true;
            		                }
	                            }
                            ?>
                            <mi:hasSyndicationRights><?php echo $sindacation; ?></mi:hasSyndicationRights>
                            <?php
                            /**
                             * se il valore del tag media:text fà match con Getty --> <mi:LicensorName>Getty</mi:LicensorName>
                             * altrimenti il campo va valorizzato come segue:
                             * se media:text contiene almeno un punto "." --> substring del valore di media:text a partire dall'ultima occorrenza del punto ".";
                             * se nel valore di media:text non compare il punto "." --> mi:LicensorName viene valorizzato con media:text
                             */
                            $licensor = "";
                            if($getty)
                                $licensor = "Getty";
                            else{
                                if(strpos(strtolower($excerpt), ".") !== false){
                                    $arr_excerpt = explode(".", $excerpt);
                                    $licensor = $arr_excerpt[count($arr_excerpt)-1];
                                }else{
                                    $licensor = $excerpt;
                                }
                            }
                            if($licensor){
                                ?>
                                <mi:LicensorName><?php echo $licensor; ?></mi:LicensorName>
                                <?php
                            }
                            ?>

                            <media:credit scheme="urn:ebu">Fornito da Business Insider Italia</media:credit>
                            <?php if($excerpt != ""){ ?>
                            <media:text><![CDATA[<?php echo $excerpt; ?>]]></media:text>
                            <?php } ?>
                            <media:thumbnail url="<?php echo $image[0]; ?>"/>
                            <?php // if($title != ""){ ?>
                            <media:title><?php the_title_rss(); ?></media:title>
                            <?php // } ?>
                        </media:content>
                        <?php
                    }
                }
                }
                ?>
            </item>
        <?php endwhile; ?>
    </channel>
</rss>