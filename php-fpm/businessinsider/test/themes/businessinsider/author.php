<?php
//Recupero i dati dell'autore
$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
$acf_key = "user_".$curauth->ID;

get_header(); ?>
<!-- MAIN -->
<main style="min-height: 100vh;">
	<div class="quicklook-wrapper wrapper">
		<div class="wrapper">
			<div class="group-primary">
				<div class="search-results">
					<h1><?php echo $curauth->display_name; ?></h1>

					<?php
					/*
					if((!empty($curauth->user_email) || !empty($curauth->user_url)) && !in_array('bi_contributor', $curauth->roles)): ?>
						<span class="article-meta">
							<?php if(!empty($curauth->user_email)): ?>
	                            <a href="mailto:<?php echo $curauth->user_email; ?>">Email</a>
							<?php endif; ?>
							<?php if(!empty($curauth->user_url)): ?>
	                            <a href="<?php echo $curauth->user_url; ?>">Website</a>
							<?php endif; ?>
	                    </span>
					<?php endif;
					*/?>

				</div>
				<?php if(have_posts()):
					$counter = 0;
					?>

					<div class="teaser teaser-small author-info">
						<div class="result-list">
							<?php
							$immagine_utente = get_field( "immagine_utente", $acf_key );
							$bio = get_field( "bio", $acf_key );
							$twitter = get_field( "twitter", $acf_key );
							$mail_pubblica = get_field( "mail_pubblica", $acf_key );
							$job = get_field( "job", $acf_key );
							$src_img = $immagine_utente["sizes"]["user_profile"];
							if($src_img) {
								?>
								<div class="teaser-image">
									<img src="<?php echo $src_img; ?>" border="0">

									<div class="meta">
										<h4><?php echo $curauth->display_name; ?></h4>
										<?php echo $job; ?>
										<div class="contact">
											<?php if($mail_pubblica){ ?><a href="mailto:<?php echo $mail_pubblica; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $mail_pubblica; ?>"><i class="fa fa-envelope-o"></i></a><?php } ?>
											<a href="<?php echo get_author_posts_url($curauth->ID); ?>feed/" data-toggle="tooltip" data-placement="top" title="" data-original-title="RSS feed"><i class="fa fa-rss"></i></a>
											<?php if($twitter){ ?><a href="<?php echo $twitter; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter feed"><i class="fa fa-twitter"></i></a><?php } ?>
										</div>
									</div>

								</div>
								<?php
							}
								?>
							<div class="teaser-content">
								<div class="teaser-text">

									<p class="description">
										<?php echo $bio; ?>
									</p>

									<div class="">
									</div>

								</div>

							</div>
						</div>
					</div>

				<div id="feed">
					<?php $count = 1; while (have_posts()): the_post();
						$counter ++;
						?>
						<?php $native = get_field('native'); ?>
						<?php if($native): ?>
							<?php get_template_part('template/teaser', 'small_native'); ?>
						<?php else: ?>
							<?php get_template_part('template/teaser', 'small'); ?>
						<?php endif; ?>
						<?php if($count == 2): ?>
							<div id="adv-Middle1-dev"><script>try { MNZ_RICH('Middle1-dev'); } catch(e) { }</script></div>
						<?php endif; $count++;


						if($counter == 3){
							?>
							<div id="adv-x22"><script>try { MNZ_RICH('x22'); } catch(e) { }</script></div>
							<?php
						}
						?>
					<?php endwhile; ?>
					<?php global $wp_query;
						if($wp_query->max_num_pages > 1): ?>
						<input type="button" class="btn btn-big btn-attend" id="load-more" data-page="2" value="Carica altri" data-url="<?php bloginfo('url')?>" data-action="load-more" data-number="10" data-type="author" data-id="<?php echo $curauth->ID; ?>" data-container="feed" />
					<?php endif; ?>
				</div>
				<?php endif;  ?>

				<div id="spinner-small" class="spinner small hide">
					<div class="bounce1"></div>
					<div class="bounce2"></div>
					<div class="bounce3"></div>
				</div>
			</div>
			<div class="group-minor group-minor-sidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>

</main>
<!-- END MAIN -->
<?php get_footer(); ?>