<?php get_header();

$search_results = bi_search_result(10);
$count_result = $search_results->result->doc->count();
?>
<!-- MAIN -->
<main style="min-height: 100vh;">

	<div class="quicklook-wrapper wrapper">

		<div class="wrapper">

			<div class="group-primary">
				<div class="search-results">
					<div class="search-phrase">
						<div class="search-field form">
							<form method="get" action="<?php get_bloginfo('url'); ?>">
								<input type="search" id="search-word" style="width: 80%;" class="search-input" name="s" value=""/>
							</form>
						</div>
					</div>
					<div class="search-options">
						<ul>
							<li>
								Ordina per:
								<?php $categorie = get_categories(); ?>
								<?php if(isset($_GET['s']) && !empty($_GET['s'])):
									$s = sanitize_text_field(get_search_query());
								else:
									$s = '*';
								endif;
								?>
								<a class="sort-value <?php if(!isset($_GET['sort'])): echo 'disabled'; endif; ?>" id="relevance" data-url="<?php echo get_bloginfo('url'); ?>" data-sort-value="Relevance" data-s="<?php echo $s; ?>" <?php
								if(isset($_GET['categories']) && !empty($_GET['categories'])):
									echo 'data-categories"' . $_GET['categories'] . '"';
								endif; ?>>Rilevanza</a>
								<a class="sort-value <?php if(isset($_GET['sort'])): echo 'disabled'; endif;  ?>" id="date" data-url="<?php echo get_bloginfo('url'); ?>" data-sort-value="Date" data-s="<?php echo $s; ?>" <?php
								if(isset($_GET['categories']) && !empty($_GET['categories'])):
									echo 'data-categories"' . $_GET['categories'] . '"';
								endif; ?>>Data</a>
							</li>
							<li>Resultati per: </li>
							<li class="dropdown form">

								<select id="solr_categories" name="solr_categorie" data-url="<?php echo get_bloginfo('url'); ?>" data-s="<?php echo $s; ?>">
									<option id="section-0" data-id="0" value="">Tutte</option>
									<?php foreach ($categorie as $categoria): ?>
										<option id="section-<?php echo $categoria->term_id; ?>" data-id="0" value="<?php echo $categoria->name; ?>"
											<?php
												if(isset($_GET['categories']) && !empty($_GET['categories'])):
													selected( $categoria->name, $_GET['categories'] );
												endif; ?> >
											<?php echo $categoria->name; ?>
										</option>
									<?php endforeach; ?>
								</select>
							</li>
							<li></li>
						</ul>
						<h3 style="-webkit-margin-before: 0; -webkit-margin-after: 0; margin-top: 0; margin-bottom: 0;" id="results-item">
							<span class="search_result_number"><?php echo $count_result; ?></span>
							<?php if($count_result > 1) :?>
								risultati
							<?php else: ?>
								risultato
							<?php endif; ?>
							di <?php print xml_attribute($search_results->result, 'numFound'); ?> totali per <i><?php if(isset($s) && !empty($s)) echo $s; ?></i> in
							<i><?php if(isset($_GET['categories']) && !empty($_GET['categories'])): echo $_GET['categories']; else: echo 'Tutte'; endif;?></i>
						</h3>
					</div>
				</div>
			</div>

			<div class="group-secondary">

				<div id="feed">

					<?php foreach($search_results->result->doc as $post_search):
						search_result_setup_post($post_search);
					endforeach;  ?>
					<?php if(xml_attribute($search_results->result, 'numFound') > 10) :?>
					<input type="button" class="btn btn-big btn-attend" id="load-more" data-page="2" value="Carica altri" data-url="<?php bloginfo('url')?>" data-action="load-more-search" data-number="10" data-type="search" data-id="search"
					       data-s="<?php echo $s ?>"
						<?php if(isset($_GET['categories']) && !empty($_GET['categories'])):?>
							data-categories="<?php echo $_GET['categories']; ?>"
						<?php endif;?>
					       data-container="feed" />
					<?php endif;  ?>
				</div>

				<div id="spinner-small" class="spinner small hide">
					<div class="bounce1"></div>
					<div class="bounce2"></div>
					<div class="bounce3"></div>
				</div>
			</div>

		</div>
	</div>
</main>
<!-- END MAIN -->
<?php get_footer(); ?>