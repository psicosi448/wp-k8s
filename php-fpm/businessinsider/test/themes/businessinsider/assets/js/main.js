/* Include libraries and plugins */
/*! jQuery v1.11.1 | (c) 2005, 2014 jQuery Foundation, Inc. | jquery.org/license */
!function (a, b) { "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function (a) { if (!a.document) throw new Error("jQuery requires a window with a document"); return b(a) } : b(a) }("undefined" != typeof window ? window : this, function (a, b) {
  var c = [], d = c.slice, e = c.concat, f = c.push, g = c.indexOf, h = {}, i = h.toString, j = h.hasOwnProperty, k = {}, l = "1.11.1", m = function (a, b) { return new m.fn.init(a, b) }, n = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, o = /^-ms-/, p = /-([\da-z])/gi, q = function (a, b) { return b.toUpperCase() }; m.fn = m.prototype = { jquery: l, constructor: m, selector: "", length: 0, toArray: function () { return d.call(this) }, get: function (a) { return null != a ? 0 > a ? this[a + this.length] : this[a] : d.call(this) }, pushStack: function (a) { var b = m.merge(this.constructor(), a); return b.prevObject = this, b.context = this.context, b }, each: function (a, b) { return m.each(this, a, b) }, map: function (a) { return this.pushStack(m.map(this, function (b, c) { return a.call(b, c, b) })) }, slice: function () { return this.pushStack(d.apply(this, arguments)) }, first: function () { return this.eq(0) }, last: function () { return this.eq(-1) }, eq: function (a) { var b = this.length, c = +a + (0 > a ? b : 0); return this.pushStack(c >= 0 && b > c ? [this[c]] : []) }, end: function () { return this.prevObject || this.constructor(null) }, push: f, sort: c.sort, splice: c.splice }, m.extend = m.fn.extend = function () { var a, b, c, d, e, f, g = arguments[0] || {}, h = 1, i = arguments.length, j = !1; for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || m.isFunction(g) || (g = {}), h === i && (g = this, h--) ; i > h; h++) if (null != (e = arguments[h])) for (d in e) a = g[d], c = e[d], g !== c && (j && c && (m.isPlainObject(c) || (b = m.isArray(c))) ? (b ? (b = !1, f = a && m.isArray(a) ? a : []) : f = a && m.isPlainObject(a) ? a : {}, g[d] = m.extend(j, f, c)) : void 0 !== c && (g[d] = c)); return g }, m.extend({ expando: "jQuery" + (l + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (a) { throw new Error(a) }, noop: function () { }, isFunction: function (a) { return "function" === m.type(a) }, isArray: Array.isArray || function (a) { return "array" === m.type(a) }, isWindow: function (a) { return null != a && a == a.window }, isNumeric: function (a) { return !m.isArray(a) && a - parseFloat(a) >= 0 }, isEmptyObject: function (a) { var b; for (b in a) return !1; return !0 }, isPlainObject: function (a) { var b; if (!a || "object" !== m.type(a) || a.nodeType || m.isWindow(a)) return !1; try { if (a.constructor && !j.call(a, "constructor") && !j.call(a.constructor.prototype, "isPrototypeOf")) return !1 } catch (c) { return !1 } if (k.ownLast) for (b in a) return j.call(a, b); for (b in a); return void 0 === b || j.call(a, b) }, type: function (a) { return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? h[i.call(a)] || "object" : typeof a }, globalEval: function (b) { b && m.trim(b) && (a.execScript || function (b) { a.eval.call(a, b) })(b) }, camelCase: function (a) { return a.replace(o, "ms-").replace(p, q) }, nodeName: function (a, b) { return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase() }, each: function (a, b, c) { var d, e = 0, f = a.length, g = r(a); if (c) { if (g) { for (; f > e; e++) if (d = b.apply(a[e], c), d === !1) break } else for (e in a) if (d = b.apply(a[e], c), d === !1) break } else if (g) { for (; f > e; e++) if (d = b.call(a[e], e, a[e]), d === !1) break } else for (e in a) if (d = b.call(a[e], e, a[e]), d === !1) break; return a }, trim: function (a) { return null == a ? "" : (a + "").replace(n, "") }, makeArray: function (a, b) { var c = b || []; return null != a && (r(Object(a)) ? m.merge(c, "string" == typeof a ? [a] : a) : f.call(c, a)), c }, inArray: function (a, b, c) { var d; if (b) { if (g) return g.call(b, a, c); for (d = b.length, c = c ? 0 > c ? Math.max(0, d + c) : c : 0; d > c; c++) if (c in b && b[c] === a) return c } return -1 }, merge: function (a, b) { var c = +b.length, d = 0, e = a.length; while (c > d) a[e++] = b[d++]; if (c !== c) while (void 0 !== b[d]) a[e++] = b[d++]; return a.length = e, a }, grep: function (a, b, c) { for (var d, e = [], f = 0, g = a.length, h = !c; g > f; f++) d = !b(a[f], f), d !== h && e.push(a[f]); return e }, map: function (a, b, c) { var d, f = 0, g = a.length, h = r(a), i = []; if (h) for (; g > f; f++) d = b(a[f], f, c), null != d && i.push(d); else for (f in a) d = b(a[f], f, c), null != d && i.push(d); return e.apply([], i) }, guid: 1, proxy: function (a, b) { var c, e, f; return "string" == typeof b && (f = a[b], b = a, a = f), m.isFunction(a) ? (c = d.call(arguments, 2), e = function () { return a.apply(b || this, c.concat(d.call(arguments))) }, e.guid = a.guid = a.guid || m.guid++, e) : void 0 }, now: function () { return +new Date }, support: k }), m.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (a, b) { h["[object " + b + "]"] = b.toLowerCase() }); function r(a) { var b = a.length, c = m.type(a); return "function" === c || m.isWindow(a) ? !1 : 1 === a.nodeType && b ? !0 : "array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a } var s = function (a) { var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = "sizzle" + -new Date, v = a.document, w = 0, x = 0, y = gb(), z = gb(), A = gb(), B = function (a, b) { return a === b && (l = !0), 0 }, C = "undefined", D = 1 << 31, E = {}.hasOwnProperty, F = [], G = F.pop, H = F.push, I = F.push, J = F.slice, K = F.indexOf || function (a) { for (var b = 0, c = this.length; c > b; b++) if (this[b] === a) return b; return -1 }, L = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", M = "[\\x20\\t\\r\\n\\f]", N = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", O = N.replace("w", "w#"), P = "\\[" + M + "*(" + N + ")(?:" + M + "*([*^$|!~]?=)" + M + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + O + "))|)" + M + "*\\]", Q = ":(" + N + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + P + ")*)|.*)\\)|)", R = new RegExp("^" + M + "+|((?:^|[^\\\\])(?:\\\\.)*)" + M + "+$", "g"), S = new RegExp("^" + M + "*," + M + "*"), T = new RegExp("^" + M + "*([>+~]|" + M + ")" + M + "*"), U = new RegExp("=" + M + "*([^\\]'\"]*?)" + M + "*\\]", "g"), V = new RegExp(Q), W = new RegExp("^" + O + "$"), X = { ID: new RegExp("^#(" + N + ")"), CLASS: new RegExp("^\\.(" + N + ")"), TAG: new RegExp("^(" + N.replace("w", "w*") + ")"), ATTR: new RegExp("^" + P), PSEUDO: new RegExp("^" + Q), CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + M + "*(even|odd|(([+-]|)(\\d*)n|)" + M + "*(?:([+-]|)" + M + "*(\\d+)|))" + M + "*\\)|)", "i"), bool: new RegExp("^(?:" + L + ")$", "i"), needsContext: new RegExp("^" + M + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + M + "*((?:-\\d)?\\d*)" + M + "*\\)|)(?=[^-]|$)", "i") }, Y = /^(?:input|select|textarea|button)$/i, Z = /^h\d$/i, $ = /^[^{]+\{\s*\[native \w/, _ = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ab = /[+~]/, bb = /'|\\/g, cb = new RegExp("\\\\([\\da-f]{1,6}" + M + "?|(" + M + ")|.)", "ig"), db = function (a, b, c) { var d = "0x" + b - 65536; return d !== d || c ? b : 0 > d ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320) }; try { I.apply(F = J.call(v.childNodes), v.childNodes), F[v.childNodes.length].nodeType } catch (eb) { I = { apply: F.length ? function (a, b) { H.apply(a, J.call(b)) } : function (a, b) { var c = a.length, d = 0; while (a[c++] = b[d++]); a.length = c - 1 } } } function fb(a, b, d, e) { var f, h, j, k, l, o, r, s, w, x; if ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, d = d || [], !a || "string" != typeof a) return d; if (1 !== (k = b.nodeType) && 9 !== k) return []; if (p && !e) { if (f = _.exec(a)) if (j = f[1]) { if (9 === k) { if (h = b.getElementById(j), !h || !h.parentNode) return d; if (h.id === j) return d.push(h), d } else if (b.ownerDocument && (h = b.ownerDocument.getElementById(j)) && t(b, h) && h.id === j) return d.push(h), d } else { if (f[2]) return I.apply(d, b.getElementsByTagName(a)), d; if ((j = f[3]) && c.getElementsByClassName && b.getElementsByClassName) return I.apply(d, b.getElementsByClassName(j)), d } if (c.qsa && (!q || !q.test(a))) { if (s = r = u, w = b, x = 9 === k && a, 1 === k && "object" !== b.nodeName.toLowerCase()) { o = g(a), (r = b.getAttribute("id")) ? s = r.replace(bb, "\\$&") : b.setAttribute("id", s), s = "[id='" + s + "'] ", l = o.length; while (l--) o[l] = s + qb(o[l]); w = ab.test(a) && ob(b.parentNode) || b, x = o.join(",") } if (x) try { return I.apply(d, w.querySelectorAll(x)), d } catch (y) { } finally { r || b.removeAttribute("id") } } } return i(a.replace(R, "$1"), b, d, e) } function gb() { var a = []; function b(c, e) { return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e } return b } function hb(a) { return a[u] = !0, a } function ib(a) { var b = n.createElement("div"); try { return !!a(b) } catch (c) { return !1 } finally { b.parentNode && b.parentNode.removeChild(b), b = null } } function jb(a, b) { var c = a.split("|"), e = a.length; while (e--) d.attrHandle[c[e]] = b } function kb(a, b) { var c = b && a, d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || D) - (~a.sourceIndex || D); if (d) return d; if (c) while (c = c.nextSibling) if (c === b) return -1; return a ? 1 : -1 } function lb(a) { return function (b) { var c = b.nodeName.toLowerCase(); return "input" === c && b.type === a } } function mb(a) { return function (b) { var c = b.nodeName.toLowerCase(); return ("input" === c || "button" === c) && b.type === a } } function nb(a) { return hb(function (b) { return b = +b, hb(function (c, d) { var e, f = a([], c.length, b), g = f.length; while (g--) c[e = f[g]] && (c[e] = !(d[e] = c[e])) }) }) } function ob(a) { return a && typeof a.getElementsByTagName !== C && a } c = fb.support = {}, f = fb.isXML = function (a) { var b = a && (a.ownerDocument || a).documentElement; return b ? "HTML" !== b.nodeName : !1 }, m = fb.setDocument = function (a) { var b, e = a ? a.ownerDocument || a : v, g = e.defaultView; return e !== n && 9 === e.nodeType && e.documentElement ? (n = e, o = e.documentElement, p = !f(e), g && g !== g.top && (g.addEventListener ? g.addEventListener("unload", function () { m() }, !1) : g.attachEvent && g.attachEvent("onunload", function () { m() })), c.attributes = ib(function (a) { return a.className = "i", !a.getAttribute("className") }), c.getElementsByTagName = ib(function (a) { return a.appendChild(e.createComment("")), !a.getElementsByTagName("*").length }), c.getElementsByClassName = $.test(e.getElementsByClassName) && ib(function (a) { return a.innerHTML = "<div class='a'></div><div class='a i'></div>", a.firstChild.className = "i", 2 === a.getElementsByClassName("i").length }), c.getById = ib(function (a) { return o.appendChild(a).id = u, !e.getElementsByName || !e.getElementsByName(u).length }), c.getById ? (d.find.ID = function (a, b) { if (typeof b.getElementById !== C && p) { var c = b.getElementById(a); return c && c.parentNode ? [c] : [] } }, d.filter.ID = function (a) { var b = a.replace(cb, db); return function (a) { return a.getAttribute("id") === b } }) : (delete d.find.ID, d.filter.ID = function (a) { var b = a.replace(cb, db); return function (a) { var c = typeof a.getAttributeNode !== C && a.getAttributeNode("id"); return c && c.value === b } }), d.find.TAG = c.getElementsByTagName ? function (a, b) { return typeof b.getElementsByTagName !== C ? b.getElementsByTagName(a) : void 0 } : function (a, b) { var c, d = [], e = 0, f = b.getElementsByTagName(a); if ("*" === a) { while (c = f[e++]) 1 === c.nodeType && d.push(c); return d } return f }, d.find.CLASS = c.getElementsByClassName && function (a, b) { return typeof b.getElementsByClassName !== C && p ? b.getElementsByClassName(a) : void 0 }, r = [], q = [], (c.qsa = $.test(e.querySelectorAll)) && (ib(function (a) { a.innerHTML = "<select msallowclip=''><option selected=''></option></select>", a.querySelectorAll("[msallowclip^='']").length && q.push("[*^$]=" + M + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || q.push("\\[" + M + "*(?:value|" + L + ")"), a.querySelectorAll(":checked").length || q.push(":checked") }), ib(function (a) { var b = e.createElement("input"); b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + M + "*[*^$|!~]?="), a.querySelectorAll(":enabled").length || q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), q.push(",.*:") })), (c.matchesSelector = $.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ib(function (a) { c.disconnectedMatch = s.call(a, "div"), s.call(a, "[s!='']:x"), r.push("!=", Q) }), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), b = $.test(o.compareDocumentPosition), t = b || $.test(o.contains) ? function (a, b) { var c = 9 === a.nodeType ? a.documentElement : a, d = b && b.parentNode; return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d))) } : function (a, b) { if (b) while (b = b.parentNode) if (b === a) return !0; return !1 }, B = b ? function (a, b) { if (a === b) return l = !0, 0; var d = !a.compareDocumentPosition - !b.compareDocumentPosition; return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === e || a.ownerDocument === v && t(v, a) ? -1 : b === e || b.ownerDocument === v && t(v, b) ? 1 : k ? K.call(k, a) - K.call(k, b) : 0 : 4 & d ? -1 : 1) } : function (a, b) { if (a === b) return l = !0, 0; var c, d = 0, f = a.parentNode, g = b.parentNode, h = [a], i = [b]; if (!f || !g) return a === e ? -1 : b === e ? 1 : f ? -1 : g ? 1 : k ? K.call(k, a) - K.call(k, b) : 0; if (f === g) return kb(a, b); c = a; while (c = c.parentNode) h.unshift(c); c = b; while (c = c.parentNode) i.unshift(c); while (h[d] === i[d]) d++; return d ? kb(h[d], i[d]) : h[d] === v ? -1 : i[d] === v ? 1 : 0 }, e) : n }, fb.matches = function (a, b) { return fb(a, null, null, b) }, fb.matchesSelector = function (a, b) { if ((a.ownerDocument || a) !== n && m(a), b = b.replace(U, "='$1']"), !(!c.matchesSelector || !p || r && r.test(b) || q && q.test(b))) try { var d = s.call(a, b); if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d } catch (e) { } return fb(b, n, null, [a]).length > 0 }, fb.contains = function (a, b) { return (a.ownerDocument || a) !== n && m(a), t(a, b) }, fb.attr = function (a, b) { (a.ownerDocument || a) !== n && m(a); var e = d.attrHandle[b.toLowerCase()], f = e && E.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0; return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null }, fb.error = function (a) { throw new Error("Syntax error, unrecognized expression: " + a) }, fb.uniqueSort = function (a) { var b, d = [], e = 0, f = 0; if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) { while (b = a[f++]) b === a[f] && (e = d.push(f)); while (e--) a.splice(d[e], 1) } return k = null, a }, e = fb.getText = function (a) { var b, c = "", d = 0, f = a.nodeType; if (f) { if (1 === f || 9 === f || 11 === f) { if ("string" == typeof a.textContent) return a.textContent; for (a = a.firstChild; a; a = a.nextSibling) c += e(a) } else if (3 === f || 4 === f) return a.nodeValue } else while (b = a[d++]) c += e(b); return c }, d = fb.selectors = { cacheLength: 50, createPseudo: hb, match: X, attrHandle: {}, find: {}, relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } }, preFilter: { ATTR: function (a) { return a[1] = a[1].replace(cb, db), a[3] = (a[3] || a[4] || a[5] || "").replace(cb, db), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4) }, CHILD: function (a) { return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || fb.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && fb.error(a[0]), a }, PSEUDO: function (a) { var b, c = !a[6] && a[2]; return X.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && V.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3)) } }, filter: { TAG: function (a) { var b = a.replace(cb, db).toLowerCase(); return "*" === a ? function () { return !0 } : function (a) { return a.nodeName && a.nodeName.toLowerCase() === b } }, CLASS: function (a) { var b = y[a + " "]; return b || (b = new RegExp("(^|" + M + ")" + a + "(" + M + "|$)")) && y(a, function (a) { return b.test("string" == typeof a.className && a.className || typeof a.getAttribute !== C && a.getAttribute("class") || "") }) }, ATTR: function (a, b, c) { return function (d) { var e = fb.attr(d, a); return null == e ? "!=" === b : b ? (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e + " ").indexOf(c) > -1 : "|=" === b ? e === c || e.slice(0, c.length + 1) === c + "-" : !1) : !0 } }, CHILD: function (a, b, c, d, e) { var f = "nth" !== a.slice(0, 3), g = "last" !== a.slice(-4), h = "of-type" === b; return 1 === d && 0 === e ? function (a) { return !!a.parentNode } : function (b, c, i) { var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling", q = b.parentNode, r = h && b.nodeName.toLowerCase(), s = !i && !h; if (q) { if (f) { while (p) { l = b; while (l = l[p]) if (h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType) return !1; o = p = "only" === a && !o && "nextSibling" } return !0 } if (o = [g ? q.firstChild : q.lastChild], g && s) { k = q[u] || (q[u] = {}), j = k[a] || [], n = j[0] === w && j[1], m = j[0] === w && j[2], l = n && q.childNodes[n]; while (l = ++n && l && l[p] || (m = n = 0) || o.pop()) if (1 === l.nodeType && ++m && l === b) { k[a] = [w, n, m]; break } } else if (s && (j = (b[u] || (b[u] = {}))[a]) && j[0] === w) m = j[1]; else while (l = ++n && l && l[p] || (m = n = 0) || o.pop()) if ((h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType) && ++m && (s && ((l[u] || (l[u] = {}))[a] = [w, m]), l === b)) break; return m -= e, m === d || m % d === 0 && m / d >= 0 } } }, PSEUDO: function (a, b) { var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || fb.error("unsupported pseudo: " + a); return e[u] ? e(b) : e.length > 1 ? (c = [a, a, "", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? hb(function (a, c) { var d, f = e(a, b), g = f.length; while (g--) d = K.call(a, f[g]), a[d] = !(c[d] = f[g]) }) : function (a) { return e(a, 0, c) }) : e } }, pseudos: { not: hb(function (a) { var b = [], c = [], d = h(a.replace(R, "$1")); return d[u] ? hb(function (a, b, c, e) { var f, g = d(a, null, e, []), h = a.length; while (h--) (f = g[h]) && (a[h] = !(b[h] = f)) }) : function (a, e, f) { return b[0] = a, d(b, null, f, c), !c.pop() } }), has: hb(function (a) { return function (b) { return fb(a, b).length > 0 } }), contains: hb(function (a) { return function (b) { return (b.textContent || b.innerText || e(b)).indexOf(a) > -1 } }), lang: hb(function (a) { return W.test(a || "") || fb.error("unsupported lang: " + a), a = a.replace(cb, db).toLowerCase(), function (b) { var c; do if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-"); while ((b = b.parentNode) && 1 === b.nodeType); return !1 } }), target: function (b) { var c = a.location && a.location.hash; return c && c.slice(1) === b.id }, root: function (a) { return a === o }, focus: function (a) { return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex) }, enabled: function (a) { return a.disabled === !1 }, disabled: function (a) { return a.disabled === !0 }, checked: function (a) { var b = a.nodeName.toLowerCase(); return "input" === b && !!a.checked || "option" === b && !!a.selected }, selected: function (a) { return a.parentNode && a.parentNode.selectedIndex, a.selected === !0 }, empty: function (a) { for (a = a.firstChild; a; a = a.nextSibling) if (a.nodeType < 6) return !1; return !0 }, parent: function (a) { return !d.pseudos.empty(a) }, header: function (a) { return Z.test(a.nodeName) }, input: function (a) { return Y.test(a.nodeName) }, button: function (a) { var b = a.nodeName.toLowerCase(); return "input" === b && "button" === a.type || "button" === b }, text: function (a) { var b; return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase()) }, first: nb(function () { return [0] }), last: nb(function (a, b) { return [b - 1] }), eq: nb(function (a, b, c) { return [0 > c ? c + b : c] }), even: nb(function (a, b) { for (var c = 0; b > c; c += 2) a.push(c); return a }), odd: nb(function (a, b) { for (var c = 1; b > c; c += 2) a.push(c); return a }), lt: nb(function (a, b, c) { for (var d = 0 > c ? c + b : c; --d >= 0;) a.push(d); return a }), gt: nb(function (a, b, c) { for (var d = 0 > c ? c + b : c; ++d < b;) a.push(d); return a }) } }, d.pseudos.nth = d.pseudos.eq; for (b in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }) d.pseudos[b] = lb(b); for (b in { submit: !0, reset: !0 }) d.pseudos[b] = mb(b); function pb() { } pb.prototype = d.filters = d.pseudos, d.setFilters = new pb, g = fb.tokenize = function (a, b) { var c, e, f, g, h, i, j, k = z[a + " "]; if (k) return b ? 0 : k.slice(0); h = a, i = [], j = d.preFilter; while (h) { (!c || (e = S.exec(h))) && (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = T.exec(h)) && (c = e.shift(), f.push({ value: c, type: e[0].replace(R, " ") }), h = h.slice(c.length)); for (g in d.filter) !(e = X[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({ value: c, type: g, matches: e }), h = h.slice(c.length)); if (!c) break } return b ? h.length : h ? fb.error(a) : z(a, i).slice(0) }; function qb(a) { for (var b = 0, c = a.length, d = ""; c > b; b++) d += a[b].value; return d } function rb(a, b, c) { var d = b.dir, e = c && "parentNode" === d, f = x++; return b.first ? function (b, c, f) { while (b = b[d]) if (1 === b.nodeType || e) return a(b, c, f) } : function (b, c, g) { var h, i, j = [w, f]; if (g) { while (b = b[d]) if ((1 === b.nodeType || e) && a(b, c, g)) return !0 } else while (b = b[d]) if (1 === b.nodeType || e) { if (i = b[u] || (b[u] = {}), (h = i[d]) && h[0] === w && h[1] === f) return j[2] = h[2]; if (i[d] = j, j[2] = a(b, c, g)) return !0 } } } function sb(a) { return a.length > 1 ? function (b, c, d) { var e = a.length; while (e--) if (!a[e](b, c, d)) return !1; return !0 } : a[0] } function tb(a, b, c) { for (var d = 0, e = b.length; e > d; d++) fb(a, b[d], c); return c } function ub(a, b, c, d, e) { for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++) (f = a[h]) && (!c || c(f, d, e)) && (g.push(f), j && b.push(h)); return g } function vb(a, b, c, d, e, f) { return d && !d[u] && (d = vb(d)), e && !e[u] && (e = vb(e, f)), hb(function (f, g, h, i) { var j, k, l, m = [], n = [], o = g.length, p = f || tb(b || "*", h.nodeType ? [h] : h, []), q = !a || !f && b ? p : ub(p, m, a, h, i), r = c ? e || (f ? a : o || d) ? [] : g : q; if (c && c(q, r, h, i), d) { j = ub(r, n), d(j, [], h, i), k = j.length; while (k--) (l = j[k]) && (r[n[k]] = !(q[n[k]] = l)) } if (f) { if (e || a) { if (e) { j = [], k = r.length; while (k--) (l = r[k]) && j.push(q[k] = l); e(null, r = [], j, i) } k = r.length; while (k--) (l = r[k]) && (j = e ? K.call(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l)) } } else r = ub(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : I.apply(g, r) }) } function wb(a) { for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = rb(function (a) { return a === b }, h, !0), l = rb(function (a) { return K.call(b, a) > -1 }, h, !0), m = [function (a, c, d) { return !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d)) }]; f > i; i++) if (c = d.relative[a[i].type]) m = [rb(sb(m), c)]; else { if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) { for (e = ++i; f > e; e++) if (d.relative[a[e].type]) break; return vb(i > 1 && sb(m), i > 1 && qb(a.slice(0, i - 1).concat({ value: " " === a[i - 2].type ? "*" : "" })).replace(R, "$1"), c, e > i && wb(a.slice(i, e)), f > e && wb(a = a.slice(e)), f > e && qb(a)) } m.push(c) } return sb(m) } function xb(a, b) { var c = b.length > 0, e = a.length > 0, f = function (f, g, h, i, k) { var l, m, o, p = 0, q = "0", r = f && [], s = [], t = j, u = f || e && d.find.TAG("*", k), v = w += null == t ? 1 : Math.random() || .1, x = u.length; for (k && (j = g !== n && g) ; q !== x && null != (l = u[q]) ; q++) { if (e && l) { m = 0; while (o = a[m++]) if (o(l, g, h)) { i.push(l); break } k && (w = v) } c && ((l = !o && l) && p--, f && r.push(l)) } if (p += q, c && q !== p) { m = 0; while (o = b[m++]) o(r, s, g, h); if (f) { if (p > 0) while (q--) r[q] || s[q] || (s[q] = G.call(i)); s = ub(s) } I.apply(i, s), k && !f && s.length > 0 && p + b.length > 1 && fb.uniqueSort(i) } return k && (w = v, j = t), r }; return c ? hb(f) : f } return h = fb.compile = function (a, b) { var c, d = [], e = [], f = A[a + " "]; if (!f) { b || (b = g(a)), c = b.length; while (c--) f = wb(b[c]), f[u] ? d.push(f) : e.push(f); f = A(a, xb(e, d)), f.selector = a } return f }, i = fb.select = function (a, b, e, f) { var i, j, k, l, m, n = "function" == typeof a && a, o = !f && g(a = n.selector || a); if (e = e || [], 1 === o.length) { if (j = o[0] = o[0].slice(0), j.length > 2 && "ID" === (k = j[0]).type && c.getById && 9 === b.nodeType && p && d.relative[j[1].type]) { if (b = (d.find.ID(k.matches[0].replace(cb, db), b) || [])[0], !b) return e; n && (b = b.parentNode), a = a.slice(j.shift().value.length) } i = X.needsContext.test(a) ? 0 : j.length; while (i--) { if (k = j[i], d.relative[l = k.type]) break; if ((m = d.find[l]) && (f = m(k.matches[0].replace(cb, db), ab.test(j[0].type) && ob(b.parentNode) || b))) { if (j.splice(i, 1), a = f.length && qb(j), !a) return I.apply(e, f), e; break } } } return (n || h(a, o))(f, b, !p, e, ab.test(a) && ob(b.parentNode) || b), e }, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ib(function (a) { return 1 & a.compareDocumentPosition(n.createElement("div")) }), ib(function (a) { return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href") }) || jb("type|href|height|width", function (a, b, c) { return c ? void 0 : a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2) }), c.attributes && ib(function (a) { return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value") }) || jb("value", function (a, b, c) { return c || "input" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue }), ib(function (a) { return null == a.getAttribute("disabled") }) || jb(L, function (a, b, c) { var d; return c ? void 0 : a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null }), fb }(a); m.find = s, m.expr = s.selectors, m.expr[":"] = m.expr.pseudos, m.unique = s.uniqueSort, m.text = s.getText, m.isXMLDoc = s.isXML, m.contains = s.contains; var t = m.expr.match.needsContext, u = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, v = /^.[^:#\[\.,]*$/; function w(a, b, c) { if (m.isFunction(b)) return m.grep(a, function (a, d) { return !!b.call(a, d, a) !== c }); if (b.nodeType) return m.grep(a, function (a) { return a === b !== c }); if ("string" == typeof b) { if (v.test(b)) return m.filter(b, a, c); b = m.filter(b, a) } return m.grep(a, function (a) { return m.inArray(a, b) >= 0 !== c }) } m.filter = function (a, b, c) { var d = b[0]; return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? m.find.matchesSelector(d, a) ? [d] : [] : m.find.matches(a, m.grep(b, function (a) { return 1 === a.nodeType })) }, m.fn.extend({ find: function (a) { var b, c = [], d = this, e = d.length; if ("string" != typeof a) return this.pushStack(m(a).filter(function () { for (b = 0; e > b; b++) if (m.contains(d[b], this)) return !0 })); for (b = 0; e > b; b++) m.find(a, d[b], c); return c = this.pushStack(e > 1 ? m.unique(c) : c), c.selector = this.selector ? this.selector + " " + a : a, c }, filter: function (a) { return this.pushStack(w(this, a || [], !1)) }, not: function (a) { return this.pushStack(w(this, a || [], !0)) }, is: function (a) { return !!w(this, "string" == typeof a && t.test(a) ? m(a) : a || [], !1).length } }); var x, y = a.document, z = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, A = m.fn.init = function (a, b) { var c, d; if (!a) return this; if ("string" == typeof a) { if (c = "<" === a.charAt(0) && ">" === a.charAt(a.length - 1) && a.length >= 3 ? [null, a, null] : z.exec(a), !c || !c[1] && b) return !b || b.jquery ? (b || x).find(a) : this.constructor(b).find(a); if (c[1]) { if (b = b instanceof m ? b[0] : b, m.merge(this, m.parseHTML(c[1], b && b.nodeType ? b.ownerDocument || b : y, !0)), u.test(c[1]) && m.isPlainObject(b)) for (c in b) m.isFunction(this[c]) ? this[c](b[c]) : this.attr(c, b[c]); return this } if (d = y.getElementById(c[2]), d && d.parentNode) { if (d.id !== c[2]) return x.find(a); this.length = 1, this[0] = d } return this.context = y, this.selector = a, this } return a.nodeType ? (this.context = this[0] = a, this.length = 1, this) : m.isFunction(a) ? "undefined" != typeof x.ready ? x.ready(a) : a(m) : (void 0 !== a.selector && (this.selector = a.selector, this.context = a.context), m.makeArray(a, this)) }; A.prototype = m.fn, x = m(y); var B = /^(?:parents|prev(?:Until|All))/, C = { children: !0, contents: !0, next: !0, prev: !0 }; m.extend({ dir: function (a, b, c) { var d = [], e = a[b]; while (e && 9 !== e.nodeType && (void 0 === c || 1 !== e.nodeType || !m(e).is(c))) 1 === e.nodeType && d.push(e), e = e[b]; return d }, sibling: function (a, b) { for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a); return c } }), m.fn.extend({ has: function (a) { var b, c = m(a, this), d = c.length; return this.filter(function () { for (b = 0; d > b; b++) if (m.contains(this, c[b])) return !0 }) }, closest: function (a, b) { for (var c, d = 0, e = this.length, f = [], g = t.test(a) || "string" != typeof a ? m(a, b || this.context) : 0; e > d; d++) for (c = this[d]; c && c !== b; c = c.parentNode) if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && m.find.matchesSelector(c, a))) { f.push(c); break } return this.pushStack(f.length > 1 ? m.unique(f) : f) }, index: function (a) { return a ? "string" == typeof a ? m.inArray(this[0], m(a)) : m.inArray(a.jquery ? a[0] : a, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1 }, add: function (a, b) { return this.pushStack(m.unique(m.merge(this.get(), m(a, b)))) }, addBack: function (a) { return this.add(null == a ? this.prevObject : this.prevObject.filter(a)) } }); function D(a, b) { do a = a[b]; while (a && 1 !== a.nodeType); return a } m.each({ parent: function (a) { var b = a.parentNode; return b && 11 !== b.nodeType ? b : null }, parents: function (a) { return m.dir(a, "parentNode") }, parentsUntil: function (a, b, c) { return m.dir(a, "parentNode", c) }, next: function (a) { return D(a, "nextSibling") }, prev: function (a) { return D(a, "previousSibling") }, nextAll: function (a) { return m.dir(a, "nextSibling") }, prevAll: function (a) { return m.dir(a, "previousSibling") }, nextUntil: function (a, b, c) { return m.dir(a, "nextSibling", c) }, prevUntil: function (a, b, c) { return m.dir(a, "previousSibling", c) }, siblings: function (a) { return m.sibling((a.parentNode || {}).firstChild, a) }, children: function (a) { return m.sibling(a.firstChild) }, contents: function (a) { return m.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : m.merge([], a.childNodes) } }, function (a, b) { m.fn[a] = function (c, d) { var e = m.map(this, b, c); return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = m.filter(d, e)), this.length > 1 && (C[a] || (e = m.unique(e)), B.test(a) && (e = e.reverse())), this.pushStack(e) } }); var E = /\S+/g, F = {}; function G(a) { var b = F[a] = {}; return m.each(a.match(E) || [], function (a, c) { b[c] = !0 }), b } m.Callbacks = function (a) { a = "string" == typeof a ? F[a] || G(a) : m.extend({}, a); var b, c, d, e, f, g, h = [], i = !a.once && [], j = function (l) { for (c = a.memory && l, d = !0, f = g || 0, g = 0, e = h.length, b = !0; h && e > f; f++) if (h[f].apply(l[0], l[1]) === !1 && a.stopOnFalse) { c = !1; break } b = !1, h && (i ? i.length && j(i.shift()) : c ? h = [] : k.disable()) }, k = { add: function () { if (h) { var d = h.length; !function f(b) { m.each(b, function (b, c) { var d = m.type(c); "function" === d ? a.unique && k.has(c) || h.push(c) : c && c.length && "string" !== d && f(c) }) }(arguments), b ? e = h.length : c && (g = d, j(c)) } return this }, remove: function () { return h && m.each(arguments, function (a, c) { var d; while ((d = m.inArray(c, h, d)) > -1) h.splice(d, 1), b && (e >= d && e--, f >= d && f--) }), this }, has: function (a) { return a ? m.inArray(a, h) > -1 : !(!h || !h.length) }, empty: function () { return h = [], e = 0, this }, disable: function () { return h = i = c = void 0, this }, disabled: function () { return !h }, lock: function () { return i = void 0, c || k.disable(), this }, locked: function () { return !i }, fireWith: function (a, c) { return !h || d && !i || (c = c || [], c = [a, c.slice ? c.slice() : c], b ? i.push(c) : j(c)), this }, fire: function () { return k.fireWith(this, arguments), this }, fired: function () { return !!d } }; return k }, m.extend({ Deferred: function (a) { var b = [["resolve", "done", m.Callbacks("once memory"), "resolved"], ["reject", "fail", m.Callbacks("once memory"), "rejected"], ["notify", "progress", m.Callbacks("memory")]], c = "pending", d = { state: function () { return c }, always: function () { return e.done(arguments).fail(arguments), this }, then: function () { var a = arguments; return m.Deferred(function (c) { m.each(b, function (b, f) { var g = m.isFunction(a[b]) && a[b]; e[f[1]](function () { var a = g && g.apply(this, arguments); a && m.isFunction(a.promise) ? a.promise().done(c.resolve).fail(c.reject).progress(c.notify) : c[f[0] + "With"](this === d ? c.promise() : this, g ? [a] : arguments) }) }), a = null }).promise() }, promise: function (a) { return null != a ? m.extend(a, d) : d } }, e = {}; return d.pipe = d.then, m.each(b, function (a, f) { var g = f[2], h = f[3]; d[f[1]] = g.add, h && g.add(function () { c = h }, b[1 ^ a][2].disable, b[2][2].lock), e[f[0]] = function () { return e[f[0] + "With"](this === e ? d : this, arguments), this }, e[f[0] + "With"] = g.fireWith }), d.promise(e), a && a.call(e, e), e }, when: function (a) { var b = 0, c = d.call(arguments), e = c.length, f = 1 !== e || a && m.isFunction(a.promise) ? e : 0, g = 1 === f ? a : m.Deferred(), h = function (a, b, c) { return function (e) { b[a] = this, c[a] = arguments.length > 1 ? d.call(arguments) : e, c === i ? g.notifyWith(b, c) : --f || g.resolveWith(b, c) } }, i, j, k; if (e > 1) for (i = new Array(e), j = new Array(e), k = new Array(e) ; e > b; b++) c[b] && m.isFunction(c[b].promise) ? c[b].promise().done(h(b, k, c)).fail(g.reject).progress(h(b, j, i)) : --f; return f || g.resolveWith(k, c), g.promise() } }); var H; m.fn.ready = function (a) { return m.ready.promise().done(a), this }, m.extend({ isReady: !1, readyWait: 1, holdReady: function (a) { a ? m.readyWait++ : m.ready(!0) }, ready: function (a) { if (a === !0 ? !--m.readyWait : !m.isReady) { if (!y.body) return setTimeout(m.ready); m.isReady = !0, a !== !0 && --m.readyWait > 0 || (H.resolveWith(y, [m]), m.fn.triggerHandler && (m(y).triggerHandler("ready"), m(y).off("ready"))) } } }); function I() { y.addEventListener ? (y.removeEventListener("DOMContentLoaded", J, !1), a.removeEventListener("load", J, !1)) : (y.detachEvent("onreadystatechange", J), a.detachEvent("onload", J)) } function J() { (y.addEventListener || "load" === event.type || "complete" === y.readyState) && (I(), m.ready()) } m.ready.promise = function (b) { if (!H) if (H = m.Deferred(), "complete" === y.readyState) setTimeout(m.ready); else if (y.addEventListener) y.addEventListener("DOMContentLoaded", J, !1), a.addEventListener("load", J, !1); else { y.attachEvent("onreadystatechange", J), a.attachEvent("onload", J); var c = !1; try { c = null == a.frameElement && y.documentElement } catch (d) { } c && c.doScroll && !function e() { if (!m.isReady) { try { c.doScroll("left") } catch (a) { return setTimeout(e, 50) } I(), m.ready() } }() } return H.promise(b) }; var K = "undefined", L; for (L in m(k)) break; k.ownLast = "0" !== L, k.inlineBlockNeedsLayout = !1, m(function () { var a, b, c, d; c = y.getElementsByTagName("body")[0], c && c.style && (b = y.createElement("div"), d = y.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(d).appendChild(b), typeof b.style.zoom !== K && (b.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", k.inlineBlockNeedsLayout = a = 3 === b.offsetWidth, a && (c.style.zoom = 1)), c.removeChild(d)) }), function () { var a = y.createElement("div"); if (null == k.deleteExpando) { k.deleteExpando = !0; try { delete a.test } catch (b) { k.deleteExpando = !1 } } a = null }(), m.acceptData = function (a) { var b = m.noData[(a.nodeName + " ").toLowerCase()], c = +a.nodeType || 1; return 1 !== c && 9 !== c ? !1 : !b || b !== !0 && a.getAttribute("classid") === b }; var M = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, N = /([A-Z])/g; function O(a, b, c) { if (void 0 === c && 1 === a.nodeType) { var d = "data-" + b.replace(N, "-$1").toLowerCase(); if (c = a.getAttribute(d), "string" == typeof c) { try { c = "true" === c ? !0 : "false" === c ? !1 : "null" === c ? null : +c + "" === c ? +c : M.test(c) ? m.parseJSON(c) : c } catch (e) { } m.data(a, b, c) } else c = void 0 } return c } function P(a) { var b; for (b in a) if (("data" !== b || !m.isEmptyObject(a[b])) && "toJSON" !== b) return !1; return !0 } function Q(a, b, d, e) {
    if (m.acceptData(a)) {
      var f, g, h = m.expando, i = a.nodeType, j = i ? m.cache : a, k = i ? a[h] : a[h] && h;
      if (k && j[k] && (e || j[k].data) || void 0 !== d || "string" != typeof b) return k || (k = i ? a[h] = c.pop() || m.guid++ : h), j[k] || (j[k] = i ? {} : { toJSON: m.noop }), ("object" == typeof b || "function" == typeof b) && (e ? j[k] = m.extend(j[k], b) : j[k].data = m.extend(j[k].data, b)), g = j[k], e || (g.data || (g.data = {}), g = g.data), void 0 !== d && (g[m.camelCase(b)] = d), "string" == typeof b ? (f = g[b], null == f && (f = g[m.camelCase(b)])) : f = g, f
    }
  } function R(a, b, c) { if (m.acceptData(a)) { var d, e, f = a.nodeType, g = f ? m.cache : a, h = f ? a[m.expando] : m.expando; if (g[h]) { if (b && (d = c ? g[h] : g[h].data)) { m.isArray(b) ? b = b.concat(m.map(b, m.camelCase)) : b in d ? b = [b] : (b = m.camelCase(b), b = b in d ? [b] : b.split(" ")), e = b.length; while (e--) delete d[b[e]]; if (c ? !P(d) : !m.isEmptyObject(d)) return } (c || (delete g[h].data, P(g[h]))) && (f ? m.cleanData([a], !0) : k.deleteExpando || g != g.window ? delete g[h] : g[h] = null) } } } m.extend({ cache: {}, noData: { "applet ": !0, "embed ": !0, "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" }, hasData: function (a) { return a = a.nodeType ? m.cache[a[m.expando]] : a[m.expando], !!a && !P(a) }, data: function (a, b, c) { return Q(a, b, c) }, removeData: function (a, b) { return R(a, b) }, _data: function (a, b, c) { return Q(a, b, c, !0) }, _removeData: function (a, b) { return R(a, b, !0) } }), m.fn.extend({ data: function (a, b) { var c, d, e, f = this[0], g = f && f.attributes; if (void 0 === a) { if (this.length && (e = m.data(f), 1 === f.nodeType && !m._data(f, "parsedAttrs"))) { c = g.length; while (c--) g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = m.camelCase(d.slice(5)), O(f, d, e[d]))); m._data(f, "parsedAttrs", !0) } return e } return "object" == typeof a ? this.each(function () { m.data(this, a) }) : arguments.length > 1 ? this.each(function () { m.data(this, a, b) }) : f ? O(f, a, m.data(f, a)) : void 0 }, removeData: function (a) { return this.each(function () { m.removeData(this, a) }) } }), m.extend({ queue: function (a, b, c) { var d; return a ? (b = (b || "fx") + "queue", d = m._data(a, b), c && (!d || m.isArray(c) ? d = m._data(a, b, m.makeArray(c)) : d.push(c)), d || []) : void 0 }, dequeue: function (a, b) { b = b || "fx"; var c = m.queue(a, b), d = c.length, e = c.shift(), f = m._queueHooks(a, b), g = function () { m.dequeue(a, b) }; "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire() }, _queueHooks: function (a, b) { var c = b + "queueHooks"; return m._data(a, c) || m._data(a, c, { empty: m.Callbacks("once memory").add(function () { m._removeData(a, b + "queue"), m._removeData(a, c) }) }) } }), m.fn.extend({ queue: function (a, b) { var c = 2; return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? m.queue(this[0], a) : void 0 === b ? this : this.each(function () { var c = m.queue(this, a, b); m._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && m.dequeue(this, a) }) }, dequeue: function (a) { return this.each(function () { m.dequeue(this, a) }) }, clearQueue: function (a) { return this.queue(a || "fx", []) }, promise: function (a, b) { var c, d = 1, e = m.Deferred(), f = this, g = this.length, h = function () { --d || e.resolveWith(f, [f]) }; "string" != typeof a && (b = a, a = void 0), a = a || "fx"; while (g--) c = m._data(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h)); return h(), e.promise(b) } }); var S = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, T = ["Top", "Right", "Bottom", "Left"], U = function (a, b) { return a = b || a, "none" === m.css(a, "display") || !m.contains(a.ownerDocument, a) }, V = m.access = function (a, b, c, d, e, f, g) { var h = 0, i = a.length, j = null == c; if ("object" === m.type(c)) { e = !0; for (h in c) m.access(a, b, h, c[h], !0, f, g) } else if (void 0 !== d && (e = !0, m.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function (a, b, c) { return j.call(m(a), c) })), b)) for (; i > h; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c))); return e ? a : j ? b.call(a) : i ? b(a[0], c) : f }, W = /^(?:checkbox|radio)$/i; !function () { var a = y.createElement("input"), b = y.createElement("div"), c = y.createDocumentFragment(); if (b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", k.leadingWhitespace = 3 === b.firstChild.nodeType, k.tbody = !b.getElementsByTagName("tbody").length, k.htmlSerialize = !!b.getElementsByTagName("link").length, k.html5Clone = "<:nav></:nav>" !== y.createElement("nav").cloneNode(!0).outerHTML, a.type = "checkbox", a.checked = !0, c.appendChild(a), k.appendChecked = a.checked, b.innerHTML = "<textarea>x</textarea>", k.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue, c.appendChild(b), b.innerHTML = "<input type='radio' checked='checked' name='t'/>", k.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, k.noCloneEvent = !0, b.attachEvent && (b.attachEvent("onclick", function () { k.noCloneEvent = !1 }), b.cloneNode(!0).click()), null == k.deleteExpando) { k.deleteExpando = !0; try { delete b.test } catch (d) { k.deleteExpando = !1 } } }(), function () { var b, c, d = y.createElement("div"); for (b in { submit: !0, change: !0, focusin: !0 }) c = "on" + b, (k[b + "Bubbles"] = c in a) || (d.setAttribute(c, "t"), k[b + "Bubbles"] = d.attributes[c].expando === !1); d = null }(); var X = /^(?:input|select|textarea)$/i, Y = /^key/, Z = /^(?:mouse|pointer|contextmenu)|click/, $ = /^(?:focusinfocus|focusoutblur)$/, _ = /^([^.]*)(?:\.(.+)|)$/; function ab() { return !0 } function bb() { return !1 } function cb() { try { return y.activeElement } catch (a) { } } m.event = { global: {}, add: function (a, b, c, d, e) { var f, g, h, i, j, k, l, n, o, p, q, r = m._data(a); if (r) { c.handler && (i = c, c = i.handler, e = i.selector), c.guid || (c.guid = m.guid++), (g = r.events) || (g = r.events = {}), (k = r.handle) || (k = r.handle = function (a) { return typeof m === K || a && m.event.triggered === a.type ? void 0 : m.event.dispatch.apply(k.elem, arguments) }, k.elem = a), b = (b || "").match(E) || [""], h = b.length; while (h--) f = _.exec(b[h]) || [], o = q = f[1], p = (f[2] || "").split(".").sort(), o && (j = m.event.special[o] || {}, o = (e ? j.delegateType : j.bindType) || o, j = m.event.special[o] || {}, l = m.extend({ type: o, origType: q, data: d, handler: c, guid: c.guid, selector: e, needsContext: e && m.expr.match.needsContext.test(e), namespace: p.join(".") }, i), (n = g[o]) || (n = g[o] = [], n.delegateCount = 0, j.setup && j.setup.call(a, d, p, k) !== !1 || (a.addEventListener ? a.addEventListener(o, k, !1) : a.attachEvent && a.attachEvent("on" + o, k))), j.add && (j.add.call(a, l), l.handler.guid || (l.handler.guid = c.guid)), e ? n.splice(n.delegateCount++, 0, l) : n.push(l), m.event.global[o] = !0); a = null } }, remove: function (a, b, c, d, e) { var f, g, h, i, j, k, l, n, o, p, q, r = m.hasData(a) && m._data(a); if (r && (k = r.events)) { b = (b || "").match(E) || [""], j = b.length; while (j--) if (h = _.exec(b[j]) || [], o = q = h[1], p = (h[2] || "").split(".").sort(), o) { l = m.event.special[o] || {}, o = (d ? l.delegateType : l.bindType) || o, n = k[o] || [], h = h[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), i = f = n.length; while (f--) g = n[f], !e && q !== g.origType || c && c.guid !== g.guid || h && !h.test(g.namespace) || d && d !== g.selector && ("**" !== d || !g.selector) || (n.splice(f, 1), g.selector && n.delegateCount--, l.remove && l.remove.call(a, g)); i && !n.length && (l.teardown && l.teardown.call(a, p, r.handle) !== !1 || m.removeEvent(a, o, r.handle), delete k[o]) } else for (o in k) m.event.remove(a, o + b[j], c, d, !0); m.isEmptyObject(k) && (delete r.handle, m._removeData(a, "events")) } }, trigger: function (b, c, d, e) { var f, g, h, i, k, l, n, o = [d || y], p = j.call(b, "type") ? b.type : b, q = j.call(b, "namespace") ? b.namespace.split(".") : []; if (h = l = d = d || y, 3 !== d.nodeType && 8 !== d.nodeType && !$.test(p + m.event.triggered) && (p.indexOf(".") >= 0 && (q = p.split("."), p = q.shift(), q.sort()), g = p.indexOf(":") < 0 && "on" + p, b = b[m.expando] ? b : new m.Event(p, "object" == typeof b && b), b.isTrigger = e ? 2 : 3, b.namespace = q.join("."), b.namespace_re = b.namespace ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = d), c = null == c ? [b] : m.makeArray(c, [b]), k = m.event.special[p] || {}, e || !k.trigger || k.trigger.apply(d, c) !== !1)) { if (!e && !k.noBubble && !m.isWindow(d)) { for (i = k.delegateType || p, $.test(i + p) || (h = h.parentNode) ; h; h = h.parentNode) o.push(h), l = h; l === (d.ownerDocument || y) && o.push(l.defaultView || l.parentWindow || a) } n = 0; while ((h = o[n++]) && !b.isPropagationStopped()) b.type = n > 1 ? i : k.bindType || p, f = (m._data(h, "events") || {})[b.type] && m._data(h, "handle"), f && f.apply(h, c), f = g && h[g], f && f.apply && m.acceptData(h) && (b.result = f.apply(h, c), b.result === !1 && b.preventDefault()); if (b.type = p, !e && !b.isDefaultPrevented() && (!k._default || k._default.apply(o.pop(), c) === !1) && m.acceptData(d) && g && d[p] && !m.isWindow(d)) { l = d[g], l && (d[g] = null), m.event.triggered = p; try { d[p]() } catch (r) { } m.event.triggered = void 0, l && (d[g] = l) } return b.result } }, dispatch: function (a) { a = m.event.fix(a); var b, c, e, f, g, h = [], i = d.call(arguments), j = (m._data(this, "events") || {})[a.type] || [], k = m.event.special[a.type] || {}; if (i[0] = a, a.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, a) !== !1) { h = m.event.handlers.call(this, a, j), b = 0; while ((f = h[b++]) && !a.isPropagationStopped()) { a.currentTarget = f.elem, g = 0; while ((e = f.handlers[g++]) && !a.isImmediatePropagationStopped()) (!a.namespace_re || a.namespace_re.test(e.namespace)) && (a.handleObj = e, a.data = e.data, c = ((m.event.special[e.origType] || {}).handle || e.handler).apply(f.elem, i), void 0 !== c && (a.result = c) === !1 && (a.preventDefault(), a.stopPropagation())) } return k.postDispatch && k.postDispatch.call(this, a), a.result } }, handlers: function (a, b) { var c, d, e, f, g = [], h = b.delegateCount, i = a.target; if (h && i.nodeType && (!a.button || "click" !== a.type)) for (; i != this; i = i.parentNode || this) if (1 === i.nodeType && (i.disabled !== !0 || "click" !== a.type)) { for (e = [], f = 0; h > f; f++) d = b[f], c = d.selector + " ", void 0 === e[c] && (e[c] = d.needsContext ? m(c, this).index(i) >= 0 : m.find(c, this, null, [i]).length), e[c] && e.push(d); e.length && g.push({ elem: i, handlers: e }) } return h < b.length && g.push({ elem: this, handlers: b.slice(h) }), g }, fix: function (a) { if (a[m.expando]) return a; var b, c, d, e = a.type, f = a, g = this.fixHooks[e]; g || (this.fixHooks[e] = g = Z.test(e) ? this.mouseHooks : Y.test(e) ? this.keyHooks : {}), d = g.props ? this.props.concat(g.props) : this.props, a = new m.Event(f), b = d.length; while (b--) c = d[b], a[c] = f[c]; return a.target || (a.target = f.srcElement || y), 3 === a.target.nodeType && (a.target = a.target.parentNode), a.metaKey = !!a.metaKey, g.filter ? g.filter(a, f) : a }, props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "), fixHooks: {}, keyHooks: { props: "char charCode key keyCode".split(" "), filter: function (a, b) { return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), a } }, mouseHooks: { props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "), filter: function (a, b) { var c, d, e, f = b.button, g = b.fromElement; return null == a.pageX && null != b.clientX && (d = a.target.ownerDocument || y, e = d.documentElement, c = d.body, a.pageX = b.clientX + (e && e.scrollLeft || c && c.scrollLeft || 0) - (e && e.clientLeft || c && c.clientLeft || 0), a.pageY = b.clientY + (e && e.scrollTop || c && c.scrollTop || 0) - (e && e.clientTop || c && c.clientTop || 0)), !a.relatedTarget && g && (a.relatedTarget = g === a.target ? b.toElement : g), a.which || void 0 === f || (a.which = 1 & f ? 1 : 2 & f ? 3 : 4 & f ? 2 : 0), a } }, special: { load: { noBubble: !0 }, focus: { trigger: function () { if (this !== cb() && this.focus) try { return this.focus(), !1 } catch (a) { } }, delegateType: "focusin" }, blur: { trigger: function () { return this === cb() && this.blur ? (this.blur(), !1) : void 0 }, delegateType: "focusout" }, click: { trigger: function () { return m.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0 }, _default: function (a) { return m.nodeName(a.target, "a") } }, beforeunload: { postDispatch: function (a) { void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result) } } }, simulate: function (a, b, c, d) { var e = m.extend(new m.Event, c, { type: a, isSimulated: !0, originalEvent: {} }); d ? m.event.trigger(e, null, b) : m.event.dispatch.call(b, e), e.isDefaultPrevented() && c.preventDefault() } }, m.removeEvent = y.removeEventListener ? function (a, b, c) { a.removeEventListener && a.removeEventListener(b, c, !1) } : function (a, b, c) { var d = "on" + b; a.detachEvent && (typeof a[d] === K && (a[d] = null), a.detachEvent(d, c)) }, m.Event = function (a, b) { return this instanceof m.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? ab : bb) : this.type = a, b && m.extend(this, b), this.timeStamp = a && a.timeStamp || m.now(), void (this[m.expando] = !0)) : new m.Event(a, b) }, m.Event.prototype = { isDefaultPrevented: bb, isPropagationStopped: bb, isImmediatePropagationStopped: bb, preventDefault: function () { var a = this.originalEvent; this.isDefaultPrevented = ab, a && (a.preventDefault ? a.preventDefault() : a.returnValue = !1) }, stopPropagation: function () { var a = this.originalEvent; this.isPropagationStopped = ab, a && (a.stopPropagation && a.stopPropagation(), a.cancelBubble = !0) }, stopImmediatePropagation: function () { var a = this.originalEvent; this.isImmediatePropagationStopped = ab, a && a.stopImmediatePropagation && a.stopImmediatePropagation(), this.stopPropagation() } }, m.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, function (a, b) { m.event.special[a] = { delegateType: b, bindType: b, handle: function (a) { var c, d = this, e = a.relatedTarget, f = a.handleObj; return (!e || e !== d && !m.contains(d, e)) && (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c } } }), k.submitBubbles || (m.event.special.submit = { setup: function () { return m.nodeName(this, "form") ? !1 : void m.event.add(this, "click._submit keypress._submit", function (a) { var b = a.target, c = m.nodeName(b, "input") || m.nodeName(b, "button") ? b.form : void 0; c && !m._data(c, "submitBubbles") && (m.event.add(c, "submit._submit", function (a) { a._submit_bubble = !0 }), m._data(c, "submitBubbles", !0)) }) }, postDispatch: function (a) { a._submit_bubble && (delete a._submit_bubble, this.parentNode && !a.isTrigger && m.event.simulate("submit", this.parentNode, a, !0)) }, teardown: function () { return m.nodeName(this, "form") ? !1 : void m.event.remove(this, "._submit") } }), k.changeBubbles || (m.event.special.change = { setup: function () { return X.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (m.event.add(this, "propertychange._change", function (a) { "checked" === a.originalEvent.propertyName && (this._just_changed = !0) }), m.event.add(this, "click._change", function (a) { this._just_changed && !a.isTrigger && (this._just_changed = !1), m.event.simulate("change", this, a, !0) })), !1) : void m.event.add(this, "beforeactivate._change", function (a) { var b = a.target; X.test(b.nodeName) && !m._data(b, "changeBubbles") && (m.event.add(b, "change._change", function (a) { !this.parentNode || a.isSimulated || a.isTrigger || m.event.simulate("change", this.parentNode, a, !0) }), m._data(b, "changeBubbles", !0)) }) }, handle: function (a) { var b = a.target; return this !== b || a.isSimulated || a.isTrigger || "radio" !== b.type && "checkbox" !== b.type ? a.handleObj.handler.apply(this, arguments) : void 0 }, teardown: function () { return m.event.remove(this, "._change"), !X.test(this.nodeName) } }), k.focusinBubbles || m.each({ focus: "focusin", blur: "focusout" }, function (a, b) { var c = function (a) { m.event.simulate(b, a.target, m.event.fix(a), !0) }; m.event.special[b] = { setup: function () { var d = this.ownerDocument || this, e = m._data(d, b); e || d.addEventListener(a, c, !0), m._data(d, b, (e || 0) + 1) }, teardown: function () { var d = this.ownerDocument || this, e = m._data(d, b) - 1; e ? m._data(d, b, e) : (d.removeEventListener(a, c, !0), m._removeData(d, b)) } } }), m.fn.extend({ on: function (a, b, c, d, e) { var f, g; if ("object" == typeof a) { "string" != typeof b && (c = c || b, b = void 0); for (f in a) this.on(f, b, c, a[f], e); return this } if (null == c && null == d ? (d = b, c = b = void 0) : null == d && ("string" == typeof b ? (d = c, c = void 0) : (d = c, c = b, b = void 0)), d === !1) d = bb; else if (!d) return this; return 1 === e && (g = d, d = function (a) { return m().off(a), g.apply(this, arguments) }, d.guid = g.guid || (g.guid = m.guid++)), this.each(function () { m.event.add(this, a, d, c, b) }) }, one: function (a, b, c, d) { return this.on(a, b, c, d, 1) }, off: function (a, b, c) { var d, e; if (a && a.preventDefault && a.handleObj) return d = a.handleObj, m(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this; if ("object" == typeof a) { for (e in a) this.off(e, b, a[e]); return this } return (b === !1 || "function" == typeof b) && (c = b, b = void 0), c === !1 && (c = bb), this.each(function () { m.event.remove(this, a, c, b) }) }, trigger: function (a, b) { return this.each(function () { m.event.trigger(a, b, this) }) }, triggerHandler: function (a, b) { var c = this[0]; return c ? m.event.trigger(a, b, c, !0) : void 0 } }); function db(a) { var b = eb.split("|"), c = a.createDocumentFragment(); if (c.createElement) while (b.length) c.createElement(b.pop()); return c } var eb = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video", fb = / jQuery\d+="(?:null|\d+)"/g, gb = new RegExp("<(?:" + eb + ")[\\s/>]", "i"), hb = /^\s+/, ib = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, jb = /<([\w:]+)/, kb = /<tbody/i, lb = /<|&#?\w+;/, mb = /<(?:script|style|link)/i, nb = /checked\s*(?:[^=]|=\s*.checked.)/i, ob = /^$|\/(?:java|ecma)script/i, pb = /^true\/(.*)/, qb = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, rb = { option: [1, "<select multiple='multiple'>", "</select>"], legend: [1, "<fieldset>", "</fieldset>"], area: [1, "<map>", "</map>"], param: [1, "<object>", "</object>"], thead: [1, "<table>", "</table>"], tr: [2, "<table><tbody>", "</tbody></table>"], col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], _default: k.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"] }, sb = db(y), tb = sb.appendChild(y.createElement("div")); rb.optgroup = rb.option, rb.tbody = rb.tfoot = rb.colgroup = rb.caption = rb.thead, rb.th = rb.td; function ub(a, b) { var c, d, e = 0, f = typeof a.getElementsByTagName !== K ? a.getElementsByTagName(b || "*") : typeof a.querySelectorAll !== K ? a.querySelectorAll(b || "*") : void 0; if (!f) for (f = [], c = a.childNodes || a; null != (d = c[e]) ; e++) !b || m.nodeName(d, b) ? f.push(d) : m.merge(f, ub(d, b)); return void 0 === b || b && m.nodeName(a, b) ? m.merge([a], f) : f } function vb(a) { W.test(a.type) && (a.defaultChecked = a.checked) } function wb(a, b) { return m.nodeName(a, "table") && m.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a } function xb(a) { return a.type = (null !== m.find.attr(a, "type")) + "/" + a.type, a } function yb(a) { var b = pb.exec(a.type); return b ? a.type = b[1] : a.removeAttribute("type"), a } function zb(a, b) { for (var c, d = 0; null != (c = a[d]) ; d++) m._data(c, "globalEval", !b || m._data(b[d], "globalEval")) } function Ab(a, b) { if (1 === b.nodeType && m.hasData(a)) { var c, d, e, f = m._data(a), g = m._data(b, f), h = f.events; if (h) { delete g.handle, g.events = {}; for (c in h) for (d = 0, e = h[c].length; e > d; d++) m.event.add(b, c, h[c][d]) } g.data && (g.data = m.extend({}, g.data)) } } function Bb(a, b) { var c, d, e; if (1 === b.nodeType) { if (c = b.nodeName.toLowerCase(), !k.noCloneEvent && b[m.expando]) { e = m._data(b); for (d in e.events) m.removeEvent(b, d, e.handle); b.removeAttribute(m.expando) } "script" === c && b.text !== a.text ? (xb(b).text = a.text, yb(b)) : "object" === c ? (b.parentNode && (b.outerHTML = a.outerHTML), k.html5Clone && a.innerHTML && !m.trim(b.innerHTML) && (b.innerHTML = a.innerHTML)) : "input" === c && W.test(a.type) ? (b.defaultChecked = b.checked = a.checked, b.value !== a.value && (b.value = a.value)) : "option" === c ? b.defaultSelected = b.selected = a.defaultSelected : ("input" === c || "textarea" === c) && (b.defaultValue = a.defaultValue) } } m.extend({ clone: function (a, b, c) { var d, e, f, g, h, i = m.contains(a.ownerDocument, a); if (k.html5Clone || m.isXMLDoc(a) || !gb.test("<" + a.nodeName + ">") ? f = a.cloneNode(!0) : (tb.innerHTML = a.outerHTML, tb.removeChild(f = tb.firstChild)), !(k.noCloneEvent && k.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || m.isXMLDoc(a))) for (d = ub(f), h = ub(a), g = 0; null != (e = h[g]) ; ++g) d[g] && Bb(e, d[g]); if (b) if (c) for (h = h || ub(a), d = d || ub(f), g = 0; null != (e = h[g]) ; g++) Ab(e, d[g]); else Ab(a, f); return d = ub(f, "script"), d.length > 0 && zb(d, !i && ub(a, "script")), d = h = e = null, f }, buildFragment: function (a, b, c, d) { for (var e, f, g, h, i, j, l, n = a.length, o = db(b), p = [], q = 0; n > q; q++) if (f = a[q], f || 0 === f) if ("object" === m.type(f)) m.merge(p, f.nodeType ? [f] : f); else if (lb.test(f)) { h = h || o.appendChild(b.createElement("div")), i = (jb.exec(f) || ["", ""])[1].toLowerCase(), l = rb[i] || rb._default, h.innerHTML = l[1] + f.replace(ib, "<$1></$2>") + l[2], e = l[0]; while (e--) h = h.lastChild; if (!k.leadingWhitespace && hb.test(f) && p.push(b.createTextNode(hb.exec(f)[0])), !k.tbody) { f = "table" !== i || kb.test(f) ? "<table>" !== l[1] || kb.test(f) ? 0 : h : h.firstChild, e = f && f.childNodes.length; while (e--) m.nodeName(j = f.childNodes[e], "tbody") && !j.childNodes.length && f.removeChild(j) } m.merge(p, h.childNodes), h.textContent = ""; while (h.firstChild) h.removeChild(h.firstChild); h = o.lastChild } else p.push(b.createTextNode(f)); h && o.removeChild(h), k.appendChecked || m.grep(ub(p, "input"), vb), q = 0; while (f = p[q++]) if ((!d || -1 === m.inArray(f, d)) && (g = m.contains(f.ownerDocument, f), h = ub(o.appendChild(f), "script"), g && zb(h), c)) { e = 0; while (f = h[e++]) ob.test(f.type || "") && c.push(f) } return h = null, o }, cleanData: function (a, b) { for (var d, e, f, g, h = 0, i = m.expando, j = m.cache, l = k.deleteExpando, n = m.event.special; null != (d = a[h]) ; h++) if ((b || m.acceptData(d)) && (f = d[i], g = f && j[f])) { if (g.events) for (e in g.events) n[e] ? m.event.remove(d, e) : m.removeEvent(d, e, g.handle); j[f] && (delete j[f], l ? delete d[i] : typeof d.removeAttribute !== K ? d.removeAttribute(i) : d[i] = null, c.push(f)) } } }), m.fn.extend({ text: function (a) { return V(this, function (a) { return void 0 === a ? m.text(this) : this.empty().append((this[0] && this[0].ownerDocument || y).createTextNode(a)) }, null, a, arguments.length) }, append: function () { return this.domManip(arguments, function (a) { if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) { var b = wb(this, a); b.appendChild(a) } }) }, prepend: function () { return this.domManip(arguments, function (a) { if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) { var b = wb(this, a); b.insertBefore(a, b.firstChild) } }) }, before: function () { return this.domManip(arguments, function (a) { this.parentNode && this.parentNode.insertBefore(a, this) }) }, after: function () { return this.domManip(arguments, function (a) { this.parentNode && this.parentNode.insertBefore(a, this.nextSibling) }) }, remove: function (a, b) { for (var c, d = a ? m.filter(a, this) : this, e = 0; null != (c = d[e]) ; e++) b || 1 !== c.nodeType || m.cleanData(ub(c)), c.parentNode && (b && m.contains(c.ownerDocument, c) && zb(ub(c, "script")), c.parentNode.removeChild(c)); return this }, empty: function () { for (var a, b = 0; null != (a = this[b]) ; b++) { 1 === a.nodeType && m.cleanData(ub(a, !1)); while (a.firstChild) a.removeChild(a.firstChild); a.options && m.nodeName(a, "select") && (a.options.length = 0) } return this }, clone: function (a, b) { return a = null == a ? !1 : a, b = null == b ? a : b, this.map(function () { return m.clone(this, a, b) }) }, html: function (a) { return V(this, function (a) { var b = this[0] || {}, c = 0, d = this.length; if (void 0 === a) return 1 === b.nodeType ? b.innerHTML.replace(fb, "") : void 0; if (!("string" != typeof a || mb.test(a) || !k.htmlSerialize && gb.test(a) || !k.leadingWhitespace && hb.test(a) || rb[(jb.exec(a) || ["", ""])[1].toLowerCase()])) { a = a.replace(ib, "<$1></$2>"); try { for (; d > c; c++) b = this[c] || {}, 1 === b.nodeType && (m.cleanData(ub(b, !1)), b.innerHTML = a); b = 0 } catch (e) { } } b && this.empty().append(a) }, null, a, arguments.length) }, replaceWith: function () { var a = arguments[0]; return this.domManip(arguments, function (b) { a = this.parentNode, m.cleanData(ub(this)), a && a.replaceChild(b, this) }), a && (a.length || a.nodeType) ? this : this.remove() }, detach: function (a) { return this.remove(a, !0) }, domManip: function (a, b) { a = e.apply([], a); var c, d, f, g, h, i, j = 0, l = this.length, n = this, o = l - 1, p = a[0], q = m.isFunction(p); if (q || l > 1 && "string" == typeof p && !k.checkClone && nb.test(p)) return this.each(function (c) { var d = n.eq(c); q && (a[0] = p.call(this, c, d.html())), d.domManip(a, b) }); if (l && (i = m.buildFragment(a, this[0].ownerDocument, !1, this), c = i.firstChild, 1 === i.childNodes.length && (i = c), c)) { for (g = m.map(ub(i, "script"), xb), f = g.length; l > j; j++) d = i, j !== o && (d = m.clone(d, !0, !0), f && m.merge(g, ub(d, "script"))), b.call(this[j], d, j); if (f) for (h = g[g.length - 1].ownerDocument, m.map(g, yb), j = 0; f > j; j++) d = g[j], ob.test(d.type || "") && !m._data(d, "globalEval") && m.contains(h, d) && (d.src ? m._evalUrl && m._evalUrl(d.src) : m.globalEval((d.text || d.textContent || d.innerHTML || "").replace(qb, ""))); i = c = null } return this } }), m.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (a, b) { m.fn[a] = function (a) { for (var c, d = 0, e = [], g = m(a), h = g.length - 1; h >= d; d++) c = d === h ? this : this.clone(!0), m(g[d])[b](c), f.apply(e, c.get()); return this.pushStack(e) } }); var Cb, Db = {}; function Eb(b, c) { var d, e = m(c.createElement(b)).appendTo(c.body), f = a.getDefaultComputedStyle && (d = a.getDefaultComputedStyle(e[0])) ? d.display : m.css(e[0], "display"); return e.detach(), f } function Fb(a) { var b = y, c = Db[a]; return c || (c = Eb(a, b), "none" !== c && c || (Cb = (Cb || m("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement), b = (Cb[0].contentWindow || Cb[0].contentDocument).document, b.write(), b.close(), c = Eb(a, b), Cb.detach()), Db[a] = c), c } !function () { var a; k.shrinkWrapBlocks = function () { if (null != a) return a; a = !1; var b, c, d; return c = y.getElementsByTagName("body")[0], c && c.style ? (b = y.createElement("div"), d = y.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(d).appendChild(b), typeof b.style.zoom !== K && (b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", b.appendChild(y.createElement("div")).style.width = "5px", a = 3 !== b.offsetWidth), c.removeChild(d), a) : void 0 } }(); var Gb = /^margin/, Hb = new RegExp("^(" + S + ")(?!px)[a-z%]+$", "i"), Ib, Jb, Kb = /^(top|right|bottom|left)$/; a.getComputedStyle ? (Ib = function (a) { return a.ownerDocument.defaultView.getComputedStyle(a, null) }, Jb = function (a, b, c) { var d, e, f, g, h = a.style; return c = c || Ib(a), g = c ? c.getPropertyValue(b) || c[b] : void 0, c && ("" !== g || m.contains(a.ownerDocument, a) || (g = m.style(a, b)), Hb.test(g) && Gb.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 === g ? g : g + "" }) : y.documentElement.currentStyle && (Ib = function (a) { return a.currentStyle }, Jb = function (a, b, c) { var d, e, f, g, h = a.style; return c = c || Ib(a), g = c ? c[b] : void 0, null == g && h && h[b] && (g = h[b]), Hb.test(g) && !Kb.test(b) && (d = h.left, e = a.runtimeStyle, f = e && e.left, f && (e.left = a.currentStyle.left), h.left = "fontSize" === b ? "1em" : g, g = h.pixelLeft + "px", h.left = d, f && (e.left = f)), void 0 === g ? g : g + "" || "auto" }); function Lb(a, b) { return { get: function () { var c = a(); if (null != c) return c ? void delete this.get : (this.get = b).apply(this, arguments) } } } !function () { var b, c, d, e, f, g, h; if (b = y.createElement("div"), b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", d = b.getElementsByTagName("a")[0], c = d && d.style) { c.cssText = "float:left;opacity:.5", k.opacity = "0.5" === c.opacity, k.cssFloat = !!c.cssFloat, b.style.backgroundClip = "content-box", b.cloneNode(!0).style.backgroundClip = "", k.clearCloneStyle = "content-box" === b.style.backgroundClip, k.boxSizing = "" === c.boxSizing || "" === c.MozBoxSizing || "" === c.WebkitBoxSizing, m.extend(k, { reliableHiddenOffsets: function () { return null == g && i(), g }, boxSizingReliable: function () { return null == f && i(), f }, pixelPosition: function () { return null == e && i(), e }, reliableMarginRight: function () { return null == h && i(), h } }); function i() { var b, c, d, i; c = y.getElementsByTagName("body")[0], c && c.style && (b = y.createElement("div"), d = y.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(d).appendChild(b), b.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", e = f = !1, h = !0, a.getComputedStyle && (e = "1%" !== (a.getComputedStyle(b, null) || {}).top, f = "4px" === (a.getComputedStyle(b, null) || { width: "4px" }).width, i = b.appendChild(y.createElement("div")), i.style.cssText = b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", i.style.marginRight = i.style.width = "0", b.style.width = "1px", h = !parseFloat((a.getComputedStyle(i, null) || {}).marginRight)), b.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", i = b.getElementsByTagName("td"), i[0].style.cssText = "margin:0;border:0;padding:0;display:none", g = 0 === i[0].offsetHeight, g && (i[0].style.display = "", i[1].style.display = "none", g = 0 === i[0].offsetHeight), c.removeChild(d)) } } }(), m.swap = function (a, b, c, d) { var e, f, g = {}; for (f in b) g[f] = a.style[f], a.style[f] = b[f]; e = c.apply(a, d || []); for (f in b) a.style[f] = g[f]; return e }; var Mb = /alpha\([^)]*\)/i, Nb = /opacity\s*=\s*([^)]*)/, Ob = /^(none|table(?!-c[ea]).+)/, Pb = new RegExp("^(" + S + ")(.*)$", "i"), Qb = new RegExp("^([+-])=(" + S + ")", "i"), Rb = { position: "absolute", visibility: "hidden", display: "block" }, Sb = { letterSpacing: "0", fontWeight: "400" }, Tb = ["Webkit", "O", "Moz", "ms"]; function Ub(a, b) { if (b in a) return b; var c = b.charAt(0).toUpperCase() + b.slice(1), d = b, e = Tb.length; while (e--) if (b = Tb[e] + c, b in a) return b; return d } function Vb(a, b) { for (var c, d, e, f = [], g = 0, h = a.length; h > g; g++) d = a[g], d.style && (f[g] = m._data(d, "olddisplay"), c = d.style.display, b ? (f[g] || "none" !== c || (d.style.display = ""), "" === d.style.display && U(d) && (f[g] = m._data(d, "olddisplay", Fb(d.nodeName)))) : (e = U(d), (c && "none" !== c || !e) && m._data(d, "olddisplay", e ? c : m.css(d, "display")))); for (g = 0; h > g; g++) d = a[g], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[g] || "" : "none")); return a } function Wb(a, b, c) { var d = Pb.exec(b); return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b } function Xb(a, b, c, d, e) { for (var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; 4 > f; f += 2) "margin" === c && (g += m.css(a, c + T[f], !0, e)), d ? ("content" === c && (g -= m.css(a, "padding" + T[f], !0, e)), "margin" !== c && (g -= m.css(a, "border" + T[f] + "Width", !0, e))) : (g += m.css(a, "padding" + T[f], !0, e), "padding" !== c && (g += m.css(a, "border" + T[f] + "Width", !0, e))); return g } function Yb(a, b, c) { var d = !0, e = "width" === b ? a.offsetWidth : a.offsetHeight, f = Ib(a), g = k.boxSizing && "border-box" === m.css(a, "boxSizing", !1, f); if (0 >= e || null == e) { if (e = Jb(a, b, f), (0 > e || null == e) && (e = a.style[b]), Hb.test(e)) return e; d = g && (k.boxSizingReliable() || e === a.style[b]), e = parseFloat(e) || 0 } return e + Xb(a, b, c || (g ? "border" : "content"), d, f) + "px" } m.extend({ cssHooks: { opacity: { get: function (a, b) { if (b) { var c = Jb(a, "opacity"); return "" === c ? "1" : c } } } }, cssNumber: { columnCount: !0, fillOpacity: !0, flexGrow: !0, flexShrink: !0, fontWeight: !0, lineHeight: !0, opacity: !0, order: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0 }, cssProps: { "float": k.cssFloat ? "cssFloat" : "styleFloat" }, style: function (a, b, c, d) { if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) { var e, f, g, h = m.camelCase(b), i = a.style; if (b = m.cssProps[h] || (m.cssProps[h] = Ub(i, h)), g = m.cssHooks[b] || m.cssHooks[h], void 0 === c) return g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b]; if (f = typeof c, "string" === f && (e = Qb.exec(c)) && (c = (e[1] + 1) * e[2] + parseFloat(m.css(a, b)), f = "number"), null != c && c === c && ("number" !== f || m.cssNumber[h] || (c += "px"), k.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), !(g && "set" in g && void 0 === (c = g.set(a, c, d))))) try { i[b] = c } catch (j) { } } }, css: function (a, b, c, d) { var e, f, g, h = m.camelCase(b); return b = m.cssProps[h] || (m.cssProps[h] = Ub(a.style, h)), g = m.cssHooks[b] || m.cssHooks[h], g && "get" in g && (f = g.get(a, !0, c)), void 0 === f && (f = Jb(a, b, d)), "normal" === f && b in Sb && (f = Sb[b]), "" === c || c ? (e = parseFloat(f), c === !0 || m.isNumeric(e) ? e || 0 : f) : f } }), m.each(["height", "width"], function (a, b) { m.cssHooks[b] = { get: function (a, c, d) { return c ? Ob.test(m.css(a, "display")) && 0 === a.offsetWidth ? m.swap(a, Rb, function () { return Yb(a, b, d) }) : Yb(a, b, d) : void 0 }, set: function (a, c, d) { var e = d && Ib(a); return Wb(a, c, d ? Xb(a, b, d, k.boxSizing && "border-box" === m.css(a, "boxSizing", !1, e), e) : 0) } } }), k.opacity || (m.cssHooks.opacity = { get: function (a, b) { return Nb.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : b ? "1" : "" }, set: function (a, b) { var c = a.style, d = a.currentStyle, e = m.isNumeric(b) ? "alpha(opacity=" + 100 * b + ")" : "", f = d && d.filter || c.filter || ""; c.zoom = 1, (b >= 1 || "" === b) && "" === m.trim(f.replace(Mb, "")) && c.removeAttribute && (c.removeAttribute("filter"), "" === b || d && !d.filter) || (c.filter = Mb.test(f) ? f.replace(Mb, e) : f + " " + e) } }), m.cssHooks.marginRight = Lb(k.reliableMarginRight, function (a, b) { return b ? m.swap(a, { display: "inline-block" }, Jb, [a, "marginRight"]) : void 0 }), m.each({ margin: "", padding: "", border: "Width" }, function (a, b) { m.cssHooks[a + b] = { expand: function (c) { for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; 4 > d; d++) e[a + T[d] + b] = f[d] || f[d - 2] || f[0]; return e } }, Gb.test(a) || (m.cssHooks[a + b].set = Wb) }), m.fn.extend({ css: function (a, b) { return V(this, function (a, b, c) { var d, e, f = {}, g = 0; if (m.isArray(b)) { for (d = Ib(a), e = b.length; e > g; g++) f[b[g]] = m.css(a, b[g], !1, d); return f } return void 0 !== c ? m.style(a, b, c) : m.css(a, b) }, a, b, arguments.length > 1) }, show: function () { return Vb(this, !0) }, hide: function () { return Vb(this) }, toggle: function (a) { return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function () { U(this) ? m(this).show() : m(this).hide() }) } }); function Zb(a, b, c, d, e) { return new Zb.prototype.init(a, b, c, d, e) } m.Tween = Zb, Zb.prototype = {
    constructor: Zb, init: function (a, b, c, d, e, f) {
      this.elem = a, this.prop = c, this.easing = e || "swing", this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (m.cssNumber[c] ? "" : "px")
    }, cur: function () { var a = Zb.propHooks[this.prop]; return a && a.get ? a.get(this) : Zb.propHooks._default.get(this) }, run: function (a) { var b, c = Zb.propHooks[this.prop]; return this.pos = b = this.options.duration ? m.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : Zb.propHooks._default.set(this), this }
  }, Zb.prototype.init.prototype = Zb.prototype, Zb.propHooks = { _default: { get: function (a) { var b; return null == a.elem[a.prop] || a.elem.style && null != a.elem.style[a.prop] ? (b = m.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0) : a.elem[a.prop] }, set: function (a) { m.fx.step[a.prop] ? m.fx.step[a.prop](a) : a.elem.style && (null != a.elem.style[m.cssProps[a.prop]] || m.cssHooks[a.prop]) ? m.style(a.elem, a.prop, a.now + a.unit) : a.elem[a.prop] = a.now } } }, Zb.propHooks.scrollTop = Zb.propHooks.scrollLeft = { set: function (a) { a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now) } }, m.easing = { linear: function (a) { return a }, swing: function (a) { return .5 - Math.cos(a * Math.PI) / 2 } }, m.fx = Zb.prototype.init, m.fx.step = {}; var $b, _b, ac = /^(?:toggle|show|hide)$/, bc = new RegExp("^(?:([+-])=|)(" + S + ")([a-z%]*)$", "i"), cc = /queueHooks$/, dc = [ic], ec = { "*": [function (a, b) { var c = this.createTween(a, b), d = c.cur(), e = bc.exec(b), f = e && e[3] || (m.cssNumber[a] ? "" : "px"), g = (m.cssNumber[a] || "px" !== f && +d) && bc.exec(m.css(c.elem, a)), h = 1, i = 20; if (g && g[3] !== f) { f = f || g[3], e = e || [], g = +d || 1; do h = h || ".5", g /= h, m.style(c.elem, a, g + f); while (h !== (h = c.cur() / d) && 1 !== h && --i) } return e && (g = c.start = +g || +d || 0, c.unit = f, c.end = e[1] ? g + (e[1] + 1) * e[2] : +e[2]), c }] }; function fc() { return setTimeout(function () { $b = void 0 }), $b = m.now() } function gc(a, b) { var c, d = { height: a }, e = 0; for (b = b ? 1 : 0; 4 > e; e += 2 - b) c = T[e], d["margin" + c] = d["padding" + c] = a; return b && (d.opacity = d.width = a), d } function hc(a, b, c) { for (var d, e = (ec[b] || []).concat(ec["*"]), f = 0, g = e.length; g > f; f++) if (d = e[f].call(c, b, a)) return d } function ic(a, b, c) { var d, e, f, g, h, i, j, l, n = this, o = {}, p = a.style, q = a.nodeType && U(a), r = m._data(a, "fxshow"); c.queue || (h = m._queueHooks(a, "fx"), null == h.unqueued && (h.unqueued = 0, i = h.empty.fire, h.empty.fire = function () { h.unqueued || i() }), h.unqueued++, n.always(function () { n.always(function () { h.unqueued--, m.queue(a, "fx").length || h.empty.fire() }) })), 1 === a.nodeType && ("height" in b || "width" in b) && (c.overflow = [p.overflow, p.overflowX, p.overflowY], j = m.css(a, "display"), l = "none" === j ? m._data(a, "olddisplay") || Fb(a.nodeName) : j, "inline" === l && "none" === m.css(a, "float") && (k.inlineBlockNeedsLayout && "inline" !== Fb(a.nodeName) ? p.zoom = 1 : p.display = "inline-block")), c.overflow && (p.overflow = "hidden", k.shrinkWrapBlocks() || n.always(function () { p.overflow = c.overflow[0], p.overflowX = c.overflow[1], p.overflowY = c.overflow[2] })); for (d in b) if (e = b[d], ac.exec(e)) { if (delete b[d], f = f || "toggle" === e, e === (q ? "hide" : "show")) { if ("show" !== e || !r || void 0 === r[d]) continue; q = !0 } o[d] = r && r[d] || m.style(a, d) } else j = void 0; if (m.isEmptyObject(o)) "inline" === ("none" === j ? Fb(a.nodeName) : j) && (p.display = j); else { r ? "hidden" in r && (q = r.hidden) : r = m._data(a, "fxshow", {}), f && (r.hidden = !q), q ? m(a).show() : n.done(function () { m(a).hide() }), n.done(function () { var b; m._removeData(a, "fxshow"); for (b in o) m.style(a, b, o[b]) }); for (d in o) g = hc(q ? r[d] : 0, d, n), d in r || (r[d] = g.start, q && (g.end = g.start, g.start = "width" === d || "height" === d ? 1 : 0)) } } function jc(a, b) { var c, d, e, f, g; for (c in a) if (d = m.camelCase(c), e = b[d], f = a[c], m.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = m.cssHooks[d], g && "expand" in g) { f = g.expand(f), delete a[d]; for (c in f) c in a || (a[c] = f[c], b[c] = e) } else b[d] = e } function kc(a, b, c) { var d, e, f = 0, g = dc.length, h = m.Deferred().always(function () { delete i.elem }), i = function () { if (e) return !1; for (var b = $b || fc(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++) j.tweens[g].run(f); return h.notifyWith(a, [j, f, c]), 1 > f && i ? c : (h.resolveWith(a, [j]), !1) }, j = h.promise({ elem: a, props: m.extend({}, b), opts: m.extend(!0, { specialEasing: {} }, c), originalProperties: b, originalOptions: c, startTime: $b || fc(), duration: c.duration, tweens: [], createTween: function (b, c) { var d = m.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing); return j.tweens.push(d), d }, stop: function (b) { var c = 0, d = b ? j.tweens.length : 0; if (e) return this; for (e = !0; d > c; c++) j.tweens[c].run(1); return b ? h.resolveWith(a, [j, b]) : h.rejectWith(a, [j, b]), this } }), k = j.props; for (jc(k, j.opts.specialEasing) ; g > f; f++) if (d = dc[f].call(j, a, k, j.opts)) return d; return m.map(k, hc, j), m.isFunction(j.opts.start) && j.opts.start.call(a, j), m.fx.timer(m.extend(i, { elem: a, anim: j, queue: j.opts.queue })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always) } m.Animation = m.extend(kc, { tweener: function (a, b) { m.isFunction(a) ? (b = a, a = ["*"]) : a = a.split(" "); for (var c, d = 0, e = a.length; e > d; d++) c = a[d], ec[c] = ec[c] || [], ec[c].unshift(b) }, prefilter: function (a, b) { b ? dc.unshift(a) : dc.push(a) } }), m.speed = function (a, b, c) { var d = a && "object" == typeof a ? m.extend({}, a) : { complete: c || !c && b || m.isFunction(a) && a, duration: a, easing: c && b || b && !m.isFunction(b) && b }; return d.duration = m.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in m.fx.speeds ? m.fx.speeds[d.duration] : m.fx.speeds._default, (null == d.queue || d.queue === !0) && (d.queue = "fx"), d.old = d.complete, d.complete = function () { m.isFunction(d.old) && d.old.call(this), d.queue && m.dequeue(this, d.queue) }, d }, m.fn.extend({ fadeTo: function (a, b, c, d) { return this.filter(U).css("opacity", 0).show().end().animate({ opacity: b }, a, c, d) }, animate: function (a, b, c, d) { var e = m.isEmptyObject(a), f = m.speed(b, c, d), g = function () { var b = kc(this, m.extend({}, a), f); (e || m._data(this, "finish")) && b.stop(!0) }; return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g) }, stop: function (a, b, c) { var d = function (a) { var b = a.stop; delete a.stop, b(c) }; return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function () { var b = !0, e = null != a && a + "queueHooks", f = m.timers, g = m._data(this); if (e) g[e] && g[e].stop && d(g[e]); else for (e in g) g[e] && g[e].stop && cc.test(e) && d(g[e]); for (e = f.length; e--;) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1)); (b || !c) && m.dequeue(this, a) }) }, finish: function (a) { return a !== !1 && (a = a || "fx"), this.each(function () { var b, c = m._data(this), d = c[a + "queue"], e = c[a + "queueHooks"], f = m.timers, g = d ? d.length : 0; for (c.finish = !0, m.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1)); for (b = 0; g > b; b++) d[b] && d[b].finish && d[b].finish.call(this); delete c.finish }) } }), m.each(["toggle", "show", "hide"], function (a, b) { var c = m.fn[b]; m.fn[b] = function (a, d, e) { return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(gc(b, !0), a, d, e) } }), m.each({ slideDown: gc("show"), slideUp: gc("hide"), slideToggle: gc("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function (a, b) { m.fn[a] = function (a, c, d) { return this.animate(b, a, c, d) } }), m.timers = [], m.fx.tick = function () { var a, b = m.timers, c = 0; for ($b = m.now() ; c < b.length; c++) a = b[c], a() || b[c] !== a || b.splice(c--, 1); b.length || m.fx.stop(), $b = void 0 }, m.fx.timer = function (a) { m.timers.push(a), a() ? m.fx.start() : m.timers.pop() }, m.fx.interval = 13, m.fx.start = function () { _b || (_b = setInterval(m.fx.tick, m.fx.interval)) }, m.fx.stop = function () { clearInterval(_b), _b = null }, m.fx.speeds = { slow: 600, fast: 200, _default: 400 }, m.fn.delay = function (a, b) { return a = m.fx ? m.fx.speeds[a] || a : a, b = b || "fx", this.queue(b, function (b, c) { var d = setTimeout(b, a); c.stop = function () { clearTimeout(d) } }) }, function () { var a, b, c, d, e; b = y.createElement("div"), b.setAttribute("className", "t"), b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", d = b.getElementsByTagName("a")[0], c = y.createElement("select"), e = c.appendChild(y.createElement("option")), a = b.getElementsByTagName("input")[0], d.style.cssText = "top:1px", k.getSetAttribute = "t" !== b.className, k.style = /top/.test(d.getAttribute("style")), k.hrefNormalized = "/a" === d.getAttribute("href"), k.checkOn = !!a.value, k.optSelected = e.selected, k.enctype = !!y.createElement("form").enctype, c.disabled = !0, k.optDisabled = !e.disabled, a = y.createElement("input"), a.setAttribute("value", ""), k.input = "" === a.getAttribute("value"), a.value = "t", a.setAttribute("type", "radio"), k.radioValue = "t" === a.value }(); var lc = /\r/g; m.fn.extend({ val: function (a) { var b, c, d, e = this[0]; { if (arguments.length) return d = m.isFunction(a), this.each(function (c) { var e; 1 === this.nodeType && (e = d ? a.call(this, c, m(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : m.isArray(e) && (e = m.map(e, function (a) { return null == a ? "" : a + "" })), b = m.valHooks[this.type] || m.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e)) }); if (e) return b = m.valHooks[e.type] || m.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(lc, "") : null == c ? "" : c) } } }), m.extend({ valHooks: { option: { get: function (a) { var b = m.find.attr(a, "value"); return null != b ? b : m.trim(m.text(a)) } }, select: { get: function (a) { for (var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || 0 > e, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++) if (c = d[i], !(!c.selected && i !== e || (k.optDisabled ? c.disabled : null !== c.getAttribute("disabled")) || c.parentNode.disabled && m.nodeName(c.parentNode, "optgroup"))) { if (b = m(c).val(), f) return b; g.push(b) } return g }, set: function (a, b) { var c, d, e = a.options, f = m.makeArray(b), g = e.length; while (g--) if (d = e[g], m.inArray(m.valHooks.option.get(d), f) >= 0) try { d.selected = c = !0 } catch (h) { d.scrollHeight } else d.selected = !1; return c || (a.selectedIndex = -1), e } } } }), m.each(["radio", "checkbox"], function () { m.valHooks[this] = { set: function (a, b) { return m.isArray(b) ? a.checked = m.inArray(m(a).val(), b) >= 0 : void 0 } }, k.checkOn || (m.valHooks[this].get = function (a) { return null === a.getAttribute("value") ? "on" : a.value }) }); var mc, nc, oc = m.expr.attrHandle, pc = /^(?:checked|selected)$/i, qc = k.getSetAttribute, rc = k.input; m.fn.extend({ attr: function (a, b) { return V(this, m.attr, a, b, arguments.length > 1) }, removeAttr: function (a) { return this.each(function () { m.removeAttr(this, a) }) } }), m.extend({ attr: function (a, b, c) { var d, e, f = a.nodeType; if (a && 3 !== f && 8 !== f && 2 !== f) return typeof a.getAttribute === K ? m.prop(a, b, c) : (1 === f && m.isXMLDoc(a) || (b = b.toLowerCase(), d = m.attrHooks[b] || (m.expr.match.bool.test(b) ? nc : mc)), void 0 === c ? d && "get" in d && null !== (e = d.get(a, b)) ? e : (e = m.find.attr(a, b), null == e ? void 0 : e) : null !== c ? d && "set" in d && void 0 !== (e = d.set(a, c, b)) ? e : (a.setAttribute(b, c + ""), c) : void m.removeAttr(a, b)) }, removeAttr: function (a, b) { var c, d, e = 0, f = b && b.match(E); if (f && 1 === a.nodeType) while (c = f[e++]) d = m.propFix[c] || c, m.expr.match.bool.test(c) ? rc && qc || !pc.test(c) ? a[d] = !1 : a[m.camelCase("default-" + c)] = a[d] = !1 : m.attr(a, c, ""), a.removeAttribute(qc ? c : d) }, attrHooks: { type: { set: function (a, b) { if (!k.radioValue && "radio" === b && m.nodeName(a, "input")) { var c = a.value; return a.setAttribute("type", b), c && (a.value = c), b } } } } }), nc = { set: function (a, b, c) { return b === !1 ? m.removeAttr(a, c) : rc && qc || !pc.test(c) ? a.setAttribute(!qc && m.propFix[c] || c, c) : a[m.camelCase("default-" + c)] = a[c] = !0, c } }, m.each(m.expr.match.bool.source.match(/\w+/g), function (a, b) { var c = oc[b] || m.find.attr; oc[b] = rc && qc || !pc.test(b) ? function (a, b, d) { var e, f; return d || (f = oc[b], oc[b] = e, e = null != c(a, b, d) ? b.toLowerCase() : null, oc[b] = f), e } : function (a, b, c) { return c ? void 0 : a[m.camelCase("default-" + b)] ? b.toLowerCase() : null } }), rc && qc || (m.attrHooks.value = { set: function (a, b, c) { return m.nodeName(a, "input") ? void (a.defaultValue = b) : mc && mc.set(a, b, c) } }), qc || (mc = { set: function (a, b, c) { var d = a.getAttributeNode(c); return d || a.setAttributeNode(d = a.ownerDocument.createAttribute(c)), d.value = b += "", "value" === c || b === a.getAttribute(c) ? b : void 0 } }, oc.id = oc.name = oc.coords = function (a, b, c) { var d; return c ? void 0 : (d = a.getAttributeNode(b)) && "" !== d.value ? d.value : null }, m.valHooks.button = { get: function (a, b) { var c = a.getAttributeNode(b); return c && c.specified ? c.value : void 0 }, set: mc.set }, m.attrHooks.contenteditable = { set: function (a, b, c) { mc.set(a, "" === b ? !1 : b, c) } }, m.each(["width", "height"], function (a, b) { m.attrHooks[b] = { set: function (a, c) { return "" === c ? (a.setAttribute(b, "auto"), c) : void 0 } } })), k.style || (m.attrHooks.style = { get: function (a) { return a.style.cssText || void 0 }, set: function (a, b) { return a.style.cssText = b + "" } }); var sc = /^(?:input|select|textarea|button|object)$/i, tc = /^(?:a|area)$/i; m.fn.extend({ prop: function (a, b) { return V(this, m.prop, a, b, arguments.length > 1) }, removeProp: function (a) { return a = m.propFix[a] || a, this.each(function () { try { this[a] = void 0, delete this[a] } catch (b) { } }) } }), m.extend({ propFix: { "for": "htmlFor", "class": "className" }, prop: function (a, b, c) { var d, e, f, g = a.nodeType; if (a && 3 !== g && 8 !== g && 2 !== g) return f = 1 !== g || !m.isXMLDoc(a), f && (b = m.propFix[b] || b, e = m.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b] }, propHooks: { tabIndex: { get: function (a) { var b = m.find.attr(a, "tabindex"); return b ? parseInt(b, 10) : sc.test(a.nodeName) || tc.test(a.nodeName) && a.href ? 0 : -1 } } } }), k.hrefNormalized || m.each(["href", "src"], function (a, b) { m.propHooks[b] = { get: function (a) { return a.getAttribute(b, 4) } } }), k.optSelected || (m.propHooks.selected = { get: function (a) { var b = a.parentNode; return b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex), null } }), m.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () { m.propFix[this.toLowerCase()] = this }), k.enctype || (m.propFix.enctype = "encoding"); var uc = /[\t\r\n\f]/g; m.fn.extend({ addClass: function (a) { var b, c, d, e, f, g, h = 0, i = this.length, j = "string" == typeof a && a; if (m.isFunction(a)) return this.each(function (b) { m(this).addClass(a.call(this, b, this.className)) }); if (j) for (b = (a || "").match(E) || []; i > h; h++) if (c = this[h], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(uc, " ") : " ")) { f = 0; while (e = b[f++]) d.indexOf(" " + e + " ") < 0 && (d += e + " "); g = m.trim(d), c.className !== g && (c.className = g) } return this }, removeClass: function (a) { var b, c, d, e, f, g, h = 0, i = this.length, j = 0 === arguments.length || "string" == typeof a && a; if (m.isFunction(a)) return this.each(function (b) { m(this).removeClass(a.call(this, b, this.className)) }); if (j) for (b = (a || "").match(E) || []; i > h; h++) if (c = this[h], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(uc, " ") : "")) { f = 0; while (e = b[f++]) while (d.indexOf(" " + e + " ") >= 0) d = d.replace(" " + e + " ", " "); g = a ? m.trim(d) : "", c.className !== g && (c.className = g) } return this }, toggleClass: function (a, b) { var c = typeof a; return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : this.each(m.isFunction(a) ? function (c) { m(this).toggleClass(a.call(this, c, this.className, b), b) } : function () { if ("string" === c) { var b, d = 0, e = m(this), f = a.match(E) || []; while (b = f[d++]) e.hasClass(b) ? e.removeClass(b) : e.addClass(b) } else (c === K || "boolean" === c) && (this.className && m._data(this, "__className__", this.className), this.className = this.className || a === !1 ? "" : m._data(this, "__className__") || "") }) }, hasClass: function (a) { for (var b = " " + a + " ", c = 0, d = this.length; d > c; c++) if (1 === this[c].nodeType && (" " + this[c].className + " ").replace(uc, " ").indexOf(b) >= 0) return !0; return !1 } }), m.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (a, b) { m.fn[b] = function (a, c) { return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b) } }), m.fn.extend({ hover: function (a, b) { return this.mouseenter(a).mouseleave(b || a) }, bind: function (a, b, c) { return this.on(a, null, b, c) }, unbind: function (a, b) { return this.off(a, null, b) }, delegate: function (a, b, c, d) { return this.on(b, a, c, d) }, undelegate: function (a, b, c) { return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c) } }); var vc = m.now(), wc = /\?/, xc = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g; m.parseJSON = function (b) { if (a.JSON && a.JSON.parse) return a.JSON.parse(b + ""); var c, d = null, e = m.trim(b + ""); return e && !m.trim(e.replace(xc, function (a, b, e, f) { return c && b && (d = 0), 0 === d ? a : (c = e || b, d += !f - !e, "") })) ? Function("return " + e)() : m.error("Invalid JSON: " + b) }, m.parseXML = function (b) { var c, d; if (!b || "string" != typeof b) return null; try { a.DOMParser ? (d = new DOMParser, c = d.parseFromString(b, "text/xml")) : (c = new ActiveXObject("Microsoft.XMLDOM"), c.async = "false", c.loadXML(b)) } catch (e) { c = void 0 } return c && c.documentElement && !c.getElementsByTagName("parsererror").length || m.error("Invalid XML: " + b), c }; var yc, zc, Ac = /#.*$/, Bc = /([?&])_=[^&]*/, Cc = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, Dc = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, Ec = /^(?:GET|HEAD)$/, Fc = /^\/\//, Gc = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, Hc = {}, Ic = {}, Jc = "*/".concat("*"); try { zc = location.href } catch (Kc) { zc = y.createElement("a"), zc.href = "", zc = zc.href } yc = Gc.exec(zc.toLowerCase()) || []; function Lc(a) { return function (b, c) { "string" != typeof b && (c = b, b = "*"); var d, e = 0, f = b.toLowerCase().match(E) || []; if (m.isFunction(c)) while (d = f[e++]) "+" === d.charAt(0) ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c) } } function Mc(a, b, c, d) { var e = {}, f = a === Ic; function g(h) { var i; return e[h] = !0, m.each(a[h] || [], function (a, h) { var j = h(b, c, d); return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), g(j), !1) }), i } return g(b.dataTypes[0]) || !e["*"] && g("*") } function Nc(a, b) { var c, d, e = m.ajaxSettings.flatOptions || {}; for (d in b) void 0 !== b[d] && ((e[d] ? a : c || (c = {}))[d] = b[d]); return c && m.extend(!0, a, c), a } function Oc(a, b, c) { var d, e, f, g, h = a.contents, i = a.dataTypes; while ("*" === i[0]) i.shift(), void 0 === e && (e = a.mimeType || b.getResponseHeader("Content-Type")); if (e) for (g in h) if (h[g] && h[g].test(e)) { i.unshift(g); break } if (i[0] in c) f = i[0]; else { for (g in c) { if (!i[0] || a.converters[g + " " + i[0]]) { f = g; break } d || (d = g) } f = f || d } return f ? (f !== i[0] && i.unshift(f), c[f]) : void 0 } function Pc(a, b, c, d) { var e, f, g, h, i, j = {}, k = a.dataTypes.slice(); if (k[1]) for (g in a.converters) j[g.toLowerCase()] = a.converters[g]; f = k.shift(); while (f) if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift()) if ("*" === f) f = i; else if ("*" !== i && i !== f) { if (g = j[i + " " + f] || j["* " + f], !g) for (e in j) if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) { g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1])); break } if (g !== !0) if (g && a["throws"]) b = g(b); else try { b = g(b) } catch (l) { return { state: "parsererror", error: g ? l : "No conversion from " + i + " to " + f } } } return { state: "success", data: b } } m.extend({ active: 0, lastModified: {}, etag: {}, ajaxSettings: { url: zc, type: "GET", isLocal: Dc.test(yc[1]), global: !0, processData: !0, async: !0, contentType: "application/x-www-form-urlencoded; charset=UTF-8", accepts: { "*": Jc, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" }, contents: { xml: /xml/, html: /html/, json: /json/ }, responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" }, converters: { "* text": String, "text html": !0, "text json": m.parseJSON, "text xml": m.parseXML }, flatOptions: { url: !0, context: !0 } }, ajaxSetup: function (a, b) { return b ? Nc(Nc(a, m.ajaxSettings), b) : Nc(m.ajaxSettings, a) }, ajaxPrefilter: Lc(Hc), ajaxTransport: Lc(Ic), ajax: function (a, b) { "object" == typeof a && (b = a, a = void 0), b = b || {}; var c, d, e, f, g, h, i, j, k = m.ajaxSetup({}, b), l = k.context || k, n = k.context && (l.nodeType || l.jquery) ? m(l) : m.event, o = m.Deferred(), p = m.Callbacks("once memory"), q = k.statusCode || {}, r = {}, s = {}, t = 0, u = "canceled", v = { readyState: 0, getResponseHeader: function (a) { var b; if (2 === t) { if (!j) { j = {}; while (b = Cc.exec(f)) j[b[1].toLowerCase()] = b[2] } b = j[a.toLowerCase()] } return null == b ? null : b }, getAllResponseHeaders: function () { return 2 === t ? f : null }, setRequestHeader: function (a, b) { var c = a.toLowerCase(); return t || (a = s[c] = s[c] || a, r[a] = b), this }, overrideMimeType: function (a) { return t || (k.mimeType = a), this }, statusCode: function (a) { var b; if (a) if (2 > t) for (b in a) q[b] = [q[b], a[b]]; else v.always(a[v.status]); return this }, abort: function (a) { var b = a || u; return i && i.abort(b), x(0, b), this } }; if (o.promise(v).complete = p.add, v.success = v.done, v.error = v.fail, k.url = ((a || k.url || zc) + "").replace(Ac, "").replace(Fc, yc[1] + "//"), k.type = b.method || b.type || k.method || k.type, k.dataTypes = m.trim(k.dataType || "*").toLowerCase().match(E) || [""], null == k.crossDomain && (c = Gc.exec(k.url.toLowerCase()), k.crossDomain = !(!c || c[1] === yc[1] && c[2] === yc[2] && (c[3] || ("http:" === c[1] ? "80" : "443")) === (yc[3] || ("http:" === yc[1] ? "80" : "443")))), k.data && k.processData && "string" != typeof k.data && (k.data = m.param(k.data, k.traditional)), Mc(Hc, k, b, v), 2 === t) return v; h = k.global, h && 0 === m.active++ && m.event.trigger("ajaxStart"), k.type = k.type.toUpperCase(), k.hasContent = !Ec.test(k.type), e = k.url, k.hasContent || (k.data && (e = k.url += (wc.test(e) ? "&" : "?") + k.data, delete k.data), k.cache === !1 && (k.url = Bc.test(e) ? e.replace(Bc, "$1_=" + vc++) : e + (wc.test(e) ? "&" : "?") + "_=" + vc++)), k.ifModified && (m.lastModified[e] && v.setRequestHeader("If-Modified-Since", m.lastModified[e]), m.etag[e] && v.setRequestHeader("If-None-Match", m.etag[e])), (k.data && k.hasContent && k.contentType !== !1 || b.contentType) && v.setRequestHeader("Content-Type", k.contentType), v.setRequestHeader("Accept", k.dataTypes[0] && k.accepts[k.dataTypes[0]] ? k.accepts[k.dataTypes[0]] + ("*" !== k.dataTypes[0] ? ", " + Jc + "; q=0.01" : "") : k.accepts["*"]); for (d in k.headers) v.setRequestHeader(d, k.headers[d]); if (k.beforeSend && (k.beforeSend.call(l, v, k) === !1 || 2 === t)) return v.abort(); u = "abort"; for (d in { success: 1, error: 1, complete: 1 }) v[d](k[d]); if (i = Mc(Ic, k, b, v)) { v.readyState = 1, h && n.trigger("ajaxSend", [v, k]), k.async && k.timeout > 0 && (g = setTimeout(function () { v.abort("timeout") }, k.timeout)); try { t = 1, i.send(r, x) } catch (w) { if (!(2 > t)) throw w; x(-1, w) } } else x(-1, "No Transport"); function x(a, b, c, d) { var j, r, s, u, w, x = b; 2 !== t && (t = 2, g && clearTimeout(g), i = void 0, f = d || "", v.readyState = a > 0 ? 4 : 0, j = a >= 200 && 300 > a || 304 === a, c && (u = Oc(k, v, c)), u = Pc(k, u, v, j), j ? (k.ifModified && (w = v.getResponseHeader("Last-Modified"), w && (m.lastModified[e] = w), w = v.getResponseHeader("etag"), w && (m.etag[e] = w)), 204 === a || "HEAD" === k.type ? x = "nocontent" : 304 === a ? x = "notmodified" : (x = u.state, r = u.data, s = u.error, j = !s)) : (s = x, (a || !x) && (x = "error", 0 > a && (a = 0))), v.status = a, v.statusText = (b || x) + "", j ? o.resolveWith(l, [r, x, v]) : o.rejectWith(l, [v, x, s]), v.statusCode(q), q = void 0, h && n.trigger(j ? "ajaxSuccess" : "ajaxError", [v, k, j ? r : s]), p.fireWith(l, [v, x]), h && (n.trigger("ajaxComplete", [v, k]), --m.active || m.event.trigger("ajaxStop"))) } return v }, getJSON: function (a, b, c) { return m.get(a, b, c, "json") }, getScript: function (a, b) { return m.get(a, void 0, b, "script") } }), m.each(["get", "post"], function (a, b) { m[b] = function (a, c, d, e) { return m.isFunction(c) && (e = e || d, d = c, c = void 0), m.ajax({ url: a, type: b, dataType: e, data: c, success: d }) } }), m.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (a, b) { m.fn[b] = function (a) { return this.on(b, a) } }), m._evalUrl = function (a) { return m.ajax({ url: a, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0 }) }, m.fn.extend({ wrapAll: function (a) { if (m.isFunction(a)) return this.each(function (b) { m(this).wrapAll(a.call(this, b)) }); if (this[0]) { var b = m(a, this[0].ownerDocument).eq(0).clone(!0); this[0].parentNode && b.insertBefore(this[0]), b.map(function () { var a = this; while (a.firstChild && 1 === a.firstChild.nodeType) a = a.firstChild; return a }).append(this) } return this }, wrapInner: function (a) { return this.each(m.isFunction(a) ? function (b) { m(this).wrapInner(a.call(this, b)) } : function () { var b = m(this), c = b.contents(); c.length ? c.wrapAll(a) : b.append(a) }) }, wrap: function (a) { var b = m.isFunction(a); return this.each(function (c) { m(this).wrapAll(b ? a.call(this, c) : a) }) }, unwrap: function () { return this.parent().each(function () { m.nodeName(this, "body") || m(this).replaceWith(this.childNodes) }).end() } }), m.expr.filters.hidden = function (a) { return a.offsetWidth <= 0 && a.offsetHeight <= 0 || !k.reliableHiddenOffsets() && "none" === (a.style && a.style.display || m.css(a, "display")) }, m.expr.filters.visible = function (a) { return !m.expr.filters.hidden(a) }; var Qc = /%20/g, Rc = /\[\]$/, Sc = /\r?\n/g, Tc = /^(?:submit|button|image|reset|file)$/i, Uc = /^(?:input|select|textarea|keygen)/i; function Vc(a, b, c, d) { var e; if (m.isArray(b)) m.each(b, function (b, e) { c || Rc.test(a) ? d(a, e) : Vc(a + "[" + ("object" == typeof e ? b : "") + "]", e, c, d) }); else if (c || "object" !== m.type(b)) d(a, b); else for (e in b) Vc(a + "[" + e + "]", b[e], c, d) } m.param = function (a, b) { var c, d = [], e = function (a, b) { b = m.isFunction(b) ? b() : null == b ? "" : b, d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b) }; if (void 0 === b && (b = m.ajaxSettings && m.ajaxSettings.traditional), m.isArray(a) || a.jquery && !m.isPlainObject(a)) m.each(a, function () { e(this.name, this.value) }); else for (c in a) Vc(c, a[c], b, e); return d.join("&").replace(Qc, "+") }, m.fn.extend({ serialize: function () { return m.param(this.serializeArray()) }, serializeArray: function () { return this.map(function () { var a = m.prop(this, "elements"); return a ? m.makeArray(a) : this }).filter(function () { var a = this.type; return this.name && !m(this).is(":disabled") && Uc.test(this.nodeName) && !Tc.test(a) && (this.checked || !W.test(a)) }).map(function (a, b) { var c = m(this).val(); return null == c ? null : m.isArray(c) ? m.map(c, function (a) { return { name: b.name, value: a.replace(Sc, "\r\n") } }) : { name: b.name, value: c.replace(Sc, "\r\n") } }).get() } }), m.ajaxSettings.xhr = void 0 !== a.ActiveXObject ? function () { return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && Zc() || $c() } : Zc; var Wc = 0, Xc = {}, Yc = m.ajaxSettings.xhr(); a.ActiveXObject && m(a).on("unload", function () { for (var a in Xc) Xc[a](void 0, !0) }), k.cors = !!Yc && "withCredentials" in Yc, Yc = k.ajax = !!Yc, Yc && m.ajaxTransport(function (a) { if (!a.crossDomain || k.cors) { var b; return { send: function (c, d) { var e, f = a.xhr(), g = ++Wc; if (f.open(a.type, a.url, a.async, a.username, a.password), a.xhrFields) for (e in a.xhrFields) f[e] = a.xhrFields[e]; a.mimeType && f.overrideMimeType && f.overrideMimeType(a.mimeType), a.crossDomain || c["X-Requested-With"] || (c["X-Requested-With"] = "XMLHttpRequest"); for (e in c) void 0 !== c[e] && f.setRequestHeader(e, c[e] + ""); f.send(a.hasContent && a.data || null), b = function (c, e) { var h, i, j; if (b && (e || 4 === f.readyState)) if (delete Xc[g], b = void 0, f.onreadystatechange = m.noop, e) 4 !== f.readyState && f.abort(); else { j = {}, h = f.status, "string" == typeof f.responseText && (j.text = f.responseText); try { i = f.statusText } catch (k) { i = "" } h || !a.isLocal || a.crossDomain ? 1223 === h && (h = 204) : h = j.text ? 200 : 404 } j && d(h, i, j, f.getAllResponseHeaders()) }, a.async ? 4 === f.readyState ? setTimeout(b) : f.onreadystatechange = Xc[g] = b : b() }, abort: function () { b && b(void 0, !0) } } } }); function Zc() { try { return new a.XMLHttpRequest } catch (b) { } } function $c() { try { return new a.ActiveXObject("Microsoft.XMLHTTP") } catch (b) { } } m.ajaxSetup({ accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" }, contents: { script: /(?:java|ecma)script/ }, converters: { "text script": function (a) { return m.globalEval(a), a } } }), m.ajaxPrefilter("script", function (a) { void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET", a.global = !1) }), m.ajaxTransport("script", function (a) { if (a.crossDomain) { var b, c = y.head || m("head")[0] || y.documentElement; return { send: function (d, e) { b = y.createElement("script"), b.async = !0, a.scriptCharset && (b.charset = a.scriptCharset), b.src = a.url, b.onload = b.onreadystatechange = function (a, c) { (c || !b.readyState || /loaded|complete/.test(b.readyState)) && (b.onload = b.onreadystatechange = null, b.parentNode && b.parentNode.removeChild(b), b = null, c || e(200, "success")) }, c.insertBefore(b, c.firstChild) }, abort: function () { b && b.onload(void 0, !0) } } } }); var _c = [], ad = /(=)\?(?=&|$)|\?\?/; m.ajaxSetup({ jsonp: "callback", jsonpCallback: function () { var a = _c.pop() || m.expando + "_" + vc++; return this[a] = !0, a } }), m.ajaxPrefilter("json jsonp", function (b, c, d) { var e, f, g, h = b.jsonp !== !1 && (ad.test(b.url) ? "url" : "string" == typeof b.data && !(b.contentType || "").indexOf("application/x-www-form-urlencoded") && ad.test(b.data) && "data"); return h || "jsonp" === b.dataTypes[0] ? (e = b.jsonpCallback = m.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(ad, "$1" + e) : b.jsonp !== !1 && (b.url += (wc.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function () { return g || m.error(e + " was not called"), g[0] }, b.dataTypes[0] = "json", f = a[e], a[e] = function () { g = arguments }, d.always(function () { a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, _c.push(e)), g && m.isFunction(f) && f(g[0]), g = f = void 0 }), "script") : void 0 }), m.parseHTML = function (a, b, c) { if (!a || "string" != typeof a) return null; "boolean" == typeof b && (c = b, b = !1), b = b || y; var d = u.exec(a), e = !c && []; return d ? [b.createElement(d[1])] : (d = m.buildFragment([a], b, e), e && e.length && m(e).remove(), m.merge([], d.childNodes)) }; var bd = m.fn.load; m.fn.load = function (a, b, c) { if ("string" != typeof a && bd) return bd.apply(this, arguments); var d, e, f, g = this, h = a.indexOf(" "); return h >= 0 && (d = m.trim(a.slice(h, a.length)), a = a.slice(0, h)), m.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (f = "POST"), g.length > 0 && m.ajax({ url: a, type: f, dataType: "html", data: b }).done(function (a) { e = arguments, g.html(d ? m("<div>").append(m.parseHTML(a)).find(d) : a) }).complete(c && function (a, b) { g.each(c, e || [a.responseText, b, a]) }), this }, m.expr.filters.animated = function (a) { return m.grep(m.timers, function (b) { return a === b.elem }).length }; var cd = a.document.documentElement; function dd(a) { return m.isWindow(a) ? a : 9 === a.nodeType ? a.defaultView || a.parentWindow : !1 } m.offset = { setOffset: function (a, b, c) { var d, e, f, g, h, i, j, k = m.css(a, "position"), l = m(a), n = {}; "static" === k && (a.style.position = "relative"), h = l.offset(), f = m.css(a, "top"), i = m.css(a, "left"), j = ("absolute" === k || "fixed" === k) && m.inArray("auto", [f, i]) > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), m.isFunction(b) && (b = b.call(a, c, h)), null != b.top && (n.top = b.top - h.top + g), null != b.left && (n.left = b.left - h.left + e), "using" in b ? b.using.call(a, n) : l.css(n) } }, m.fn.extend({ offset: function (a) { if (arguments.length) return void 0 === a ? this : this.each(function (b) { m.offset.setOffset(this, a, b) }); var b, c, d = { top: 0, left: 0 }, e = this[0], f = e && e.ownerDocument; if (f) return b = f.documentElement, m.contains(b, e) ? (typeof e.getBoundingClientRect !== K && (d = e.getBoundingClientRect()), c = dd(f), { top: d.top + (c.pageYOffset || b.scrollTop) - (b.clientTop || 0), left: d.left + (c.pageXOffset || b.scrollLeft) - (b.clientLeft || 0) }) : d }, position: function () { if (this[0]) { var a, b, c = { top: 0, left: 0 }, d = this[0]; return "fixed" === m.css(d, "position") ? b = d.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), m.nodeName(a[0], "html") || (c = a.offset()), c.top += m.css(a[0], "borderTopWidth", !0), c.left += m.css(a[0], "borderLeftWidth", !0)), { top: b.top - c.top - m.css(d, "marginTop", !0), left: b.left - c.left - m.css(d, "marginLeft", !0) } } }, offsetParent: function () { return this.map(function () { var a = this.offsetParent || cd; while (a && !m.nodeName(a, "html") && "static" === m.css(a, "position")) a = a.offsetParent; return a || cd }) } }), m.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (a, b) { var c = /Y/.test(b); m.fn[a] = function (d) { return V(this, function (a, d, e) { var f = dd(a); return void 0 === e ? f ? b in f ? f[b] : f.document.documentElement[d] : a[d] : void (f ? f.scrollTo(c ? m(f).scrollLeft() : e, c ? e : m(f).scrollTop()) : a[d] = e) }, a, d, arguments.length, null) } }), m.each(["top", "left"], function (a, b) { m.cssHooks[b] = Lb(k.pixelPosition, function (a, c) { return c ? (c = Jb(a, b), Hb.test(c) ? m(a).position()[b] + "px" : c) : void 0 }) }), m.each({ Height: "height", Width: "width" }, function (a, b) { m.each({ padding: "inner" + a, content: b, "": "outer" + a }, function (c, d) { m.fn[d] = function (d, e) { var f = arguments.length && (c || "boolean" != typeof d), g = c || (d === !0 || e === !0 ? "margin" : "border"); return V(this, function (b, c, d) { var e; return m.isWindow(b) ? b.document.documentElement["client" + a] : 9 === b.nodeType ? (e = b.documentElement, Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a])) : void 0 === d ? m.css(b, c, g) : m.style(b, c, d, g) }, b, f ? d : void 0, f, null) } }) }), m.fn.size = function () { return this.length }, m.fn.andSelf = m.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () { return m }); var ed = a.jQuery, fd = a.$; return m.noConflict = function (b) { return a.$ === m && (a.$ = fd), b && a.jQuery === m && (a.jQuery = ed), m }, typeof b === K && (a.jQuery = a.$ = m), m
});


(function (global, factory) { if (typeof exports === "object" && exports) { factory(exports) } else if (typeof define === "function" && define.amd) { define(["exports"], factory) } else { factory(global.Mustache = {}) } })(this, function (mustache) { var Object_toString = Object.prototype.toString; var isArray = Array.isArray || function (object) { return Object_toString.call(object) === "[object Array]" }; function isFunction(object) { return typeof object === "function" } function escapeRegExp(string) { return string.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&") } var RegExp_test = RegExp.prototype.test; function testRegExp(re, string) { return RegExp_test.call(re, string) } var nonSpaceRe = /\S/; function isWhitespace(string) { return !testRegExp(nonSpaceRe, string) } var entityMap = { "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;", "/": "&#x2F;" }; function escapeHtml(string) { return String(string).replace(/[&<>"'\/]/g, function (s) { return entityMap[s] }) } var whiteRe = /\s*/; var spaceRe = /\s+/; var equalsRe = /\s*=/; var curlyRe = /\s*\}/; var tagRe = /#|\^|\/|>|\{|&|=|!/; function parseTemplate(template, tags) { if (!template) return []; var sections = []; var tokens = []; var spaces = []; var hasTag = false; var nonSpace = false; function stripSpace() { if (hasTag && !nonSpace) { while (spaces.length) delete tokens[spaces.pop()] } else { spaces = [] } hasTag = false; nonSpace = false } var openingTagRe, closingTagRe, closingCurlyRe; function compileTags(tags) { if (typeof tags === "string") tags = tags.split(spaceRe, 2); if (!isArray(tags) || tags.length !== 2) throw new Error("Invalid tags: " + tags); openingTagRe = new RegExp(escapeRegExp(tags[0]) + "\\s*"); closingTagRe = new RegExp("\\s*" + escapeRegExp(tags[1])); closingCurlyRe = new RegExp("\\s*" + escapeRegExp("}" + tags[1])) } compileTags(tags || mustache.tags); var scanner = new Scanner(template); var start, type, value, chr, token, openSection; while (!scanner.eos()) { start = scanner.pos; value = scanner.scanUntil(openingTagRe); if (value) { for (var i = 0, valueLength = value.length; i < valueLength; ++i) { chr = value.charAt(i); if (isWhitespace(chr)) { spaces.push(tokens.length) } else { nonSpace = true } tokens.push(["text", chr, start, start + 1]); start += 1; if (chr === "\n") stripSpace() } } if (!scanner.scan(openingTagRe)) break; hasTag = true; type = scanner.scan(tagRe) || "name"; scanner.scan(whiteRe); if (type === "=") { value = scanner.scanUntil(equalsRe); scanner.scan(equalsRe); scanner.scanUntil(closingTagRe) } else if (type === "{") { value = scanner.scanUntil(closingCurlyRe); scanner.scan(curlyRe); scanner.scanUntil(closingTagRe); type = "&" } else { value = scanner.scanUntil(closingTagRe) } if (!scanner.scan(closingTagRe)) throw new Error("Unclosed tag at " + scanner.pos); token = [type, value, start, scanner.pos]; tokens.push(token); if (type === "#" || type === "^") { sections.push(token) } else if (type === "/") { openSection = sections.pop(); if (!openSection) throw new Error('Unopened section "' + value + '" at ' + start); if (openSection[1] !== value) throw new Error('Unclosed section "' + openSection[1] + '" at ' + start) } else if (type === "name" || type === "{" || type === "&") { nonSpace = true } else if (type === "=") { compileTags(value) } } openSection = sections.pop(); if (openSection) throw new Error('Unclosed section "' + openSection[1] + '" at ' + scanner.pos); return nestTokens(squashTokens(tokens)) } function squashTokens(tokens) { var squashedTokens = []; var token, lastToken; for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) { token = tokens[i]; if (token) { if (token[0] === "text" && lastToken && lastToken[0] === "text") { lastToken[1] += token[1]; lastToken[3] = token[3] } else { squashedTokens.push(token); lastToken = token } } } return squashedTokens } function nestTokens(tokens) { var nestedTokens = []; var collector = nestedTokens; var sections = []; var token, section; for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) { token = tokens[i]; switch (token[0]) { case "#": case "^": collector.push(token); sections.push(token); collector = token[4] = []; break; case "/": section = sections.pop(); section[5] = token[2]; collector = sections.length > 0 ? sections[sections.length - 1][4] : nestedTokens; break; default: collector.push(token) } } return nestedTokens } function Scanner(string) { this.string = string; this.tail = string; this.pos = 0 } Scanner.prototype.eos = function () { return this.tail === "" }; Scanner.prototype.scan = function (re) { var match = this.tail.match(re); if (!match || match.index !== 0) return ""; var string = match[0]; this.tail = this.tail.substring(string.length); this.pos += string.length; return string }; Scanner.prototype.scanUntil = function (re) { var index = this.tail.search(re), match; switch (index) { case -1: match = this.tail; this.tail = ""; break; case 0: match = ""; break; default: match = this.tail.substring(0, index); this.tail = this.tail.substring(index) } this.pos += match.length; return match }; function Context(view, parentContext) { this.view = view; this.cache = { ".": this.view }; this.parent = parentContext } Context.prototype.push = function (view) { return new Context(view, this) }; Context.prototype.lookup = function (name) { var cache = this.cache; var value; if (name in cache) { value = cache[name] } else { var context = this, names, index, lookupHit = false; while (context) { if (name.indexOf(".") > 0) { value = context.view; names = name.split("."); index = 0; while (value != null && index < names.length) { if (index === names.length - 1 && value != null) lookupHit = typeof value === "object" && value.hasOwnProperty(names[index]); value = value[names[index++]] } } else if (context.view != null && typeof context.view === "object") { value = context.view[name]; lookupHit = context.view.hasOwnProperty(name) } if (lookupHit) break; context = context.parent } cache[name] = value } if (isFunction(value)) value = value.call(this.view); return value }; function Writer() { this.cache = {} } Writer.prototype.clearCache = function () { this.cache = {} }; Writer.prototype.parse = function (template, tags) { var cache = this.cache; var tokens = cache[template]; if (tokens == null) tokens = cache[template] = parseTemplate(template, tags); return tokens }; Writer.prototype.render = function (template, view, partials) { var tokens = this.parse(template); var context = view instanceof Context ? view : new Context(view); return this.renderTokens(tokens, context, partials, template) }; Writer.prototype.renderTokens = function (tokens, context, partials, originalTemplate) { var buffer = ""; var token, symbol, value; for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) { value = undefined; token = tokens[i]; symbol = token[0]; if (symbol === "#") value = this._renderSection(token, context, partials, originalTemplate); else if (symbol === "^") value = this._renderInverted(token, context, partials, originalTemplate); else if (symbol === ">") value = this._renderPartial(token, context, partials, originalTemplate); else if (symbol === "&") value = this._unescapedValue(token, context); else if (symbol === "name") value = this._escapedValue(token, context); else if (symbol === "text") value = this._rawValue(token); if (value !== undefined) buffer += value } return buffer }; Writer.prototype._renderSection = function (token, context, partials, originalTemplate) { var self = this; var buffer = ""; var value = context.lookup(token[1]); function subRender(template) { return self.render(template, context, partials) } if (!value) return; if (isArray(value)) { for (var j = 0, valueLength = value.length; j < valueLength; ++j) { buffer += this.renderTokens(token[4], context.push(value[j]), partials, originalTemplate) } } else if (typeof value === "object" || typeof value === "string" || typeof value === "number") { buffer += this.renderTokens(token[4], context.push(value), partials, originalTemplate) } else if (isFunction(value)) { if (typeof originalTemplate !== "string") throw new Error("Cannot use higher-order sections without the original template"); value = value.call(context.view, originalTemplate.slice(token[3], token[5]), subRender); if (value != null) buffer += value } else { buffer += this.renderTokens(token[4], context, partials, originalTemplate) } return buffer }; Writer.prototype._renderInverted = function (token, context, partials, originalTemplate) { var value = context.lookup(token[1]); if (!value || isArray(value) && value.length === 0) return this.renderTokens(token[4], context, partials, originalTemplate) }; Writer.prototype._renderPartial = function (token, context, partials) { if (!partials) return; var value = isFunction(partials) ? partials(token[1]) : partials[token[1]]; if (value != null) return this.renderTokens(this.parse(value), context, partials, value) }; Writer.prototype._unescapedValue = function (token, context) { var value = context.lookup(token[1]); if (value != null) return value }; Writer.prototype._escapedValue = function (token, context) { var value = context.lookup(token[1]); if (value != null) return mustache.escape(value) }; Writer.prototype._rawValue = function (token) { return token[1] }; mustache.name = "mustache.js"; mustache.version = "2.0.0"; mustache.tags = ["{{", "}}"]; var defaultWriter = new Writer; mustache.clearCache = function () { return defaultWriter.clearCache() }; mustache.parse = function (template, tags) { return defaultWriter.parse(template, tags) }; mustache.render = function (template, view, partials) { return defaultWriter.render(template, view, partials) }; mustache.to_html = function (template, view, partials, send) { var result = mustache.render(template, view, partials); if (isFunction(send)) { send(result) } else { return result } }; mustache.escape = escapeHtml; mustache.Scanner = Scanner; mustache.Context = Context; mustache.Writer = Writer });

/*
 Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net
*/
(function () {
  var c, f; c = this.jQuery || window.jQuery; f = c(window); c.fn.stick_in_parent = function (b) {
    var A, w, J, n, B, K, p, q, L, k, E, t; null == b && (b = {}); t = b.sticky_class; B = b.inner_scrolling; E = b.recalc_every; k = b.parent; q = b.offset_top; p = b.spacer; w = b.bottoming; null == q && (q = 0); null == k && (k = void 0); null == B && (B = !0); null == t && (t = "is_stuck"); A = c(document); null == w && (w = !0); L = function (a) {
      var b; return window.getComputedStyle ? (a = window.getComputedStyle(a[0]), b = parseFloat(a.getPropertyValue("width")) + parseFloat(a.getPropertyValue("margin-left")) +
      parseFloat(a.getPropertyValue("margin-right")), "border-box" !== a.getPropertyValue("box-sizing") && (b += parseFloat(a.getPropertyValue("border-left-width")) + parseFloat(a.getPropertyValue("border-right-width")) + parseFloat(a.getPropertyValue("padding-left")) + parseFloat(a.getPropertyValue("padding-right"))), b) : a.outerWidth(!0)
    }; J = function (a, b, n, C, F, u, r, G) {
      var v, H, m, D, I, d, g, x, y, z, h, l; if (!a.data("sticky_kit")) {
        a.data("sticky_kit", !0); I = A.height(); g = a.parent(); null != k && (g = g.closest(k)); if (!g.length) throw "failed to find stick parent";
        v = m = !1; (h = null != p ? p && a.closest(p) : c("<div />")) && h.css("position", a.css("position")); x = function () {
          var d, f, e; if (!G && (I = A.height(), d = parseInt(g.css("border-top-width"), 10), f = parseInt(g.css("padding-top"), 10), b = parseInt(g.css("padding-bottom"), 10), n = g.offset().top + d + f, C = g.height(), m && (v = m = !1, null == p && (a.insertAfter(h), h.detach()), a.css({ position: "", top: "", width: "", bottom: "" }).removeClass(t), e = !0), F = a.offset().top - (parseInt(a.css("margin-top"), 10) || 0) - q, u = a.outerHeight(!0), r = a.css("float"), h && h.css({
            width: L(a),
            height: u, display: a.css("display"), "vertical-align": a.css("vertical-align"), "float": r
          }), e)) return l()
        }; x(); if (u !== C) return D = void 0, d = q, z = E, l = function () {
          var c, l, e, k; if (!G && (e = !1, null != z && (--z, 0 >= z && (z = E, x(), e = !0)), e || A.height() === I || x(), e = f.scrollTop(), null != D && (l = e - D), D = e, m ? (w && (k = e + u + d > C + n, v && !k && (v = !1, a.css({ position: "fixed", bottom: "", top: d }).trigger("sticky_kit:unbottom"))), e < F && (m = !1, d = q, null == p && ("left" !== r && "right" !== r || a.insertAfter(h), h.detach()), c = { position: "", width: "", top: "" }, a.css(c).removeClass(t).trigger("sticky_kit:unstick")),
          B && (c = f.height(), u + q > c && !v && (d -= l, d = Math.max(c - u, d), d = Math.min(q, d), m && a.css({ top: d + "px" })))) : e > F && (m = !0, c = { position: "fixed", top: d }, c.width = "border-box" === a.css("box-sizing") ? a.outerWidth() + "px" : a.width() + "px", a.css(c).addClass(t), null == p && (a.after(h), "left" !== r && "right" !== r || h.append(a)), a.trigger("sticky_kit:stick")), m && w && (null == k && (k = e + u + d > C + n), !v && k))) return v = !0, "static" === g.css("position") && g.css({ position: "relative" }), a.css({ position: "absolute", bottom: b, top: "auto" }).trigger("sticky_kit:bottom")
        },
        y = function () { x(); return l() }, H = function () { G = !0; f.off("touchmove", l); f.off("scroll", l); f.off("resize", y); c(document.body).off("sticky_kit:recalc", y); a.off("sticky_kit:detach", H); a.removeData("sticky_kit"); a.css({ position: "", bottom: "", top: "", width: "" }); g.position("position", ""); if (m) return null == p && ("left" !== r && "right" !== r || a.insertAfter(h), h.remove()), a.removeClass(t) }, f.on("touchmove", l), f.on("scroll", l), f.on("resize", y), c(document.body).on("sticky_kit:recalc", y), a.on("sticky_kit:detach", H), setTimeout(l,
        0)
      }
    }; n = 0; for (K = this.length; n < K; n++) b = this[n], J(c(b)); return this
  }
}).call(this);
/*
Copyright 2012 Igor Vaynberg

Version: 3.5.1 Timestamp: Tue Jul 22 18:58:56 EDT 2014

This software is licensed under the Apache License, Version 2.0 (the "Apache License") or the GNU
General Public License version 2 (the "GPL License"). You may choose either license to govern your
use of this software only upon the condition that you accept all of the terms of either the Apache
License or the GPL License.

You may obtain a copy of the Apache License and the GPL License at:

    http://www.apache.org/licenses/LICENSE-2.0
    http://www.gnu.org/licenses/gpl-2.0.html

Unless required by applicable law or agreed to in writing, software distributed under the
Apache License or the GPL License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the Apache License and the GPL License for
the specific language governing permissions and limitations under the Apache License and the GPL License.
*/
(function ($) {
  if (typeof $.fn.each2 == "undefined") {
    $.extend($.fn, {
      /*
      * 4-10 times faster .each replacement
      * use it carefully, as it overrides jQuery context of element on each iteration
      */
      each2: function (c) {
        var j = $([0]), i = -1, l = this.length;
        while (
            ++i < l
            && (j.context = j[0] = this[i])
            && c.call(j[0], i, j) !== false //"this"=DOM, i=index, j=jQuery object
        );
        return this;
      }
    });
  }
})(jQuery);

(function ($, undefined) {
  "use strict";
  /*global document, window, jQuery, console */

  if (window.Select2 !== undefined) {
    return;
  }

  var KEY, AbstractSelect2, SingleSelect2, MultiSelect2, nextUid, sizer,
      lastMousePosition = { x: 0, y: 0 }, $document, scrollBarDimensions,

  KEY = {
    TAB: 9,
    ENTER: 13,
    ESC: 27,
    SPACE: 32,
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    SHIFT: 16,
    CTRL: 17,
    ALT: 18,
    PAGE_UP: 33,
    PAGE_DOWN: 34,
    HOME: 36,
    END: 35,
    BACKSPACE: 8,
    DELETE: 46,
    isArrow: function (k) {
      k = k.which ? k.which : k;
      switch (k) {
        case KEY.LEFT:
        case KEY.RIGHT:
        case KEY.UP:
        case KEY.DOWN:
          return true;
      }
      return false;
    },
    isControl: function (e) {
      var k = e.which;
      switch (k) {
        case KEY.SHIFT:
        case KEY.CTRL:
        case KEY.ALT:
          return true;
      }

      if (e.metaKey) return true;

      return false;
    },
    isFunctionKey: function (k) {
      k = k.which ? k.which : k;
      return k >= 112 && k <= 123;
    }
  },
  MEASURE_SCROLLBAR_TEMPLATE = "<div class='select2-measure-scrollbar'></div>",

  DIACRITICS = { "\u24B6": "A", "\uFF21": "A", "\u00C0": "A", "\u00C1": "A", "\u00C2": "A", "\u1EA6": "A", "\u1EA4": "A", "\u1EAA": "A", "\u1EA8": "A", "\u00C3": "A", "\u0100": "A", "\u0102": "A", "\u1EB0": "A", "\u1EAE": "A", "\u1EB4": "A", "\u1EB2": "A", "\u0226": "A", "\u01E0": "A", "\u00C4": "A", "\u01DE": "A", "\u1EA2": "A", "\u00C5": "A", "\u01FA": "A", "\u01CD": "A", "\u0200": "A", "\u0202": "A", "\u1EA0": "A", "\u1EAC": "A", "\u1EB6": "A", "\u1E00": "A", "\u0104": "A", "\u023A": "A", "\u2C6F": "A", "\uA732": "AA", "\u00C6": "AE", "\u01FC": "AE", "\u01E2": "AE", "\uA734": "AO", "\uA736": "AU", "\uA738": "AV", "\uA73A": "AV", "\uA73C": "AY", "\u24B7": "B", "\uFF22": "B", "\u1E02": "B", "\u1E04": "B", "\u1E06": "B", "\u0243": "B", "\u0182": "B", "\u0181": "B", "\u24B8": "C", "\uFF23": "C", "\u0106": "C", "\u0108": "C", "\u010A": "C", "\u010C": "C", "\u00C7": "C", "\u1E08": "C", "\u0187": "C", "\u023B": "C", "\uA73E": "C", "\u24B9": "D", "\uFF24": "D", "\u1E0A": "D", "\u010E": "D", "\u1E0C": "D", "\u1E10": "D", "\u1E12": "D", "\u1E0E": "D", "\u0110": "D", "\u018B": "D", "\u018A": "D", "\u0189": "D", "\uA779": "D", "\u01F1": "DZ", "\u01C4": "DZ", "\u01F2": "Dz", "\u01C5": "Dz", "\u24BA": "E", "\uFF25": "E", "\u00C8": "E", "\u00C9": "E", "\u00CA": "E", "\u1EC0": "E", "\u1EBE": "E", "\u1EC4": "E", "\u1EC2": "E", "\u1EBC": "E", "\u0112": "E", "\u1E14": "E", "\u1E16": "E", "\u0114": "E", "\u0116": "E", "\u00CB": "E", "\u1EBA": "E", "\u011A": "E", "\u0204": "E", "\u0206": "E", "\u1EB8": "E", "\u1EC6": "E", "\u0228": "E", "\u1E1C": "E", "\u0118": "E", "\u1E18": "E", "\u1E1A": "E", "\u0190": "E", "\u018E": "E", "\u24BB": "F", "\uFF26": "F", "\u1E1E": "F", "\u0191": "F", "\uA77B": "F", "\u24BC": "G", "\uFF27": "G", "\u01F4": "G", "\u011C": "G", "\u1E20": "G", "\u011E": "G", "\u0120": "G", "\u01E6": "G", "\u0122": "G", "\u01E4": "G", "\u0193": "G", "\uA7A0": "G", "\uA77D": "G", "\uA77E": "G", "\u24BD": "H", "\uFF28": "H", "\u0124": "H", "\u1E22": "H", "\u1E26": "H", "\u021E": "H", "\u1E24": "H", "\u1E28": "H", "\u1E2A": "H", "\u0126": "H", "\u2C67": "H", "\u2C75": "H", "\uA78D": "H", "\u24BE": "I", "\uFF29": "I", "\u00CC": "I", "\u00CD": "I", "\u00CE": "I", "\u0128": "I", "\u012A": "I", "\u012C": "I", "\u0130": "I", "\u00CF": "I", "\u1E2E": "I", "\u1EC8": "I", "\u01CF": "I", "\u0208": "I", "\u020A": "I", "\u1ECA": "I", "\u012E": "I", "\u1E2C": "I", "\u0197": "I", "\u24BF": "J", "\uFF2A": "J", "\u0134": "J", "\u0248": "J", "\u24C0": "K", "\uFF2B": "K", "\u1E30": "K", "\u01E8": "K", "\u1E32": "K", "\u0136": "K", "\u1E34": "K", "\u0198": "K", "\u2C69": "K", "\uA740": "K", "\uA742": "K", "\uA744": "K", "\uA7A2": "K", "\u24C1": "L", "\uFF2C": "L", "\u013F": "L", "\u0139": "L", "\u013D": "L", "\u1E36": "L", "\u1E38": "L", "\u013B": "L", "\u1E3C": "L", "\u1E3A": "L", "\u0141": "L", "\u023D": "L", "\u2C62": "L", "\u2C60": "L", "\uA748": "L", "\uA746": "L", "\uA780": "L", "\u01C7": "LJ", "\u01C8": "Lj", "\u24C2": "M", "\uFF2D": "M", "\u1E3E": "M", "\u1E40": "M", "\u1E42": "M", "\u2C6E": "M", "\u019C": "M", "\u24C3": "N", "\uFF2E": "N", "\u01F8": "N", "\u0143": "N", "\u00D1": "N", "\u1E44": "N", "\u0147": "N", "\u1E46": "N", "\u0145": "N", "\u1E4A": "N", "\u1E48": "N", "\u0220": "N", "\u019D": "N", "\uA790": "N", "\uA7A4": "N", "\u01CA": "NJ", "\u01CB": "Nj", "\u24C4": "O", "\uFF2F": "O", "\u00D2": "O", "\u00D3": "O", "\u00D4": "O", "\u1ED2": "O", "\u1ED0": "O", "\u1ED6": "O", "\u1ED4": "O", "\u00D5": "O", "\u1E4C": "O", "\u022C": "O", "\u1E4E": "O", "\u014C": "O", "\u1E50": "O", "\u1E52": "O", "\u014E": "O", "\u022E": "O", "\u0230": "O", "\u00D6": "O", "\u022A": "O", "\u1ECE": "O", "\u0150": "O", "\u01D1": "O", "\u020C": "O", "\u020E": "O", "\u01A0": "O", "\u1EDC": "O", "\u1EDA": "O", "\u1EE0": "O", "\u1EDE": "O", "\u1EE2": "O", "\u1ECC": "O", "\u1ED8": "O", "\u01EA": "O", "\u01EC": "O", "\u00D8": "O", "\u01FE": "O", "\u0186": "O", "\u019F": "O", "\uA74A": "O", "\uA74C": "O", "\u01A2": "OI", "\uA74E": "OO", "\u0222": "OU", "\u24C5": "P", "\uFF30": "P", "\u1E54": "P", "\u1E56": "P", "\u01A4": "P", "\u2C63": "P", "\uA750": "P", "\uA752": "P", "\uA754": "P", "\u24C6": "Q", "\uFF31": "Q", "\uA756": "Q", "\uA758": "Q", "\u024A": "Q", "\u24C7": "R", "\uFF32": "R", "\u0154": "R", "\u1E58": "R", "\u0158": "R", "\u0210": "R", "\u0212": "R", "\u1E5A": "R", "\u1E5C": "R", "\u0156": "R", "\u1E5E": "R", "\u024C": "R", "\u2C64": "R", "\uA75A": "R", "\uA7A6": "R", "\uA782": "R", "\u24C8": "S", "\uFF33": "S", "\u1E9E": "S", "\u015A": "S", "\u1E64": "S", "\u015C": "S", "\u1E60": "S", "\u0160": "S", "\u1E66": "S", "\u1E62": "S", "\u1E68": "S", "\u0218": "S", "\u015E": "S", "\u2C7E": "S", "\uA7A8": "S", "\uA784": "S", "\u24C9": "T", "\uFF34": "T", "\u1E6A": "T", "\u0164": "T", "\u1E6C": "T", "\u021A": "T", "\u0162": "T", "\u1E70": "T", "\u1E6E": "T", "\u0166": "T", "\u01AC": "T", "\u01AE": "T", "\u023E": "T", "\uA786": "T", "\uA728": "TZ", "\u24CA": "U", "\uFF35": "U", "\u00D9": "U", "\u00DA": "U", "\u00DB": "U", "\u0168": "U", "\u1E78": "U", "\u016A": "U", "\u1E7A": "U", "\u016C": "U", "\u00DC": "U", "\u01DB": "U", "\u01D7": "U", "\u01D5": "U", "\u01D9": "U", "\u1EE6": "U", "\u016E": "U", "\u0170": "U", "\u01D3": "U", "\u0214": "U", "\u0216": "U", "\u01AF": "U", "\u1EEA": "U", "\u1EE8": "U", "\u1EEE": "U", "\u1EEC": "U", "\u1EF0": "U", "\u1EE4": "U", "\u1E72": "U", "\u0172": "U", "\u1E76": "U", "\u1E74": "U", "\u0244": "U", "\u24CB": "V", "\uFF36": "V", "\u1E7C": "V", "\u1E7E": "V", "\u01B2": "V", "\uA75E": "V", "\u0245": "V", "\uA760": "VY", "\u24CC": "W", "\uFF37": "W", "\u1E80": "W", "\u1E82": "W", "\u0174": "W", "\u1E86": "W", "\u1E84": "W", "\u1E88": "W", "\u2C72": "W", "\u24CD": "X", "\uFF38": "X", "\u1E8A": "X", "\u1E8C": "X", "\u24CE": "Y", "\uFF39": "Y", "\u1EF2": "Y", "\u00DD": "Y", "\u0176": "Y", "\u1EF8": "Y", "\u0232": "Y", "\u1E8E": "Y", "\u0178": "Y", "\u1EF6": "Y", "\u1EF4": "Y", "\u01B3": "Y", "\u024E": "Y", "\u1EFE": "Y", "\u24CF": "Z", "\uFF3A": "Z", "\u0179": "Z", "\u1E90": "Z", "\u017B": "Z", "\u017D": "Z", "\u1E92": "Z", "\u1E94": "Z", "\u01B5": "Z", "\u0224": "Z", "\u2C7F": "Z", "\u2C6B": "Z", "\uA762": "Z", "\u24D0": "a", "\uFF41": "a", "\u1E9A": "a", "\u00E0": "a", "\u00E1": "a", "\u00E2": "a", "\u1EA7": "a", "\u1EA5": "a", "\u1EAB": "a", "\u1EA9": "a", "\u00E3": "a", "\u0101": "a", "\u0103": "a", "\u1EB1": "a", "\u1EAF": "a", "\u1EB5": "a", "\u1EB3": "a", "\u0227": "a", "\u01E1": "a", "\u00E4": "a", "\u01DF": "a", "\u1EA3": "a", "\u00E5": "a", "\u01FB": "a", "\u01CE": "a", "\u0201": "a", "\u0203": "a", "\u1EA1": "a", "\u1EAD": "a", "\u1EB7": "a", "\u1E01": "a", "\u0105": "a", "\u2C65": "a", "\u0250": "a", "\uA733": "aa", "\u00E6": "ae", "\u01FD": "ae", "\u01E3": "ae", "\uA735": "ao", "\uA737": "au", "\uA739": "av", "\uA73B": "av", "\uA73D": "ay", "\u24D1": "b", "\uFF42": "b", "\u1E03": "b", "\u1E05": "b", "\u1E07": "b", "\u0180": "b", "\u0183": "b", "\u0253": "b", "\u24D2": "c", "\uFF43": "c", "\u0107": "c", "\u0109": "c", "\u010B": "c", "\u010D": "c", "\u00E7": "c", "\u1E09": "c", "\u0188": "c", "\u023C": "c", "\uA73F": "c", "\u2184": "c", "\u24D3": "d", "\uFF44": "d", "\u1E0B": "d", "\u010F": "d", "\u1E0D": "d", "\u1E11": "d", "\u1E13": "d", "\u1E0F": "d", "\u0111": "d", "\u018C": "d", "\u0256": "d", "\u0257": "d", "\uA77A": "d", "\u01F3": "dz", "\u01C6": "dz", "\u24D4": "e", "\uFF45": "e", "\u00E8": "e", "\u00E9": "e", "\u00EA": "e", "\u1EC1": "e", "\u1EBF": "e", "\u1EC5": "e", "\u1EC3": "e", "\u1EBD": "e", "\u0113": "e", "\u1E15": "e", "\u1E17": "e", "\u0115": "e", "\u0117": "e", "\u00EB": "e", "\u1EBB": "e", "\u011B": "e", "\u0205": "e", "\u0207": "e", "\u1EB9": "e", "\u1EC7": "e", "\u0229": "e", "\u1E1D": "e", "\u0119": "e", "\u1E19": "e", "\u1E1B": "e", "\u0247": "e", "\u025B": "e", "\u01DD": "e", "\u24D5": "f", "\uFF46": "f", "\u1E1F": "f", "\u0192": "f", "\uA77C": "f", "\u24D6": "g", "\uFF47": "g", "\u01F5": "g", "\u011D": "g", "\u1E21": "g", "\u011F": "g", "\u0121": "g", "\u01E7": "g", "\u0123": "g", "\u01E5": "g", "\u0260": "g", "\uA7A1": "g", "\u1D79": "g", "\uA77F": "g", "\u24D7": "h", "\uFF48": "h", "\u0125": "h", "\u1E23": "h", "\u1E27": "h", "\u021F": "h", "\u1E25": "h", "\u1E29": "h", "\u1E2B": "h", "\u1E96": "h", "\u0127": "h", "\u2C68": "h", "\u2C76": "h", "\u0265": "h", "\u0195": "hv", "\u24D8": "i", "\uFF49": "i", "\u00EC": "i", "\u00ED": "i", "\u00EE": "i", "\u0129": "i", "\u012B": "i", "\u012D": "i", "\u00EF": "i", "\u1E2F": "i", "\u1EC9": "i", "\u01D0": "i", "\u0209": "i", "\u020B": "i", "\u1ECB": "i", "\u012F": "i", "\u1E2D": "i", "\u0268": "i", "\u0131": "i", "\u24D9": "j", "\uFF4A": "j", "\u0135": "j", "\u01F0": "j", "\u0249": "j", "\u24DA": "k", "\uFF4B": "k", "\u1E31": "k", "\u01E9": "k", "\u1E33": "k", "\u0137": "k", "\u1E35": "k", "\u0199": "k", "\u2C6A": "k", "\uA741": "k", "\uA743": "k", "\uA745": "k", "\uA7A3": "k", "\u24DB": "l", "\uFF4C": "l", "\u0140": "l", "\u013A": "l", "\u013E": "l", "\u1E37": "l", "\u1E39": "l", "\u013C": "l", "\u1E3D": "l", "\u1E3B": "l", "\u017F": "l", "\u0142": "l", "\u019A": "l", "\u026B": "l", "\u2C61": "l", "\uA749": "l", "\uA781": "l", "\uA747": "l", "\u01C9": "lj", "\u24DC": "m", "\uFF4D": "m", "\u1E3F": "m", "\u1E41": "m", "\u1E43": "m", "\u0271": "m", "\u026F": "m", "\u24DD": "n", "\uFF4E": "n", "\u01F9": "n", "\u0144": "n", "\u00F1": "n", "\u1E45": "n", "\u0148": "n", "\u1E47": "n", "\u0146": "n", "\u1E4B": "n", "\u1E49": "n", "\u019E": "n", "\u0272": "n", "\u0149": "n", "\uA791": "n", "\uA7A5": "n", "\u01CC": "nj", "\u24DE": "o", "\uFF4F": "o", "\u00F2": "o", "\u00F3": "o", "\u00F4": "o", "\u1ED3": "o", "\u1ED1": "o", "\u1ED7": "o", "\u1ED5": "o", "\u00F5": "o", "\u1E4D": "o", "\u022D": "o", "\u1E4F": "o", "\u014D": "o", "\u1E51": "o", "\u1E53": "o", "\u014F": "o", "\u022F": "o", "\u0231": "o", "\u00F6": "o", "\u022B": "o", "\u1ECF": "o", "\u0151": "o", "\u01D2": "o", "\u020D": "o", "\u020F": "o", "\u01A1": "o", "\u1EDD": "o", "\u1EDB": "o", "\u1EE1": "o", "\u1EDF": "o", "\u1EE3": "o", "\u1ECD": "o", "\u1ED9": "o", "\u01EB": "o", "\u01ED": "o", "\u00F8": "o", "\u01FF": "o", "\u0254": "o", "\uA74B": "o", "\uA74D": "o", "\u0275": "o", "\u01A3": "oi", "\u0223": "ou", "\uA74F": "oo", "\u24DF": "p", "\uFF50": "p", "\u1E55": "p", "\u1E57": "p", "\u01A5": "p", "\u1D7D": "p", "\uA751": "p", "\uA753": "p", "\uA755": "p", "\u24E0": "q", "\uFF51": "q", "\u024B": "q", "\uA757": "q", "\uA759": "q", "\u24E1": "r", "\uFF52": "r", "\u0155": "r", "\u1E59": "r", "\u0159": "r", "\u0211": "r", "\u0213": "r", "\u1E5B": "r", "\u1E5D": "r", "\u0157": "r", "\u1E5F": "r", "\u024D": "r", "\u027D": "r", "\uA75B": "r", "\uA7A7": "r", "\uA783": "r", "\u24E2": "s", "\uFF53": "s", "\u00DF": "s", "\u015B": "s", "\u1E65": "s", "\u015D": "s", "\u1E61": "s", "\u0161": "s", "\u1E67": "s", "\u1E63": "s", "\u1E69": "s", "\u0219": "s", "\u015F": "s", "\u023F": "s", "\uA7A9": "s", "\uA785": "s", "\u1E9B": "s", "\u24E3": "t", "\uFF54": "t", "\u1E6B": "t", "\u1E97": "t", "\u0165": "t", "\u1E6D": "t", "\u021B": "t", "\u0163": "t", "\u1E71": "t", "\u1E6F": "t", "\u0167": "t", "\u01AD": "t", "\u0288": "t", "\u2C66": "t", "\uA787": "t", "\uA729": "tz", "\u24E4": "u", "\uFF55": "u", "\u00F9": "u", "\u00FA": "u", "\u00FB": "u", "\u0169": "u", "\u1E79": "u", "\u016B": "u", "\u1E7B": "u", "\u016D": "u", "\u00FC": "u", "\u01DC": "u", "\u01D8": "u", "\u01D6": "u", "\u01DA": "u", "\u1EE7": "u", "\u016F": "u", "\u0171": "u", "\u01D4": "u", "\u0215": "u", "\u0217": "u", "\u01B0": "u", "\u1EEB": "u", "\u1EE9": "u", "\u1EEF": "u", "\u1EED": "u", "\u1EF1": "u", "\u1EE5": "u", "\u1E73": "u", "\u0173": "u", "\u1E77": "u", "\u1E75": "u", "\u0289": "u", "\u24E5": "v", "\uFF56": "v", "\u1E7D": "v", "\u1E7F": "v", "\u028B": "v", "\uA75F": "v", "\u028C": "v", "\uA761": "vy", "\u24E6": "w", "\uFF57": "w", "\u1E81": "w", "\u1E83": "w", "\u0175": "w", "\u1E87": "w", "\u1E85": "w", "\u1E98": "w", "\u1E89": "w", "\u2C73": "w", "\u24E7": "x", "\uFF58": "x", "\u1E8B": "x", "\u1E8D": "x", "\u24E8": "y", "\uFF59": "y", "\u1EF3": "y", "\u00FD": "y", "\u0177": "y", "\u1EF9": "y", "\u0233": "y", "\u1E8F": "y", "\u00FF": "y", "\u1EF7": "y", "\u1E99": "y", "\u1EF5": "y", "\u01B4": "y", "\u024F": "y", "\u1EFF": "y", "\u24E9": "z", "\uFF5A": "z", "\u017A": "z", "\u1E91": "z", "\u017C": "z", "\u017E": "z", "\u1E93": "z", "\u1E95": "z", "\u01B6": "z", "\u0225": "z", "\u0240": "z", "\u2C6C": "z", "\uA763": "z", "\u0386": "\u0391", "\u0388": "\u0395", "\u0389": "\u0397", "\u038A": "\u0399", "\u03AA": "\u0399", "\u038C": "\u039F", "\u038E": "\u03A5", "\u03AB": "\u03A5", "\u038F": "\u03A9", "\u03AC": "\u03B1", "\u03AD": "\u03B5", "\u03AE": "\u03B7", "\u03AF": "\u03B9", "\u03CA": "\u03B9", "\u0390": "\u03B9", "\u03CC": "\u03BF", "\u03CD": "\u03C5", "\u03CB": "\u03C5", "\u03B0": "\u03C5", "\u03C9": "\u03C9", "\u03C2": "\u03C3" };

  $document = $(document);

  nextUid = (function () { var counter = 1; return function () { return counter++; }; }());


  function reinsertElement(element) {
    var placeholder = $(document.createTextNode(''));

    element.before(placeholder);
    placeholder.before(element);
    placeholder.remove();
  }

  function stripDiacritics(str) {
    // Used 'uni range + named function' from http://jsperf.com/diacritics/18
    function match(a) {
      return DIACRITICS[a] || a;
    }

    return str.replace(/[^\u0000-\u007E]/g, match);
  }

  function indexOf(value, array) {
    var i = 0, l = array.length;
    for (; i < l; i = i + 1) {
      if (equal(value, array[i])) return i;
    }
    return -1;
  }

  function measureScrollbar() {
    var $template = $(MEASURE_SCROLLBAR_TEMPLATE);
    $template.appendTo('body');

    var dim = {
      width: $template.width() - $template[0].clientWidth,
      height: $template.height() - $template[0].clientHeight
    };
    $template.remove();

    return dim;
  }

  /**
   * Compares equality of a and b
   * @param a
   * @param b
   */
  function equal(a, b) {
    if (a === b) return true;
    if (a === undefined || b === undefined) return false;
    if (a === null || b === null) return false;
    // Check whether 'a' or 'b' is a string (primitive or object).
    // The concatenation of an empty string (+'') converts its argument to a string's primitive.
    if (a.constructor === String) return a + '' === b + ''; // a+'' - in case 'a' is a String object
    if (b.constructor === String) return b + '' === a + ''; // b+'' - in case 'b' is a String object
    return false;
  }

  /**
   * Splits the string into an array of values, trimming each value. An empty array is returned for nulls or empty
   * strings
   * @param string
   * @param separator
   */
  function splitVal(string, separator) {
    var val, i, l;
    if (string === null || string.length < 1) return [];
    val = string.split(separator);
    for (i = 0, l = val.length; i < l; i = i + 1) val[i] = $.trim(val[i]);
    return val;
  }

  function getSideBorderPadding(element) {
    return element.outerWidth(false) - element.width();
  }

  function installKeyUpChangeEvent(element) {
    var key = "keyup-change-value";
    element.on("keydown", function () {
      if ($.data(element, key) === undefined) {
        $.data(element, key, element.val());
      }
    });
    element.on("keyup", function () {
      var val = $.data(element, key);
      if (val !== undefined && element.val() !== val) {
        $.removeData(element, key);
        element.trigger("keyup-change");
      }
    });
  }


  /**
   * filters mouse events so an event is fired only if the mouse moved.
   *
   * filters out mouse events that occur when mouse is stationary but
   * the elements under the pointer are scrolled.
   */
  function installFilteredMouseMove(element) {
    element.on("mousemove", function (e) {
      var lastpos = lastMousePosition;
      if (lastpos === undefined || lastpos.x !== e.pageX || lastpos.y !== e.pageY) {
        $(e.target).trigger("mousemove-filtered", e);
      }
    });
  }

  /**
   * Debounces a function. Returns a function that calls the original fn function only if no invocations have been made
   * within the last quietMillis milliseconds.
   *
   * @param quietMillis number of milliseconds to wait before invoking fn
   * @param fn function to be debounced
   * @param ctx object to be used as this reference within fn
   * @return debounced version of fn
   */
  function debounce(quietMillis, fn, ctx) {
    ctx = ctx || undefined;
    var timeout;
    return function () {
      var args = arguments;
      window.clearTimeout(timeout);
      timeout = window.setTimeout(function () {
        fn.apply(ctx, args);
      }, quietMillis);
    };
  }

  function installDebouncedScroll(threshold, element) {
    var notify = debounce(threshold, function (e) { element.trigger("scroll-debounced", e); });
    element.on("scroll", function (e) {
      if (indexOf(e.target, element.get()) >= 0) notify(e);
    });
  }

  function focus($el) {
    if ($el[0] === document.activeElement) return;

    /* set the focus in a 0 timeout - that way the focus is set after the processing
        of the current event has finished - which seems like the only reliable way
        to set focus */
    window.setTimeout(function () {
      var el = $el[0], pos = $el.val().length, range;

      $el.focus();

      /* make sure el received focus so we do not error out when trying to manipulate the caret.
          sometimes modals or others listeners may steal it after its set */
      var isVisible = (el.offsetWidth > 0 || el.offsetHeight > 0);
      if (isVisible && el === document.activeElement) {

        /* after the focus is set move the caret to the end, necessary when we val()
            just before setting focus */
        if (el.setSelectionRange) {
          el.setSelectionRange(pos, pos);
        }
        else if (el.createTextRange) {
          range = el.createTextRange();
          range.collapse(false);
          range.select();
        }
      }
    }, 0);
  }

  function getCursorInfo(el) {
    el = $(el)[0];
    var offset = 0;
    var length = 0;
    if ('selectionStart' in el) {
      offset = el.selectionStart;
      length = el.selectionEnd - offset;
    } else if ('selection' in document) {
      el.focus();
      var sel = document.selection.createRange();
      length = document.selection.createRange().text.length;
      sel.moveStart('character', -el.value.length);
      offset = sel.text.length - length;
    }
    return { offset: offset, length: length };
  }

  function killEvent(event) {
    event.preventDefault();
    event.stopPropagation();
  }
  function killEventImmediately(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
  }

  function measureTextWidth(e) {
    if (!sizer) {
      var style = e[0].currentStyle || window.getComputedStyle(e[0], null);
      sizer = $(document.createElement("div")).css({
        position: "absolute",
        left: "-10000px",
        top: "-10000px",
        display: "none",
        fontSize: style.fontSize,
        fontFamily: style.fontFamily,
        fontStyle: style.fontStyle,
        fontWeight: style.fontWeight,
        letterSpacing: style.letterSpacing,
        textTransform: style.textTransform,
        whiteSpace: "nowrap"
      });
      sizer.attr("class", "select2-sizer");
      $("body").append(sizer);
    }
    sizer.text(e.val());
    return sizer.width();
  }

  function syncCssClasses(dest, src, adapter) {
    var classes, replacements = [], adapted;

    classes = $.trim(dest.attr("class"));

    if (classes) {
      classes = '' + classes; // for IE which returns object

      $(classes.split(/\s+/)).each2(function () {
        if (this.indexOf("select2-") === 0) {
          replacements.push(this);
        }
      });
    }

    classes = $.trim(src.attr("class"));

    if (classes) {
      classes = '' + classes; // for IE which returns object

      $(classes.split(/\s+/)).each2(function () {
        if (this.indexOf("select2-") !== 0) {
          adapted = adapter(this);

          if (adapted) {
            replacements.push(adapted);
          }
        }
      });
    }

    dest.attr("class", replacements.join(" "));
  }


  function markMatch(text, term, markup, escapeMarkup) {
    var match = stripDiacritics(text.toUpperCase()).indexOf(stripDiacritics(term.toUpperCase())),
        tl = term.length;

    if (match < 0) {
      markup.push(escapeMarkup(text));
      return;
    }

    markup.push(escapeMarkup(text.substring(0, match)));
    markup.push("<span class='select2-match'>");
    markup.push(escapeMarkup(text.substring(match, match + tl)));
    markup.push("</span>");
    markup.push(escapeMarkup(text.substring(match + tl, text.length)));
  }

  function defaultEscapeMarkup(markup) {
    var replace_map = {
      '\\': '&#92;',
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#39;',
      "/": '&#47;'
    };

    return String(markup).replace(/[&<>"'\/\\]/g, function (match) {
      return replace_map[match];
    });
  }

  /**
   * Produces an ajax-based query function
   *
   * @param options object containing configuration parameters
   * @param options.params parameter map for the transport ajax call, can contain such options as cache, jsonpCallback, etc. see $.ajax
   * @param options.transport function that will be used to execute the ajax request. must be compatible with parameters supported by $.ajax
   * @param options.url url for the data
   * @param options.data a function(searchTerm, pageNumber, context) that should return an object containing query string parameters for the above url.
   * @param options.dataType request data type: ajax, jsonp, other datatypes supported by jQuery's $.ajax function or the transport function if specified
   * @param options.quietMillis (optional) milliseconds to wait before making the ajaxRequest, helps debounce the ajax function if invoked too often
   * @param options.results a function(remoteData, pageNumber, query) that converts data returned form the remote request to the format expected by Select2.
   *      The expected format is an object containing the following keys:
   *      results array of objects that will be used as choices
   *      more (optional) boolean indicating whether there are more results available
   *      Example: {results:[{id:1, text:'Red'},{id:2, text:'Blue'}], more:true}
   */
  function ajax(options) {
    var timeout, // current scheduled but not yet executed request
        handler = null,
        quietMillis = options.quietMillis || 100,
        ajaxUrl = options.url,
        self = this;

    return function (query) {
      window.clearTimeout(timeout);
      timeout = window.setTimeout(function () {
        var data = options.data, // ajax data function
            url = ajaxUrl, // ajax url string or function
            transport = options.transport || $.fn.select2.ajaxDefaults.transport,
            // deprecated - to be removed in 4.0  - use params instead
            deprecated = {
              type: options.type || 'GET', // set type of request (GET or POST)
              cache: options.cache || false,
              jsonpCallback: options.jsonpCallback || undefined,
              dataType: options.dataType || "json"
            },
            params = $.extend({}, $.fn.select2.ajaxDefaults.params, deprecated);

        data = data ? data.call(self, query.term, query.page, query.context) : null;
        url = (typeof url === 'function') ? url.call(self, query.term, query.page, query.context) : url;

        if (handler && typeof handler.abort === "function") { handler.abort(); }

        if (options.params) {
          if ($.isFunction(options.params)) {
            $.extend(params, options.params.call(self));
          } else {
            $.extend(params, options.params);
          }
        }

        $.extend(params, {
          url: url,
          dataType: options.dataType,
          data: data,
          success: function (data) {
            // TODO - replace query.page with query so users have access to term, page, etc.
            // added query as third paramter to keep backwards compatibility
            var results = options.results(data, query.page, query);
            query.callback(results);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            var results = {
              hasError: true,
              jqXHR: jqXHR,
              textStatus: textStatus,
              errorThrown: errorThrown
            };

            query.callback(results);
          }
        });
        handler = transport.call(self, params);
      }, quietMillis);
    };
  }

  /**
   * Produces a query function that works with a local array
   *
   * @param options object containing configuration parameters. The options parameter can either be an array or an
   * object.
   *
   * If the array form is used it is assumed that it contains objects with 'id' and 'text' keys.
   *
   * If the object form is used it is assumed that it contains 'data' and 'text' keys. The 'data' key should contain
   * an array of objects that will be used as choices. These objects must contain at least an 'id' key. The 'text'
   * key can either be a String in which case it is expected that each element in the 'data' array has a key with the
   * value of 'text' which will be used to match choices. Alternatively, text can be a function(item) that can extract
   * the text.
   */
  function local(options) {
    var data = options, // data elements
        dataText,
        tmp,
        text = function (item) { return "" + item.text; }; // function used to retrieve the text portion of a data item that is matched against the search

    if ($.isArray(data)) {
      tmp = data;
      data = { results: tmp };
    }

    if ($.isFunction(data) === false) {
      tmp = data;
      data = function () { return tmp; };
    }

    var dataItem = data();
    if (dataItem.text) {
      text = dataItem.text;
      // if text is not a function we assume it to be a key name
      if (!$.isFunction(text)) {
        dataText = dataItem.text; // we need to store this in a separate variable because in the next step data gets reset and data.text is no longer available
        text = function (item) { return item[dataText]; };
      }
    }

    return function (query) {
      var t = query.term, filtered = { results: [] }, process;
      if (t === "") {
        query.callback(data());
        return;
      }

      process = function (datum, collection) {
        var group, attr;
        datum = datum[0];
        if (datum.children) {
          group = {};
          for (attr in datum) {
            if (datum.hasOwnProperty(attr)) group[attr] = datum[attr];
          }
          group.children = [];
          $(datum.children).each2(function (i, childDatum) { process(childDatum, group.children); });
          if (group.children.length || query.matcher(t, text(group), datum)) {
            collection.push(group);
          }
        } else {
          if (query.matcher(t, text(datum), datum)) {
            collection.push(datum);
          }
        }
      };

      $(data().results).each2(function (i, datum) { process(datum, filtered.results); });
      query.callback(filtered);
    };
  }

  // TODO javadoc
  function tags(data) {
    var isFunc = $.isFunction(data);
    return function (query) {
      var t = query.term, filtered = { results: [] };
      var result = isFunc ? data(query) : data;
      if ($.isArray(result)) {
        $(result).each(function () {
          var isObject = this.text !== undefined,
              text = isObject ? this.text : this;
          if (t === "" || query.matcher(t, text)) {
            filtered.results.push(isObject ? this : { id: this, text: this });
          }
        });
        query.callback(filtered);
      }
    };
  }

  /**
   * Checks if the formatter function should be used.
   *
   * Throws an error if it is not a function. Returns true if it should be used,
   * false if no formatting should be performed.
   *
   * @param formatter
   */
  function checkFormatter(formatter, formatterName) {
    if ($.isFunction(formatter)) return true;
    if (!formatter) return false;
    if (typeof (formatter) === 'string') return true;
    throw new Error(formatterName + " must be a string, function, or falsy value");
  }

  /**
   * Returns a given value
   * If given a function, returns its output
   *
   * @param val string|function
   * @param context value of "this" to be passed to function
   * @returns {*}
   */
  function evaluate(val, context) {
    if ($.isFunction(val)) {
      var args = Array.prototype.slice.call(arguments, 2);
      return val.apply(context, args);
    }
    return val;
  }

  function countResults(results) {
    var count = 0;
    $.each(results, function (i, item) {
      if (item.children) {
        count += countResults(item.children);
      } else {
        count++;
      }
    });
    return count;
  }

  /**
   * Default tokenizer. This function uses breaks the input on substring match of any string from the
   * opts.tokenSeparators array and uses opts.createSearchChoice to create the choice object. Both of those
   * two options have to be defined in order for the tokenizer to work.
   *
   * @param input text user has typed so far or pasted into the search field
   * @param selection currently selected choices
   * @param selectCallback function(choice) callback tho add the choice to selection
   * @param opts select2's opts
   * @return undefined/null to leave the current input unchanged, or a string to change the input to the returned value
   */
  function defaultTokenizer(input, selection, selectCallback, opts) {
    var original = input, // store the original so we can compare and know if we need to tell the search to update its text
        dupe = false, // check for whether a token we extracted represents a duplicate selected choice
        token, // token
        index, // position at which the separator was found
        i, l, // looping variables
        separator; // the matched separator

    if (!opts.createSearchChoice || !opts.tokenSeparators || opts.tokenSeparators.length < 1) return undefined;

    while (true) {
      index = -1;

      for (i = 0, l = opts.tokenSeparators.length; i < l; i++) {
        separator = opts.tokenSeparators[i];
        index = input.indexOf(separator);
        if (index >= 0) break;
      }

      if (index < 0) break; // did not find any token separator in the input string, bail

      token = input.substring(0, index);
      input = input.substring(index + separator.length);

      if (token.length > 0) {
        token = opts.createSearchChoice.call(this, token, selection);
        if (token !== undefined && token !== null && opts.id(token) !== undefined && opts.id(token) !== null) {
          dupe = false;
          for (i = 0, l = selection.length; i < l; i++) {
            if (equal(opts.id(token), opts.id(selection[i]))) {
              dupe = true; break;
            }
          }

          if (!dupe) selectCallback(token);
        }
      }
    }

    if (original !== input) return input;
  }

  function cleanupJQueryElements() {
    var self = this;

    $.each(arguments, function (i, element) {
      self[element].remove();
      self[element] = null;
    });
  }

  /**
   * Creates a new class
   *
   * @param superClass
   * @param methods
   */
  function clazz(SuperClass, methods) {
    var constructor = function () { };
    constructor.prototype = new SuperClass;
    constructor.prototype.constructor = constructor;
    constructor.prototype.parent = SuperClass.prototype;
    constructor.prototype = $.extend(constructor.prototype, methods);
    return constructor;
  }

  AbstractSelect2 = clazz(Object, {

    // abstract
    bind: function (func) {
      var self = this;
      return function () {
        func.apply(self, arguments);
      };
    },

    // abstract
    init: function (opts) {
      var results, search, resultsSelector = ".select2-results";

      // prepare options
      this.opts = opts = this.prepareOpts(opts);

      this.id = opts.id;

      // destroy if called on an existing component
      if (opts.element.data("select2") !== undefined &&
          opts.element.data("select2") !== null) {
        opts.element.data("select2").destroy();
      }

      this.container = this.createContainer();

      this.liveRegion = $("<span>", {
        role: "status",
        "aria-live": "polite"
      })
          .addClass("select2-hidden-accessible")
          .appendTo(document.body);

      this.containerId = "s2id_" + (opts.element.attr("id") || "autogen" + nextUid());
      this.containerEventName = this.containerId
          .replace(/([.])/g, '_')
          .replace(/([;&,\-\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, '\\$1');
      this.container.attr("id", this.containerId);

      this.container.attr("title", opts.element.attr("title"));

      this.body = $("body");

      syncCssClasses(this.container, this.opts.element, this.opts.adaptContainerCssClass);

      this.container.attr("style", opts.element.attr("style"));
      this.container.css(evaluate(opts.containerCss, this.opts.element));
      this.container.addClass(evaluate(opts.containerCssClass, this.opts.element));

      this.elementTabIndex = this.opts.element.attr("tabindex");

      // swap container for the element
      this.opts.element
          .data("select2", this)
          .attr("tabindex", "-1")
          .before(this.container)
          .on("click.select2", killEvent); // do not leak click events

      this.container.data("select2", this);

      this.dropdown = this.container.find(".select2-drop");

      syncCssClasses(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass);

      this.dropdown.addClass(evaluate(opts.dropdownCssClass, this.opts.element));
      this.dropdown.data("select2", this);
      this.dropdown.on("click", killEvent);

      this.results = results = this.container.find(resultsSelector);
      this.search = search = this.container.find("input.select2-input");

      this.queryCount = 0;
      this.resultsPage = 0;
      this.context = null;

      // initialize the container
      this.initContainer();

      this.container.on("click", killEvent);

      installFilteredMouseMove(this.results);

      this.dropdown.on("mousemove-filtered", resultsSelector, this.bind(this.highlightUnderEvent));
      this.dropdown.on("touchstart touchmove touchend", resultsSelector, this.bind(function (event) {
        this._touchEvent = true;
        this.highlightUnderEvent(event);
      }));
      this.dropdown.on("touchmove", resultsSelector, this.bind(this.touchMoved));
      this.dropdown.on("touchstart touchend", resultsSelector, this.bind(this.clearTouchMoved));

      // Waiting for a click event on touch devices to select option and hide dropdown
      // otherwise click will be triggered on an underlying element
      this.dropdown.on('click', this.bind(function (event) {
        if (this._touchEvent) {
          this._touchEvent = false;
          this.selectHighlighted();
        }
      }));

      installDebouncedScroll(80, this.results);
      this.dropdown.on("scroll-debounced", resultsSelector, this.bind(this.loadMoreIfNeeded));

      // do not propagate change event from the search field out of the component
      $(this.container).on("change", ".select2-input", function (e) { e.stopPropagation(); });
      $(this.dropdown).on("change", ".select2-input", function (e) { e.stopPropagation(); });

      // if jquery.mousewheel plugin is installed we can prevent out-of-bounds scrolling of results via mousewheel
      if ($.fn.mousewheel) {
        results.mousewheel(function (e, delta, deltaX, deltaY) {
          var top = results.scrollTop();
          if (deltaY > 0 && top - deltaY <= 0) {
            results.scrollTop(0);
            killEvent(e);
          } else if (deltaY < 0 && results.get(0).scrollHeight - results.scrollTop() + deltaY <= results.height()) {
            results.scrollTop(results.get(0).scrollHeight - results.height());
            killEvent(e);
          }
        });
      }

      installKeyUpChangeEvent(search);
      search.on("keyup-change input paste", this.bind(this.updateResults));
      search.on("focus", function () { search.addClass("select2-focused"); });
      search.on("blur", function () { search.removeClass("select2-focused"); });

      this.dropdown.on("mouseup", resultsSelector, this.bind(function (e) {
        if ($(e.target).closest(".select2-result-selectable").length > 0) {
          this.highlightUnderEvent(e);
          this.selectHighlighted(e);
        }
      }));

      // trap all mouse events from leaving the dropdown. sometimes there may be a modal that is listening
      // for mouse events outside of itself so it can close itself. since the dropdown is now outside the select2's
      // dom it will trigger the popup close, which is not what we want
      // focusin can cause focus wars between modals and select2 since the dropdown is outside the modal.
      this.dropdown.on("click mouseup mousedown touchstart touchend focusin", function (e) { e.stopPropagation(); });

      this.nextSearchTerm = undefined;

      if ($.isFunction(this.opts.initSelection)) {
        // initialize selection based on the current value of the source element
        this.initSelection();

        // if the user has provided a function that can set selection based on the value of the source element
        // we monitor the change event on the element and trigger it, allowing for two way synchronization
        this.monitorSource();
      }

      if (opts.maximumInputLength !== null) {
        this.search.attr("maxlength", opts.maximumInputLength);
      }

      var disabled = opts.element.prop("disabled");
      if (disabled === undefined) disabled = false;
      this.enable(!disabled);

      var readonly = opts.element.prop("readonly");
      if (readonly === undefined) readonly = false;
      this.readonly(readonly);

      // Calculate size of scrollbar
      scrollBarDimensions = scrollBarDimensions || measureScrollbar();

      this.autofocus = opts.element.prop("autofocus");
      opts.element.prop("autofocus", false);
      if (this.autofocus) this.focus();

      this.search.attr("placeholder", opts.searchInputPlaceholder);
    },

    // abstract
    destroy: function () {
      var element = this.opts.element, select2 = element.data("select2"), self = this;

      this.close();

      if (element.length && element[0].detachEvent) {
        element.each(function () {
          this.detachEvent("onpropertychange", self._sync);
        });
      }
      if (this.propertyObserver) {
        this.propertyObserver.disconnect();
        this.propertyObserver = null;
      }
      this._sync = null;

      if (select2 !== undefined) {
        select2.container.remove();
        select2.liveRegion.remove();
        select2.dropdown.remove();
        element
            .removeClass("select2-offscreen")
            .removeData("select2")
            .off(".select2")
            .prop("autofocus", this.autofocus || false);
        if (this.elementTabIndex) {
          element.attr({ tabindex: this.elementTabIndex });
        } else {
          element.removeAttr("tabindex");
        }
        element.show();
      }

      cleanupJQueryElements.call(this,
          "container",
          "liveRegion",
          "dropdown",
          "results",
          "search"
      );
    },

    // abstract
    optionToData: function (element) {
      if (element.is("option")) {
        return {
          id: element.prop("value"),
          text: element.text(),
          element: element.get(),
          css: element.attr("class"),
          disabled: element.prop("disabled"),
          locked: equal(element.attr("locked"), "locked") || equal(element.data("locked"), true)
        };
      } else if (element.is("optgroup")) {
        return {
          text: element.attr("label"),
          children: [],
          element: element.get(),
          css: element.attr("class")
        };
      }
    },

    // abstract
    prepareOpts: function (opts) {
      var element, select, idKey, ajaxUrl, self = this;

      element = opts.element;

      if (element.get(0).tagName.toLowerCase() === "select") {
        this.select = select = opts.element;
      }

      if (select) {
        // these options are not allowed when attached to a select because they are picked up off the element itself
        $.each(["id", "multiple", "ajax", "query", "createSearchChoice", "initSelection", "data", "tags"], function () {
          if (this in opts) {
            throw new Error("Option '" + this + "' is not allowed for Select2 when attached to a <select> element.");
          }
        });
      }

      opts = $.extend({}, {
        populateResults: function (container, results, query) {
          var populate, id = this.opts.id, liveRegion = this.liveRegion;

          populate = function (results, container, depth) {

            var i, l, result, selectable, disabled, compound, node, label, innerContainer, formatted;

            results = opts.sortResults(results, container, query);

            // collect the created nodes for bulk append
            var nodes = [];
            for (i = 0, l = results.length; i < l; i = i + 1) {

              result = results[i];

              disabled = (result.disabled === true);
              selectable = (!disabled) && (id(result) !== undefined);

              compound = result.children && result.children.length > 0;

              node = $("<li></li>");
              node.addClass("select2-results-dept-" + depth);
              node.addClass("select2-result");
              node.addClass(selectable ? "select2-result-selectable" : "select2-result-unselectable");
              if (disabled) { node.addClass("select2-disabled"); }
              if (compound) { node.addClass("select2-result-with-children"); }
              node.addClass(self.opts.formatResultCssClass(result));
              node.attr("role", "presentation");

              label = $(document.createElement("div"));
              label.addClass("select2-result-label");
              label.attr("id", "select2-result-label-" + nextUid());
              label.attr("role", "option");

              formatted = opts.formatResult(result, label, query, self.opts.escapeMarkup);
              if (formatted !== undefined) {
                label.html(formatted);
                node.append(label);
              }


              if (compound) {

                innerContainer = $("<ul></ul>");
                innerContainer.addClass("select2-result-sub");
                populate(result.children, innerContainer, depth + 1);
                node.append(innerContainer);
              }

              node.data("select2-data", result);
              nodes.push(node[0]);
            }

            // bulk append the created nodes
            container.append(nodes);
            liveRegion.text(opts.formatMatches(results.length));
          };

          populate(results, container, 0);
        }
      }, $.fn.select2.defaults, opts);

      if (typeof (opts.id) !== "function") {
        idKey = opts.id;
        opts.id = function (e) { return e[idKey]; };
      }

      if ($.isArray(opts.element.data("select2Tags"))) {
        if ("tags" in opts) {
          throw "tags specified as both an attribute 'data-select2-tags' and in options of Select2 " + opts.element.attr("id");
        }
        opts.tags = opts.element.data("select2Tags");
      }

      if (select) {
        opts.query = this.bind(function (query) {
          var data = { results: [], more: false },
              term = query.term,
              children, placeholderOption, process;

          process = function (element, collection) {
            var group;
            if (element.is("option")) {
              if (query.matcher(term, element.text(), element)) {
                collection.push(self.optionToData(element));
              }
            } else if (element.is("optgroup")) {
              group = self.optionToData(element);
              element.children().each2(function (i, elm) { process(elm, group.children); });
              if (group.children.length > 0) {
                collection.push(group);
              }
            }
          };

          children = element.children();

          // ignore the placeholder option if there is one
          if (this.getPlaceholder() !== undefined && children.length > 0) {
            placeholderOption = this.getPlaceholderOption();
            if (placeholderOption) {
              children = children.not(placeholderOption);
            }
          }

          children.each2(function (i, elm) { process(elm, data.results); });

          query.callback(data);
        });
        // this is needed because inside val() we construct choices from options and their id is hardcoded
        opts.id = function (e) { return e.id; };
      } else {
        if (!("query" in opts)) {

          if ("ajax" in opts) {
            ajaxUrl = opts.element.data("ajax-url");
            if (ajaxUrl && ajaxUrl.length > 0) {
              opts.ajax.url = ajaxUrl;
            }
            opts.query = ajax.call(opts.element, opts.ajax);
          } else if ("data" in opts) {
            opts.query = local(opts.data);
          } else if ("tags" in opts) {
            opts.query = tags(opts.tags);
            if (opts.createSearchChoice === undefined) {
              opts.createSearchChoice = function (term) { return { id: $.trim(term), text: $.trim(term) }; };
            }
            if (opts.initSelection === undefined) {
              opts.initSelection = function (element, callback) {
                var data = [];
                $(splitVal(element.val(), opts.separator)).each(function () {
                  var obj = { id: this, text: this },
                      tags = opts.tags;
                  if ($.isFunction(tags)) tags = tags();
                  $(tags).each(function () { if (equal(this.id, obj.id)) { obj = this; return false; } });
                  data.push(obj);
                });

                callback(data);
              };
            }
          }
        }
      }
      if (typeof (opts.query) !== "function") {
        throw "query function not defined for Select2 " + opts.element.attr("id");
      }

      if (opts.createSearchChoicePosition === 'top') {
        opts.createSearchChoicePosition = function (list, item) { list.unshift(item); };
      }
      else if (opts.createSearchChoicePosition === 'bottom') {
        opts.createSearchChoicePosition = function (list, item) { list.push(item); };
      }
      else if (typeof (opts.createSearchChoicePosition) !== "function") {
        throw "invalid createSearchChoicePosition option must be 'top', 'bottom' or a custom function";
      }

      return opts;
    },

    /**
     * Monitor the original element for changes and update select2 accordingly
     */
    // abstract
    monitorSource: function () {
      var el = this.opts.element, observer, self = this;

      el.on("change.select2", this.bind(function (e) {
        if (this.opts.element.data("select2-change-triggered") !== true) {
          this.initSelection();
        }
      }));

      this._sync = this.bind(function () {

        // sync enabled state
        var disabled = el.prop("disabled");
        if (disabled === undefined) disabled = false;
        this.enable(!disabled);

        var readonly = el.prop("readonly");
        if (readonly === undefined) readonly = false;
        this.readonly(readonly);

        syncCssClasses(this.container, this.opts.element, this.opts.adaptContainerCssClass);
        this.container.addClass(evaluate(this.opts.containerCssClass, this.opts.element));

        syncCssClasses(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass);
        this.dropdown.addClass(evaluate(this.opts.dropdownCssClass, this.opts.element));

      });

      // IE8-10 (IE9/10 won't fire propertyChange via attachEventListener)
      if (el.length && el[0].attachEvent) {
        el.each(function () {
          this.attachEvent("onpropertychange", self._sync);
        });
      }

      // safari, chrome, firefox, IE11
      observer = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
      if (observer !== undefined) {
        if (this.propertyObserver) { delete this.propertyObserver; this.propertyObserver = null; }
        this.propertyObserver = new observer(function (mutations) {
          $.each(mutations, self._sync);
        });
        this.propertyObserver.observe(el.get(0), { attributes: true, subtree: false });
      }
    },

    // abstract
    triggerSelect: function (data) {
      var evt = $.Event("select2-selecting", { val: this.id(data), object: data, choice: data });
      this.opts.element.trigger(evt);
      return !evt.isDefaultPrevented();
    },

    /**
     * Triggers the change event on the source element
     */
    // abstract
    triggerChange: function (details) {

      details = details || {};
      details = $.extend({}, details, { type: "change", val: this.val() });
      // prevents recursive triggering
      this.opts.element.data("select2-change-triggered", true);
      this.opts.element.trigger(details);
      this.opts.element.data("select2-change-triggered", false);

      // some validation frameworks ignore the change event and listen instead to keyup, click for selects
      // so here we trigger the click event manually
      this.opts.element.click();

      // ValidationEngine ignores the change event and listens instead to blur
      // so here we trigger the blur event manually if so desired
      if (this.opts.blurOnChange)
        this.opts.element.blur();
    },

    //abstract
    isInterfaceEnabled: function () {
      return this.enabledInterface === true;
    },

    // abstract
    enableInterface: function () {
      var enabled = this._enabled && !this._readonly,
          disabled = !enabled;

      if (enabled === this.enabledInterface) return false;

      this.container.toggleClass("select2-container-disabled", disabled);
      this.close();
      this.enabledInterface = enabled;

      return true;
    },

    // abstract
    enable: function (enabled) {
      if (enabled === undefined) enabled = true;
      if (this._enabled === enabled) return;
      this._enabled = enabled;

      this.opts.element.prop("disabled", !enabled);
      this.enableInterface();
    },

    // abstract
    disable: function () {
      this.enable(false);
    },

    // abstract
    readonly: function (enabled) {
      if (enabled === undefined) enabled = false;
      if (this._readonly === enabled) return;
      this._readonly = enabled;

      this.opts.element.prop("readonly", enabled);
      this.enableInterface();
    },

    // abstract
    opened: function () {
      return (this.container) ? this.container.hasClass("select2-dropdown-open") : false;
    },

    // abstract
    positionDropdown: function () {
      var $dropdown = this.dropdown,
          offset = this.container.offset(),
          height = this.container.outerHeight(false),
          width = this.container.outerWidth(false),
          dropHeight = $dropdown.outerHeight(false),
          $window = $(window),
          windowWidth = $window.width(),
          windowHeight = $window.height(),
          viewPortRight = $window.scrollLeft() + windowWidth,
          viewportBottom = $window.scrollTop() + windowHeight,
          dropTop = offset.top + height,
          dropLeft = offset.left,
          enoughRoomBelow = dropTop + dropHeight <= viewportBottom,
          enoughRoomAbove = (offset.top - dropHeight) >= $window.scrollTop(),
          dropWidth = $dropdown.outerWidth(false),
          enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight,
          aboveNow = $dropdown.hasClass("select2-drop-above"),
          bodyOffset,
          above,
          changeDirection,
          css,
          resultsListNode;

      // always prefer the current above/below alignment, unless there is not enough room
      if (aboveNow) {
        above = true;
        if (!enoughRoomAbove && enoughRoomBelow) {
          changeDirection = true;
          above = false;
        }
      } else {
        above = false;
        if (!enoughRoomBelow && enoughRoomAbove) {
          changeDirection = true;
          above = true;
        }
      }

      //if we are changing direction we need to get positions when dropdown is hidden;
      if (changeDirection) {
        $dropdown.hide();
        offset = this.container.offset();
        height = this.container.outerHeight(false);
        width = this.container.outerWidth(false);
        dropHeight = $dropdown.outerHeight(false);
        viewPortRight = $window.scrollLeft() + windowWidth;
        viewportBottom = $window.scrollTop() + windowHeight;
        dropTop = offset.top + height;
        dropLeft = offset.left;
        dropWidth = $dropdown.outerWidth(false);
        enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight;
        $dropdown.show();

        // fix so the cursor does not move to the left within the search-textbox in IE
        this.focusSearch();
      }

      if (this.opts.dropdownAutoWidth) {
        resultsListNode = $('.select2-results', $dropdown)[0];
        $dropdown.addClass('select2-drop-auto-width');
        $dropdown.css('width', '');
        // Add scrollbar width to dropdown if vertical scrollbar is present
        dropWidth = $dropdown.outerWidth(false) + (resultsListNode.scrollHeight === resultsListNode.clientHeight ? 0 : scrollBarDimensions.width);
        dropWidth > width ? width = dropWidth : dropWidth = width;
        dropHeight = $dropdown.outerHeight(false);
        enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight;
      }
      else {
        this.container.removeClass('select2-drop-auto-width');
      }

      //console.log("below/ droptop:", dropTop, "dropHeight", dropHeight, "sum", (dropTop+dropHeight)+" viewport bottom", viewportBottom, "enough?", enoughRoomBelow);
      //console.log("above/ offset.top", offset.top, "dropHeight", dropHeight, "top", (offset.top-dropHeight), "scrollTop", this.body.scrollTop(), "enough?", enoughRoomAbove);

      // fix positioning when body has an offset and is not position: static
      if (this.body.css('position') !== 'static') {
        bodyOffset = this.body.offset();
        dropTop -= bodyOffset.top;
        dropLeft -= bodyOffset.left;
      }

      if (!enoughRoomOnRight) {
        dropLeft = offset.left + this.container.outerWidth(false) - dropWidth;
      }

      css = {
        left: dropLeft,
        width: width
      };

      if (above) {
        css.top = offset.top - dropHeight;
        css.bottom = 'auto';
        this.container.addClass("select2-drop-above");
        $dropdown.addClass("select2-drop-above");
      }
      else {
        css.top = dropTop;
        css.bottom = 'auto';
        this.container.removeClass("select2-drop-above");
        $dropdown.removeClass("select2-drop-above");
      }
      css = $.extend(css, evaluate(this.opts.dropdownCss, this.opts.element));

      $dropdown.css(css);
    },

    // abstract
    shouldOpen: function () {
      var event;

      if (this.opened()) return false;

      if (this._enabled === false || this._readonly === true) return false;

      event = $.Event("select2-opening");
      this.opts.element.trigger(event);
      return !event.isDefaultPrevented();
    },

    // abstract
    clearDropdownAlignmentPreference: function () {
      // clear the classes used to figure out the preference of where the dropdown should be opened
      this.container.removeClass("select2-drop-above");
      this.dropdown.removeClass("select2-drop-above");
    },

    /**
     * Opens the dropdown
     *
     * @return {Boolean} whether or not dropdown was opened. This method will return false if, for example,
     * the dropdown is already open, or if the 'open' event listener on the element called preventDefault().
     */
    // abstract
    open: function () {

      if (!this.shouldOpen()) return false;

      this.opening();

      // Only bind the document mousemove when the dropdown is visible
      $document.on("mousemove.select2Event", function (e) {
        lastMousePosition.x = e.pageX;
        lastMousePosition.y = e.pageY;
      });

      return true;
    },

    /**
     * Performs the opening of the dropdown
     */
    // abstract
    opening: function () {
      var cid = this.containerEventName,
          scroll = "scroll." + cid,
          resize = "resize." + cid,
          orient = "orientationchange." + cid,
          mask;

      this.container.addClass("select2-dropdown-open").addClass("select2-container-active");

      this.clearDropdownAlignmentPreference();

      if (this.dropdown[0] !== this.body.children().last()[0]) {
        this.dropdown.detach().appendTo(this.body);
      }

      // create the dropdown mask if doesn't already exist
      mask = $("#select2-drop-mask");
      if (mask.length == 0) {
        mask = $(document.createElement("div"));
        mask.attr("id", "select2-drop-mask").attr("class", "select2-drop-mask");
        mask.hide();
        mask.appendTo(this.body);
        mask.on("mousedown touchstart click", function (e) {
          // Prevent IE from generating a click event on the body
          reinsertElement(mask);

          var dropdown = $("#select2-drop"), self;
          if (dropdown.length > 0) {
            self = dropdown.data("select2");
            if (self.opts.selectOnBlur) {
              self.selectHighlighted({ noFocus: true });
            }
            self.close();
            e.preventDefault();
            e.stopPropagation();
          }
        });
      }

      // ensure the mask is always right before the dropdown
      if (this.dropdown.prev()[0] !== mask[0]) {
        this.dropdown.before(mask);
      }

      // move the global id to the correct dropdown
      $("#select2-drop").removeAttr("id");
      this.dropdown.attr("id", "select2-drop");

      // show the elements
      mask.show();

      this.positionDropdown();
      this.dropdown.show();
      this.positionDropdown();

      this.dropdown.addClass("select2-drop-active");

      // attach listeners to events that can change the position of the container and thus require
      // the position of the dropdown to be updated as well so it does not come unglued from the container
      var that = this;
      this.container.parents().add(window).each(function () {
        $(this).on(resize + " " + scroll + " " + orient, function (e) {
          if (that.opened()) that.positionDropdown();
        });
      });


    },

    // abstract
    close: function () {
      if (!this.opened()) return;

      var cid = this.containerEventName,
          scroll = "scroll." + cid,
          resize = "resize." + cid,
          orient = "orientationchange." + cid;

      // unbind event listeners
      this.container.parents().add(window).each(function () { $(this).off(scroll).off(resize).off(orient); });

      this.clearDropdownAlignmentPreference();

      $("#select2-drop-mask").hide();
      this.dropdown.removeAttr("id"); // only the active dropdown has the select2-drop id
      this.dropdown.hide();
      this.container.removeClass("select2-dropdown-open").removeClass("select2-container-active");
      this.results.empty();

      // Now that the dropdown is closed, unbind the global document mousemove event
      $document.off("mousemove.select2Event");

      this.clearSearch();
      this.search.removeClass("select2-active");
      this.opts.element.trigger($.Event("select2-close"));
    },

    /**
     * Opens control, sets input value, and updates results.
     */
    // abstract
    externalSearch: function (term) {
      this.open();
      this.search.val(term);
      this.updateResults(false);
    },

    // abstract
    clearSearch: function () {

    },

    //abstract
    getMaximumSelectionSize: function () {
      return evaluate(this.opts.maximumSelectionSize, this.opts.element);
    },

    // abstract
    ensureHighlightVisible: function () {
      var results = this.results, children, index, child, hb, rb, y, more, topOffset;

      index = this.highlight();

      if (index < 0) return;

      if (index == 0) {

        // if the first element is highlighted scroll all the way to the top,
        // that way any unselectable headers above it will also be scrolled
        // into view

        results.scrollTop(0);
        return;
      }

      children = this.findHighlightableChoices().find('.select2-result-label');

      child = $(children[index]);

      topOffset = (child.offset() || {}).top || 0;

      hb = topOffset + child.outerHeight(true);

      // if this is the last child lets also make sure select2-more-results is visible
      if (index === children.length - 1) {
        more = results.find("li.select2-more-results");
        if (more.length > 0) {
          hb = more.offset().top + more.outerHeight(true);
        }
      }

      rb = results.offset().top + results.outerHeight(true);
      if (hb > rb) {
        results.scrollTop(results.scrollTop() + (hb - rb));
      }
      y = topOffset - results.offset().top;

      // make sure the top of the element is visible
      if (y < 0 && child.css('display') != 'none') {
        results.scrollTop(results.scrollTop() + y); // y is negative
      }
    },

    // abstract
    findHighlightableChoices: function () {
      return this.results.find(".select2-result-selectable:not(.select2-disabled):not(.select2-selected)");
    },

    // abstract
    moveHighlight: function (delta) {
      var choices = this.findHighlightableChoices(),
          index = this.highlight();

      while (index > -1 && index < choices.length) {
        index += delta;
        var choice = $(choices[index]);
        if (choice.hasClass("select2-result-selectable") && !choice.hasClass("select2-disabled") && !choice.hasClass("select2-selected")) {
          this.highlight(index);
          break;
        }
      }
    },

    // abstract
    highlight: function (index) {
      var choices = this.findHighlightableChoices(),
          choice,
          data;

      if (arguments.length === 0) {
        return indexOf(choices.filter(".select2-highlighted")[0], choices.get());
      }

      if (index >= choices.length) index = choices.length - 1;
      if (index < 0) index = 0;

      this.removeHighlight();

      choice = $(choices[index]);
      choice.addClass("select2-highlighted");

      // ensure assistive technology can determine the active choice
      this.search.attr("aria-activedescendant", choice.find(".select2-result-label").attr("id"));

      this.ensureHighlightVisible();

      this.liveRegion.text(choice.text());

      data = choice.data("select2-data");
      if (data) {
        this.opts.element.trigger({ type: "select2-highlight", val: this.id(data), choice: data });
      }
    },

    removeHighlight: function () {
      this.results.find(".select2-highlighted").removeClass("select2-highlighted");
    },

    touchMoved: function () {
      this._touchMoved = true;
    },

    clearTouchMoved: function () {
      this._touchMoved = false;
    },

    // abstract
    countSelectableResults: function () {
      return this.findHighlightableChoices().length;
    },

    // abstract
    highlightUnderEvent: function (event) {
      var el = $(event.target).closest(".select2-result-selectable");
      if (el.length > 0 && !el.is(".select2-highlighted")) {
        var choices = this.findHighlightableChoices();
        this.highlight(choices.index(el));
      } else if (el.length == 0) {
        // if we are over an unselectable item remove all highlights
        this.removeHighlight();
      }
    },

    // abstract
    loadMoreIfNeeded: function () {
      var results = this.results,
          more = results.find("li.select2-more-results"),
          below, // pixels the element is below the scroll fold, below==0 is when the element is starting to be visible
          page = this.resultsPage + 1,
          self = this,
          term = this.search.val(),
          context = this.context;

      if (more.length === 0) return;
      below = more.offset().top - results.offset().top - results.height();

      if (below <= this.opts.loadMorePadding) {
        more.addClass("select2-active");
        this.opts.query({
          element: this.opts.element,
          term: term,
          page: page,
          context: context,
          matcher: this.opts.matcher,
          callback: this.bind(function (data) {

            // ignore a response if the select2 has been closed before it was received
            if (!self.opened()) return;


            self.opts.populateResults.call(this, results, data.results, { term: term, page: page, context: context });
            self.postprocessResults(data, false, false);

            if (data.more === true) {
              more.detach().appendTo(results).text(evaluate(self.opts.formatLoadMore, self.opts.element, page + 1));
              window.setTimeout(function () { self.loadMoreIfNeeded(); }, 10);
            } else {
              more.remove();
            }
            self.positionDropdown();
            self.resultsPage = page;
            self.context = data.context;
            this.opts.element.trigger({ type: "select2-loaded", items: data });
          })
        });
      }
    },

    /**
     * Default tokenizer function which does nothing
     */
    tokenize: function () {

    },

    /**
     * @param initial whether or not this is the call to this method right after the dropdown has been opened
     */
    // abstract
    updateResults: function (initial) {
      var search = this.search,
          results = this.results,
          opts = this.opts,
          data,
          self = this,
          input,
          term = search.val(),
          lastTerm = $.data(this.container, "select2-last-term"),
          // sequence number used to drop out-of-order responses
          queryNumber;

      // prevent duplicate queries against the same term
      if (initial !== true && lastTerm && equal(term, lastTerm)) return;

      $.data(this.container, "select2-last-term", term);

      // if the search is currently hidden we do not alter the results
      if (initial !== true && (this.showSearchInput === false || !this.opened())) {
        return;
      }

      function postRender() {
        search.removeClass("select2-active");
        self.positionDropdown();
        if (results.find('.select2-no-results,.select2-selection-limit,.select2-searching').length) {
          self.liveRegion.text(results.text());
        }
        else {
          self.liveRegion.text(self.opts.formatMatches(results.find('.select2-result-selectable').length));
        }
      }

      function render(html) {
        results.html(html);
        postRender();
      }

      queryNumber = ++this.queryCount;

      var maxSelSize = this.getMaximumSelectionSize();
      if (maxSelSize >= 1) {
        data = this.data();
        if ($.isArray(data) && data.length >= maxSelSize && checkFormatter(opts.formatSelectionTooBig, "formatSelectionTooBig")) {
          render("<li class='select2-selection-limit'>" + evaluate(opts.formatSelectionTooBig, opts.element, maxSelSize) + "</li>");
          return;
        }
      }

      if (search.val().length < opts.minimumInputLength) {
        if (checkFormatter(opts.formatInputTooShort, "formatInputTooShort")) {
          render("<li class='select2-no-results'>" + evaluate(opts.formatInputTooShort, opts.element, search.val(), opts.minimumInputLength) + "</li>");
        } else {
          render("");
        }
        if (initial && this.showSearch) this.showSearch(true);
        return;
      }

      if (opts.maximumInputLength && search.val().length > opts.maximumInputLength) {
        if (checkFormatter(opts.formatInputTooLong, "formatInputTooLong")) {
          render("<li class='select2-no-results'>" + evaluate(opts.formatInputTooLong, opts.element, search.val(), opts.maximumInputLength) + "</li>");
        } else {
          render("");
        }
        return;
      }

      if (opts.formatSearching && this.findHighlightableChoices().length === 0) {
        render("<li class='select2-searching'>" + evaluate(opts.formatSearching, opts.element) + "</li>");
      }

      search.addClass("select2-active");

      this.removeHighlight();

      // give the tokenizer a chance to pre-process the input
      input = this.tokenize();
      if (input != undefined && input != null) {
        search.val(input);
      }

      this.resultsPage = 1;

      opts.query({
        element: opts.element,
        term: search.val(),
        page: this.resultsPage,
        context: null,
        matcher: opts.matcher,
        callback: this.bind(function (data) {
          var def; // default choice

          // ignore old responses
          if (queryNumber != this.queryCount) {
            return;
          }

          // ignore a response if the select2 has been closed before it was received
          if (!this.opened()) {
            this.search.removeClass("select2-active");
            return;
          }

          // handle ajax error
          if (data.hasError !== undefined && checkFormatter(opts.formatAjaxError, "formatAjaxError")) {
            render("<li class='select2-ajax-error'>" + evaluate(opts.formatAjaxError, opts.element, data.jqXHR, data.textStatus, data.errorThrown) + "</li>");
            return;
          }

          // save context, if any
          this.context = (data.context === undefined) ? null : data.context;
          // create a default choice and prepend it to the list
          if (this.opts.createSearchChoice && search.val() !== "") {
            def = this.opts.createSearchChoice.call(self, search.val(), data.results);
            if (def !== undefined && def !== null && self.id(def) !== undefined && self.id(def) !== null) {
              if ($(data.results).filter(
                  function () {
                      return equal(self.id(this), self.id(def));
              }).length === 0) {
                this.opts.createSearchChoicePosition(data.results, def);
              }
            }
          }

          if (data.results.length === 0 && checkFormatter(opts.formatNoMatches, "formatNoMatches")) {
            render("<li class='select2-no-results'>" + evaluate(opts.formatNoMatches, opts.element, search.val()) + "</li>");
            return;
          }

          results.empty();
          self.opts.populateResults.call(this, results, data.results, { term: search.val(), page: this.resultsPage, context: null });

          if (data.more === true && checkFormatter(opts.formatLoadMore, "formatLoadMore")) {
            results.append("<li class='select2-more-results'>" + opts.escapeMarkup(evaluate(opts.formatLoadMore, opts.element, this.resultsPage)) + "</li>");
            window.setTimeout(function () { self.loadMoreIfNeeded(); }, 10);
          }

          this.postprocessResults(data, initial);

          postRender();

          this.opts.element.trigger({ type: "select2-loaded", items: data });
        })
      });
    },

    // abstract
    cancel: function () {
      this.close();
    },

    // abstract
    blur: function () {
      // if selectOnBlur == true, select the currently highlighted option
      if (this.opts.selectOnBlur)
        this.selectHighlighted({ noFocus: true });

      this.close();
      this.container.removeClass("select2-container-active");
      // synonymous to .is(':focus'), which is available in jquery >= 1.6
      if (this.search[0] === document.activeElement) { this.search.blur(); }
      this.clearSearch();
      this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
    },

    // abstract
    focusSearch: function () {
      focus(this.search);
    },

    // abstract
    selectHighlighted: function (options) {
      if (this._touchMoved) {
        this.clearTouchMoved();
        return;
      }
      var index = this.highlight(),
          highlighted = this.results.find(".select2-highlighted"),
          data = highlighted.closest('.select2-result').data("select2-data");

      if (data) {
        this.highlight(index);
        this.onSelect(data, options);
      } else if (options && options.noFocus) {
        this.close();
      }
    },

    // abstract
    getPlaceholder: function () {
      var placeholderOption;
      return this.opts.element.attr("placeholder") ||
          this.opts.element.attr("data-placeholder") || // jquery 1.4 compat
          this.opts.element.data("placeholder") ||
          this.opts.placeholder ||
          ((placeholderOption = this.getPlaceholderOption()) !== undefined ? placeholderOption.text() : undefined);
    },

    // abstract
    getPlaceholderOption: function () {
      if (this.select) {
        var firstOption = this.select.children('option').first();
        if (this.opts.placeholderOption !== undefined) {
          //Determine the placeholder option based on the specified placeholderOption setting
          return (this.opts.placeholderOption === "first" && firstOption) ||
                 (typeof this.opts.placeholderOption === "function" && this.opts.placeholderOption(this.select));
        } else if ($.trim(firstOption.text()) === "" && firstOption.val() === "") {
          //No explicit placeholder option specified, use the first if it's blank
          return firstOption;
        }
      }
    },

    /**
     * Get the desired width for the container element.  This is
     * derived first from option `width` passed to select2, then
     * the inline 'style' on the original element, and finally
     * falls back to the jQuery calculated element width.
     */
    // abstract
    initContainerWidth: function () {
      function resolveContainerWidth() {
        var style, attrs, matches, i, l, attr;

        if (this.opts.width === "off") {
          return null;
        } else if (this.opts.width === "element") {
          return this.opts.element.outerWidth(false) === 0 ? 'auto' : this.opts.element.outerWidth(false) + 'px';
        } else if (this.opts.width === "copy" || this.opts.width === "resolve") {
          // check if there is inline style on the element that contains width
          style = this.opts.element.attr('style');
          if (style !== undefined) {
            attrs = style.split(';');
            for (i = 0, l = attrs.length; i < l; i = i + 1) {
              attr = attrs[i].replace(/\s/g, '');
              matches = attr.match(/^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i);
              if (matches !== null && matches.length >= 1)
                return matches[1];
            }
          }

          if (this.opts.width === "resolve") {
            // next check if css('width') can resolve a width that is percent based, this is sometimes possible
            // when attached to input type=hidden or elements hidden via css
            style = this.opts.element.css('width');
            if (style.indexOf("%") > 0) return style;

            // finally, fallback on the calculated width of the element
            return (this.opts.element.outerWidth(false) === 0 ? 'auto' : this.opts.element.outerWidth(false) + 'px');
          }

          return null;
        } else if ($.isFunction(this.opts.width)) {
          return this.opts.width();
        } else {
          return this.opts.width;
        }
      };

      var width = resolveContainerWidth.call(this);
      if (width !== null) {
        this.container.css("width", width);
      }
    }
  });

  SingleSelect2 = clazz(AbstractSelect2, {

    // single

    createContainer: function () {
      var container = $(document.createElement("div")).attr({
        "class": "select2-container"
      }).html([
          "<a href='javascript:void(0)' class='select2-choice' tabindex='-1'>",
          "   <span class='select2-chosen'>&#160;</span><abbr class='select2-search-choice-close'></abbr>",
          "   <span class='select2-arrow' role='presentation'><b role='presentation'></b></span>",
          "</a>",
          "<label for='' class='select2-offscreen'></label>",
          "<input class='select2-focusser select2-offscreen' type='text' aria-haspopup='true' role='button' />",
          "<div class='select2-drop select2-display-none'>",
          "   <div class='select2-search'>",
          "       <label for='' class='select2-offscreen'></label>",
          "       <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input' role='combobox' aria-expanded='true'",
          "       aria-autocomplete='list' />",
          "   </div>",
          "   <ul class='select2-results' role='listbox'>",
          "   </ul>",
          "</div>"].join(""));
      return container;
    },

    // single
    enableInterface: function () {
      if (this.parent.enableInterface.apply(this, arguments)) {
        this.focusser.prop("disabled", !this.isInterfaceEnabled());
      }
    },

    // single
    opening: function () {
      var el, range, len;

      if (this.opts.minimumResultsForSearch >= 0) {
        this.showSearch(true);
      }

      this.parent.opening.apply(this, arguments);

      if (this.showSearchInput !== false) {
        // IE appends focusser.val() at the end of field :/ so we manually insert it at the beginning using a range
        // all other browsers handle this just fine

        this.search.val(this.focusser.val());
      }
      if (this.opts.shouldFocusInput(this)) {
        this.search.focus();
        // move the cursor to the end after focussing, otherwise it will be at the beginning and
        // new text will appear *before* focusser.val()
        el = this.search.get(0);
        if (el.createTextRange) {
          range = el.createTextRange();
          range.collapse(false);
          range.select();
        } else if (el.setSelectionRange) {
          len = this.search.val().length;
          el.setSelectionRange(len, len);
        }
      }

      // initializes search's value with nextSearchTerm (if defined by user)
      // ignore nextSearchTerm if the dropdown is opened by the user pressing a letter
      if (this.search.val() === "") {
        if (this.nextSearchTerm != undefined) {
          this.search.val(this.nextSearchTerm);
          this.search.select();
        }
      }

      this.focusser.prop("disabled", true).val("");
      this.updateResults(true);
      this.opts.element.trigger($.Event("select2-open"));
    },

    // single
    close: function () {
      if (!this.opened()) return;
      this.parent.close.apply(this, arguments);

      this.focusser.prop("disabled", false);

      if (this.opts.shouldFocusInput(this)) {
        this.focusser.focus();
      }
    },

    // single
    focus: function () {
      if (this.opened()) {
        this.close();
      } else {
        this.focusser.prop("disabled", false);
        if (this.opts.shouldFocusInput(this)) {
          this.focusser.focus();
        }
      }
    },

    // single
    isFocused: function () {
      return this.container.hasClass("select2-container-active");
    },

    // single
    cancel: function () {
      this.parent.cancel.apply(this, arguments);
      this.focusser.prop("disabled", false);

      if (this.opts.shouldFocusInput(this)) {
        this.focusser.focus();
      }
    },

    // single
    destroy: function () {
      $("label[for='" + this.focusser.attr('id') + "']")
          .attr('for', this.opts.element.attr("id"));
      this.parent.destroy.apply(this, arguments);

      cleanupJQueryElements.call(this,
          "selection",
          "focusser"
      );
    },

    // single
    initContainer: function () {

      var selection,
          container = this.container,
          dropdown = this.dropdown,
          idSuffix = nextUid(),
          elementLabel;

      if (this.opts.minimumResultsForSearch < 0) {
        this.showSearch(false);
      } else {
        this.showSearch(true);
      }

      this.selection = selection = container.find(".select2-choice");

      this.focusser = container.find(".select2-focusser");

      // add aria associations
      selection.find(".select2-chosen").attr("id", "select2-chosen-" + idSuffix);
      this.focusser.attr("aria-labelledby", "select2-chosen-" + idSuffix);
      this.results.attr("id", "select2-results-" + idSuffix);
      this.search.attr("aria-owns", "select2-results-" + idSuffix);

      // rewrite labels from original element to focusser
      this.focusser.attr("id", "s2id_autogen" + idSuffix);

      elementLabel = $("label[for='" + this.opts.element.attr("id") + "']");

      this.focusser.prev()
          .text(elementLabel.text())
          .attr('for', this.focusser.attr('id'));

      // Ensure the original element retains an accessible name
      var originalTitle = this.opts.element.attr("title");
      this.opts.element.attr("title", (originalTitle || elementLabel.text()));

      this.focusser.attr("tabindex", this.elementTabIndex);

      // write label for search field using the label from the focusser element
      this.search.attr("id", this.focusser.attr('id') + '_search');

      this.search.prev()
          .text($("label[for='" + this.focusser.attr('id') + "']").text())
          .attr('for', this.search.attr('id'));

      this.search.on("keydown", this.bind(function (e) {
        if (!this.isInterfaceEnabled()) return;

        // filter 229 keyCodes (input method editor is processing key input)
        if (229 == e.keyCode) return;

        if (e.which === KEY.PAGE_UP || e.which === KEY.PAGE_DOWN) {
          // prevent the page from scrolling
          killEvent(e);
          return;
        }

        switch (e.which) {
          case KEY.UP:
          case KEY.DOWN:
            this.moveHighlight((e.which === KEY.UP) ? -1 : 1);
            killEvent(e);
            return;
          case KEY.ENTER:
            this.selectHighlighted();
            killEvent(e);
            return;
          case KEY.TAB:
            this.selectHighlighted({ noFocus: true });
            return;
          case KEY.ESC:
            this.cancel(e);
            killEvent(e);
            return;
        }
      }));

      this.search.on("blur", this.bind(function (e) {
        // a workaround for chrome to keep the search field focussed when the scroll bar is used to scroll the dropdown.
        // without this the search field loses focus which is annoying
        if (document.activeElement === this.body.get(0)) {
          window.setTimeout(this.bind(function () {
            if (this.opened()) {
              this.search.focus();
            }
          }), 0);
        }
      }));

      this.focusser.on("keydown", this.bind(function (e) {
        if (!this.isInterfaceEnabled()) return;

        if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e) || e.which === KEY.ESC) {
          return;
        }

        if (this.opts.openOnEnter === false && e.which === KEY.ENTER) {
          killEvent(e);
          return;
        }

        if (e.which == KEY.DOWN || e.which == KEY.UP
            || (e.which == KEY.ENTER && this.opts.openOnEnter)) {

          if (e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) return;

          this.open();
          killEvent(e);
          return;
        }

        if (e.which == KEY.DELETE || e.which == KEY.BACKSPACE) {
          if (this.opts.allowClear) {
            this.clear();
          }
          killEvent(e);
          return;
        }
      }));


      installKeyUpChangeEvent(this.focusser);
      this.focusser.on("keyup-change input", this.bind(function (e) {
        if (this.opts.minimumResultsForSearch >= 0) {
          e.stopPropagation();
          if (this.opened()) return;
          this.open();
        }
      }));

      selection.on("mousedown touchstart", "abbr", this.bind(function (e) {
        if (!this.isInterfaceEnabled()) return;
        this.clear();
        killEventImmediately(e);
        this.close();
        this.selection.focus();
      }));

      selection.on("mousedown touchstart", this.bind(function (e) {
        // Prevent IE from generating a click event on the body
        reinsertElement(selection);

        if (!this.container.hasClass("select2-container-active")) {
          this.opts.element.trigger($.Event("select2-focus"));
        }

        if (this.opened()) {
          this.close();
        } else if (this.isInterfaceEnabled()) {
          this.open();
        }

        killEvent(e);
      }));

      dropdown.on("mousedown touchstart", this.bind(function () {
        if (this.opts.shouldFocusInput(this)) {
          this.search.focus();
        }
      }));

      selection.on("focus", this.bind(function (e) {
        killEvent(e);
      }));

      this.focusser.on("focus", this.bind(function () {
        if (!this.container.hasClass("select2-container-active")) {
          this.opts.element.trigger($.Event("select2-focus"));
        }
        this.container.addClass("select2-container-active");
      })).on("blur", this.bind(function () {
        if (!this.opened()) {
          this.container.removeClass("select2-container-active");
          this.opts.element.trigger($.Event("select2-blur"));
        }
      }));
      this.search.on("focus", this.bind(function () {
        if (!this.container.hasClass("select2-container-active")) {
          this.opts.element.trigger($.Event("select2-focus"));
        }
        this.container.addClass("select2-container-active");
      }));

      this.initContainerWidth();
      this.opts.element.addClass("select2-offscreen");
      this.setPlaceholder();

    },

    // single
    clear: function (triggerChange) {
      var data = this.selection.data("select2-data");
      if (data) { // guard against queued quick consecutive clicks
        var evt = $.Event("select2-clearing");
        this.opts.element.trigger(evt);
        if (evt.isDefaultPrevented()) {
          return;
        }
        var placeholderOption = this.getPlaceholderOption();
        this.opts.element.val(placeholderOption ? placeholderOption.val() : "");
        this.selection.find(".select2-chosen").empty();
        this.selection.removeData("select2-data");
        this.setPlaceholder();

        if (triggerChange !== false) {
          this.opts.element.trigger({ type: "select2-removed", val: this.id(data), choice: data });
          this.triggerChange({ removed: data });
        }
      }
    },

    /**
     * Sets selection based on source element's value
     */
    // single
    initSelection: function () {
      var selected;
      if (this.isPlaceholderOptionSelected()) {
        this.updateSelection(null);
        this.close();
        this.setPlaceholder();
      } else {
        var self = this;
        this.opts.initSelection.call(null, this.opts.element, function (selected) {
          if (selected !== undefined && selected !== null) {
            self.updateSelection(selected);
            self.close();
            self.setPlaceholder();
            self.nextSearchTerm = self.opts.nextSearchTerm(selected, self.search.val());
          }
        });
      }
    },

    isPlaceholderOptionSelected: function () {
      var placeholderOption;
      if (this.getPlaceholder() === undefined) return false; // no placeholder specified so no option should be considered
      return ((placeholderOption = this.getPlaceholderOption()) !== undefined && placeholderOption.prop("selected"))
          || (this.opts.element.val() === "")
          || (this.opts.element.val() === undefined)
          || (this.opts.element.val() === null);
    },

    // single
    prepareOpts: function () {
      var opts = this.parent.prepareOpts.apply(this, arguments),
          self = this;

      if (opts.element.get(0).tagName.toLowerCase() === "select") {
        // install the selection initializer
        opts.initSelection = function (element, callback) {
          var selected = element.find("option").filter(function () { return this.selected && !this.disabled });
          // a single select box always has a value, no need to null check 'selected'
          callback(self.optionToData(selected));
        };
      } else if ("data" in opts) {
        // install default initSelection when applied to hidden input and data is local
        opts.initSelection = opts.initSelection || function (element, callback) {
          var id = element.val();
          //search in data by id, storing the actual matching item
          var match = null;
          opts.query({
            matcher: function (term, text, el) {
              var is_match = equal(id, opts.id(el));
              if (is_match) {
                match = el;
              }
              return is_match;
            },
            callback: !$.isFunction(callback) ? $.noop : function () {
              callback(match);
            }
          });
        };
      }

      return opts;
    },

    // single
    getPlaceholder: function () {
      // if a placeholder is specified on a single select without a valid placeholder option ignore it
      if (this.select) {
        if (this.getPlaceholderOption() === undefined) {
          return undefined;
        }
      }

      return this.parent.getPlaceholder.apply(this, arguments);
    },

    // single
    setPlaceholder: function () {
      var placeholder = this.getPlaceholder();

      if (this.isPlaceholderOptionSelected() && placeholder !== undefined) {

        // check for a placeholder option if attached to a select
        if (this.select && this.getPlaceholderOption() === undefined) return;

        this.selection.find(".select2-chosen").html(this.opts.escapeMarkup(placeholder));

        this.selection.addClass("select2-default");

        this.container.removeClass("select2-allowclear");
      }
    },

    // single
    postprocessResults: function (data, initial, noHighlightUpdate) {
      var selected = 0, self = this, showSearchInput = true;

      // find the selected element in the result list

      this.findHighlightableChoices().each2(function (i, elm) {
        if (equal(self.id(elm.data("select2-data")), self.opts.element.val())) {
          selected = i;
          return false;
        }
      });

      // and highlight it
      if (noHighlightUpdate !== false) {
        if (initial === true && selected >= 0) {
          this.highlight(selected);
        } else {
          this.highlight(0);
        }
      }

      // hide the search box if this is the first we got the results and there are enough of them for search

      if (initial === true) {
        var min = this.opts.minimumResultsForSearch;
        if (min >= 0) {
          this.showSearch(countResults(data.results) >= min);
        }
      }
    },

    // single
    showSearch: function (showSearchInput) {
      if (this.showSearchInput === showSearchInput) return;

      this.showSearchInput = showSearchInput;

      this.dropdown.find(".select2-search").toggleClass("select2-search-hidden", !showSearchInput);
      this.dropdown.find(".select2-search").toggleClass("select2-offscreen", !showSearchInput);
      //add "select2-with-searchbox" to the container if search box is shown
      $(this.dropdown, this.container).toggleClass("select2-with-searchbox", showSearchInput);
    },

    // single
    onSelect: function (data, options) {

      if (!this.triggerSelect(data)) { return; }

      var old = this.opts.element.val(),
          oldData = this.data();

      this.opts.element.val(this.id(data));
      this.updateSelection(data);

      this.opts.element.trigger({ type: "select2-selected", val: this.id(data), choice: data });

      this.nextSearchTerm = this.opts.nextSearchTerm(data, this.search.val());
      this.close();

      if ((!options || !options.noFocus) && this.opts.shouldFocusInput(this)) {
        this.focusser.focus();
      }

      if (!equal(old, this.id(data))) {
        this.triggerChange({ added: data, removed: oldData });
      }
    },

    // single
    updateSelection: function (data) {

      var container = this.selection.find(".select2-chosen"), formatted, cssClass;

      this.selection.data("select2-data", data);

      container.empty();
      if (data !== null) {
        formatted = this.opts.formatSelection(data, container, this.opts.escapeMarkup);
      }
      if (formatted !== undefined) {
        container.append(formatted);
      }
      cssClass = this.opts.formatSelectionCssClass(data, container);
      if (cssClass !== undefined) {
        container.addClass(cssClass);
      }

      this.selection.removeClass("select2-default");

      if (this.opts.allowClear && this.getPlaceholder() !== undefined) {
        this.container.addClass("select2-allowclear");
      }
    },

    // single
    val: function () {
      var val,
          triggerChange = false,
          data = null,
          self = this,
          oldData = this.data();

      if (arguments.length === 0) {
        return this.opts.element.val();
      }

      val = arguments[0];

      if (arguments.length > 1) {
        triggerChange = arguments[1];
      }

      if (this.select) {
        this.select
            .val(val)
            .find("option").filter(function () { return this.selected }).each2(function (i, elm) {
              data = self.optionToData(elm);
              return false;
            });
        this.updateSelection(data);
        this.setPlaceholder();
        if (triggerChange) {
          this.triggerChange({ added: data, removed: oldData });
        }
      } else {
        // val is an id. !val is true for [undefined,null,'',0] - 0 is legal
        if (!val && val !== 0) {
          this.clear(triggerChange);
          return;
        }
        if (this.opts.initSelection === undefined) {
          throw new Error("cannot call val() if initSelection() is not defined");
        }
        this.opts.element.val(val);
        this.opts.initSelection(this.opts.element, function (data) {
          self.opts.element.val(!data ? "" : self.id(data));
          self.updateSelection(data);
          self.setPlaceholder();
          if (triggerChange) {
            self.triggerChange({ added: data, removed: oldData });
          }
        });
      }
    },

    // single
    clearSearch: function () {
      this.search.val("");
      this.focusser.val("");
    },

    // single
    data: function (value) {
      var data,
          triggerChange = false;

      if (arguments.length === 0) {
        data = this.selection.data("select2-data");
        if (data == undefined) data = null;
        return data;
      } else {
        if (arguments.length > 1) {
          triggerChange = arguments[1];
        }
        if (!value) {
          this.clear(triggerChange);
        } else {
          data = this.data();
          this.opts.element.val(!value ? "" : this.id(value));
          this.updateSelection(value);
          if (triggerChange) {
            this.triggerChange({ added: value, removed: data });
          }
        }
      }
    }
  });

  MultiSelect2 = clazz(AbstractSelect2, {

    // multi
    createContainer: function () {
      var container = $(document.createElement("div")).attr({
        "class": "select2-container select2-container-multi"
      }).html([
          "<ul class='select2-choices'>",
          "  <li class='select2-search-field'>",
          "    <label for='' class='select2-offscreen'></label>",
          "    <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input'>",
          "  </li>",
          "</ul>",
          "<div class='select2-drop select2-drop-multi select2-display-none'>",
          "   <ul class='select2-results'>",
          "   </ul>",
          "</div>"].join(""));
      return container;
    },

    // multi
    prepareOpts: function () {
      var opts = this.parent.prepareOpts.apply(this, arguments),
          self = this;

      // TODO validate placeholder is a string if specified

      if (opts.element.get(0).tagName.toLowerCase() === "select") {
        // install the selection initializer
        opts.initSelection = function (element, callback) {

          var data = [];

          element.find("option").filter(function () { return this.selected && !this.disabled }).each2(function (i, elm) {
            data.push(self.optionToData(elm));
          });
          callback(data);
        };
      } else if ("data" in opts) {
        // install default initSelection when applied to hidden input and data is local
        opts.initSelection = opts.initSelection || function (element, callback) {
          var ids = splitVal(element.val(), opts.separator);
          //search in data by array of ids, storing matching items in a list
          var matches = [];
          opts.query({
            matcher: function (term, text, el) {
              var is_match = $.grep(ids, function (id) {
                return equal(id, opts.id(el));
              }).length;
              if (is_match) {
                matches.push(el);
              }
              return is_match;
            },
            callback: !$.isFunction(callback) ? $.noop : function () {
              // reorder matches based on the order they appear in the ids array because right now
              // they are in the order in which they appear in data array
              var ordered = [];
              for (var i = 0; i < ids.length; i++) {
                var id = ids[i];
                for (var j = 0; j < matches.length; j++) {
                  var match = matches[j];
                  if (equal(id, opts.id(match))) {
                    ordered.push(match);
                    matches.splice(j, 1);
                    break;
                  }
                }
              }
              callback(ordered);
            }
          });
        };
      }

      return opts;
    },

    // multi
    selectChoice: function (choice) {

      var selected = this.container.find(".select2-search-choice-focus");
      if (selected.length && choice && choice[0] == selected[0]) {

      } else {
        if (selected.length) {
          this.opts.element.trigger("choice-deselected", selected);
        }
        selected.removeClass("select2-search-choice-focus");
        if (choice && choice.length) {
          this.close();
          choice.addClass("select2-search-choice-focus");
          this.opts.element.trigger("choice-selected", choice);
        }
      }
    },

    // multi
    destroy: function () {
      $("label[for='" + this.search.attr('id') + "']")
          .attr('for', this.opts.element.attr("id"));
      this.parent.destroy.apply(this, arguments);

      cleanupJQueryElements.call(this,
          "searchContainer",
          "selection"
      );
    },

    // multi
    initContainer: function () {

      var selector = ".select2-choices", selection;

      this.searchContainer = this.container.find(".select2-search-field");
      this.selection = selection = this.container.find(selector);

      var _this = this;
      this.selection.on("click", ".select2-search-choice:not(.select2-locked)", function (e) {
        //killEvent(e);
        _this.search[0].focus();
        _this.selectChoice($(this));
      });

      // rewrite labels from original element to focusser
      this.search.attr("id", "s2id_autogen" + nextUid());

      this.search.prev()
          .text($("label[for='" + this.opts.element.attr("id") + "']").text())
          .attr('for', this.search.attr('id'));

      this.search.on("input paste", this.bind(function () {
        if (this.search.attr('placeholder') && this.search.val().length == 0) return;
        if (!this.isInterfaceEnabled()) return;
        if (!this.opened()) {
          this.open();
        }
      }));

      this.search.attr("tabindex", this.elementTabIndex);

      this.keydowns = 0;
      this.search.on("keydown", this.bind(function (e) {
        if (!this.isInterfaceEnabled()) return;

        ++this.keydowns;
        var selected = selection.find(".select2-search-choice-focus");
        var prev = selected.prev(".select2-search-choice:not(.select2-locked)");
        var next = selected.next(".select2-search-choice:not(.select2-locked)");
        var pos = getCursorInfo(this.search);

        if (selected.length &&
            (e.which == KEY.LEFT || e.which == KEY.RIGHT || e.which == KEY.BACKSPACE || e.which == KEY.DELETE || e.which == KEY.ENTER)) {
          var selectedChoice = selected;
          if (e.which == KEY.LEFT && prev.length) {
            selectedChoice = prev;
          }
          else if (e.which == KEY.RIGHT) {
            selectedChoice = next.length ? next : null;
          }
          else if (e.which === KEY.BACKSPACE) {
            if (this.unselect(selected.first())) {
              this.search.width(10);
              selectedChoice = prev.length ? prev : next;
            }
          } else if (e.which == KEY.DELETE) {
            if (this.unselect(selected.first())) {
              this.search.width(10);
              selectedChoice = next.length ? next : null;
            }
          } else if (e.which == KEY.ENTER) {
            selectedChoice = null;
          }

          this.selectChoice(selectedChoice);
          killEvent(e);
          if (!selectedChoice || !selectedChoice.length) {
            this.open();
          }
          return;
        } else if (((e.which === KEY.BACKSPACE && this.keydowns == 1)
            || e.which == KEY.LEFT) && (pos.offset == 0 && !pos.length)) {

          this.selectChoice(selection.find(".select2-search-choice:not(.select2-locked)").last());
          killEvent(e);
          return;
        } else {
          this.selectChoice(null);
        }

        if (this.opened()) {
          switch (e.which) {
            case KEY.UP:
            case KEY.DOWN:
              this.moveHighlight((e.which === KEY.UP) ? -1 : 1);
              killEvent(e);
              return;
            case KEY.ENTER:
              this.selectHighlighted();
              killEvent(e);
              return;
            case KEY.TAB:
              this.selectHighlighted({ noFocus: true });
              this.close();
              return;
            case KEY.ESC:
              this.cancel(e);
              killEvent(e);
              return;
          }
        }

        if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e)
         || e.which === KEY.BACKSPACE || e.which === KEY.ESC) {
          return;
        }

        if (e.which === KEY.ENTER) {
          if (this.opts.openOnEnter === false) {
            return;
          } else if (e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
            return;
          }
        }

        this.open();

        if (e.which === KEY.PAGE_UP || e.which === KEY.PAGE_DOWN) {
          // prevent the page from scrolling
          killEvent(e);
        }

        if (e.which === KEY.ENTER) {
          // prevent form from being submitted
          killEvent(e);
        }

      }));

      this.search.on("keyup", this.bind(function (e) {
        this.keydowns = 0;
        this.resizeSearch();
      })
      );

      this.search.on("blur", this.bind(function (e) {
        this.container.removeClass("select2-container-active");
        this.search.removeClass("select2-focused");
        this.selectChoice(null);
        if (!this.opened()) this.clearSearch();
        e.stopImmediatePropagation();
        this.opts.element.trigger($.Event("select2-blur"));
      }));

      this.container.on("click", selector, this.bind(function (e) {
        if (!this.isInterfaceEnabled()) return;
        if ($(e.target).closest(".select2-search-choice").length > 0) {
          // clicked inside a select2 search choice, do not open
          return;
        }
        this.selectChoice(null);
        this.clearPlaceholder();
        if (!this.container.hasClass("select2-container-active")) {
          this.opts.element.trigger($.Event("select2-focus"));
        }
        this.open();
        this.focusSearch();
        e.preventDefault();
      }));

      this.container.on("focus", selector, this.bind(function () {
        if (!this.isInterfaceEnabled()) return;
        if (!this.container.hasClass("select2-container-active")) {
          this.opts.element.trigger($.Event("select2-focus"));
        }
        this.container.addClass("select2-container-active");
        this.dropdown.addClass("select2-drop-active");
        this.clearPlaceholder();
      }));

      this.initContainerWidth();
      this.opts.element.addClass("select2-offscreen");

      // set the placeholder if necessary
      this.clearSearch();
    },

    // multi
    enableInterface: function () {
      if (this.parent.enableInterface.apply(this, arguments)) {
        this.search.prop("disabled", !this.isInterfaceEnabled());
      }
    },

    // multi
    initSelection: function () {
      var data;
      if (this.opts.element.val() === "" && this.opts.element.text() === "") {
        this.updateSelection([]);
        this.close();
        // set the placeholder if necessary
        this.clearSearch();
      }
      if (this.select || this.opts.element.val() !== "") {
        var self = this;
        this.opts.initSelection.call(null, this.opts.element, function (data) {
          if (data !== undefined && data !== null) {
            self.updateSelection(data);
            self.close();
            // set the placeholder if necessary
            self.clearSearch();
          }
        });
      }
    },

    // multi
    clearSearch: function () {
      var placeholder = this.getPlaceholder(),
          maxWidth = this.getMaxSearchWidth();

      if (placeholder !== undefined && this.getVal().length === 0 && this.search.hasClass("select2-focused") === false) {
        this.search.val(placeholder).addClass("select2-default");
        // stretch the search box to full width of the container so as much of the placeholder is visible as possible
        // we could call this.resizeSearch(), but we do not because that requires a sizer and we do not want to create one so early because of a firefox bug, see #944
        this.search.width(maxWidth > 0 ? maxWidth : this.container.css("width"));
      } else {
        this.search.val("").width(10);
      }
    },

    // multi
    clearPlaceholder: function () {
      if (this.search.hasClass("select2-default")) {
        this.search.val("").removeClass("select2-default");
      }
    },

    // multi
    opening: function () {
      this.clearPlaceholder(); // should be done before super so placeholder is not used to search
      this.resizeSearch();

      this.parent.opening.apply(this, arguments);

      this.focusSearch();

      // initializes search's value with nextSearchTerm (if defined by user)
      // ignore nextSearchTerm if the dropdown is opened by the user pressing a letter
      if (this.search.val() === "") {
        if (this.nextSearchTerm != undefined) {
          this.search.val(this.nextSearchTerm);
          this.search.select();
        }
      }

      this.updateResults(true);
      if (this.opts.shouldFocusInput(this)) {
        this.search.focus();
      }
      this.opts.element.trigger($.Event("select2-open"));
    },

    // multi
    close: function () {
      if (!this.opened()) return;
      this.parent.close.apply(this, arguments);
    },

    // multi
    focus: function () {
      this.close();
      this.search.focus();
    },

    // multi
    isFocused: function () {
      return this.search.hasClass("select2-focused");
    },

    // multi
    updateSelection: function (data) {
      var ids = [], filtered = [], self = this;

      // filter out duplicates
      $(data).each(function () {
        if (indexOf(self.id(this), ids) < 0) {
          ids.push(self.id(this));
          filtered.push(this);
        }
      });
      data = filtered;

      this.selection.find(".select2-search-choice").remove();
      $(data).each(function () {
        self.addSelectedChoice(this);
      });
      self.postprocessResults();
    },

    // multi
    tokenize: function () {
      var input = this.search.val();
      input = this.opts.tokenizer.call(this, input, this.data(), this.bind(this.onSelect), this.opts);
      if (input != null && input != undefined) {
        this.search.val(input);
        if (input.length > 0) {
          this.open();
        }
      }

    },

    // multi
    onSelect: function (data, options) {

      if (!this.triggerSelect(data) || data.text === "") { return; }

      this.addSelectedChoice(data);

      this.opts.element.trigger({ type: "selected", val: this.id(data), choice: data });

      // keep track of the search's value before it gets cleared
      this.nextSearchTerm = this.opts.nextSearchTerm(data, this.search.val());

      this.clearSearch();
      this.updateResults();

      if (this.select || !this.opts.closeOnSelect) this.postprocessResults(data, false, this.opts.closeOnSelect === true);

      if (this.opts.closeOnSelect) {
        this.close();
        this.search.width(10);
      } else {
        if (this.countSelectableResults() > 0) {
          this.search.width(10);
          this.resizeSearch();
          if (this.getMaximumSelectionSize() > 0 && this.val().length >= this.getMaximumSelectionSize()) {
            // if we reached max selection size repaint the results so choices
            // are replaced with the max selection reached message
            this.updateResults(true);
          } else {
            // initializes search's value with nextSearchTerm and update search result
            if (this.nextSearchTerm != undefined) {
              this.search.val(this.nextSearchTerm);
              this.updateResults();
              this.search.select();
            }
          }
          this.positionDropdown();
        } else {
          // if nothing left to select close
          this.close();
          this.search.width(10);
        }
      }

      // since its not possible to select an element that has already been
      // added we do not need to check if this is a new element before firing change
      this.triggerChange({ added: data });

      if (!options || !options.noFocus)
        this.focusSearch();
    },

    // multi
    cancel: function () {
      this.close();
      this.focusSearch();
    },

    addSelectedChoice: function (data) {
      var enableChoice = !data.locked,
          enabledItem = $(
              "<li class='select2-search-choice'>" +
              "    <div></div>" +
              "    <a href='#' class='select2-search-choice-close' tabindex='-1'></a>" +
              "</li>"),
          disabledItem = $(
              "<li class='select2-search-choice select2-locked'>" +
              "<div></div>" +
              "</li>");
      var choice = enableChoice ? enabledItem : disabledItem,
          id = this.id(data),
          val = this.getVal(),
          formatted,
          cssClass;

      formatted = this.opts.formatSelection(data, choice.find("div"), this.opts.escapeMarkup);
      if (formatted != undefined) {
        choice.find("div").replaceWith("<div>" + formatted + "</div>");
      }
      cssClass = this.opts.formatSelectionCssClass(data, choice.find("div"));
      if (cssClass != undefined) {
        choice.addClass(cssClass);
      }

      if (enableChoice) {
        choice.find(".select2-search-choice-close")
            .on("mousedown", killEvent)
            .on("click dblclick", this.bind(function (e) {
              if (!this.isInterfaceEnabled()) return;

              this.unselect($(e.target));
              this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
              killEvent(e);
              this.close();
              this.focusSearch();
            })).on("focus", this.bind(function () {
              if (!this.isInterfaceEnabled()) return;
              this.container.addClass("select2-container-active");
              this.dropdown.addClass("select2-drop-active");
            }));
      }

      choice.data("select2-data", data);
      choice.insertBefore(this.searchContainer);

      val.push(id);
      this.setVal(val);
    },

    // multi
    unselect: function (selected) {
      var val = this.getVal(),
          data,
          index;
      selected = selected.closest(".select2-search-choice");

      if (selected.length === 0) {
        throw "Invalid argument: " + selected + ". Must be .select2-search-choice";
      }

      data = selected.data("select2-data");

      if (!data) {
        // prevent a race condition when the 'x' is clicked really fast repeatedly the event can be queued
        // and invoked on an element already removed
        return;
      }

      var evt = $.Event("select2-removing");
      evt.val = this.id(data);
      evt.choice = data;
      this.opts.element.trigger(evt);

      if (evt.isDefaultPrevented()) {
        return false;
      }

      while ((index = indexOf(this.id(data), val)) >= 0) {
        val.splice(index, 1);
        this.setVal(val);
        if (this.select) this.postprocessResults();
      }

      selected.remove();

      this.opts.element.trigger({ type: "select2-removed", val: this.id(data), choice: data });
      this.triggerChange({ removed: data });

      return true;
    },

    // multi
    postprocessResults: function (data, initial, noHighlightUpdate) {
      var val = this.getVal(),
          choices = this.results.find(".select2-result"),
          compound = this.results.find(".select2-result-with-children"),
          self = this;

      choices.each2(function (i, choice) {
        var id = self.id(choice.data("select2-data"));
        if (indexOf(id, val) >= 0) {
          choice.addClass("select2-selected");
          // mark all children of the selected parent as selected
          choice.find(".select2-result-selectable").addClass("select2-selected");
        }
      });

      compound.each2(function (i, choice) {
        // hide an optgroup if it doesn't have any selectable children
        if (!choice.is('.select2-result-selectable')
            && choice.find(".select2-result-selectable:not(.select2-selected)").length === 0) {
          choice.addClass("select2-selected");
        }
      });

      if (this.highlight() == -1 && noHighlightUpdate !== false) {
        self.highlight(0);
      }

      //If all results are chosen render formatNoMatches
      if (!this.opts.createSearchChoice && !choices.filter('.select2-result:not(.select2-selected)').length > 0) {
        if (!data || data && !data.more && this.results.find(".select2-no-results").length === 0) {
          if (checkFormatter(self.opts.formatNoMatches, "formatNoMatches")) {
            this.results.append("<li class='select2-no-results'>" + evaluate(self.opts.formatNoMatches, self.opts.element, self.search.val()) + "</li>");
          }
        }
      }

    },

    // multi
    getMaxSearchWidth: function () {
      return this.selection.width() - getSideBorderPadding(this.search);
    },

    // multi
    resizeSearch: function () {
      var minimumWidth, left, maxWidth, containerLeft, searchWidth,
          sideBorderPadding = getSideBorderPadding(this.search);

      minimumWidth = measureTextWidth(this.search) + 10;

      left = this.search.offset().left;

      maxWidth = this.selection.width();
      containerLeft = this.selection.offset().left;

      searchWidth = maxWidth - (left - containerLeft) - sideBorderPadding;

      if (searchWidth < minimumWidth) {
        searchWidth = maxWidth - sideBorderPadding;
      }

      if (searchWidth < 40) {
        searchWidth = maxWidth - sideBorderPadding;
      }

      if (searchWidth <= 0) {
        searchWidth = minimumWidth;
      }

      this.search.width(Math.floor(searchWidth));
    },

    // multi
    getVal: function () {
      var val;
      if (this.select) {
        val = this.select.val();
        return val === null ? [] : val;
      } else {
        val = this.opts.element.val();
        return splitVal(val, this.opts.separator);
      }
    },

    // multi
    setVal: function (val) {
      var unique;
      if (this.select) {
        this.select.val(val);
      } else {
        unique = [];
        // filter out duplicates
        $(val).each(function () {
          if (indexOf(this, unique) < 0) unique.push(this);
        });
        this.opts.element.val(unique.length === 0 ? "" : unique.join(this.opts.separator));
      }
    },

    // multi
    buildChangeDetails: function (old, current) {
      var current = current.slice(0),
          old = old.slice(0);

      // remove intersection from each array
      for (var i = 0; i < current.length; i++) {
        for (var j = 0; j < old.length; j++) {
          if (equal(this.opts.id(current[i]), this.opts.id(old[j]))) {
            current.splice(i, 1);
            if (i > 0) {
              i--;
            }
            old.splice(j, 1);
            j--;
          }
        }
      }

      return { added: current, removed: old };
    },


    // multi
    val: function (val, triggerChange) {
      var oldData, self = this;

      if (arguments.length === 0) {
        return this.getVal();
      }

      oldData = this.data();
      if (!oldData.length) oldData = [];

      // val is an id. !val is true for [undefined,null,'',0] - 0 is legal
      if (!val && val !== 0) {
        this.opts.element.val("");
        this.updateSelection([]);
        this.clearSearch();
        if (triggerChange) {
          this.triggerChange({ added: this.data(), removed: oldData });
        }
        return;
      }

      // val is a list of ids
      this.setVal(val);

      if (this.select) {
        this.opts.initSelection(this.select, this.bind(this.updateSelection));
        if (triggerChange) {
          this.triggerChange(this.buildChangeDetails(oldData, this.data()));
        }
      } else {
        if (this.opts.initSelection === undefined) {
          throw new Error("val() cannot be called if initSelection() is not defined");
        }

        this.opts.initSelection(this.opts.element, function (data) {
          var ids = $.map(data, self.id);
          self.setVal(ids);
          self.updateSelection(data);
          self.clearSearch();
          if (triggerChange) {
            self.triggerChange(self.buildChangeDetails(oldData, self.data()));
          }
        });
      }
      this.clearSearch();
    },

    // multi
    onSortStart: function () {
      if (this.select) {
        throw new Error("Sorting of elements is not supported when attached to <select>. Attach to <input type='hidden'/> instead.");
      }

      // collapse search field into 0 width so its container can be collapsed as well
      this.search.width(0);
      // hide the container
      this.searchContainer.hide();
    },

    // multi
    onSortEnd: function () {

      var val = [], self = this;

      // show search and move it to the end of the list
      this.searchContainer.show();
      // make sure the search container is the last item in the list
      this.searchContainer.appendTo(this.searchContainer.parent());
      // since we collapsed the width in dragStarted, we resize it here
      this.resizeSearch();

      // update selection
      this.selection.find(".select2-search-choice").each(function () {
        val.push(self.opts.id($(this).data("select2-data")));
      });
      this.setVal(val);
      this.triggerChange();
    },

    // multi
    data: function (values, triggerChange) {
      var self = this, ids, old;
      if (arguments.length === 0) {
        return this.selection
            .children(".select2-search-choice")
            .map(function () { return $(this).data("select2-data"); })
            .get();
      } else {
        old = this.data();
        if (!values) { values = []; }
        ids = $.map(values, function (e) { return self.opts.id(e); });
        this.setVal(ids);
        this.updateSelection(values);
        this.clearSearch();
        if (triggerChange) {
          this.triggerChange(this.buildChangeDetails(old, this.data()));
        }
      }
    }
  });

  $.fn.select2 = function () {

    var args = Array.prototype.slice.call(arguments, 0),
        opts,
        select2,
        method, value, multiple,
        allowedMethods = ["val", "destroy", "opened", "open", "close", "focus", "isFocused", "container", "dropdown", "onSortStart", "onSortEnd", "enable", "disable", "readonly", "positionDropdown", "data", "search"],
        valueMethods = ["opened", "isFocused", "container", "dropdown"],
        propertyMethods = ["val", "data"],
        methodsMap = { search: "externalSearch" };

    this.each(function () {
      if (args.length === 0 || typeof (args[0]) === "object") {
        opts = args.length === 0 ? {} : $.extend({}, args[0]);
        opts.element = $(this);

        if (opts.element.get(0).tagName.toLowerCase() === "select") {
          multiple = opts.element.prop("multiple");
        } else {
          multiple = opts.multiple || false;
          if ("tags" in opts) { opts.multiple = multiple = true; }
        }

        select2 = multiple ? new window.Select2["class"].multi() : new window.Select2["class"].single();
        select2.init(opts);
      } else if (typeof (args[0]) === "string") {

        if (indexOf(args[0], allowedMethods) < 0) {
          throw "Unknown method: " + args[0];
        }

        value = undefined;
        select2 = $(this).data("select2");
        if (select2 === undefined) return;

        method = args[0];

        if (method === "container") {
          value = select2.container;
        } else if (method === "dropdown") {
          value = select2.dropdown;
        } else {
          if (methodsMap[method]) method = methodsMap[method];

          value = select2[method].apply(select2, args.slice(1));
        }
        if (indexOf(args[0], valueMethods) >= 0
            || (indexOf(args[0], propertyMethods) >= 0 && args.length == 1)) {
          return false; // abort the iteration, ready to return first matched value
        }
      } else {
        throw "Invalid arguments to select2 plugin: " + args;
      }
    });
    return (value === undefined) ? this : value;
  };

  // plugin defaults, accessible to users
  $.fn.select2.defaults = {
    width: "copy",
    loadMorePadding: 0,
    closeOnSelect: true,
    openOnEnter: true,
    containerCss: {},
    dropdownCss: {},
    containerCssClass: "",
    dropdownCssClass: "",
    formatResult: function (result, container, query, escapeMarkup) {
      var markup = [];
      markMatch(result.text, query.term, markup, escapeMarkup);
      return markup.join("");
    },
    formatSelection: function (data, container, escapeMarkup) {
      return data ? escapeMarkup(data.text) : undefined;
    },
    sortResults: function (results, container, query) {
      return results;
    },
    formatResultCssClass: function (data) { return data.css; },
    formatSelectionCssClass: function (data, container) { return undefined; },
    minimumResultsForSearch: 0,
    minimumInputLength: 0,
    maximumInputLength: null,
    maximumSelectionSize: 0,
    id: function (e) { return e == undefined ? null : e.id; },
    matcher: function (term, text) {
      return stripDiacritics('' + text).toUpperCase().indexOf(stripDiacritics('' + term).toUpperCase()) >= 0;
    },
    separator: ",",
    tokenSeparators: [],
    tokenizer: defaultTokenizer,
    escapeMarkup: defaultEscapeMarkup,
    blurOnChange: false,
    selectOnBlur: false,
    adaptContainerCssClass: function (c) { return c; },
    adaptDropdownCssClass: function (c) { return null; },
    nextSearchTerm: function (selectedObject, currentSearchTerm) { return undefined; },
    searchInputPlaceholder: '',
    createSearchChoicePosition: 'top',
    shouldFocusInput: function (instance) {
      // Attempt to detect touch devices
      var supportsTouchEvents = (('ontouchstart' in window) ||
                                 (navigator.msMaxTouchPoints > 0));

      // Only devices which support touch events should be special cased
      if (!supportsTouchEvents) {
        return true;
      }

      // Never focus the input if search is disabled
      if (instance.opts.minimumResultsForSearch < 0) {
        return false;
      }

      return true;
    }
  };

  $.fn.select2.locales = [];

  $.fn.select2.locales['en'] = {
    formatMatches: function (matches) { if (matches === 1) { return "One result is available, press enter to select it."; } return matches + " results are available, use up and down arrow keys to navigate."; },
    formatNoMatches: function () { return "No matches found"; },
    formatAjaxError: function (jqXHR, textStatus, errorThrown) { return "Loading failed"; },
    formatInputTooShort: function (input, min) { var n = min - input.length; return "Please enter " + n + " or more character" + (n == 1 ? "" : "s"); },
    formatInputTooLong: function (input, max) { var n = input.length - max; return "Please delete " + n + " character" + (n == 1 ? "" : "s"); },
    formatSelectionTooBig: function (limit) { return "You can only select " + limit + " item" + (limit == 1 ? "" : "s"); },
    formatLoadMore: function (pageNumber) { return "Loading more results…"; },
    formatSearching: function () { return "Searching…"; }
  };

  $.extend($.fn.select2.defaults, $.fn.select2.locales['en']);

  $.fn.select2.ajaxDefaults = {
    transport: $.ajax,
    params: {
      type: "GET",
      cache: false,
      dataType: "json"
    }
  };

  // exports
  window.Select2 = {
    query: {
      ajax: ajax,
      local: local,
      tags: tags
    }, util: {
      debounce: debounce,
      markMatch: markMatch,
      escapeMarkup: defaultEscapeMarkup,
      stripDiacritics: stripDiacritics
    }, "class": {
      "abstract": AbstractSelect2,
      "single": SingleSelect2,
      "multi": MultiSelect2
    }
  };

}(jQuery));

/*!
 * Datepicker for Bootstrap v1.5.0-dev (https://github.com/eternicode/bootstrap-datepicker)
 *
 * Copyright 2012 Stefan Petre
 * Improvements by Andrew Rowls
 * Licensed under the Apache License v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
!function (a, b) { function c() { return new Date(Date.UTC.apply(Date, arguments)) } function d() { var a = new Date; return c(a.getFullYear(), a.getMonth(), a.getDate()) } function e(a, b) { return a.getUTCFullYear() === b.getUTCFullYear() && a.getUTCMonth() === b.getUTCMonth() && a.getUTCDate() === b.getUTCDate() } function f(a) { return function () { return this[a].apply(this, arguments) } } function g(b, c) { function d(a, b) { return b.toLowerCase() } var e, f = a(b).data(), g = {}, h = new RegExp("^" + c.toLowerCase() + "([A-Z])"); c = new RegExp("^" + c.toLowerCase()); for (var i in f) c.test(i) && (e = i.replace(h, d), g[e] = f[i]); return g } function h(b) { var c = {}; if (p[b] || (b = b.split("-")[0], p[b])) { var d = p[b]; return a.each(o, function (a, b) { b in d && (c[b] = d[b]) }), c } } var i = function () { var b = { get: function (a) { return this.slice(a)[0] }, contains: function (a) { for (var b = a && a.valueOf(), c = 0, d = this.length; d > c; c++) if (this[c].valueOf() === b) return c; return -1 }, remove: function (a) { this.splice(a, 1) }, replace: function (b) { b && (a.isArray(b) || (b = [b]), this.clear(), this.push.apply(this, b)) }, clear: function () { this.length = 0 }, copy: function () { var a = new i; return a.replace(this), a } }; return function () { var c = []; return c.push.apply(c, arguments), a.extend(c, b), c } }(), j = function (b, c) { this._process_options(c), this.dates = new i, this.viewDate = this.o.defaultViewDate, this.focusDate = null, this.element = a(b), this.isInline = !1, this.isInput = this.element.is("input"), this.component = this.element.hasClass("date") ? this.element.find(".add-on, .input-group-addon, .btn") : !1, this.hasInput = this.component && this.element.find("input").length, this.component && 0 === this.component.length && (this.component = !1), this.picker = a(q.template), this._buildEvents(), this._attachEvents(), this.isInline ? this.picker.addClass("datepicker-inline").appendTo(this.element) : this.picker.addClass("datepicker-dropdown dropdown-menu"), this.o.rtl && this.picker.addClass("datepicker-rtl"), this.viewMode = this.o.startView, this.o.calendarWeeks && this.picker.find("tfoot .today, tfoot .clear").attr("colspan", function (a, b) { return parseInt(b) + 1 }), this._allow_update = !1, this.setStartDate(this._o.startDate), this.setEndDate(this._o.endDate), this.setDaysOfWeekDisabled(this.o.daysOfWeekDisabled), this.setDatesDisabled(this.o.datesDisabled), this.fillDow(), this.fillMonths(), this._allow_update = !0, this.update(), this.showMode(), this.isInline && this.show() }; j.prototype = { constructor: j, _process_options: function (e) { this._o = a.extend({}, this._o, e); var f = this.o = a.extend({}, this._o), g = f.language; switch (p[g] || (g = g.split("-")[0], p[g] || (g = n.language)), f.language = g, f.startView) { case 2: case "decade": f.startView = 2; break; case 1: case "year": f.startView = 1; break; default: f.startView = 0 } switch (f.minViewMode) { case 1: case "months": f.minViewMode = 1; break; case 2: case "years": f.minViewMode = 2; break; default: f.minViewMode = 0 } f.startView = Math.max(f.startView, f.minViewMode), f.multidate !== !0 && (f.multidate = Number(f.multidate) || !1, f.multidate !== !1 && (f.multidate = Math.max(0, f.multidate))), f.multidateSeparator = String(f.multidateSeparator), f.weekStart %= 7, f.weekEnd = (f.weekStart + 6) % 7; var h = q.parseFormat(f.format); if (f.startDate !== -1 / 0 && (f.startDate = f.startDate ? f.startDate instanceof Date ? this._local_to_utc(this._zero_time(f.startDate)) : q.parseDate(f.startDate, h, f.language) : -1 / 0), 1 / 0 !== f.endDate && (f.endDate = f.endDate ? f.endDate instanceof Date ? this._local_to_utc(this._zero_time(f.endDate)) : q.parseDate(f.endDate, h, f.language) : 1 / 0), f.daysOfWeekDisabled = f.daysOfWeekDisabled || [], a.isArray(f.daysOfWeekDisabled) || (f.daysOfWeekDisabled = f.daysOfWeekDisabled.split(/[,\s]*/)), f.daysOfWeekDisabled = a.map(f.daysOfWeekDisabled, function (a) { return parseInt(a, 10) }), f.datesDisabled = f.datesDisabled || [], !a.isArray(f.datesDisabled)) { var i = []; i.push(q.parseDate(f.datesDisabled, h, f.language)), f.datesDisabled = i } f.datesDisabled = a.map(f.datesDisabled, function (a) { return q.parseDate(a, h, f.language) }); var j = String(f.orientation).toLowerCase().split(/\s+/g), k = f.orientation.toLowerCase(); if (j = a.grep(j, function (a) { return /^auto|left|right|top|bottom$/.test(a) }), f.orientation = { x: "auto", y: "auto" }, k && "auto" !== k) if (1 === j.length) switch (j[0]) { case "top": case "bottom": f.orientation.y = j[0]; break; case "left": case "right": f.orientation.x = j[0] } else k = a.grep(j, function (a) { return /^left|right$/.test(a) }), f.orientation.x = k[0] || "auto", k = a.grep(j, function (a) { return /^top|bottom$/.test(a) }), f.orientation.y = k[0] || "auto"; else; if (f.defaultViewDate) { var l = f.defaultViewDate.year || (new Date).getFullYear(), m = f.defaultViewDate.month || 0, o = f.defaultViewDate.day || 1; f.defaultViewDate = c(l, m, o) } else f.defaultViewDate = d(); f.showOnFocus = f.showOnFocus !== b ? f.showOnFocus : !0 }, _events: [], _secondaryEvents: [], _applyEvents: function (a) { for (var c, d, e, f = 0; f < a.length; f++) c = a[f][0], 2 === a[f].length ? (d = b, e = a[f][1]) : 3 === a[f].length && (d = a[f][1], e = a[f][2]), c.on(e, d) }, _unapplyEvents: function (a) { for (var c, d, e, f = 0; f < a.length; f++) c = a[f][0], 2 === a[f].length ? (e = b, d = a[f][1]) : 3 === a[f].length && (e = a[f][1], d = a[f][2]), c.off(d, e) }, _buildEvents: function () { var b = { keyup: a.proxy(function (b) { -1 === a.inArray(b.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) && this.update() }, this), keydown: a.proxy(this.keydown, this), paste: a.proxy(this.paste, this) }; this.o.showOnFocus === !0 && (b.focus = a.proxy(this.show, this)), this.isInput ? this._events = [[this.element, b]] : this.component && this.hasInput ? this._events = [[this.element.find("input"), b], [this.component, { click: a.proxy(this.show, this) }]] : this.element.is("div") ? this.isInline = !0 : this._events = [[this.element, { click: a.proxy(this.show, this) }]], this._events.push([this.element, "*", { blur: a.proxy(function (a) { this._focused_from = a.target }, this) }], [this.element, { blur: a.proxy(function (a) { this._focused_from = a.target }, this) }]), this.o.immediateUpdates && this._events.push([this.element, { "changeYear changeMonth": a.proxy(function (a) { this.update(a.date) }, this) }]), this._secondaryEvents = [[this.picker, { click: a.proxy(this.click, this) }], [a(window), { resize: a.proxy(this.place, this) }], [a(document), { mousedown: a.proxy(function (b) { this.element.is(b.target) || this.element.find(b.target).length || this.picker.is(b.target) || this.picker.find(b.target).length || a(this.picker).hide() }, this) }]] }, _attachEvents: function () { this._detachEvents(), this._applyEvents(this._events) }, _detachEvents: function () { this._unapplyEvents(this._events) }, _attachSecondaryEvents: function () { this._detachSecondaryEvents(), this._applyEvents(this._secondaryEvents) }, _detachSecondaryEvents: function () { this._unapplyEvents(this._secondaryEvents) }, _trigger: function (b, c) { var d = c || this.dates.get(-1), e = this._utc_to_local(d); this.element.trigger({ type: b, date: e, dates: a.map(this.dates, this._utc_to_local), format: a.proxy(function (a, b) { 0 === arguments.length ? (a = this.dates.length - 1, b = this.o.format) : "string" == typeof a && (b = a, a = this.dates.length - 1), b = b || this.o.format; var c = this.dates.get(a); return q.formatDate(c, b, this.o.language) }, this) }) }, show: function () { return this.element.attr("readonly") && this.o.enableOnReadonly === !1 ? void 0 : (this.isInline || this.picker.appendTo(this.o.container), this.place(), this.picker.show(), this._attachSecondaryEvents(), this._trigger("show"), (window.navigator.msMaxTouchPoints || "ontouchstart" in document) && this.o.disableTouchKeyboard && a(this.element).blur(), this) }, hide: function () { return this.isInline ? this : this.picker.is(":visible") ? (this.focusDate = null, this.picker.hide().detach(), this._detachSecondaryEvents(), this.viewMode = this.o.startView, this.showMode(), this.o.forceParse && (this.isInput && this.element.val() || this.hasInput && this.element.find("input").val()) && this.setValue(), this._trigger("hide"), this) : this }, remove: function () { return this.hide(), this._detachEvents(), this._detachSecondaryEvents(), this.picker.remove(), delete this.element.data().datepicker, this.isInput || delete this.element.data().date, this }, paste: function (b) { var c; if (b.originalEvent.clipboardData && b.originalEvent.clipboardData.types && -1 !== a.inArray("text/plain", b.originalEvent.clipboardData.types)) c = b.originalEvent.clipboardData.getData("text/plain"); else { if (!window.clipboardData) return; c = window.clipboardData.getData("Text") } this.setDate(c), this.update(), b.preventDefault() }, _utc_to_local: function (a) { return a && new Date(a.getTime() + 6e4 * a.getTimezoneOffset()) }, _local_to_utc: function (a) { return a && new Date(a.getTime() - 6e4 * a.getTimezoneOffset()) }, _zero_time: function (a) { return a && new Date(a.getFullYear(), a.getMonth(), a.getDate()) }, _zero_utc_time: function (a) { return a && new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate())) }, getDates: function () { return a.map(this.dates, this._utc_to_local) }, getUTCDates: function () { return a.map(this.dates, function (a) { return new Date(a) }) }, getDate: function () { return this._utc_to_local(this.getUTCDate()) }, getUTCDate: function () { var a = this.dates.get(-1); return "undefined" != typeof a ? new Date(a) : null }, clearDates: function () { var a; this.isInput ? a = this.element : this.component && (a = this.element.find("input")), a && a.val("").change(), this.update(), this._trigger("changeDate"), this.o.autoclose && this.hide() }, setDates: function () { var b = a.isArray(arguments[0]) ? arguments[0] : arguments; return this.update.apply(this, b), this._trigger("changeDate"), this.setValue(), this }, setUTCDates: function () { var b = a.isArray(arguments[0]) ? arguments[0] : arguments; return this.update.apply(this, a.map(b, this._utc_to_local)), this._trigger("changeDate"), this.setValue(), this }, setDate: f("setDates"), setUTCDate: f("setUTCDates"), setValue: function () { var a = this.getFormattedDate(); return this.isInput ? this.element.val(a).change() : this.component && this.element.find("input").val(a).change(), this }, getFormattedDate: function (c) { c === b && (c = this.o.format); var d = this.o.language; return a.map(this.dates, function (a) { return q.formatDate(a, c, d) }).join(this.o.multidateSeparator) }, setStartDate: function (a) { return this._process_options({ startDate: a }), this.update(), this.updateNavArrows(), this }, setEndDate: function (a) { return this._process_options({ endDate: a }), this.update(), this.updateNavArrows(), this }, setDaysOfWeekDisabled: function (a) { return this._process_options({ daysOfWeekDisabled: a }), this.update(), this.updateNavArrows(), this }, setDatesDisabled: function (a) { this._process_options({ datesDisabled: a }), this.update(), this.updateNavArrows() }, place: function () { if (this.isInline) return this; var b = this.picker.outerWidth(), c = this.picker.outerHeight(), d = 10, e = a(this.o.container).width(), f = a(this.o.container).height(), g = a(this.o.container).scrollTop(), h = a(this.o.container).offset(), i = []; this.element.parents().each(function () { var b = a(this).css("z-index"); "auto" !== b && 0 !== b && i.push(parseInt(b)) }); var j = Math.max.apply(Math, i) + 10, k = this.component ? this.component.parent().offset() : this.element.offset(), l = this.component ? this.component.outerHeight(!0) : this.element.outerHeight(!1), m = this.component ? this.component.outerWidth(!0) : this.element.outerWidth(!1), n = k.left - h.left, o = k.top - h.top; this.picker.removeClass("datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left"), "auto" !== this.o.orientation.x ? (this.picker.addClass("datepicker-orient-" + this.o.orientation.x), "right" === this.o.orientation.x && (n -= b - m)) : k.left < 0 ? (this.picker.addClass("datepicker-orient-left"), n -= k.left - d) : n + b > e ? (this.picker.addClass("datepicker-orient-right"), n = k.left + m - b) : this.picker.addClass("datepicker-orient-left"); var p, q, r = this.o.orientation.y; if ("auto" === r && (p = -g + o - c, q = g + f - (o + l + c), r = Math.max(p, q) === q ? "top" : "bottom"), this.picker.addClass("datepicker-orient-" + r), "top" === r ? o += l : o -= c + parseInt(this.picker.css("padding-top")), this.o.rtl) { var s = e - (n + m); this.picker.css({ top: o, right: s, zIndex: j }) } else this.picker.css({ top: o, left: n, zIndex: j }); return this }, _allow_update: !0, update: function () { if (!this._allow_update) return this; var b = this.dates.copy(), c = [], d = !1; return arguments.length ? (a.each(arguments, a.proxy(function (a, b) { b instanceof Date && (b = this._local_to_utc(b)), c.push(b) }, this)), d = !0) : (c = this.isInput ? this.element.val() : this.element.data("date") || this.element.find("input").val(), c = c && this.o.multidate ? c.split(this.o.multidateSeparator) : [c], delete this.element.data().date), c = a.map(c, a.proxy(function (a) { return q.parseDate(a, this.o.format, this.o.language) }, this)), c = a.grep(c, a.proxy(function (a) { return a < this.o.startDate || a > this.o.endDate || !a }, this), !0), this.dates.replace(c), this.dates.length ? this.viewDate = new Date(this.dates.get(-1)) : this.viewDate < this.o.startDate ? this.viewDate = new Date(this.o.startDate) : this.viewDate > this.o.endDate && (this.viewDate = new Date(this.o.endDate)), d ? this.setValue() : c.length && String(b) !== String(this.dates) && this._trigger("changeDate"), !this.dates.length && b.length && this._trigger("clearDate"), this.fill(), this }, fillDow: function () { var a = this.o.weekStart, b = "<tr>"; if (this.o.calendarWeeks) { this.picker.find(".datepicker-days thead tr:first-child .datepicker-switch").attr("colspan", function (a, b) { return parseInt(b) + 1 }); var c = '<th class="cw">&#160;</th>'; b += c } for (; a < this.o.weekStart + 7;) b += '<th class="dow">' + p[this.o.language].daysMin[a++ % 7] + "</th>"; b += "</tr>", this.picker.find(".datepicker-days thead").append(b) }, fillMonths: function () { for (var a = "", b = 0; 12 > b;) a += '<span class="month">' + p[this.o.language].monthsShort[b++] + "</span>"; this.picker.find(".datepicker-months td").html(a) }, setRange: function (b) { b && b.length ? this.range = a.map(b, function (a) { return a.valueOf() }) : delete this.range, this.fill() }, getClassNames: function (b) { var c = [], d = this.viewDate.getUTCFullYear(), f = this.viewDate.getUTCMonth(), g = new Date; return b.getUTCFullYear() < d || b.getUTCFullYear() === d && b.getUTCMonth() < f ? c.push("old") : (b.getUTCFullYear() > d || b.getUTCFullYear() === d && b.getUTCMonth() > f) && c.push("new"), this.focusDate && b.valueOf() === this.focusDate.valueOf() && c.push("focused"), this.o.todayHighlight && b.getUTCFullYear() === g.getFullYear() && b.getUTCMonth() === g.getMonth() && b.getUTCDate() === g.getDate() && c.push("today"), -1 !== this.dates.contains(b) && c.push("active"), (b.valueOf() < this.o.startDate || b.valueOf() > this.o.endDate || -1 !== a.inArray(b.getUTCDay(), this.o.daysOfWeekDisabled)) && c.push("disabled"), this.o.datesDisabled.length > 0 && a.grep(this.o.datesDisabled, function (a) { return e(b, a) }).length > 0 && c.push("disabled", "disabled-date"), this.range && (b > this.range[0] && b < this.range[this.range.length - 1] && c.push("range"), -1 !== a.inArray(b.valueOf(), this.range) && c.push("selected")), c }, fill: function () { var d, e = new Date(this.viewDate), f = e.getUTCFullYear(), g = e.getUTCMonth(), h = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCFullYear() : -1 / 0, i = this.o.startDate !== -1 / 0 ? this.o.startDate.getUTCMonth() : -1 / 0, j = 1 / 0 !== this.o.endDate ? this.o.endDate.getUTCFullYear() : 1 / 0, k = 1 / 0 !== this.o.endDate ? this.o.endDate.getUTCMonth() : 1 / 0, l = p[this.o.language].today || p.en.today || "", m = p[this.o.language].clear || p.en.clear || ""; if (!isNaN(f) && !isNaN(g)) { this.picker.find(".datepicker-days thead .datepicker-switch").text(p[this.o.language].months[g] + " " + f), this.picker.find("tfoot .today").text(l).toggle(this.o.todayBtn !== !1), this.picker.find("tfoot .clear").text(m).toggle(this.o.clearBtn !== !1), this.updateNavArrows(), this.fillMonths(); var n = c(f, g - 1, 28), o = q.getDaysInMonth(n.getUTCFullYear(), n.getUTCMonth()); n.setUTCDate(o), n.setUTCDate(o - (n.getUTCDay() - this.o.weekStart + 7) % 7); var r = new Date(n); r.setUTCDate(r.getUTCDate() + 42), r = r.valueOf(); for (var s, t = []; n.valueOf() < r;) { if (n.getUTCDay() === this.o.weekStart && (t.push("<tr>"), this.o.calendarWeeks)) { var u = new Date(+n + (this.o.weekStart - n.getUTCDay() - 7) % 7 * 864e5), v = new Date(Number(u) + (11 - u.getUTCDay()) % 7 * 864e5), w = new Date(Number(w = c(v.getUTCFullYear(), 0, 1)) + (11 - w.getUTCDay()) % 7 * 864e5), x = (v - w) / 864e5 / 7 + 1; t.push('<td class="cw">' + x + "</td>") } if (s = this.getClassNames(n), s.push("day"), this.o.beforeShowDay !== a.noop) { var y = this.o.beforeShowDay(this._utc_to_local(n)); y === b ? y = {} : "boolean" == typeof y ? y = { enabled: y } : "string" == typeof y && (y = { classes: y }), y.enabled === !1 && s.push("disabled"), y.classes && (s = s.concat(y.classes.split(/\s+/))), y.tooltip && (d = y.tooltip) } s = a.unique(s), t.push('<td class="' + s.join(" ") + '"' + (d ? ' title="' + d + '"' : "") + ">" + n.getUTCDate() + "</td>"), d = null, n.getUTCDay() === this.o.weekEnd && t.push("</tr>"), n.setUTCDate(n.getUTCDate() + 1) } this.picker.find(".datepicker-days tbody").empty().append(t.join("")); var z = this.picker.find(".datepicker-months").find("th:eq(1)").text(f).end().find("span").removeClass("active"); if (a.each(this.dates, function (a, b) { b.getUTCFullYear() === f && z.eq(b.getUTCMonth()).addClass("active") }), (h > f || f > j) && z.addClass("disabled"), f === h && z.slice(0, i).addClass("disabled"), f === j && z.slice(k + 1).addClass("disabled"), this.o.beforeShowMonth !== a.noop) { var A = this; a.each(z, function (b, c) { if (!a(c).hasClass("disabled")) { var d = new Date(f, b, 1), e = A.o.beforeShowMonth(d); e === !1 && a(c).addClass("disabled") } }) } t = "", f = 10 * parseInt(f / 10, 10); var B = this.picker.find(".datepicker-years").find("th:eq(1)").text(f + "-" + (f + 9)).end().find("td"); f -= 1; for (var C, D = a.map(this.dates, function (a) { return a.getUTCFullYear() }), E = -1; 11 > E; E++) C = ["year"], -1 === E ? C.push("old") : 10 === E && C.push("new"), -1 !== a.inArray(f, D) && C.push("active"), (h > f || f > j) && C.push("disabled"), t += '<span class="' + C.join(" ") + '">' + f + "</span>", f += 1; B.html(t) } }, updateNavArrows: function () { if (this._allow_update) { var a = new Date(this.viewDate), b = a.getUTCFullYear(), c = a.getUTCMonth(); switch (this.viewMode) { case 0: this.picker.find(".prev").css(this.o.startDate !== -1 / 0 && b <= this.o.startDate.getUTCFullYear() && c <= this.o.startDate.getUTCMonth() ? { visibility: "hidden" } : { visibility: "visible" }), this.picker.find(".next").css(1 / 0 !== this.o.endDate && b >= this.o.endDate.getUTCFullYear() && c >= this.o.endDate.getUTCMonth() ? { visibility: "hidden" } : { visibility: "visible" }); break; case 1: case 2: this.picker.find(".prev").css(this.o.startDate !== -1 / 0 && b <= this.o.startDate.getUTCFullYear() ? { visibility: "hidden" } : { visibility: "visible" }), this.picker.find(".next").css(1 / 0 !== this.o.endDate && b >= this.o.endDate.getUTCFullYear() ? { visibility: "hidden" } : { visibility: "visible" }) } } }, click: function (b) { b.preventDefault(); var d, e, f, g = a(b.target).closest("span, td, th"); if (1 === g.length) switch (g[0].nodeName.toLowerCase()) { case "th": switch (g[0].className) { case "datepicker-switch": this.showMode(1); break; case "prev": case "next": var h = q.modes[this.viewMode].navStep * ("prev" === g[0].className ? -1 : 1); switch (this.viewMode) { case 0: this.viewDate = this.moveMonth(this.viewDate, h), this._trigger("changeMonth", this.viewDate); break; case 1: case 2: this.viewDate = this.moveYear(this.viewDate, h), 1 === this.viewMode && this._trigger("changeYear", this.viewDate) } this.fill(); break; case "today": var i = new Date; i = c(i.getFullYear(), i.getMonth(), i.getDate(), 0, 0, 0), this.showMode(-2); var j = "linked" === this.o.todayBtn ? null : "view"; this._setDate(i, j); break; case "clear": this.clearDates() } break; case "span": g.hasClass("disabled") || (this.viewDate.setUTCDate(1), g.hasClass("month") ? (f = 1, e = g.parent().find("span").index(g), d = this.viewDate.getUTCFullYear(), this.viewDate.setUTCMonth(e), this._trigger("changeMonth", this.viewDate), 1 === this.o.minViewMode ? (this._setDate(c(d, e, f)), this.showMode()) : this.showMode(-1)) : (f = 1, e = 0, d = parseInt(g.text(), 10) || 0, this.viewDate.setUTCFullYear(d), this._trigger("changeYear", this.viewDate), 2 === this.o.minViewMode && this._setDate(c(d, e, f)), this.showMode(-1)), this.fill()); break; case "td": g.hasClass("day") && !g.hasClass("disabled") && (f = parseInt(g.text(), 10) || 1, d = this.viewDate.getUTCFullYear(), e = this.viewDate.getUTCMonth(), g.hasClass("old") ? 0 === e ? (e = 11, d -= 1) : e -= 1 : g.hasClass("new") && (11 === e ? (e = 0, d += 1) : e += 1), this._setDate(c(d, e, f))) } this.picker.is(":visible") && this._focused_from && a(this._focused_from).focus(), delete this._focused_from }, _toggle_multidate: function (a) { var b = this.dates.contains(a); if (a || this.dates.clear(), -1 !== b ? (this.o.multidate === !0 || this.o.multidate > 1 || this.o.toggleActive) && this.dates.remove(b) : this.o.multidate === !1 ? (this.dates.clear(), this.dates.push(a)) : this.dates.push(a), "number" == typeof this.o.multidate) for (; this.dates.length > this.o.multidate;) this.dates.remove(0) }, _setDate: function (a, b) { b && "date" !== b || this._toggle_multidate(a && new Date(a)), b && "view" !== b || (this.viewDate = a && new Date(a)), this.fill(), this.setValue(), b && "view" === b || this._trigger("changeDate"); var c; this.isInput ? c = this.element : this.component && (c = this.element.find("input")), c && c.change(), !this.o.autoclose || b && "date" !== b || this.hide() }, moveMonth: function (a, c) { if (!a) return b; if (!c) return a; var d, e, f = new Date(a.valueOf()), g = f.getUTCDate(), h = f.getUTCMonth(), i = Math.abs(c); if (c = c > 0 ? 1 : -1, 1 === i) e = -1 === c ? function () { return f.getUTCMonth() === h } : function () { return f.getUTCMonth() !== d }, d = h + c, f.setUTCMonth(d), (0 > d || d > 11) && (d = (d + 12) % 12); else { for (var j = 0; i > j; j++) f = this.moveMonth(f, c); d = f.getUTCMonth(), f.setUTCDate(g), e = function () { return d !== f.getUTCMonth() } } for (; e() ;) f.setUTCDate(--g), f.setUTCMonth(d); return f }, moveYear: function (a, b) { return this.moveMonth(a, 12 * b) }, dateWithinRange: function (a) { return a >= this.o.startDate && a <= this.o.endDate }, keydown: function (a) { if (!this.picker.is(":visible")) return void ((40 === a.keyCode || 27 === a.keyCode) && this.show()); var b, c, e, f = !1, g = this.focusDate || this.viewDate; switch (a.keyCode) { case 27: this.focusDate ? (this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill()) : this.hide(), a.preventDefault(); break; case 37: case 39: if (!this.o.keyboardNavigation) break; b = 37 === a.keyCode ? -1 : 1, a.ctrlKey ? (c = this.moveYear(this.dates.get(-1) || d(), b), e = this.moveYear(g, b), this._trigger("changeYear", this.viewDate)) : a.shiftKey ? (c = this.moveMonth(this.dates.get(-1) || d(), b), e = this.moveMonth(g, b), this._trigger("changeMonth", this.viewDate)) : (c = new Date(this.dates.get(-1) || d()), c.setUTCDate(c.getUTCDate() + b), e = new Date(g), e.setUTCDate(g.getUTCDate() + b)), this.dateWithinRange(e) && (this.focusDate = this.viewDate = e, this.setValue(), this.fill(), a.preventDefault()); break; case 38: case 40: if (!this.o.keyboardNavigation) break; b = 38 === a.keyCode ? -1 : 1, a.ctrlKey ? (c = this.moveYear(this.dates.get(-1) || d(), b), e = this.moveYear(g, b), this._trigger("changeYear", this.viewDate)) : a.shiftKey ? (c = this.moveMonth(this.dates.get(-1) || d(), b), e = this.moveMonth(g, b), this._trigger("changeMonth", this.viewDate)) : (c = new Date(this.dates.get(-1) || d()), c.setUTCDate(c.getUTCDate() + 7 * b), e = new Date(g), e.setUTCDate(g.getUTCDate() + 7 * b)), this.dateWithinRange(e) && (this.focusDate = this.viewDate = e, this.setValue(), this.fill(), a.preventDefault()); break; case 32: break; case 13: g = this.focusDate || this.dates.get(-1) || this.viewDate, this.o.keyboardNavigation && (this._toggle_multidate(g), f = !0), this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.setValue(), this.fill(), this.picker.is(":visible") && (a.preventDefault(), "function" == typeof a.stopPropagation ? a.stopPropagation() : a.cancelBubble = !0, this.o.autoclose && this.hide()); break; case 9: this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill(), this.hide() } if (f) { this._trigger(this.dates.length ? "changeDate" : "clearDate"); var h; this.isInput ? h = this.element : this.component && (h = this.element.find("input")), h && h.change() } }, showMode: function (a) { a && (this.viewMode = Math.max(this.o.minViewMode, Math.min(2, this.viewMode + a))), this.picker.children("div").hide().filter(".datepicker-" + q.modes[this.viewMode].clsName).css("display", "block"), this.updateNavArrows() } }; var k = function (b, c) { this.element = a(b), this.inputs = a.map(c.inputs, function (a) { return a.jquery ? a[0] : a }), delete c.inputs, m.call(a(this.inputs), c).on("changeDate", a.proxy(this.dateUpdated, this)), this.pickers = a.map(this.inputs, function (b) { return a(b).data("datepicker") }), this.updateDates() }; k.prototype = { updateDates: function () { this.dates = a.map(this.pickers, function (a) { return a.getUTCDate() }), this.updateRanges() }, updateRanges: function () { var b = a.map(this.dates, function (a) { return a.valueOf() }); a.each(this.pickers, function (a, c) { c.setRange(b) }) }, dateUpdated: function (b) { if (!this.updating) { this.updating = !0; var c = a(b.target).data("datepicker"), d = c.getUTCDate(), e = a.inArray(b.target, this.inputs), f = e - 1, g = e + 1, h = this.inputs.length; if (-1 !== e) { if (a.each(this.pickers, function (a, b) { b.getUTCDate() || b.setUTCDate(d) }), d < this.dates[f]) for (; f >= 0 && d < this.dates[f];) this.pickers[f--].setUTCDate(d); else if (d > this.dates[g]) for (; h > g && d > this.dates[g];) this.pickers[g++].setUTCDate(d); this.updateDates(), delete this.updating } } }, remove: function () { a.map(this.pickers, function (a) { a.remove() }), delete this.element.data().datepicker } }; var l = a.fn.datepicker, m = function (c) { var d = Array.apply(null, arguments); d.shift(); var e; return this.each(function () { var f = a(this), i = f.data("datepicker"), l = "object" == typeof c && c; if (!i) { var m = g(this, "date"), o = a.extend({}, n, m, l), p = h(o.language), q = a.extend({}, n, p, m, l); if (f.hasClass("input-daterange") || q.inputs) { var r = { inputs: q.inputs || f.find("input").toArray() }; f.data("datepicker", i = new k(this, a.extend(q, r))) } else f.data("datepicker", i = new j(this, q)) } return "string" == typeof c && "function" == typeof i[c] && (e = i[c].apply(i, d), e !== b) ? !1 : void 0 }), e !== b ? e : this }; a.fn.datepicker = m; var n = a.fn.datepicker.defaults = { autoclose: !1, beforeShowDay: a.noop, beforeShowMonth: a.noop, calendarWeeks: !1, clearBtn: !1, toggleActive: !1, daysOfWeekDisabled: [], datesDisabled: [], endDate: 1 / 0, forceParse: !0, format: "mm/dd/yyyy", keyboardNavigation: !0, language: "en", minViewMode: 0, multidate: !1, multidateSeparator: ",", orientation: "auto", rtl: !1, startDate: -1 / 0, startView: 0, todayBtn: !1, todayHighlight: !1, weekStart: 0, disableTouchKeyboard: !1, enableOnReadonly: !0, container: "body", immediateUpdates: !1 }, o = a.fn.datepicker.locale_opts = ["format", "rtl", "weekStart"]; a.fn.datepicker.Constructor = j; var p = a.fn.datepicker.dates = { en: { days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], today: "Today", clear: "Clear" } }, q = { modes: [{ clsName: "days", navFnc: "Month", navStep: 1 }, { clsName: "months", navFnc: "FullYear", navStep: 1 }, { clsName: "years", navFnc: "FullYear", navStep: 10 }], isLeapYear: function (a) { return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0 }, getDaysInMonth: function (a, b) { return [31, q.isLeapYear(a) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][b] }, validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g, nonpunctuation: /[^ -\/:-@\[\u3400-\u9fff-`{-~\t\n\r]+/g, parseFormat: function (a) { var b = a.replace(this.validParts, "\x00").split("\x00"), c = a.match(this.validParts); if (!b || !b.length || !c || 0 === c.length) throw new Error("Invalid date format."); return { separators: b, parts: c } }, parseDate: function (d, e, f) { function g() { var a = this.slice(0, m[k].length), b = m[k].slice(0, a.length); return a.toLowerCase() === b.toLowerCase() } if (!d) return b; if (d instanceof Date) return d; "string" == typeof e && (e = q.parseFormat(e)); var h, i, k, l = /([\-+]\d+)([dmwy])/, m = d.match(/([\-+]\d+)([dmwy])/g); if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(d)) { for (d = new Date, k = 0; k < m.length; k++) switch (h = l.exec(m[k]), i = parseInt(h[1]), h[2]) { case "d": d.setUTCDate(d.getUTCDate() + i); break; case "m": d = j.prototype.moveMonth.call(j.prototype, d, i); break; case "w": d.setUTCDate(d.getUTCDate() + 7 * i); break; case "y": d = j.prototype.moveYear.call(j.prototype, d, i) } return c(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate(), 0, 0, 0) } m = d && d.match(this.nonpunctuation) || [], d = new Date; var n, o, r = {}, s = ["yyyy", "yy", "M", "MM", "m", "mm", "d", "dd"], t = { yyyy: function (a, b) { return a.setUTCFullYear(b) }, yy: function (a, b) { return a.setUTCFullYear(2e3 + b) }, m: function (a, b) { if (isNaN(a)) return a; for (b -= 1; 0 > b;) b += 12; for (b %= 12, a.setUTCMonth(b) ; a.getUTCMonth() !== b;) a.setUTCDate(a.getUTCDate() - 1); return a }, d: function (a, b) { return a.setUTCDate(b) } }; t.M = t.MM = t.mm = t.m, t.dd = t.d, d = c(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0); var u = e.parts.slice(); if (m.length !== u.length && (u = a(u).filter(function (b, c) { return -1 !== a.inArray(c, s) }).toArray()), m.length === u.length) { var v; for (k = 0, v = u.length; v > k; k++) { if (n = parseInt(m[k], 10), h = u[k], isNaN(n)) switch (h) { case "MM": o = a(p[f].months).filter(g), n = a.inArray(o[0], p[f].months) + 1; break; case "M": o = a(p[f].monthsShort).filter(g), n = a.inArray(o[0], p[f].monthsShort) + 1 } r[h] = n } var w, x; for (k = 0; k < s.length; k++) x = s[k], x in r && !isNaN(r[x]) && (w = new Date(d), t[x](w, r[x]), isNaN(w) || (d = w)) } return d }, formatDate: function (b, c, d) { if (!b) return ""; "string" == typeof c && (c = q.parseFormat(c)); var e = { d: b.getUTCDate(), D: p[d].daysShort[b.getUTCDay()], DD: p[d].days[b.getUTCDay()], m: b.getUTCMonth() + 1, M: p[d].monthsShort[b.getUTCMonth()], MM: p[d].months[b.getUTCMonth()], yy: b.getUTCFullYear().toString().substring(2), yyyy: b.getUTCFullYear() }; e.dd = (e.d < 10 ? "0" : "") + e.d, e.mm = (e.m < 10 ? "0" : "") + e.m, b = []; for (var f = a.extend([], c.separators), g = 0, h = c.parts.length; h >= g; g++) f.length && b.push(f.shift()), b.push(e[c.parts[g]]); return b.join("") }, headTemplate: '<thead><tr><th class="prev">&#171;</th><th colspan="5" class="datepicker-switch"></th><th class="next">&#187;</th></tr></thead>', contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>', footTemplate: '<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>' }; q.template = '<div class="datepicker"><div class="datepicker-days"><table class=" table-condensed">' + q.headTemplate + "<tbody></tbody>" + q.footTemplate + '</table></div><div class="datepicker-months"><table class="table-condensed">' + q.headTemplate + q.contTemplate + q.footTemplate + '</table></div><div class="datepicker-years"><table class="table-condensed">' + q.headTemplate + q.contTemplate + q.footTemplate + "</table></div></div>", a.fn.datepicker.DPGlobal = q, a.fn.datepicker.noConflict = function () { return a.fn.datepicker = l, this }, a.fn.datepicker.version = "1.4.1-dev", a(document).on("focus.datepicker.data-api click.datepicker.data-api", '[data-provide="datepicker"]', function (b) { var c = a(this); c.data("datepicker") || (b.preventDefault(), m.call(c, "show")) }), a(function () { m.call(a('[data-provide="datepicker-inline"]')) }) }(window.jQuery);

// Languages: sv, nb, no, da, fi
!function (a) { a.fn.datepicker.dates.sv = { days: ["Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag"], daysShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"], daysMin: ["Sö", "Må", "Ti", "On", "To", "Fr", "Lö"], months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"], monthsShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"], today: "Idag", format: "yyyy-mm-dd", weekStart: 1, clear: "Rensa" } }(jQuery);
!function (a) { a.fn.datepicker.dates.fi = { days: ["sunnuntai", "maanantai", "tiistai", "keskiviikko", "torstai", "perjantai", "lauantai"], daysShort: ["sun", "maa", "tii", "kes", "tor", "per", "lau"], daysMin: ["su", "ma", "ti", "ke", "to", "pe", "la"], months: ["tammikuu", "helmikuu", "maaliskuu", "huhtikuu", "toukokuu", "kesäkuu", "heinäkuu", "elokuu", "syyskuu", "lokakuu", "marraskuu", "joulukuu"], monthsShort: ["tam", "hel", "maa", "huh", "tou", "kes", "hei", "elo", "syy", "lok", "mar", "jou"], today: "tänään", clear: "Tyhjennä", weekStart: 1, format: "d.m.yyyy" } }(jQuery);
!function (a) { a.fn.datepicker.dates.nb = { days: ["Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag"], daysShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"], daysMin: ["Sø", "Ma", "Ti", "On", "To", "Fr", "Lø"], months: ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"], monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"], today: "I Dag" } }(jQuery);
!function (a) { a.fn.datepicker.dates.no = { days: ["Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag"], daysShort: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"], daysMin: ["Sø", "Ma", "Ti", "On", "To", "Fr", "Lø"], months: ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"], monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"], today: "I dag", clear: "Nullstill", weekStart: 1, format: "dd.mm.yyyy" } }(jQuery);
!function (a) { a.fn.datepicker.dates.da = { days: ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"], daysShort: ["søn", "man", "tir", "ons", "tor", "fre", "lør"], daysMin: ["sø", "ma", "ti", "on", "to", "fr", "lø"], months: ["januar", "februar", "marts", "april", "maj", "juni", "juli", "august", "september", "oktober", "november", "december"], monthsShort: ["jan", "feb", "mar", "apr", "maj", "jun", "jul", "aug", "sep", "okt", "nov", "dec"], today: "I Dag", clear: "Nulstil" } }(jQuery);




var BBM = BBM || {};

// caching some often used jQuery objects
BBM.$ = {
  win: $(window),
  body: $('body')
};

// development boolean (used for loading example json instead of real data, and so on)
BBM.dev = window.location.hostname == 'localhost' || window.location.hostname == 'oakwood.github.io' || window.location.hostname == 'local.businessinsider.se';
if (BBM.dev) {
  // override asset path
  staticAssets = '/static/dist/mustache/';
}

/* Tools */
var BBM = BBM || {};

BBM.getScrollbarWidth = function () {
  var outer = document.createElement("div");
  outer.style.visibility = "hidden";
  outer.style.width = "100px";
  outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

  document.body.appendChild(outer);

  var widthNoScroll = outer.offsetWidth;
  // force scrollbars
  outer.style.overflow = "scroll";

  // add innerdiv
  var inner = document.createElement("div");
  inner.style.width = "100%";
  outer.appendChild(inner);

  var widthWithScroll = inner.offsetWidth;

  // remove divs
  outer.parentNode.removeChild(outer);

  return widthNoScroll - widthWithScroll;
};
var BBM = BBM || {};

BBM.getPageViewsSpan = function (pageViews) {
  if (pageViews >= 5000)
    return { "class": "very-hot", "icon": "/dist/assets/icons/very-hot.svg" };

  if (pageViews >= 1500)
    return { "class": "hot", "icon": "/dist/assets/icons/hot.svg" }

  if (pageViews >= 400)
    return { "class": "warm", "icon": "/dist/assets/icons/warm.svg" };

  return { "class": "cold", "icon": "/dist/assets/icons/cool.svg" }
};

BBM.loadJsonArticle = function (url, $target, callback) {

  // strip hash, append json query
  var hash = url.indexOf('#');
  if (hash > 0) url = url.substr(0, hash);

  url = '/api/v1/search' + url;


  // load article
  $.getJSON(url, function (data) {
    // load mustache template (hopefully cached)
    //		console.log('use template: ' + data.template);
    var templateUrl = "/dist/assets/mustache/article.mustache";
    //console.log(window.location.href);
    $.get(templateUrl, function (template) {
      // render template
      data.url = window.location.href;
      data.pageViewObject = BBM.getPageViewsSpan(data.pageViews);
      data.showPageViews = data.pageViews > 100 ? true : false;

      var rendered = Mustache.render(template, data);

      // inject to dom
      $target.replaceWith(rendered);
      if (callback) callback(data);

    }).fail(function () {
      //console.log('template fetch failed :(', templateUrl);
    });

  }).fail(function () {
    //console.log('json fetch failed :(', url);
  });
};


var BBM = BBM || {};

BBM.toggleActive = function () {
  BBM.$.body.on('click', '[data-toggle-active]', function (e) {
    e.preventDefault();

    var target = $(this).toggleClass('active').data('toggle-active');
    $(target).toggleClass('active');
  });
};

/* Include global BBM methods */
var BBM = BBM || {};

BBM.readingList = {
  current: 0,
  $wrap: null,
  $list: null,
  $articles: null,

  setSticky: function () {
    $('.quicklook-wrapper .article-sidebar-sticky').stick_in_parent({
      offset_top: 66,
      recalc_every: 10
    });

    BBM.$.body.on('load', function () {
      //BBM.$.body.trigger("sticky_kit:recalc");
    });
  },

  setCurrent: function (index) {
    this.current = index;

    var $item = this.$list.eq(this.current),
			$article = this.$wrap.children().eq(this.current),
			 url = $item.attr('href');

    // set current class
    //$item.parent().addClass('current').siblings().removeClass('current');

    // replace history
    window.history.replaceState("", "", url);

    if (BBM.stickySocialButtons)
      BBM.stickySocialButtons.setURL(url);

    // load article
    if ($article.hasClass('loading')) {
      // remove to not load again
      this.loadJson(this.current);
    }
    BBM.asyncLoaded($article, null);
  },

  loadJson: function (index) {
    var $item = this.$list.eq(index),
        $article = this.$wrap.children().eq(index),
        url = $item.attr('href');

    // load article
    if ($article.hasClass('loading')) {
      // remove to not load again
      $article.removeClass('loading');
      BBM.loadJsonArticle(url, this.$articles.eq(index), function (data) {
        bbm.responsiveAdtoma.loadAdtoma();
        FB.XFBML.parse();
        twttr.widgets.load();
      });
    }
  },

  scroll: function () {
    if (!this.$articles.length) return;

    var currentPos = BBM.readingList.$wrap.children().eq(this.current)[0].getBoundingClientRect(),
      breakPoint = window.innerHeight * 0.9;


    if (this.current < (this.$list.length - 1) && currentPos.bottom <= currentPos.height / 2) {
      this.loadJson(this.current + 1);
    }


    if (this.current && currentPos.top > breakPoint) {
      this.setCurrent(this.current - 1);

    } else if (this.current < (this.$list.length - 1) && currentPos.bottom + 60 <= breakPoint) {
      this.setCurrent(this.current + 1);
    }
  },

  placeholders: function () {
    var self = this;

    self.$list.each(function (i) {
      if (i) {
        var $item = $(this);
        self.$wrap.append('<article class="article-full loading"><h1>' + $item.text() + '</h1></article>');
      }
    });

    this.$articles = this.$wrap.children();
  },

  scrollListen: function (el) {
    el.off('.readinglist').on('scroll.readinglist', $.proxy(this.scroll, this));
  },

  init: function (quicklook) {
    this.$wrap = $('.article-wrapper');
    this.$list = $('.reading-list a');


    // don't initiate twice in reading list
    if (this.$list.data('initiated') == true) return;

    if (!quicklook && this.$list.length) {
      // direct access url, fancy sticky sidebar and scroll event on window
      this.setSticky();
      this.scrollListen(BBM.$.win);

    } else {
      // quicklook, dumb sticky and sroll event on quicklook pane
      var $pane = $('.quicklook-pane'),
        $article = $pane.find('.article-wrapper');

      this.scrollListen($pane);

      var stickyOffset = $pane[0].offsetTop + parseInt($pane.css('padding-top'), 10);

      // todo: do something about this
      if (BBM.responsiveEvents.currentState == 'desktop') {
        var $sticky = $pane.find('.article-sidebar-sticky'),
          isSticky = false;

        if ($article.length) {
          $pane.on('scroll.readinglist', function () {
            var scrollTop = $(this).scrollTop(),
              offsetTop = $article[0].getBoundingClientRect().top;

            if (offsetTop < stickyOffset) {
              $sticky.css('transform', 'translateY(' + (stickyOffset - offsetTop) + 'px)');
              isSticky = true;
            } else if (offsetTop >= stickyOffset && isSticky) {
              $sticky.css('transform', '');
              isSticky = false;
            }
          });
        }
      }
    }

    this.$list.data('initiated', true);
    this.placeholders();
  }
};

var BBM = BBM || {};

BBM.quicklook = {
  isOpen: null,
  currentUrl: null,
  $pane: $('<div class="quicklook-pane"></div>').appendTo('body'),
  $headPanoClone: $('.site-banner .module-banner').clone(),
  bodyClass: null,

  //	scrollBarWidth: BBM.getScrollbarWidth(),

  open: function (url, fromHistory) {
    var self = this;
    //console.log('quicklook: ', url, fromHistory, this.currentUrl);

    if (BBM.stickySocialButtons)
      BBM.stickySocialButtons.show();

    // navigated from history?
    if (!fromHistory) {
      // nope, by click. add to history
      window.history.pushState(
				{
				  'url': url
				},
				"", url
			);
    }

    // if not loaded (first load or site refreshed)
    if (self.currentUrl != url) {
      self.$pane.empty();

      var $load = $('<div class="article-loading">Laddar innehåll... Var god dröj.</div>').appendTo(self.$pane);

      BBM.loadJsonArticle(url, $load, function (data) {
        if (data.template == 'article') {
          // add wrapper for article and sidebar for readinglist
          self.$pane.wrapInner('<div class="group-primary article-wrapper"></div>');
          var $side = $('<div class="group-minor article-sidebar-sticky"></div>').appendTo(self.$pane);

          // build reading list (todo: use external template?)

          var output = Mustache.render(
            '<div class="widget widget-free-text"><h3 class="widget-title">Rekommenderat</h3>' +
            '<ul class="reading-list">' +
              '<li class="current"><a href="' + decodeURIComponent(data.url) + '">' + data.title + '</a></li>' +
              '{{#readingList}}<li><a href="{{url}}">{{title}}</a></li>{{/readingList}}' +
              '</ul></div>', data);

          $side.html(output);

          // panorama ad container?
          if (data.adtomaAds.panorama) {

            var output = Mustache.render(
							'{{ #adtomaAds.panorama }}' +
							'<div class="module-banner full-width" data-adtoma-mobile="{{ #adtomaLayout.mobile }}{{ adtomaLayout.mobile }}:{{ /adtomaLayout.mobile }}{{ #adtomaMediazone.mobile }}{{ adtomaMediazone.mobile }}:{{ /adtomaMediazone.mobile }}{{ mobile }}" data-adtoma-desktop="{{ #adtomaLayout.desktop }}{{ adtomaLayout.desktop }}:{{ /adtomaLayout.desktop }}{{ #adtomaMediazone.desktop }}{{ adtomaMediazone.desktop }}:{{ /adtomaMediazone.desktop }}{{ desktop }}"></div>' +
							'{{ /adtomaAds.panorama }}', data);

            self.$pane.prepend(output);
          }
        }

        // clone panorama from header if not specified for article
        if ((!data.adtomaAds || !data.adtomaAds.panorama) && self.$headPanoClone.length) {
          // inject panorama
          self.$pane.prepend(self.$headPanoClone.clone().addClass('full-width'));
        }

        // custom body class?
        if (data.bodyClass && !BBM.$.body.hasClass(data.bodyClass)) {
          BBM.$.body.addClass(data.bodyClass);
          self.bodyClass = data.bodyClass;
        }

        // all set!
        BBM.asyncLoaded();
        self.currentUrl = url;
      });
    } else {
      if (self.bodyClass) BBM.$.body.addClass(self.bodyClass);
      BBM.asyncLoaded();
    }


    // open pane
    BBM.$.body.addClass('quicklook-open'); //.css('padding-right', this.scrollBarWidth);

    // open sticky menu after delay if header is visible
    if (BBM.headerResume.isHeaderInView()) {
      // todo: listen for transitionEnd instead
      setTimeout(function () {
        BBM.headerResume.setSticky(true);
      }, 800);
    }
    this.isOpen = true;
  },

  close: function () {
    BBM.$.body.removeClass('quicklook-open'); //.css('padding-right', '');

    if (BBM.stickySocialButtons)
      BBM.stickySocialButtons.hide();

    // only close sticky menu if header is visible
    if (BBM.headerResume.isHeaderInView()) BBM.headerResume.setSticky(false);

    // remove custom body class
    if (this.bodyClass) {
      BBM.$.body.removeClass(this.bodyClass);
    }

    BBM.trackPageview();
    this.isOpen = false;
  },

  goBack: function () {
    if (!this.isOpen) return;
    window.history.go(-1);
  },

  click: function (e) {
    // treat as normal link?
    if (e.metaKey || this.isOpen || !("history" in window)) return;

    e.preventDefault();
    e.stopPropagation();

    var url = $(e.currentTarget).attr('href');
    this.open(url);
  },

  construct: function () {
    this.$pane.after('<button class="quicklook-close-button" data-quicklook-close></button>');
  },

  init: function () {
    var self = this;
    self.construct();

    // open
    BBM.$.body.on('click', '[data-quicklook]', $.proxy(this.click, this));

    // close
    BBM.$.win.on('keyup', function (e) {
      if (e.keyCode == 27) self.goBack();
    });
    BBM.$.body.on('click', '.quicklook-wrapper, [data-quicklook-close]', function (e) {
      if ($(e.target).closest('.site-header').length) return;
      self.goBack();
    });

    // history events
    var onpopstate = function (e) {
      if (window.history.state && window.history.state.url) {
        // going forward in history, open quicklook
        self.open(window.history.state.url, true);
      } else {
        if (self.isOpen) {
          // close quicklook, go back to feed
          self.close();
        } else {
          // page has been reloaded on article page, reload feed
          window.location.reload();
        }
      }
    };

    // avoid popstate on page load that some browsers (Safari, old Chrome) fire
    if (document.readyState !== 'complete') {
      // load event has not fired
      window.addEventListener('load', function () {
        setTimeout(function () {
          window.addEventListener('popstate', onpopstate, false);
        }, 0);
      }, false);
    } else {
      // load event has fired
      window.addEventListener('popstate', onpopstate, false);
    }
  }
};




var BBM = BBM || {};

// Responsive events
BBM.responsiveEvents = {
  desktopBreakpoint: 910,
  currentState: null,

  onResize: function () {
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    // check if we crossed the breakpoint
    if (this.currentState != 'desktop' && width >= this.desktopBreakpoint) this.enterDesktop();
    else if (this.currentState != 'mobile' && width < this.desktopBreakpoint) this.enterMobile();
  },

  enterDesktop: function () {
    var prevState = this.currentState;

    // set current state
    this.currentState = 'desktop';

    // are we leaving a state? (doesn't trigger on page load)
    if (prevState == 'mobile') BBM.$.win.trigger('leaveMobile');

    // trigger new state
    BBM.$.win.trigger('enterDesktop');
  },

  enterMobile: function () {
    var prevState = this.currentState;
    this.currentState = 'mobile';
    if (prevState == 'desktop') BBM.$.win.trigger('leaveDesktop');
    BBM.$.win.trigger('enterMobile');
  },

  init: function (breakpoint) {
    // override default breakpoint?
    if (typeof breakpoint != 'undefined') this.desktopBreakpoint = breakpoint;

    // set init state and start listening for resize
    this.onResize();

    if (!BBM.$.win) BBM.$.win = $(window);

    BBM.$.win.on('resize', $.proxy(this.onResize, this));
  }
};


/* Init stuff */

BBM.readingList.init();
BBM.toggleActive();

/* Site specific stuff */

$('.select2').select2({
  placeholder: '',
  minimumResultsForSearch: -1
});

// stuff to run when content is loaded async (quicklook, readinglist, comments)
BBM.trackPageview = function ($article, data) {
  var url = document.location.href;

  if (typeof asyncBurtTracking == "function") {
    asyncBurtTracking();
  }
  if (typeof dataLayer != undefined) {
    var taglist;
    var sectionlist;
    var mainSection;
    var authorName;

    if ($article != null) {
      var tags = [];
      var verts = [];

      $.each($article.find('.article-author ul li a'), function (i, val) {
        verts.push(val.innerHTML);
      });

      $.each($article.find('.article-tags a'), function (i, val) {
        tags.push(val.innerHTML);
      });

      taglist = tags.join(', ');
      sectionlist = verts.join(', ');

      authorName = $article.find('div.pre-author a').text();
      mainSection = $article.find('div.article-author ul li a').first().text();
    }

    if (data != null) {
      sectionlist = data.verticals;
      taglist = data.tags;

      if (data.pageSections.length > 0) {
        mainSection = data.pageSections[0].name;
      }

      if (data.author) {
        authorName = data.author.linkText;
      }

      $('meta[name=CreatedDate').attr('content', data.dateCreated);
      $('meta[name=PublishDate').attr('content', data.datePublished);
      bbm.responsiveAdtoma.loadAdtoma();
    }

    _sf_async_config.sections = mainSection;
    _sf_async_config.authors = authorName;

    if (typeof pSUPERFLY == "object") {
      pSUPERFLY.virtualPage(window.location.pathname);
    }

    $('meta[name=sections]').attr('content', sectionlist);
    $('meta[name=tags]').attr('content', taglist);

    dataLayer.push({ 'event': 'VirtualPageview' });
  }
};

BBM.stickyElements = function () {
  $('.social-share').stick_in_parent({
    offset_top: 66,
    recalc_every: 1
  });
}

BBM.asyncLoaded = function ($article, data) {
  BBM.readingList.init(true);
  BBM.trackPageview($article, data);
  BBM.stickyElements();
};

$('body').on('click', function (e) {

  if ($('body').hasClass('fixed') && $('.main-navigation').hasClass('open'))
    $('body').removeClass('fixed');
  $('.main-navigation').removeClass('open');
  $('.header-menu-toggle').removeClass('open');

});

//Opens main-menu dropdown & closes search dropdown if its open.
$('.header-menu-toggle').on('click', function (e) {
  e.preventDefault();
  e.stopPropagation();

  $(this).toggleClass('open');
  $('.main-navigation').toggleClass('open');

  if ($('.search-form-dropdown, .search-btn').hasClass('active'))
    $('.search-form-dropdown, .search-btn').removeClass('active');

});

//Opens search dropdown & closes main-menu if its open.
$('.search-btn').on('click', function (e) {
  e.preventDefault();
  e.stopPropagation();

  $(this).toggleClass('active');
  $('.search-form-dropdown').toggleClass('active');

  if ($('.main-navigation, .header-menu-toggle ').hasClass('open'))
    $('.main-navigation, .header-menu-toggle').removeClass('open');

});

//Opens submenu content on hover.
$('.main-menu > li').hover(function () {
  $('.main-menu > li').removeClass('active');
  $('.sub-menu-navigation.active').removeClass('active');
  $('.sub-menu-navigation .latest-articles.active').removeClass('active');

  var el = $(this);

  el.addClass('active');

  var dropdownId = el.find('a').data('id');
  var dropdown = $('#' + dropdownId);

  dropdown.addClass('active');
  dropdown.find('.latest-articles:first-of-type').addClass('active');
});

$('.sub-menu > li > a').hover(function () {
  $('.sub-menu > li > a').removeClass('active');
  $('.sub-menu-navigation .latest-articles.active').removeClass('active');

  var el = $(this);

  el.addClass('active');

  var submenuId = el.data('id');
  var submenu = $('#' + submenuId);

  submenu.addClass('active');
});

$(window).scroll(function () {
  if ($(this).scrollTop() > 25) {
    $('.site-header, .category-title').addClass("fixed");
    $('.header-logo').hide();
    $('.header-logo-2').show();
  } else {
    $('.site-header, .category-title').removeClass("fixed");
    $('.header-logo').show();
    $('.header-logo-2').hide();
  }
});

BBM.stickyElements();

/* DIST JS*/
!function e(t,i,n){function a(s,r){if(!i[s]){if(!t[s]){var c="function"==typeof require&&require;if(!r&&c)return c(s,!0);if(o)return o(s,!0);var l=new Error("Cannot find module '"+s+"'");throw l.code="MODULE_NOT_FOUND",l}var u=i[s]={exports:{}};t[s][0].call(u.exports,function(e){var i=t[s][1][e];return a(i?i:e)},u,u.exports,e,t,i,n)}return i[s].exports}for(var o="function"==typeof require&&require,s=0;s<n.length;s++)a(n[s]);return a}({1:[function(e,t,i){var n=function(){};n.prototype={articles:e("./modules/articles"),searchpage:e("./modules/searchpage"),globalsearch:e("./modules/globalsearch")},t.exports=new n},{"./modules/articles":3,"./modules/globalsearch":4,"./modules/searchpage":6}],2:[function(e,t,i){!function(t){"use strict";window.BI=e("./bbm.app")}(jQuery)},{"./bbm.app":1}],3:[function(e,t,i){var n=function(e){this.options=$.extend({},this.defaults,e),this.init()};n.prototype={defaults:{lang:"en",currentPageId:0,size:10,skip:0,authorPageId:0,templateUrl:"/static/dist/mustache/articlelist.mustache"},getPageViewSpan:function(e){return e>=5e3?{"class":"very-hot",icon:"/dist/assets/icons/very-hot.svg"}:e>=1500?{"class":"hot",icon:"/dist/assets/icons/hot.svg"}:e>=400?{"class":"warm",icon:"/dist/assets/icons/warm.svg"}:{"class":"cold",icon:"/dist/assets/icons/cool.svg"}},fetchmoreArticles:function(e,t,i){var n=this,a=$("#spinner-small");a.removeClass("hide");var o="/api/v1/search/sections?page="+e+"&size="+n.options.size+"&authorPageId="+n.options.authorPageId+"&pageId="+n.options.currentPageId+"&skip="+n.options.skip;$.getJSON(o,function(e){n.options.skip+=e.items.length,n.getCorrectIcon(e.items);var t=n.options.templateUrl;$.get(t,function(t){var n=Mustache.render(t,e);a.addClass("hide"),$(i).append(n)}).fail(function(){})}).fail(function(){})},getCorrectIcon:function(e){var t=this;return $.each(e,function(e,i){this.pageViewObject=t.getPageViewSpan(this.pageViews),this.showPageViews=this.pageViews>=100}),e},bindClickEvents:function(){var e=this;$("#feed").on("click","#load-more",function(t){t.preventDefault();var i=$(this).data("page")+1;$(this).remove(),e.fetchmoreArticles(i,"","#feed")})},init:function(){this.bindClickEvents()}},t.exports=function(e){return new n(e)}},{}],4:[function(e,t,i){var n=function(e){this.options=$.extend({},this.defaults,e),this.init()};n.prototype={defaults:{lang:"en",searchUrl:""},search:function(e){var t="#query="+e.val(),i=this.options.searchUrl+t+"&sort=relevance";window.location.pathname===this.options.searchUrl?(window.location.hash=t,$(".header-search").removeClass("active"),e.val("")):window.location=i},bindSearchEvents:function(){var e=$("#search-input"),t=this;e.keyup(function(i){13==i.keyCode&&t.search(e)})},init:function(){this.bindSearchEvents()}},t.exports=function(e){return new n(e)}},{}],5:[function(e,t,i){function n(){this.getHashParameterByName=function(e){var t=window.location.hash;e=e.replace(/[\[\]]/g,"\\$&");var i=new RegExp("[#&]"+e+"(=([^&#]*)|&|#|$)"),n=i.exec(t);return n?n[2]?decodeURIComponent(n[2].replace(/\+/g," ")):"":null},this.getQueryParameterByName=function(e){var t=window.location.search;e=e.replace(/[\[\]]/g,"\\$&");var i=new RegExp("[?&]"+e+"(=([^&#]*)|&|#|$)"),n=i.exec(t);return n?n[2]?decodeURIComponent(n[2].replace(/\+/g," ")):"":null}}t.exports=new n},{}],6:[function(e,t,i){var n=function(e){this.options=$.extend({},this.defaults,e),this.init()};n.prototype={defaults:{lang:"en",searchUrl:"/api/v1/search/global",query:"#query=",take:10,page:1,templateUrl:"/static/dist/mustache/articlelist_small.mustache",sort:"Relevance",sectionPageId:null,target:"#feed"},getResultText:function(e,t,i){var n=t.page*this.options.take>t.totalHits?t.totalHits:t.page*this.options.take;e=""==e?"":"for <i>"+e+"</i>";var a=$("#section-"+i);return"Showing "+n+" of total "+t.totalHits+" results "+e+" in <i>"+a.html()+"</i>"},getSectionPageId:function(){var e=$("#sections-selector").find("option:selected").val();return e},updateUrlQuery:function(e){window.location.hash="";var t="#";""!=e.Query&&(t+="query="+e.Query),t+="&sort="+e.Sort,e.SectionPageId>0&&(t+="&sectionTag="+e.SectionPageId),history.replaceState(null,null,t)},search:function(e,t,i,n,a,o){var s=this,r=$("#results-item");r.html(""),void 0!==e&&""!==e&&null!==e||(e=$("#search-word").val()),void 0!==t&&null!=t||(t=s.options.page),void 0!==i&&null!=i||(i=s.options.take),(void 0===a||1>a||null==a)&&(a=s.getSectionPageId()),void 0!==n&&null!=n&&"undefined"!=n||(n=$(".sort-value.active").data("sort-value"));var c={Query:e,Take:i,Page:t,Sort:n,SectionPageId:a};o&&s.updateUrlQuery(c);var l;l=c.Page>1?$("#spinner-small"):$("#spinner-big"),l.removeClass("hide");var u=s.options.searchUrl+"?query="+c.Query+"&page="+c.Page+"&size="+c.Take+"&sort="+c.Sort+"&sectionTag="+a;$.getJSON(u,function(e){s.getCorrectIcon(e.items);var t=s.options.templateUrl;$.get(t,function(t){var i=Mustache.render(t,e);c.Page>1?$(s.options.target).append(i):$(s.options.target).html(i),r.html(s.getResultText(c.Query,e,c.SectionPageId)),l.addClass("hide")}).fail(function(){})}).fail(function(){})},getCorrectIcon:function(e){var t=this;return $.each(e,function(e,i){this.pageViewObject=t.getPageViewSpan(this.pageViews),this.showPageViews=this.pageViews>=100}),e},getPageViewSpan:function(e){return e>=5e3?{"class":"very-hot",icon:"/dist/assets/icons/very-hot.svg"}:e>=1500?{"class":"hot",icon:"/dist/assets/icons/hot.svg"}:e>=400?{"class":"warm",icon:"/dist/assets/icons/warm.svg"}:{"class":"cold",icon:"/dist/assets/icons/cool.svg"}},bindSortSelector:function(){var e=this;$(".sort-value").on("click",function(t){t.preventDefault(),$(".sort-value").each(function(){$(this).removeClass("active")}),$(this).addClass("active"),$(e.options.target).html("");var i=$(this).data("sort-value");e.search("",e.options.page,e.options.take,i,0,!0)})},bindOptionsSelector:function(){var e=this;$("#sections-selector").change(function(){$(e.options.target).html("");var t=$(this).find("option:selected").val();e.search("",e.options.page,e.options.take,null,t,!0)})},bindLoadMoreSearchEvent:function(){var e=this;$(e.options.target).on("click","#load-more",function(){var t=$(this).data("page")+1;$(this).remove(),e.search("",t,e.options.take,!1)})},bindSearchEvents:function(){var e=this;$("#search-word").keyup(function(t){13==t.keyCode&&($(e.options.target).html(""),e.search(this.value,e.options.page,e.options.take,!0))})},getSearchItemsFromHash:function(){var t=e("./location"),i=t.getHashParameterByName("query"),n=t.getHashParameterByName("sort"),a=t.getHashParameterByName("sectionTag");return{query:i,sort:n,sectionTag:a}},updateValuesFromHash:function(e){""!==e.query&&null!==e.query&&$("#search-word").val(e.query),null!=e.sort&&"undefined"!=e.sort?$("#"+e.sort.toLowerCase()).addClass("active"):$("#relevance").addClass("active"),null!=e.sectionTag&&$("#section-"+e.sectionTag).attr("selected","selected")},initialSearch:function(){var e=this.getSearchItemsFromHash();this.updateValuesFromHash(e),this.search(e.query,e.page,e.size,e.sort,e.sectionTag,!0)},init:function(){this.initialSearch(),this.bindLoadMoreSearchEvent(),this.bindSearchEvents(),this.bindOptionsSelector(),this.bindSortSelector()}},t.exports=function(e){return new n(e)}},{"./location":5}]},{},[2]);