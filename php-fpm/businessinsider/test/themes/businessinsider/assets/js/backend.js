/**
 * Created by matteobarale on 11/10/16.
 */

jQuery(document).ready(function () {

   jQuery('#taxonomy-category input[type=checkbox]').on('click', function () {
        var clicked_status = jQuery(this).attr('checked');
        var clicked_id = jQuery(this).attr('id');
       //console.log(clicked);
       if(clicked_status == 'checked'){
           jQuery.each(jQuery('#taxonomy-category input[type=checkbox]'), function(key, value){
               if(jQuery(value).attr('id') != clicked_id){
                   jQuery(value).attr("disabled", true).addClass('disabled');
               }
           });
       }else{
           jQuery.each(jQuery('#taxonomy-category input[type=checkbox]'), function(key, value){
               if(jQuery(value).attr('id') != clicked_id){
                   jQuery(value).attr("disabled", false).removeClass('disabled');
               }
           });
       }
    });

});
