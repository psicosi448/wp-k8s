jQuery(document).ready(function(){

    jQuery('#load-more').on('click', function (e) {
        e.preventDefault();
        jQuery(this).hide();
        jQuery('#spinner-small').attr('style','display: block !important');

        var page = jQuery(this).attr('data-page');
        var url = jQuery(this).attr('data-url');
        var action = jQuery(this).attr('data-action');
        var number = jQuery(this).attr('data-number');
        var type = jQuery(this).attr('data-type');
        var container = jQuery(this).attr('data-container');
        var id = jQuery(this).attr('data-id');
        var data = {
            page: page,
            action: action,
            number:number,
            type: type,
            id: id
        };
        var s = jQuery(this).attr('data-s');
        if (typeof s !== 'undefined' && s !== false) {
            data.s = s;
        }
        var categories = jQuery(this).attr('data-categories');
        if (typeof categories !== 'undefined' && categories !== false) {
            data.categories = categories;
        }

        console.log(data);
        jQuery.ajax({
            method: "GET",
            url: url,
            data:data,
            context: this,
            dataType: 'json',
            beforeSend: function (jqXHR, settings) {
                url = settings.url + "?" + settings.data;
                console.log(url);
            },
            success: function(data){
                if(typeof data !== undefined){
                    if(data.status == 'success'){
                        if(type == '404'){
                            jQuery('#' + container).append(data.content);
                        }else{
                            jQuery(data.content).insertBefore($(this));
                        }
                        jQuery(this).attr('data-page', parseInt(page) + 1);
                        jQuery('#spinner-small').attr('style','display: none !important');
                        jQuery(this).show();
                    }else if(data.status == 'nopost'){
                        jQuery(this).attr('value', 'Non ci sono altri post');
                        jQuery(this).attr('disabled','disabled');
                        jQuery('#spinner-small').attr('style','display: none !important');
                        jQuery(this).show();
                    }else if(data.status == 'successnopost'){
                        if(type == '404'){
                            jQuery('#' + container).append(data.content);
                        }else{
                            jQuery(data.content).insertBefore($(this));
                        }
                        jQuery(this).attr('value', 'Non ci sono altri post');
                        jQuery(this).attr('disabled','disabled');
                        jQuery('#spinner-small').attr('style','display: none !important');
                        jQuery(this).show();
                    }
                    jQuery("img.lazy").lazyload();
                }
            },
            error: function(data){
                console.log(data);
            }
        });
    });
});


jQuery(document).ready(function() {

    if(typeof clickid !== 'undefined' && typeof clickurl !== 'undefined' ){
        jQuery.ajax({
            async: true,
            cache: true,
            url: clickurl+'?ugcId='+clickid,
            error: function(e, msg) {
                console.log('errore click');
            },
            success: function(msg) {
                console.log('successo click');
            },
            dataType: 'script'
        });
    }
});

jQuery(document).ready(function () {
    jQuery( "#solr_categories" ).on('change', function() {
        var base_url = jQuery(this).attr('data-url');
        var s = jQuery(this).attr('data-s');
        if(this.value != ''){
            window.location.replace(base_url + '/?s=' + s + '&categories=' + this.value);
        }else{
            window.location.replace(base_url + '/?s=' + s);
        }

    });

    jQuery('.sort-value').on('click', function () {
        var sort = jQuery(this).attr('data-sort-value');
        var base_url = jQuery(this).attr('data-url');
        var s = jQuery(this).attr('data-s');
        var categories = jQuery(this).attr('data-categories');
        if(typeof categories !== 'undefined' && categories !== false){
            var url = base_url + '/?s=' + s + '&categories=' + this.value;
        }else{
            var url = base_url + '/?s=' + s;
        }
        if(sort == 'Date'){
            url  = url + '&sort=' + sort;
        }
        window.location.replace(url);
    });



    jQuery("img.lazy").show().lazyload({
        effect : "fadeIn",
        skip_invisible : true,
        failure_limit : 100,
    });
    jQuery('.sub-menu-navigation .teaser-text').hide();
    jQuery("li.category-menu").hover(function(){
        var dataid = jQuery(this).find('a:first');;
        dataid = dataid.attr('data-id');
        jQuery('#' + dataid + ' img.menu.lazy').each(function( index ) {
           jQuery(this).show().lazyload();
        });
    });
    $('img.menu.lazy').on('appear',function(){
        first_container = jQuery(this).parent();
        main_continer = first_container.parent();
        main_continer.find('.teaser-text').show();
    });



    /*jQuery( ".showmap" ).hover(
      function() {
        jQuery( ".site-header .main-navigation" ).show();
      }, function() {
        jQuery( ".site-header .main-navigation" ).hide();
      }
    );*/

    jQuery('.showmap').on('click', function (e) {
        e.preventDefault();
        jQuery('.main-navigation .wrapper').hide();
        jQuery('.main-navigation').show();
        jQuery('.main-navigation .main-navigation-dropdown').show();
        jQuery('.main-navigation .main-navigation-dropdown .sub-menu-navigation').show();
      });

    jQuery('.close-btn').on('click', function (e) {
          e.preventDefault();
          jQuery('.main-navigation').hide();
        });

})


var breakpoint = {};
breakpoint.refreshValue = function () {
    this.value = window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/\"/g, '');
};


var videoMove = function() {
    var videolist = document.getElementsByClassName("entry-video")[0];
    if (breakpoint.value == 'desktop') {
        document.getElementsByClassName('article-wrapper')[0].prepend(videolist);
    }else {
        document.getElementsByClassName('article-body')[0].prepend(videolist);
    }
}

if(document.body.classList.contains('single-format-video')) {
    jQuery(window).resize(function () {
        var pre = breakpoint.value;
        breakpoint.refreshValue();
        var post = breakpoint.value;
	if(pre != post) {
            videoMove();
        }
    }).resize();
}

