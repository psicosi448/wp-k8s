<?php
get_header();  $post_not_in = [];
$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;
$acf_key = $taxonomy . '_' . $term_id;
?>

    <!-- MAIN -->
    <main style="min-height: 100vh;">
        <div class="quicklook-wrapper wrapper">
            <div class="wrapper">
                <div class="group-primary">
                    <?php if(have_posts()): ?>
                        <div id="feed">
                            <div id="adv-x22"><script>try { MNZ_RICH('x22'); } catch(e) { }</script></div>
                            <?php $count = 1; while (have_posts()): the_post(); $post_not_in[] = get_the_ID();
                                if(get_the_ID() == $exclude){
                                    $count ++;
                                    continue;

                                }
                                ?>
                                <?php $native = get_field('native'); ?>
                                <?php /*
							if($native){ ?>
								<?php get_template_part('template/teaser', 'small_native'); ?>
							<?php }else { ?>
								<?php get_template_part('template/teaser', 'small'); ?>
							<?php }  */ ?>
                                <?php get_template_part('template/teaser', 'small'); ?>
                                <?php if($count == 2): ?>
                                    <div id="adv-Middle1-dev"><script>try { MNZ_RICH('Middle1-dev'); } catch(e) { }</script></div>
                                <?php endif; $count++; ?>
                            <?php endwhile;?>
                            <?php global $wp_query;
                            if($wp_query->max_num_pages > 1):  ?>
                                <input type="button" class="btn btn-big btn-attend" id="load-more" data-page="2" value="Carica altri" data-url="<?php bloginfo('url')?>" data-action="load-more" data-number="10" data-type="category" data-id="<?php echo $wp_query->get_queried_object()->term_id; ?>" data-container="feed" />
                            <?php endif; ?>
                        </div>
                    <?php endif;  ?>

                </div>


                <div class="group-minor group-minor-sidebar">
                    <?php get_sidebar(); ?>
                </div>


            </div>


        </div>
    </main>
    <!-- END MAIN -->
<?php get_footer(); ?>