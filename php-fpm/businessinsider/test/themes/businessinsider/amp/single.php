<!doctype html>
<html amp <?php echo AMP_HTML_Utils::build_attributes_string( $this->get( 'html_tag_attributes' ) ); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <?php global $post;  ?>
    <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
    <script async custom-element="amp-anim" src="https://cdn.ampproject.org/v0/amp-anim-0.1.js"></script>
    <script async custom-element="amp-user-notification" src="https://cdn.ampproject.org/v0/amp-user-notification-0.1.js"></script>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <?php do_action( 'amp_post_template_head', $this ); ?>
    <style amp-custom>
        <?php $this->load_parts( array( 'style' ) ); ?>
        <?php do_action( 'amp_post_template_css', $this ); ?>
    </style>
</head>

<body class="<?php echo esc_attr( $this->get( 'body_class' ) ); ?>">

<?php

if(strpos(get_bloginfo("url"), "test.") === false){
    $url_analytics = "https://scripts.kataweb.it/ws/wt/getWTConf.php";
}else{
    $url_analytics = "https://scripts.kataweb.it/ws/test/wt/getWTConf.php";
}


/*
 *  tag di analytics
 */
$dom = new DOMDocument;
$dom->loadHTML(apply_filters("the_content", $post->post_content));
$allElements = $dom->getElementsByTagName('*');
$elementDistribution = array();
foreach($allElements as $element) {
    if(array_key_exists($element->tagName, $elementDistribution)) {
        $elementDistribution[$element->tagName] += 1;
    } else {
        $elementDistribution[$element->tagName] = 1;
    }
}
$countimg = 1 + $elementDistribution["img"];
$countvideo = $elementDistribution["iframe"];

//$t = wp_get_post_tags($post->ID);

$categorie = wp_get_post_categories($post->ID, ['fields' => 'all']);
$tags = wp_get_post_tags($post->ID, ['fields' => 'all']);
if(!empty($tags)){
	$t= array_merge($categorie, $tags);
}else{
    $t=$categorie;
}

$brands = wp_get_post_terms($post->ID, "brand", ['fields' => 'all']);
if(!empty($brands)) {
	$t = array_merge( $brands, $t );
}

//print_r($t);
$strtag = "";
$ctag=0;
foreach ($t as $tag){
    if($ctag)
        $strtag .= ",";
    $strtag .= urlencode($tag->slug);
    $ctag++;
}

$url_analytics .= "?url=".str_replace("https://", "http://", get_permalink($post))."&pubdate=".date("Y-n-d", strtotime($post->post_date))."&pagetype=dettaglio_content&content_video=".$countvideo."&content_foto=".$countimg."&tags=".$strtag."&site=mobile&mobile=amp&format=amp";
echo "<!-- ".$url_analytics." //-->";
$result = wp_cache_get( 'amp_analytics_'.$post->ID );
if ( false === $result ) {
    $response = wp_remote_get( $url_analytics );
    if( is_array($response) ) {
        $header = $response['headers']; // array of http header lines
            $body = $response['body']; // use the content
            $result = $body;
    }
    wp_cache_set( 'amp_analytics_'.$post->ID, $result );
}
if($result){

    echo $result;
}
// fine tag analytics
?>



<amp-user-notification layout=nodisplay id="amp-user-notification-privacy">
    Su questo sito utilizziamo cookie tecnici e, previo tuo consenso, cookie di profilazione, nostri e di terze parti, per proporti pubblicità in linea con le tue preferenze. Se vuoi saperne di più o prestare il consenso solo ad alcuni utilizzi <a href="https://login.kataweb.it/static/privacy/?editore=gruppoespresso">clicca qui</a>
    <button on="tap:amp-user-notification-privacy.dismiss">OK</button>
</amp-user-notification>
<!-- esi:include  http://scripts.kataweb.it/ws/pub-amp/index.php?adref=<?php echo urlencode(get_permalink($post)); ?>&pos=Top //-->
<esi:include src="http://scripts.kataweb.it/ws/pub-amp/index.php?adref=<?php echo urlencode(get_permalink($post)); ?>&pos=Top" />

<?php $this->load_parts( array( 'header-bar' ) ); ?>

<article class="amp-wp-article">

    <header class="amp-wp-article-header">
        <?php
        $is_native = is_native($post);
        if($is_native): //Mostro la label solo se native?>
            <div  class="native-label bigpage">Contenuto sponsorizzato</div>
        <?php endif; //Fine controllo native ?>
        <?php
        // controllo se è associato ad un brand
        $brands = wp_get_post_terms( $post->ID, "brand");
        // controllo se è abilitato il link alla bigpage

        $link_bigpage = get_field('link_bigpage', $post->ID);
        //if($link_bigpage){
        if(is_array($brands) && count($brands)){
            $brand = $brands[0];
            $brandlink = get_term_link($brand);
            if(!is_wp_error($brandlink) && (($is_native && $link_bigpage) || (!$is_native))){
                echo "<div class='bigpage link-bigpage'><a href='".$brandlink."'>Vai alla pagina di ".$brand->name."</a></div>";
            }

        }
        ?>
        <h1 class="amp-wp-title"><?php echo wp_kses_data( $this->get( 'post_title' ) ); ?></h1>
        <?php $this->load_parts( apply_filters( 'amp_post_article_header_meta', array( 'meta-author', 'meta-time' ) ) ); ?>
    </header>

    <?php
    $mostro_immagine = get_field('mostrare_immagine_in_evidenza');
    if($mostro_immagine == 'si')
        $this->load_parts( array( 'featured-image' ) );
    ?>
    <!-- ADV -->
    <div class="container-ad">
        <!-- esi:include  http://scripts.kataweb.it/ws/pub-amp/index.php?adref=<?php echo urlencode(get_permalink($post)); ?>&pos=Middle1 //-->
        <esi:include src="http://scripts.kataweb.it/ws/pub-amp/index.php?adref=<?php echo urlencode(get_permalink($post)); ?>&pos=Middle1" />
    </div>

    <div class="amp-wp-article-content">
        <?php
        $format = get_post_format();
        if($format == "gallery"){
            if( have_rows('media_gallery') ):
                // Ciclo su tutti i capi
                while ( have_rows('media_gallery') ) : the_row();
                    if( get_row_layout() == 'embed' ):
                        the_sub_field('codice_embed');
                    elseif ( get_row_layout() == 'immagine' ):
                        $id = get_sub_field('immagine');
                        $url = wp_get_attachment_image_src( $id, "large");
                        echo '<amp-img width="'.$url[1].'" height="'.$url[2].'" src="'.$url[0].'" class="attachment-large size-large wp-post-image amp-wp-enforced-sizes" ></amp-img>';
                        $caption = get_sub_field('didascalia');
                        echo $caption;
                    elseif( get_row_layout() == 'blocco_testo'):
                        the_sub_field('area_di_testo');
                    endif;
                endwhile;
            endif;
        }
        echo $this->get( 'post_amp_content' ); // amphtml content; no kses ?>
    </div>

    <?php
    /*
        <footer class="amp-wp-article-footer">
        <?php $this->load_parts( apply_filters( 'amp_post_article_footer_meta', array( 'meta-taxonomy', 'meta-comments-link' ) ) ); ?>
    </footer>
     */
    ?>

    <!-- ADV -->
    <div class="container-ad">
        <!-- esi:include  http://scripts.kataweb.it/ws/pub-amp/index.php?adref=<?php echo urlencode(get_permalink($post)); ?>&pos=Middle2 //-->
        <esi:include src="http://scripts.kataweb.it/ws/pub-amp/index.php?adref=<?php echo urlencode(get_permalink($post)); ?>&pos=Middle2" />
    </div>

</article>

<?php $this->load_parts( array( 'footer' ) ); ?>

<?php do_action( 'amp_post_template_footer', $this ); ?>

<amp-analytics type="chartbeat">
    <script type="application/json">
        {
            "vars": {
                "uid": "62281",
                "domain": "it.businessinsider.com"
            }
        }
    </script>
</amp-analytics>
</body>
</html>
