
.amp-wp-article-header .amp-wp-posted-on.amp-wp-meta:first-of-type { text-align:right}
#main-header{padding:0;border-bottom:1px solid #e6e6e6;text-align:center}
#main-header .logo-container{width:245px;margin:17px auto 12px;height:30px;display:inline-block}
#main-header a{display:block}
#main-header img{height:auto;width:245px;}

amp-user-notification{min-height:30px;padding:8px;width:auto;z-index:11002;bottom:36px}
amp-user-notification button{border:none;border-radius:2px;height:26px;min-width:32px;padding:0 16px;margin:0 16px;cursor:pointer;vertical-align:middle;text-align:center}
@keyframes fadeIn{from{opacity:0}
to{opacity:1}
}
amp-user-notification.amp-active{-webkit-animation:fadeIn ease-in 1s 1 forwards;-moz-animation:fadeIn ease-in 1s 1 forwards;animation:fadeIn ease-in 1s 1 forwards}

amp-user-notification{font:14px/1.8 lato-regular,Arial,sans-serif;color:#fff;background:#1a1a1a}
amp-user-notification a{color:#fff}
amp-user-notification button{background:#037cbc;color:#fff;text-transform:uppercase;letter-spacing:0;line-height:26px}


.adv-container{text-align:center}
.container-prova{margin:0 -20px;text-align:center}
.container-ad{text-align:center}
.adv-floor .adv-placeholder amp-anim{top:18px}
.adv-floor amp-ad{margin-bottom:20px}
amp-ad{margin:5px 0;background:#f4f4f4}
.article-body amp-ad{display:block;margin:15px auto;text-align:center}
.adv-placeholder amp-anim{position:relative;top:42px}
.adv-middle1 amp-anim{position:relative;top:117px}
[creative-id*=dfp-_empty_]{display:none}

.adv-container,.article-body amp-ad{background:#f2f2f2}



.taboola-container{margin:20px 0}
.taboola-container .taboola-widget{padding:20px;background:#e5e5e5}

.fixed-bar-container{position:fixed;bottom:0;left:0;z-index:999;width:100%}

.gs-custom-theme-rep-2016-v1 .gs-sharebar-cell{width:25%;float:left;text-align:center;line-height:36px;color:#fff}
.gs-custom-theme-rep-2016-v1 .gs-sharebar-cell a{height:36px;display:block;background-repeat:no-repeat;background-position:center center;background-size:18px 18px}
.gs-custom-theme-rep-2016-v1 .gig-button-container-facebook a{background-color:#3b5998;background-image:url('data:image/svg+xml;utf8,<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 18" enable-background="new 0 0 18 18" xml:space="preserve"><path fill="%23ffffff" d="M10.3,17.4V9.7h2.5l0.4-3h-2.8V4.8c0-0.9,0.2-1.5,1.4-1.5l1.5,0V0.7c-0.3,0-1.2-0.1-2.2-0.1C8.8,0.6,7.3,2,7.3,4.5v2.2H4.8v3h2.5v7.7H10.3z"/></svg>');background-size:18px}
.gs-custom-theme-rep-2016-v1 .gig-button-container-twitter a{background-color:#00aced;background-image:url('data:image/svg+xml;utf8,<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 18" enable-background="new 0 0 18 18" xml:space="preserve"><path fill="%23ffffff" d="M17.8,3.6c-0.5,0.7-1.1,1.3-1.8,1.9v0.5c0,1.2-0.2,2.4-0.7,3.6c-0.4,1.2-1.1,2.3-2,3.3c-0.9,1-1.9,1.8-3.2,2.4c-1.3,0.6-2.7,0.9-4.4,0.9c-2,0-3.9-0.5-5.5-1.6c0.1,0,0.3,0,0.4,0s0.3,0,0.5,0c0.8,0,1.6-0.1,2.4-0.4C4.3,13.9,5,13.5,5.6,13c-0.8,0-1.5-0.3-2.1-0.7c-0.6-0.5-1-1.1-1.3-1.8c0.1,0.1,0.2,0.1,0.3,0.1c0.1,0,0.2,0,0.3,0c0.3,0,0.6,0,1-0.1c-0.8-0.2-1.5-0.6-2-1.2C1.2,8.6,1,7.8,1,6.9v0c0.5,0.3,1,0.4,1.6,0.5C2.1,7,1.7,6.6,1.4,6.1C1.1,5.5,1,5,1,4.3c0-0.6,0.2-1.2,0.5-1.8c0.9,1.1,2,2,3.2,2.6c1.3,0.7,2.7,1,4.2,1.1C8.8,6,8.8,5.8,8.8,5.5c0-0.5,0.1-0.9,0.3-1.4c0.2-0.4,0.4-0.8,0.8-1.1c0.3-0.3,0.7-0.6,1.1-0.8c0.4-0.2,0.9-0.3,1.4-0.3c1,0,1.9,0.4,2.6,1.1c0.4-0.1,0.8-0.2,1.2-0.3c0.4-0.1,0.7-0.3,1.1-0.5c-0.3,0.8-0.8,1.5-1.6,2C16.4,4,17.1,3.9,17.8,3.6z" /></svg>');background-size:18px}
.gs-custom-theme-rep-2016-v1 .gig-button-container-whatsapp a{background-color:#64d448;background-image:url('data:image/svg+xml;utf8,<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 18" enable-background="new 0 0 18 18" xml:space="preserve"><g><g><path fill-rule="evenodd" clip-rule="evenodd" fill="%23ffffff" d="M14,3.9c-1.3-1.3-3.1-2.1-5-2.1C5.1,1.9,2,5,2,8.9c0,1.2,0.3,2.5,0.9,3.5l-1,3.7l3.7-1C6.7,15.7,7.8,16,9,16h0c3.9,0,7.1-3.2,7.1-7.1C16.1,7.1,15.4,5.3,14,3.9zM9,14.8L9,14.8c-1.1,0-2.1-0.3-3-0.8l-0.2-0.1l-2.2,0.6l0.6-2.2L4,12.1c-0.6-0.9-0.9-2-0.9-3.1c0-3.2,2.6-5.9,5.9-5.9c1.6,0,3,0.6,4.2,1.7s1.7,2.6,1.7,4.2C14.9,12.2,12.3,14.8,9,14.8zM12.3,10.4c-0.2-0.1-1-0.5-1.2-0.6c-0.2-0.1-0.3-0.1-0.4,0.1c-0.1,0.2-0.5,0.6-0.6,0.7c-0.1,0.1-0.2,0.1-0.4,0C9.5,10.6,9,10.4,8.3,9.8c-0.5-0.5-0.9-1-1-1.2c-0.1-0.2,0-0.3,0.1-0.4C7.5,8.1,7.6,8,7.6,7.9c0.1-0.1,0.1-0.2,0.2-0.3c0.1-0.1,0-0.2,0-0.3S7.4,6.3,7.3,6C7.1,5.6,7,5.7,6.9,5.7c-0.1,0-0.2,0-0.3,0s-0.3,0-0.5,0.2C5.9,6.1,5.4,6.5,5.4,7.4c0,0.9,0.6,1.7,0.7,1.8c0.1,0.1,1.2,1.9,3,2.7c0.4,0.2,0.8,0.3,1,0.4c0.4,0.1,0.8,0.1,1.1,0.1c0.3-0.1,1-0.4,1.2-0.8s0.1-0.8,0.1-0.8C12.5,10.5,12.4,10.5,12.3,10.4z"/></g></g></svg>');background-size:21px}
.gs-custom-theme-rep-2016-v1 .gig-button-container-email a{background-color:#666;background-image:url('data:image/svg+xml;utf8,<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 18" enable-background="new 0 0 18 18" xml:space="preserve"><path fill="none" d="M9.5,11.2c0,0-0.1,0.1-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0c0,0,0,0,0,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0s0,0,0,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0-0.1,0-0.1-0.1l0,0c0,0,0,0,0,0l-6.3-5v6.5h13.7V6.3L9.5,11.2z"/><polygon fill="none" points="14.5,5.3 3.6,5.3 9,9.6"/><path fill="%23fff" d="M16.4,3.7H1.6c-0.6,0-1.1,0.5-1.1,1.1v8.4c0,0.6,0.5,1.1,1.1,1.1h14.7c0.6,0,1.1-0.5,1.1-1.1V4.8C17.4,4.2,16.9,3.7,16.4,3.7zM14.5,5.3L9,9.6L3.6,5.3H14.5zM2.2,12.7V6.2l6.3,5c0,0,0,0,0,0l0,0c0,0,0.1,0.1,0.1,0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0,0,0,0c0.1,0,0.1,0,0.2,0c0,0,0,0,0,0s0,0,0,0c0.1,0,0.1,0,0.2,0c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0,0,0,0c0,0,0.1,0,0.1-0.1l6.3-4.9v6.4H2.2z"/></svg>');background-size:19px}


/* Generic WP styling */

.main-footer{padding:20px 20px 45px;}
.main-footer p{font:13px/16px 'Roboto Condensed', Arial,sans-serif;text-align:center; color:#111;}
.main-footer p small{margin-top:5px;display:block}
.main-footer .privacy_link{color:#111;text-decoration:none;display:block}

.alignright {
float: right;
}

.alignleft {
float: left;
}

.aligncenter {
display: block;
margin-left: auto;
margin-right: auto;
}

.amp-wp-enforced-sizes {
/** Our sizes fallback is 100vw, and we have a padding on the container; the max-width here prevents the element from overflowing. **/
max-width: 100%;
margin: 0 auto;
}

.amp-wp-unknown-size img {
/** Worst case scenario when we can't figure out dimensions for an image. **/
/** Force the image into a box of fixed dimensions and use object-fit to scale. **/
object-fit: contain;
}

/* Template Styles */

.amp-wp-content,
.amp-wp-title-bar div {
margin: 0 auto;
max-width: 600px;
}

html {
background: #0a89c0;
}

body {
background: #fff;
color: #353535;
font-family: 'Roboto Condensed', sans-serif;
font-weight: 300;
line-height: 1.75em;
}

p,
ol,
ul,
figure {
margin: 0 0 1em;
padding: 0;
}

a,
a:visited {
color: #0a89c0;
}

a:hover,
a:active,
a:focus {
color: #353535;
}

/* Quotes */

blockquote {
color: #353535;
background: rgba(127,127,127,.125);
border-left: 2px solid #0a89c0;
margin: 8px 0 24px 0;
padding: 16px;
}

blockquote p:last-child {
margin-bottom: 0;
}

/* UI Fonts */

.amp-wp-meta,
.amp-wp-header div,
.amp-wp-title,
.wp-caption-text,
.amp-wp-tax-category,
.amp-wp-tax-tag,
.amp-wp-comments-link,
.amp-wp-footer p,
.back-to-top {
font-family: 'Roboto Condensed', sans-serif;
}

/* Header */

.amp-wp-header {
background-color: #125f7f;
}

.amp-wp-header div {
color: #fff;
font-size: 1em;
font-weight: 400;
margin: 0 auto;
max-width: calc(840px - 32px);
padding: .875em 16px;
position: relative;
}

.amp-wp-header a {
color: #fff;
text-decoration: none;
}

/* Site Icon */

.amp-wp-header .amp-wp-site-icon {
/** site icon is 32px **/
background-color: #fff;
border: 1px solid #fff;
border-radius: 50%;
position: absolute;
right: 18px;
top: 10px;
}

/* Article */

.amp-wp-article {
color: #353535;
font-weight: 400;
margin: 1.5em auto;
max-width: 840px;
overflow-wrap: break-word;
word-wrap: break-word;
font-family: 'Georgia Regular';
}

/* Article Header */

.amp-wp-article-header {
align-items: center;
align-content: stretch;
flex-wrap: wrap;
justify-content: space-between;
margin: 1.5em 16px 1.5em;
border-bottom: 1px solid #d3d3d3;
}

.amp-wp-title {
color: #000;
display: block;
flex: 1 0 100%;
margin: 0 0 .625em;
width: 100%;
font-size: 34px;
line-height: 37px;
}


/* Article Meta */

.amp-wp-meta {
color: #696969;
display: inline-block;
font-size: 14px;
line-height: 15px;
margin: 0;
padding: 0 0 10px 0;
}

.amp-wp-article-header .amp-wp-meta:last-of-type {
text-align: left;
}

.amp-wp-byline amp-img,
.amp-wp-byline .amp-wp-author {
display: inline-block;
vertical-align: middle;
border-right:1px solid #d3d3d3;
padding-right:15px;
}

.amp-wp-meta.amp-wp-posted-on {padding-left:10px; color:#000;}

.amp-wp-author.author.vcard a {text-decoration: none; font-weight: 900; color:#125f7f;}

.amp-wp-byline amp-img {
border: 1px solid #0a89c0;
border-radius: 50%;
position: relative;
margin-right: 6px;
}

.amp-wp-posted-on {
text-align: right;
}

/* Featured image */

.amp-wp-article-featured-image {
margin: 0 0 1em;
}
.amp-wp-article-featured-image amp-img {
margin: 0 auto;
}
.amp-wp-article-featured-image.wp-caption .wp-caption-text {
margin: 0 18px;
}

/* Article Content */

.amp-wp-article-content {
margin: 0 16px;
}

.amp-wp-article-content p {
font-family: 'Georgia Regular';
font-size: 20px;
line-height: 24px;
}

.amp-wp-article-content ul,
.amp-wp-article-content ol {
margin-left: 1em;
}

.amp-wp-article-content amp-img {
margin: 0 auto;
}

.amp-wp-article-content amp-img.alignright {
margin: 0 0 1em 16px;
}

.amp-wp-article-content amp-img.alignleft {
margin: 0 16px 1em 0;
}

.media-image dd {
font-size: 16px;
line-height: 16px;
font-style: italic;
padding-right: 5px;
display: block;
border-bottom: 1px solid #ddd;
margin: 0 0 10px 0;
padding-bottom:5px;
font-family: 'Georgia Regular';
}


/* Captions */

.wp-caption {
padding: 0;
}

.wp-caption.alignleft {
margin-right: 16px;
}

.wp-caption.alignright {
margin-left: 16px;
}

.wp-caption .wp-caption-text {
border-bottom: 1px solid #c2c2c2;
color: #696969;
font-size: .875em;
line-height: 1.5em;
margin: 0;
padding: .66em 10px .75em;
}

/* AMP Media */

amp-carousel {
background: #c2c2c2;
margin: 0 -16px 1.5em;
}
amp-iframe,
amp-youtube,
amp-instagram,
amp-vine {
background: #c2c2c2;
margin: 0 -16px 1.5em;
}

.amp-wp-article-content amp-carousel amp-img {
border: none;
}

amp-carousel > amp-img > img {
object-fit: contain;
}

.amp-wp-iframe-placeholder {
background: #c2c2c2 url( <?php bloginfo("url"); ?>/wp-content/plugins/amp/assets/images/placeholder-icon.png ) no-repeat center 40%;
background-size: 48px 48px;
min-height: 48px;
}

/* Article Footer Meta */

.amp-wp-article-footer .amp-wp-meta {
display: block;
}

.amp-wp-tax-category,
.amp-wp-tax-tag {
color: #696969;
font-size: .875em;
line-height: 1.5em;
margin: 1.5em 16px;
}

.amp-wp-comments-link {
color: #696969;
font-size: .875em;
line-height: 1.5em;
text-align: center;
margin: 2.25em 0 1.5em;
}

.amp-wp-comments-link a {
border-style: solid;
border-color: #c2c2c2;
border-width: 1px 1px 2px;
border-radius: 4px;
background-color: transparent;
color: #0a89c0;
cursor: pointer;
display: block;
font-size: 14px;
font-weight: 600;
line-height: 18px;
margin: 0 auto;
max-width: 200px;
padding: 11px 16px;
text-decoration: none;
width: 50%;
-webkit-transition: background-color 0.2s ease;
transition: background-color 0.2s ease;
}

/* AMP Footer */

.amp-wp-footer {
background:#f4f4f4;
padding:20px 20px 45px;
border-top: 1px solid #c2c2c2;
margin: calc(1.5em - 1px) 0 0;
}

.amp-wp-footer div {
margin: 0 auto;
max-width: calc(840px - 32px);
padding: 1.25em 16px 1.25em;
position: relative;
}

.amp-wp-footer h2 {
font-size: 1em;
line-height: 1.375em;
margin: 0 0 .5em;
}

.amp-wp-footer p {
color: #696969;
font-size: .8em;
line-height: 1.5em;
margin: 0 85px 0 0;
}

.amp-wp-footer a {
text-decoration: none;
}

.back-to-top {
bottom: 1.275em;
font-size: .8em;
font-weight: 600;
line-height: 2em;
position: absolute;
right: 16px;
}
.amp-wp-article-header .amp-wp-posted-on.amp-wp-meta:first-of-type{
text-align:right
}
/* Inline styles */
.amp-wp-inline-6958ba8c880d53a1ec508eb764481a8c{color:#008000;}
/* Inline styles */
.amp-wp-inline-fc48bf9413b3fd7ba777ef5713dcaa93{color:#ff6600; text-decoration: none;}
