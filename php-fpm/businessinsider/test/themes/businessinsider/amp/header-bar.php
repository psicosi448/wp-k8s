<header id="main-header">
    <div class="logo-container">
        <a href="https://it.businessinsider.com/">
            <amp-img
                src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_sm_clr.png"
                alt="Business Insider"
                width="245"
                height="38"
                layout="fixed"
            ></amp-img>
        </a>
    </div>
</header>
