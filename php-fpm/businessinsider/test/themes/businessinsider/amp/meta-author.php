<?php $post_author = $this->get( 'post_author' ); ?>
<?php global $post; ?>
    <div class="amp-wp-meta amp-wp-byline">
        <span class="amp-wp-author author vcard">
<?php
    $nascondi_autore = get_field("nascondi_autore", $post->ID);
    $firma = strip_tags(get_field("firma", $post->ID),"<b><i><a>");

    if($firma)
        echo $firma;

    if(!$nascondi_autore) { ?>
         <a class="tag" href="<?php echo get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')); ?>"><?php echo esc_html( $post_author->display_name ); ?></a>
    <?php }  ?>
</span>
    </div>