<?php
global $post;
$app_id = "1359467974094069";
?>

<!-- TABOOLA -->
<div class="taboola-container">
    <div class="taboola-widget">
        <amp-embed width=100 height=100
                   type=taboola
                   heights="(min-width:780px) 64%, (min-width:480px) 98%, (min-width:460px) 167%, 196%"
                   data-publisher="gruppoespresso-businessinsider"
                   data-mode="organic-thumbnails-b"
                   data-placement="Below Article Thumbnails - AMP"
                   data-article="auto"
                   data-target_type="mix"
                   data-flush="true"
                   data-consent-notification-id="amp-user-notification-privacy"
        >
        </amp-embed>
    </div>

    <div class="taboola-widget">
        <amp-embed width=100 height=100
                   type=taboola
                   heights="(min-width:780px) 64%, (min-width:480px) 98%, (min-width:460px) 167%, 196%"
                   data-publisher="gruppoespresso-businessinsider"
                   data-mode="thumbnails-b"
                   data-placement="Below Article Thumbnails 2nd- AMP"
                   data-article="auto"
                   data-target_type="mix"
                   data-flush="true"
                   data-consent-notification-id="amp-user-notification-privacy"
        >
        </amp-embed>
    </div>
</div>
<!-- FINE TABOOLA -->
<!-- ADV -->
<div class="container-ad adv-floor">
    <!-- esi:include  http://scripts.kataweb.it/ws/pub-amp/index.php?adref=<?php echo urlencode(get_permalink($post)); ?>&pos=floor //-->
    <esi:include src="http://scripts.kataweb.it/ws/pub-amp/index.php?adref=<?php echo urlencode(get_permalink($post)); ?>&pos=floor" />
</div>

<footer class="main-footer">
    <p>
        <a href="https://login.kataweb.it/static/privacy/?editore=gruppoespresso" class="privacy_link">PRIVACY</a>
        <br>
        Business Insider Italia<br>
        Copyright © 1999-2018 GEDI Digital S.r.l. Tutti i diritti riservati<br>
    </p>
</footer>


<div class="fixed-bar-container">
    <div class="gs-sharebar gs-custom-theme-rep-2016-v1">
        <div class="gs-sharebar-cell">
            <div class="gig-button-container gig-button-container-horizontal gig-button-container-count-none gig-button-container-facebook">
                <a class="share gs-link" href="https://www.facebook.com/dialog/share?app_id=<?php echo $app_id; ?>&href=<?php echo get_permalink($post->ID); ?>&redirect_uri=<?php echo get_permalink($post->ID); ?>" >
                    <span class="share-text"></span>
                </a>
            </div>
        </div>
        <div class="gs-sharebar-cell">
            <div class="gig-button-container gig-button-container-horizontal gig-button-container-count-none gig-button-container-twitter">
                <a class="share gs-link" href="https://twitter.com/intent/tweet?text=<?php echo get_the_title($post->ID); ?>&url=<?php echo get_permalink($post->ID); ?>&via=bi_italia" target="_blank">
                    <span class="share-text"></span>
                </a>
            </div>
        </div>
        <div class="gs-sharebar-cell">
            <div class="gig-button-container gig-button-container-horizontal gig-button-container-count-none gig-button-container-whatsapp">
                <a class="share gs-link" href="whatsapp://send?text=<?php echo get_the_title($post->ID); ?>+<?php echo get_permalink($post->ID); ?>" >
                    <span class="share-text"></span>
                </a>
            </div>
        </div>
        <div class="gs-sharebar-cell">
            <div class="gig-button-container gig-button-container-horizontal gig-button-container-count-none gig-button-container-email">
                <a class="share gs-link" href="mailto:?subject=<?php echo get_the_title($post->ID); ?>&body=<?php echo get_permalink($post->ID); ?>" >
                    <span class="share-text"></span>
                </a>
            </div>
        </div>
    </div>
</div>