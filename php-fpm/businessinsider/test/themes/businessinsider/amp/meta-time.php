<div class="amp-wp-meta amp-wp-posted-on">
    <time datetime="<?php echo esc_attr( date( 'c', $this->get( 'post_publish_timestamp' ) ) ); ?>">
        <?php
        global $post;
        $post_date = $post->post_date;
        $time1 = new DateTime($post_date); // string date
        $time2 = new DateTime();
        $interval = date_diff($time1, $time2);

        if(($interval->d > 0 ) || ($interval->m > 0 )){

            $tempo = get_the_time( 'j/n/Y g:i:s A', $post_search );;

        }else{
            if( $interval->h > 0 ){
                if($interval->h > 1){
                    $time_string = 'ORE';
                }else{
                    $time_string = 'ORA';
                }
                $tempo = $interval->h . ' ' . $time_string;
            }else{
                if($interval->i > 0){
                    if($interval->i > 1){
                        $time_string = 'MINUTI';
                    }else{
                        $time_string = 'MINUTO';
                    }
                    $tempo = $interval->i . ' ' . $time_string;
                }else{
                    if($interval->s > 1){
                        $time_string = 'SECONDI';
                    }else{
                        $time_string = 'SECONDO';
                    }
                    $tempo = $interval->s . ' ' . $time_string;
                }
            }
        }
        echo $tempo;
        ?>
    </time>
</div>
