<?php
/**
 * Template Name: Custom RSS Template - Feedname
 */


$section = $_GET["section"];

switch ($section){
    case "":
	    $postCount = 10; // The number of posts to show in the feed
	    $args = [
		    'post_type' => array("post"),
		    'posts_per_page' => $postCount,
	    ];
	    break;

    case "home":
	    $post_in = array();
	    $post_object = get_field('articolo_apertura', 'option');
	    if($post_object){$post = $post_object; setup_postdata( $post ); $post_in[] = get_the_ID(); }
	    wp_reset_postdata();
	    $riaperture = get_field('ri-aperture', 'option');
	    if($riaperture){
		    foreach($riaperture as $post){
			    setup_postdata($post);
			    $post_in[] = get_the_ID();
		    }
	    }
	    wp_reset_postdata();
	    $args = [
		    'post__in' => $post_in,
		    'orderby' => 'post__in'
	    ];
	    break;

    case "tecnologia":
	case "politica":
	case "economia":
	case "lifestyle":
	case "scienza":
	case "strategie":
	case "video":

	$postCount = 10; // The number of posts to show in the feed
	    $args = [
		    'post_type' => array("post"),
		    'posts_per_page' => $postCount,
		    'tax_query' => array(
		            array(
		                    'taxonomy' => 'category',
                            'field'    => 'slug',
                            'terms'    => $section,
                        )
                ),
            'meta_query' => array(
	            'relation' => 'AND',
	            array(
		            'key' => 'articolo_importato',
		            'value' => 1,
		            'compare' => '!=',

                )
            )
	    ];
        break;


}


$wpb_all_query = new WP_Query($args);

header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:media="http://search.yahoo.com/mrss/" version="2.0">
    <channel>
        <title><?php bloginfo_rss('name'); ?> - Feed</title>
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss('description') ?></description>
        <?php while($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); setup_postdata($post); ?>
            <item>
                <title><?php the_title_rss(); ?></title>
                <link><?php the_permalink_rss(); ?></link>
                <category domain="/<?php echo $post->post_type; ?>"><?php echo $post->post_type; ?></category>
                <pubDate><?php echo $post->post_date; ?></pubDate>
                <guid isPermaLink="false"><?php the_guid(); ?></guid>
                <dc:date><?php echo mysql2date('c', get_post_time('Y-m-d H:i:s', true), false); ?></dc:date>
                <dc:creator><?php
                    $firma = get_post_meta($post->ID, "wg_ss_firma", true);
                    if(trim($firma) != ""){
                        echo $firma;
                    }
                    ?></dc:creator>
                <?php
                if(has_post_thumbnail()){
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id(), "full");
                    if($image) {
                        $excerpt = get_post_field('post_excerpt', get_post_thumbnail_id());
                        $title = get_post_field('post_title', get_post_thumbnail_id());
                        $content = get_post_field('post_content', get_post_thumbnail_id());

                        ?>
                        <media:content height="<?php echo $image[2]; ?>" type="image/jpeg" width="<?php echo $image[1]; ?>" url="<?php echo $image[0]; ?>">
                            <media:text><?php echo $content; ?></media:text>
                            <media:thumbnail url="<?php echo $image[0]; ?>"/>
                            <media:title><?php echo $title; ?></media:title>
                        </media:content>
                        <?php
                    }
                }
                ?>
                <content:encoded><![CDATA[<?php
                   the_content();
                    ?>]]></content:encoded>
            </item>
        <?php endwhile; ?>
    </channel>
</rss>