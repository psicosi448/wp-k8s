<footer class="site-footer">
	<div class="wrapper">
		<div id="adv-Piede"><script>try { MNZ_RICH('Piede'); } catch(e) { }</script></div>
		<a href="<?php echo get_bloginfo('url'); ?>">
			<h1 class="site-logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_sm_blk.png" alt="">
			</h1>
		</a>
	</div>
	<div class="wrapper">
		<div class="site-footer-inner">
			<?php //Social
			$facebook = get_field('facebook', 'option');
			$twitter = get_field('twitter', 'option');
			$linkedin = get_field('linkedin', 'option');
			$youtube = get_field('youtube', 'option');
			$flipboard = get_field('flipboard', 'option');
			?>
			<?php if($facebook || $twitter || $linkedin || $youtube || $flipboard):  ?>
			<div class="social-follow" style="border-style: none;">
				<b>Seguici su</b>
				<div class="socialmedia-icon-list">
					<ul>
						<?php if($facebook): ?>
						<li style="margin-right:0">
							<a target="_blank" href="<?php echo $facebook; ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/facebook-icon-blk.svg" alt="">
							</a>
						</li>
						<?php endif;  ?>
						<?php if($twitter): ?>
						<li>
							<a target="_blank" href="<?php echo $twitter; ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/twitter-icon-blk.svg" alt="">
							</a>
						</li>
						<?php endif; ?>
						<?php if($linkedin): ?>
                            <li>
                                <a target="_blank" href="<?php echo $linkedin; ?>">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/linkedin-icon-blk.svg" alt="">
                                </a>
                            </li>
						<?php endif; ?>
						<?php if($youtube): ?>
                            <li>
                                <a target="_blank" href="<?php echo $youtube; ?>">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/youtube-icon-blk.svg" alt="">
                                </a>
                            </li>
						<?php endif; ?>
						<?php if($flipboard): ?>
                            <li>
                                <a target="_blank" href="<?php echo $flipboard; ?>">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/flipboard-icon-blk.svg" alt="">
                                </a>
                            </li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
			<?php endif;  ?>
		</div>
	</div>
	<div class="wrapper">
		<div class="list-information">
			<ul>
				<li class="copyright">
					<p>Copyright © 1999-<?php echo date('Y'); ?> GEDI Digital S.r.l. Tutti i diritti riservati - <span class="nowrap"> <a id="kw-privacy-link" href="javascript:">Privacy</a></span></p>
					<p>&nbsp;</p>
				</li>
			</ul>
		</div>
		<div class="list-information international-links">
			<ul>
				<li>Edizioni Internazionali:</li>
				<li><a target="_blank" href="http://www.businessinsider.com/?IR=T">US</a></li>
				<li><a target="_blank" href="http://www.businessinsider.de/?IR=T">DE</a></li>
				<li><a target="_blank" href="https://www.businessinsider.com/international?IR=T">INT</a></li>
				<li><a target="_blank" href="http://www.businessinsider.com.au/">AUS</a></li>
				<li><a target="_blank" href="http://www.businessinsider.in/">IN</a></li>
				<li><a target="_blank" href="https://www.businessinsider.jp/">JP</a></li>
                <li><a target="_blank" href="http://www.businessinsider.mx/">MX</a></li>
                <li><a target="_blank" href="http://www.businessinsider.my/">MY</a></li>
				<li><a target="_blank" href="http://www.businessinsider.nl/?IR=C">NL</a></li>
				<li><a target="_blank" href="http://nordic.businessinsider.com/?IR=C">SE</a></li>
				<li><a target="_blank" href="http://www.businessinsider.sg/">SG</a></li>
				<li><a target="_blank" href="http://www.businessinsider.com.pl?IR=T">PL</a></li>
				<li><a target="_blank" href="http://it.businessinsider.com">IT</a></li>
				<li><a target="_blank" href="http://www.businessinsider.fr/?IR=C">FR</a></li>
                <li><a target="_blank" href="https://www.businessinsider.es">ES</a></li>
                <li><a target="_blank" href="https://www.businessinsider.co.za">ZA</a></li>
			</ul>
		</div>
	</div>
</footer>
<?php wp_footer();

if(is_singular()){
	?>
    <script type="text/javascript" src="https://login.kataweb.it/registrazione/newsletter/js/newslettergedi2.js"></script>
    <script type="text/javascript">
        jQuery( document ).ready(function() {
            window.nlgConf = {
                "targetDomain": "https://login.kataweb.it",
                "origin":"businessinsider",
                "iframes":[
                    {
                        "newsletterId": "businessinsider",
                        "targetIFrame": "gedi-newsletter-2"}
                ]}
            newsletterGedi.injectFrame();
        });
    </script>
	<?php
}
?>
</body>
</html>
