<?php
get_header(); $post_not_in = []?>
	<!-- MAIN -->
	<main style="min-height: 100vh;">

		<div class="quicklook-wrapper wrapper">
			<div class="wrapper wrapper-one-col-sidebar">
				<?php $post_object = get_field('articolo_apertura', 'option'); //print_r($post_apertura); ?>
				<?php if($post_object): $post = $post_object; setup_postdata( $post ); $post_not_in[] = get_the_ID(); ?>
					<!-- GROUP PRIMARY -->
					<div class="group-primary">
						<div>
							<div>
								<a href="<?php the_permalink(); ?>" class=" teaser-image-overlay" data-quicklook>
									<?php $img = get_field('immagine_articolo_apertura', 'option'); ?>
									<?php
                                    if($img): ?>
										<img alt="" src="<?php echo $img['sizes']['apertura_home']; ?>" />
									<?php
                                   /* elseif(has_post_format("video") && has_shortcode($post->post_content, "tvzap-video")):
                                            echo do_shortcode(bi_get_main_video($post));
                                    */ elseif (has_post_thumbnail()): ?>
										<?php echo image_lazy(get_the_ID(), 'apertura_home') ?>
									<?php endif; ?>
									<div class="image-overlay">
										<h1 class="title"><?php the_title(); ?></h1>
										<div>
                            <span class="time-published">
								<?php
								$nascondi_autore = get_field("nascondi_autore", $post->ID);
								$firma = strip_tags(get_field("firma", $post->ID), "<b><i>");
								if($firma){
									echo '<span class="author-sign">'.$firma.'</span>';
								}
								if(!$nascondi_autore) { ?>
									<span class="author-link">
									<?php echo get_the_author(); ?>
	                            </span>
									<?php
								}
								echo orario($post, 'home'); ?>
								<?php $numeroVisualizzazioni = numeroVisualizzazioni($post, 'fire-withe'); ?>
								<?php if($numeroVisualizzazioni): ?>
									<?php echo $numeroVisualizzazioni; ?>
								<?php endif; ?>
                            </span>
										</div>
									</div>

								</a>
							</div>
						</div>
					</div>
					<?php wp_reset_postdata(); endif; ?>
				<div class="group-minor group-minor-absolute">
					<div id="adv-Middle1"><script>try { MNZ_RICH('Middle1'); } catch(e) { }</script></div>


					<div class="fb-page" data-href="https://www.facebook.com/BusinessInsiderItalia" data-width="300" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/BusinessInsiderItalia" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/BusinessInsiderItalia">Business Insider Italia</a></blockquote></div>

				</div>
				<div class="wrapper wrapper-two-col-sidebar">
					<div class="half-left">
						<div class="group-secondary">
							<!-- BANNER -->
							<div id="feed">
								<?php
								$counter=0;
								$riaperture = get_field('ri-aperture', 'option');?>
								<?php if($riaperture): ?>
									<!-- Riaperture -->
									<?php foreach($riaperture as $post): setup_postdata($post); $post_not_in[] = get_the_ID();
                                        //Controllo stato
										if(get_post_status( get_the_ID() ) != 'publish' ) continue;
										?>
										<?php get_template_part('template/teaser', 'regular');
                                        $counter ++;
                                        if($counter == 3){
                                            ?>
                                            <div id="adv-x20"><script>try { MNZ_RICH('x20'); } catch(e) { }</script></div>
                                            <?php
                                        }
									 endforeach; ?>
									<!-- Fine Riaperture -->
									<?php wp_reset_postdata(); endif; ?>
								<?php if(have_posts()): $count_post = 1; while(have_posts()): the_post(); $post_not_in[] = get_the_ID(); ?>
									<?php if($count_post <= 4): ?>
										<?php get_template_part('template/teaser', 'regular'); ?>
									<?php else: ?>
										<?php get_template_part('template/teaser', 'small'); ?>
									<?php endif;

									$counter ++;
									if($counter == 3){
										?>
										<div id="adv-x20"><script>try { MNZ_RICH('x20'); } catch(e) { }</script></div>
										<?php
									}


									if($count_post == 8){
										$count_post = 1;
									}else{
										$count_post++;
									} ?>
								<?php endwhile; endif;  ?>

								<?php global $wp_query;
								if($wp_query->max_num_pages > 1):
									//Recupero il numero di blocchi per definire quanti post mostrare al load dell'infinite scroll
									$posts_per_page = 8;
									$numero_blocchi = get_field('numero_blocchi_aperture', 'option');
									if($numero_blocchi){
										$posts_per_page = (int)$posts_per_page * (int)$numero_blocchi;
									}?>
									<input type="button" class="btn btn-big btn-attend" id="load-more" data-page="2" value="Carica altri" data-url="<?php bloginfo('url')?>" data-action="load-more" data-number="<?php echo $posts_per_page; ?>" data-type="home" data-id="home" data-container="feed" />
								<?php endif; ?>
							</div>

							<div id="spinner-small" class="spinner small hide">
								<div class="bounce1"></div>
								<div class="bounce2"></div>
								<div class="bounce3"></div>
							</div>

						</div>
						<?php
						remove_filter('pre_get_posts', 'bi_home_pre_get_post');


						$args = [
							'posts_per_page'    => 20,
							'meta_key'          => 'click_server',
							'orderby'           => 'meta_value_num',
							'post_status'       => 'publish',
							'post__not_in'      => $post_not_in,
							'meta_query' => [
							    'relation' => 'AND',
								[
									'key' => 'articolo_importato',
									'value' => 1,
									'compare' => '!=',
								],
                                [
								    'key' => 'native',
								    'value' => 1,
								    'compare' => '!=',
                                ]
							],
							'date_query' => array(
								array(
									'column' => 'post_date',
									'after' => RELATED_WEEKS_AGO_HOME.' week ago',
								)
							),

						];
						$most_read = new WP_Query($args);


						if($most_read->have_posts()): ?>
							<div class="group-minor">
								<?php while ($most_read->have_posts()): $most_read->the_post();?>
									<?php get_template_part('template/teaser', 'small_box'); ?>
								<?php endwhile; ?>
							</div>
						<?php endif; wp_reset_query();
						add_filter('pre_get_posts', 'bi_home_pre_get_post');?>
					</div>

					<div class="group-minor group-minor-sidebar">
						<?php get_sidebar(); ?>
					</div>


				</div>
			</div>
		</div>
	</main>
	<!-- END MAIN -->
<?php get_footer(); ?>