<!DOCTYPE html>
<html>
<head>
	<script type='text/javascript'>var _sf_startpt = (new Date()).getTime()</script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width initial-scale=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/favicon.ico" rel="shortcut icon" />
    <link rel="preload" as="font" crossorigin="anonymous" type="font/woff2" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/fonts/FaktSmConPro-Normal.woff2">
    <link rel="preload" as="font" crossorigin="anonymous" type="font/woff2" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/fonts/FaktSmConPro-SemiBold.woff2">

    <link rel="preload" as="style" crossorigin="anonymous" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/main.css?ver=1.1">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/print.css?ver=1.0" media="print" rel="stylesheet" type="text/css">
    <link rel="preload" as="style" crossorigin="anonymous" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/custom.css?ver=2.3">

	<?php wp_head(); ?>

    <?php
	if(is_singular()){
		global $post;
		$orig_url = get_post_meta($post->ID, "orig_link", true);
		if($orig_url != ""){
			?>
			<meta name="robots" content="noindex">
			<?php
		}
	}
	?>
	<script>
		WebFontConfig = {
			custom: {
				families: ['FaktSmConPro:n4', 'FaktSmConPro:n7'],
				urls: ['<?php echo get_stylesheet_directory_uri(); ?>/assets/css/font.css']
			},
			fontloading: function(familyName, fvd) {
				console.log("++ LOADING: ", familyName);
			},
			fontactive: function(familyName, fvd) {
				var now = (new Date()).getTime();
				var loadingTime = now - _sf_startpt;
				console.log("++ ACTIVE: ", familyName, fvd, "caricato in: "+ loadingTime + "ms");
			},
			fontinactive: function(familyName, familyName, fvd) {
				console.log("!! INACTIVE: ", familyName, fvd);
			},
			timeout: 5000
		};

		(function() {
			var wf = document.createElement('script');
			wf.src = '<?php echo get_stylesheet_directory_uri(); ?>/assets/js/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})();
	</script>


	<link rel="preconnect dns-prefetch" href="//secure-it.imrworldwide.com">
	<link rel="preconnect dns-prefetch" href="//oasjs.kataweb.it">
	<link rel="preconnect dns-prefetch" href="//oasjs.it.businessinsider.com">
	<link rel="preconnect dns-prefetch" href="//cdn.gelestatic.it">
	<link rel="preconnect dns-prefetch" href="//cdn.taboola.com">
	<link rel="preconnect dns-prefetch" href="//www.repstatic.it">
	<link rel="preconnect dns-prefetch" href="//connect.facebook.net">
	<link rel="preconnect dns-prefetch" href="//www.googletagmanager.com">
	<link rel="preconnect dns-prefetch" href="//www.facebook.com">
	<link rel="preconnect dns-prefetch" href="//sb.scorecardresearch.com">
	<link rel="preconnect dns-prefetch" href="//scripts.kataweb.it">
	<link rel="preconnect dns-prefetch" href="//www.google-analytics.com">
	<link rel="preconnect dns-prefetch" href="//login.kataweb.it">
    <link rel="preconnect dns-prefetch" href="//audiweb.azureedge.net">
    <link rel="preconnect dns-prefetch" href="//video.it.businessinsider.com">
    <link rel="preconnect dns-prefetch" href="//platform.twitter.com">
    <link rel="preconnect dns-prefetch" href="//cdn-gl.imrworldwide.com">
    <link rel="preconnect dns-prefetch" href="//click.kataweb.it">
    <link rel="preconnect dns-prefetch" href="//cdn-gl.imrworldwide.com">
    <link rel="preconnect dns-prefetch" href="//euasync01.admantx.com">
    <link rel="preconnect dns-prefetch" href="//a.gedidigital.it">
    <link rel="preconnect dns-prefetch" href="//cdn.wbtrk.net">
    <link rel="preconnect dns-prefetch" href="//w.theoutplay.com">



    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '303751827011259');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=303751827011259&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-102343341-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager --> <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TQCN53" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-TQCN53');</script> <!-- End Google Tag Manager -->
<div id="adv-Position3"><script>try { MNZ_RICH('Position3'); } catch(e) { }</script></div>
<!-- SOCIAL SCRIPTS -->

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=206682462733026";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

<script>
	window.twttr = (function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0],
			t = window.twttr || {};
		if (d.getElementById(id)) return t;
		js = d.createElement(s);
		js.id = id;
		js.src = "https://platform.twitter.com/widgets.js";
		fjs.parentNode.insertBefore(js, fjs);

		t._e = [];
		t.ready = function(f) {
			t._e.push(f);
		};

		return t;
	}(document, "script", "twitter-wjs"));</script>
<!-- END SOCIAL SCRIPTS -->


<!-- TOP HEADER ADS CONTAINER -->
<!-- BANNER -->
<!-- END TOP HEADER ADS CONTAINER -->

<!-- HEADER -->
<header class="site-header">
	<div class="wrapper">

		<button class="header-menu-toggle">
			<span>MENU</span>
		</button>

		<div class="site-logo">
			<a href="/">
				<img class="header-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_lg_clr.png" alt="">
				<img class="header-logo-2" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_sm_clr.png" alt="">
			</a>
		</div>




		<ul class="main-menu-fixed">
			<?php if(have_rows('categorie', 'option')): while (have_rows('categorie', 'option')): the_row(); ?>
				<?php
				$cl="";
                $categoria = get_sub_field('categoria');
                if($categoria){
                    if(is_category($categoria->term_id))
                        $cl=" current";
                    if(is_singular()){
                        global $post;
                        // controllo se il post è nella categoria corrente
                        if(has_category($categoria->term_id, $post))
                            $cl=" current";
                    }
                    ?>
					<li class="category-menu <?php echo $cl; ?>">
						<a data-id="dropdown-<?php echo $categoria->term_id; ?>" href="<?php echo get_term_link($categoria, 'category'); ?>">
							<?php echo $categoria->name; ?>
						</a>
					</li>
				<?php } ?>
			<?php endwhile; endif; ?>

			<li class="category-menu">
					<a data-id="dropdown-all" class="showmap" href="#">Tutte</a>
		</li>
		</ul>



		<?php
        /*
        if(is_category() || is_tax()): $term_object = get_queried_object(); ?>
		<div class="category-title">
			<h1><?php echo $term_object->name; ?></h1>
		</div>
		<?php endif;
        */
         ?>
		<button class="search-btn">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/search-icon-blk.svg" alt="">
			<span>CHIUDI</span>
		</button>




		<div class="subNav Services">


			<div class="subNav-item navNewsletter">
				<a href="<?php echo home_url("newsletter"); ?>">Newsletter</a>
			</div>

			<div class="subNav-item navEdition">
				<span class="js-navEdition navEdition-label" data-toggle-active=".navEdition-inner">
					Edizione
				</span>
				<div class="navEdition-inner">
					<h4 class="navEdition-title">
						Scegli Edizione:
					</h4>
					<ul class="navEdition-list">
						<li class="navEdition-list-item">
							<a target="_blank" href="http://www.businessinsider.com/?IR=T">
								United States
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="https://www.businessinsider.com/international?IR=T">
								International
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://www.businessinsider.de/?IR=T">
								Deutschland
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://www.businessinsider.fr/">
								France
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://www.businessinsider.com.au/">
								Australia
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://www.businessinsider.in/">
								India
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://it.businessinsider.com/">
								Italia
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="https://www.businessinsider.jp/">
								Japan
							</a>
						</li>
                        <li class="navEdition-list-item">
                            <a target="_blank" href="http://www.businessinsider.mx/">
                                Mèxico
                            </a>
                        </li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://www.businessinsider.my/">
								Malaysia
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://www.businessinsider.sg/">
								Singapore
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://www.businessinsider.com.pl/">
								Poland
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://nordic.businessinsider.com/">
								Nordic
							</a>
						</li>
						<li class="navEdition-list-item">
							<a target="_blank" href="http://www.businessinsider.nl/">
								Netherlands
							</a>
						</li>
						<li class="navEdition-list-item">
								<a target="_blank" href="https://www.businessinsider.es/">
										España
								</a>
						</li>
						<li class="navEdition-list-item">
								<a target="_blank" href="https://www.businessinsider.co.za/">
										South Africa
								</a>
						</li>
				</ul>
				</div>
			</div>


		</div>


		<div class="subNav Social">
			<?php //Social
				$facebook = get_field('facebook', 'option');
				$twitter = get_field('twitter', 'option');
				$linkedin = get_field('linkedin', 'option');
	    		$youtube = get_field('youtube', 'option');
    			$flipboard = get_field('flipboard', 'option');
			?>
			<?php if($facebook || $twitter || $linkedin || $youtube || $flipboard):  ?>
			<ul class="subNav-item navSocial">
				<?php if($facebook): ?>
				<li class="navSocial-item">
					<a target="_blank" href="<?php echo $facebook; ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/facebook-icon-blk.svg" alt="">
					</a>
				</li>
				<?php endif; ?>
				<?php if($twitter):  ?>
					<li class="navSocial-item">
						<a target="_blank" href="<?php echo $twitter; ?>">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/twitter-icon-blk.svg" alt="">
						</a>
				</li>
				<?php endif; ?>
				<?php if($linkedin):  ?>
                    <li class="navSocial-item">
                        <a target="_blank" href="<?php echo $linkedin; ?>">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/linkedin-icon-blk.svg" alt="">
                        </a>
                    </li>
				<?php endif;
				/*?>
				<?php if($youtube):  ?>
                    <li class="navSocial-item">
                        <a target="_blank" href="<?php echo $youtube; ?>">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/youtube-icon-blk.svg" alt="">
                        </a>
                    </li>
				<?php endif; ?>
				<?php if($flipboard):  ?>
                    <li class="navSocial-item">
                        <a target="_blank" href="<?php echo $flipboard; ?>">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/flipboard-icon-blk.svg" alt="">
                        </a>
                    </li>
				<?php endif;
				*/  ?>
			</ul>
			<?php if($flipboard):  ?>
									<li class="navSocial-item">
											<a target="_blank" href="<?php echo $flipboard; ?>">
													<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/flipboard-icon-blk.svg" alt="">
											</a>
									</li>
			<?php endif; ?>
			<?php endif; ?>


		</div>










	</div>









	<nav class="main-navigation">
		<div class="wrapper">
			<ul class="main-menu">

				<?php if(have_rows('categorie', 'option')): while (have_rows('categorie', 'option')): the_row(); ?>
					<?php $categoria = get_sub_field('categoria'); ?>
					<?php if($categoria): ?>
						<li class="category-menu">
							<a data-id="dropdown-<?php echo $categoria->term_id; ?>" href="<?php echo get_term_link($categoria, 'category'); ?>">
								<?php echo $categoria->name; ?>
							</a>
						</li>
					<?php endif; ?>
				<?php endwhile; endif; ?>
				<li class="category-menu">
							<a data-id="dropdown-6" href="#">
								Newsletter							</a>
						</li>
				<li>
					<a data-id="dropdown-all" class="all" href="/">
						Tutte
					</a>
				</li>
			</ul>
		</div>
		<?php if($facebook || $twitter || $linkedin || $youtube || $flipboard):  ?>
		<div class="mobile-socialmedia-container">
			<ul>
				<?php if($facebook): ?>
				<li>
					<a target="_blank" href="<?php echo $facebook; ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/facebook-icon-blk.svg" alt="">
					</a>
				</li>
				<?php endif; ?>
				<?php if($twitter): ?>
				<li>
					<a target="_blank" href="<?php echo $twitter; ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/twitter-icon-blk.svg" alt="">
					</a>
				</li>
				<?php endif; ?>
				<?php if($linkedin): ?>
                    <li>
                        <a target="_blank" href="<?php echo $linkedin; ?>">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/linkedin-icon-blk.svg" alt="">
                        </a>
                    </li>
				<?php endif; ?>
				<?php if($youtube): ?>
                    <li>
                        <a target="_blank" href="<?php echo $youtube; ?>">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/youtube-icon-blk.svg" alt="">
                        </a>
                    </li>
				<?php endif; ?>
				<?php if($flipboard): ?>
                    <li>
                        <a target="_blank" href="<?php echo $flipboard; ?>">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/flipboard-icon-blk.svg" alt="">
                        </a>
                    </li>
				<?php endif; ?>
			</ul>
		</div>
		<?php endif; ?>
		<!--<div class="mobile-menu-footer">
			<p class="list-information mobile-menu-copyright">
				<p> © 2002-<?php echo date('Y'); ?> Gruppo Editoriale L'Espresso S.p.A. Tutti i diritti riservati - P.I. 00906801006 &nbsp;<span class="nowrap"> <a href="https://login.kataweb.it/static/privacy/">Privacy</a></span></p>
			</p>
		</div>-->

		<div class="main-navigation-dropdown">




<?/*

			<?php if(have_rows('categorie', 'option')):
				if(is_home()){
					remove_filter('pre_get_posts', 'bi_home_pre_get_post');
				}
				while (have_rows('categorie', 'option')): the_row(); ?>
				<?php $categoria = get_sub_field('categoria'); ?>
				<?php if($categoria):
					$args = [
						'posts_per_page' => 4,
					    'cat' => $categoria->term_id,
						'meta_query' => [
							[
								'key' => 'articolo_importato',
								'value' => 1,
								'compare' => '!=',
							],
							[
								'key' => 'native',
								'value' => 1,
								'compare' => '!=',
							]
						]
					];
					$articoli_menu = new WP_Query($args);
				?>
					<?php if($articoli_menu->have_posts()): ?>
					<div id="dropdown-<?php echo $categoria->term_id; ?>" class="sub-menu-navigation">
						<div class="latest-articles">
							<?php while ($articoli_menu->have_posts()): $articoli_menu->the_post(); ?>
								<?php get_template_part('template/teaser', 'menu'); ?>
							<?php endwhile; ?>
						</div>
					</div>
					<?php endif; wp_reset_query(); ?>
			<?php endif;  endwhile;
				if(is_home()){
					add_filter('pre_get_posts', 'bi_home_pre_get_post');
				}
			endif; ?>

*/?>
















			<div id="dropdown-all" class="sub-menu-navigation">

				<div class="all-site-links">
					<div class="section-group">
						<h5>Sezioni A-Z</h5>
						<?php $categorie = get_categories(); ?>
						<ul>
							<?php foreach($categorie as $categoria):
								if( $categoria->name == "Featured") continue;
								?>
							<li>
								<a href="<?php echo get_category_link($categoria); ?>">
									<?php echo $categoria->name; ?>
								</a>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
                    <div class="section-group">
                        <h5>Speciali</h5>
                        <?php
                        if ( has_nav_menu( "speciali" ) ) {
                            echo wp_nav_menu(array(
                                'theme_location' => 'speciali',
                                'container' => '',
                                'menu_class' => 'bi_speciali',
                                'menu_id' => 'bi_speciali'
                            ));
                        }else{
                        ?>
                        <ul>
                            <li>
                                <a href="https://it.businessinsider.com/brand/ifa-2019/">
                                    Speciale Ifa 2019
                                </a>
                            </li>
                            <li>
                                <a href="https://it.businessinsider.com/brand/better-capitalism/">
                                    Better Capitalism
                                </a>
                            </li>
                            <li>
                                <a href="https://it.businessinsider.com/brand/salone-risparmio/">
                                    Il salone del risparmio
                                </a>
                            </li>
                            <li>
                                <a href="https://it.businessinsider.com/brand/davos-2019/">
                                    Speciale Davos
                                </a>
                            </li>
                            <li>
                                <a href="https://it.businessinsider.com/brand/new-banking/">
                                    New Banking
                                </a>
                            </li>
                            <li>
                                <a href="https://it.businessinsider.com/brand/assemblea-di-bilancio-2018/">
                                    Bilanci 2018
                                </a>
                            </li>
                            <li>
                                <a href="https://it.businessinsider.com/brand/debito-pubblico/">
                                    Debito Pubblico
                                </a>
                            </li>
                            <li>
                                <a href="https://it.businessinsider.com/brand/intelligenza-artificiale/">
                                    Intelligenza artificiale
                                </a>
                            </li>
                            <li>
                                <a href="https://it.businessinsider.com/brand/scuoladitrading/">
                                    Scuola di trading
                                </a>
                            </li>
                        </ul>
                        <?php
                        }
                        ?>
                    </div>
					<?php $page_link = get_field('pagine', 'option');
					if($page_link): ?>
					<div class="section-group">
						<h5>About BI</h5>
						<ul>
							<?php foreach($page_link as $post): setup_postdata($post); ?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?>
								</a>
							</li>
							<?php endforeach; wp_reset_postdata(); ?>
						</ul>
					</div>
					<?php endif; ?>

					<div class="section-group">
						<h5>Follow BI</h5>
						<ul>
							<?php if($facebook): ?>
							<li>
								<a href="<?php echo $facebook; ?>">
									Facebook
								</a>
							</li>
							<?php endif;  ?>

							<?php if($twitter): ?>
							<li>
								<a href="<?php echo $twitter; ?>">
									Twitter
								</a>
							</li>
							<?php endif; ?>

							<?php if($linkedin): ?>
                                <li>
                                    <a href="<?php echo $linkedin; ?>">
                                        Linkedin
                                    </a>
                                </li>
							<?php endif; ?>
						</ul>
					</div>


					<button class="close-btn">
						<span>CHIUDI</span>
					</button>


				</div>

			</div>

















		</div>
	</nav>

	<div class="search-form-dropdown">
		<div class="search-field">
			<form action="<?php bloginfo('url'); ?>" method="get">
				<input data-search-input id="search-input" name="s" type="text" placeholder="Cerca" autocomplete="off">
			</form>
		</div>
	</div>
</header>
<!-- END HEADER -->
<div class="wrapper">
	<div id="adv-Top"><script>try { MNZ_RICH('Top'); } catch(e) { }</script></div>
</div>
