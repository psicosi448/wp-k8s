<?php get_header(); ?>

<!-- MAIN -->
<main style="min-height: 100vh;">

	<div class="quicklook-wrapper wrapper">
		<div class="wrapper">
			<div class="group-primary article-wrapper"  itemscope itemtype="http://schema.org/Article">
                   <?php /* <meta  itemprop="publisher" itemtype="http://schema.org/Organization" content="<?php bloginfo("name"); ?>"> */ ?>
				<?php if(have_posts()): while (have_posts()): the_post(); ?>
				<article id="article-<?php echo get_the_ID(); ?>" class="article-full">
					<?php
					$social_desk = social_share($post, 'social-share');
					if($social_desk) echo $social_desk;
					?>
					<?php
					$is_native = is_native($post);
					if($is_native): //Mostro la label solo se native?>
						<div  class="native-label bigpage">Contenuto sponsorizzato</div>
					<?php endif; //Fine controllo native ?>
					<?php
					// controllo se è associato ad un brand
					 $brands = wp_get_post_terms( $post->ID, "brand");
					// controllo se è abilitato il link alla bigpage

					$link_bigpage = get_field('link_bigpage', $post->ID);
					//if($link_bigpage){
						if(is_array($brands) && count($brands)){
							$brand = $brands[0];
							$brandlink = get_term_link($brand);
							if(!is_wp_error($brandlink) && (($is_native && $link_bigpage) || (!$is_native))){
								echo "<div class='bigpage link-bigpage'><a href='".$brandlink."'>Vai alla pagina di ".$brand->name."</a></div>";
							}

						}

					//}


					?>
					<h1 class="article-title" itemprop="name headline">
						<?php the_title(); ?>
					</h1>
					<div class="article-author">
						<ul>
							<?php
							$foundautore=false;
							if($is_native == false): //Mostro dati ature counter e data solo se non è un contenuto native ?>
							<li itemscope itemprop="author"  itemtype="http://schema.org/Person">
								<?php
								$nascondi_autore = get_field("nascondi_autore");
								$firma = strip_tags(get_field("firma", $post->ID),"<b><i><a>");

								if($firma){
									if(!$nascondi_autore) {
										echo '<span class="author-sign" >' . $firma . '</span>';
									}else{
										echo '<span class="author-sign" itemprop="name" >' . $firma . '</span>';
										$foundautore = true;
									}
								}
								if(!$nascondi_autore) {
									$foundautore = true;
									?>
								<a class="tag" href="<?php echo get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')); ?>"><?php the_author(); ?></a>
                                    <meta itemprop="name" content="<?php the_author(); ?>">
                                    <meta itemprop="url" content="<?php echo get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')); ?>">
                                <?php }
								if(!$foundautore){
								?>
									<meta itemprop="name" content="<?php bloginfo("name"); ?>">
									<meta itemprop="url" content="<?php bloginfo("url"); ?>">
								<?php
								}
								?>
							</li>
                            <?php
							else:
								?>
								<div itemscope itemprop="author"  itemtype="http://schema.org/Person">
									<meta itemprop="name" content="<?php bloginfo("name"); ?>">
									<meta itemprop="url" content="<?php bloginfo("url"); ?>">
								</div>
								<?php
							endif; //Fine controllo native
							?>
							<li>
								<span class="time-published" >
                                     <meta itemprop="datePublished" content="<?php echo $post->post_date; ?>">
                                     <meta itemprop="dateModified" content="<?php echo $post->post_modified; ?>">
									<?php echo orario($post); ?>
								</span>
							</li>
							<?php if($is_native == false): //Mostro dati ature counter e data solo se non è un contenuto native ?>
							<?php $numeroVisualizzazioni = numeroVisualizzazioni($post, 'hot'); ?>
							<?php if($numeroVisualizzazioni){ ?>
							<li>
                                <?php echo $numeroVisualizzazioni; ?>
							</li>
							<?php } //Fine controllo native?>
							<?php endif;  ?>
						</ul>
					</div>

					<?php
					$social_mobile = social_share($post, 'social-share-mobile');
					if($social_mobile) echo $social_mobile;
					?>
					<div class="article-body">
						<?php $format = get_post_format();
						get_template_part( 'template/content', $format ); ?>
						<?php //Recupero e stampo i post tag
						$tagsDiv = biPostTag($post->ID);
						if($tagsDiv):
							echo $tagsDiv;
						endif;
						?>
						<?php
						$social_mobile = social_share($post, 'social-share-mobile end-article');
						if($social_mobile) echo $social_mobile;
						?>
						<?php //Social
						$facebook = get_field('facebook', 'option');
						$twitter = get_field('twitter', 'option');
						?>
						<?php if($facebook || $twitter):  ?>
							<div class="social-follow">
								<div class="follow-pretext"></div>
								<?php if($twitter): ?>
									<a href="<?php echo $twitter; ?>" class="twitter-follow-button" data-show-count="false">Follow @BIItaly</a>
								<?php endif; ?>
								<?php if($facebook): ?>
									<div class="fb-like" data-href="<?php echo $facebook; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<!-- BANNER -->
						<!-- BANNER -->
						<?php
						// check abilitazione commenti
						if(get_field("abilita_commenti")) {
							?>
							<a id="commenta"></a>
							<div id="gs-social-comments"></div>
							<script>
								window.gigyaCommentParams = {
									"categoryID":"it_businessinsider_001"
								};
							</script>
							<?php
						}
						?>

					</div>
				</article>
				<?php endwhile; endif; ?>
			</div>
			<div class="sidebar-sito-common">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</main>
<!-- END MAIN -->
<?php get_footer(); ?>
