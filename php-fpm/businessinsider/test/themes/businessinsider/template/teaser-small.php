<!-- TEASER SMALL -->
<div class="teaser teaser-small">
	<div class="result-list">
		<div class="teaser-image">
			<?php
            if(has_post_thumbnail()): ?>
				<a href="<?php the_permalink(); ?>">
					<?php echo image_lazy(get_the_ID(), 'teaser_regular') ?>
				</a>
			<?php endif;  ?>
		</div>
		<div class="teaser-content">
			<div class="teaser-text">
				<a href="<?php the_permalink(); ?>">
					<h4 class="teaser-title small"><?php the_title(); ?></h4>
				</a>
			</div>
			<div class="article-author">
				<ul>
					<li><?php
						$nascondi_autore = get_field("nascondi_autore", $post->ID);
						$firma = strip_tags(get_field("firma", $post->ID),"<b><i><a>");
						if($firma){
							echo '<span class="author-sign">'.$firma.'</span>';
						}
						if(!$nascondi_autore)
							the_author_posts_link();
						?></li>
					<li>
						<?php echo orario($post, 'teaser'); ?>
					</li>
					<?php
					if(!is_native($post->ID)) {
						$numeroVisualizzazioni = numeroVisualizzazioni($post, 'hot'); ?>
						<?php if ($numeroVisualizzazioni): ?>
							<li><?php echo $numeroVisualizzazioni; ?></li>
						<?php endif;
					}?>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- FINE TEASER SMALL -->