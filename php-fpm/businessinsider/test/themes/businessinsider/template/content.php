<?php
//Contenuto standard
//Verifico se esiste l'immagine ed esiste l'immagine in evidenza;
$mostro_immagine = get_field('mostrare_immagine_in_evidenza');
if(has_post_thumbnail() && $mostro_immagine == 'si'): ?>
	<?php
		$image = formatImage(get_post_thumbnail_id(), "single","", "", true);
		if($image) echo $image; ?>
<?php endif;
if(has_post_thumbnail() && $mostro_immagine != 'si') {
	// aggiungo solo il meta img come microdato
	$url = wp_get_attachment_image_src(get_post_thumbnail_id(), "single");
	if ($url) {
		$image = '<div class="media-image" itemprop="image"  itemscope itemtype="https://schema.org/ImageObject" >';
		$image .= '<meta itemprop="url" content="' . $url[0] . '">';
		$image .= '<meta itemprop="width" content="' . $url[1] . '">';
		$image .= '<meta itemprop="height" content="' . $url[2] . '">';
		$image .= "</div>";
		echo $image;
	}
}
?>
<div itemprop="description"><?php the_content(); ?></div>
<div id='gedi-newsletter-2'> </div>
<div id="taboola-below-article-thumbnails"></div>
    <div id="taboola-below-article-thumbnails-2nd"></div>
<?php
/*

<script type="text/javascript">
	window._taboola = window._taboola || [];
	_taboola.push({
		mode: 'organic-thumbnails-b',
		container: 'taboola-below-article-thumbnails',
		placement: 'Below Article Thumbnails',
		target_type: 'mix'
	});
</script>

<script type="text/javascript">
	window._taboola = window._taboola || [];
	_taboola.push({
		mode: 'thumbnails-b',
		container: 'taboola-below-article-thumbnails-2nd',
		placement: 'Below Article Thumbnails 2nd',
		target_type: 'mix'
	});
</script>
*/
?>