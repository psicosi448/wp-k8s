<?php
$financialposts = get_posts(array('suppress_filters' => true, "post_type" => "post", "posts_per_page" => 4,'tax_query' => array(
    array(
        'taxonomy' => 'brand',
        'field' => 'slug',
        'terms' => 'financialounge'
    )
)));
if(is_array($financialposts)) {
    ?>
    <div class="widget widget-financialounge">
        <p class="widget-title">FinanciaLounge</p>
        <div class="collab">
            <p>Contenuti offerti da</p>
            <img src="<?php bloginfo("template_url"); ?>/assets/img/financialounge.png" alt="prodesfin">
        </div>
        <ol class="widget-list">
            <?php
            $i=0;
            foreach ($financialposts as $item) {
                $i++;
                if($i > 4)
                    break;

                $image_url = get_the_post_thumbnail_url($item, "teaser_small");
                ?>
                <li>
                    <a href="<?php echo get_permalink($item->ID); ?>">
                        <img src="<?php echo $image_url; ?>" class="fl_image" />
                    </a>
                    <p class="widget-text">
                        <a href="<?php echo get_permalink($item->ID); ?>"><?php echo $item->post_title; ?></a>
                    </p>
                </li>

                <?php
            }
            ?>
        </ol>
    </div>
       <br clear="all" />     
    <?php
}










