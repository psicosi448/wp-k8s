<?php
/**
 * Contenuto di tipo video
 */ ?>


<?php
/* microdati */
if(has_post_thumbnail()){
	$url = wp_get_attachment_image_src(get_post_thumbnail_id(), "single");
	if ($url) {
		$image = '<div class="media-image" itemprop="image"  itemscope itemtype="https://schema.org/ImageObject" >';
		$image .= '<meta itemprop="url" content="' . $url[0] . '">';
		$image .= '<meta itemprop="width" content="' . $url[1] . '">';
		$image .= '<meta itemprop="height" content="' . $url[2] . '">';
		$image .= "</div>";
		echo $image;
	}
}


the_content(); ?>
<div id='gedi-newsletter-2'> </div>

<div id="taboola-below-article-thumbnails"></div>
    <div id="taboola-below-article-thumbnails-2nd"></div>

<?php
/*

<script type="text/javascript">
	window._taboola = window._taboola || [];
	_taboola.push({
		mode: 'organic-thumbnails-b',
		container: 'taboola-below-article-thumbnails',
		placement: 'Below Article Thumbnails',
		target_type: 'mix'
	});
</script>

<script type="text/javascript">
	window._taboola = window._taboola || [];
	_taboola.push({
		mode: 'thumbnails-b',
		container: 'taboola-below-article-thumbnails-2nd',
		placement: 'Below Article Thumbnails 2nd',
		target_type: 'mix'
	});
</script>
*/ ?>