<!-- GROUP MINOR -->
<?php
$chiave = get_field('chiave_click_server', 'option');
try{
	#$i_piu_letti = file_get_contents('http://click.kataweb.it/ws/' . $chiave .'/content_/0/20/23/hours');

	$i_piu_letti = wp_cache_get( 'click_kataweb_most_read_'.$chiave );
	if ( false === $i_piu_letti ) {
		$i_piu_letti = file_get_contents('http://click.kataweb.it/ws/' . $chiave .'/x/0/20/23/hours');
		wp_cache_set( 'click_kataweb_most_read_'.$chiave, $i_piu_letti );
	}

}catch  (Exception $ex){
	error_log( $ex );
}
if(isset($i_piu_letti) && $i_piu_letti != false){
	$i_piu_letti = simplexml_load_string($i_piu_letti);
}
?>
<?php if(isset($i_piu_letti) && $i_piu_letti != false): $count = 1; $array_check = [];?>
<div class="widget widget-most-read">
	<p class="widget-title">Più letti Oggi</p>
	<ol class="widget-list">
		<?php foreach ($i_piu_letti->hours23->children() as $item):
			if($count > 5 ) break;
			$content = (string)$item;
			$content = explode('_', $content);
			$content_id = $content[2];
			if(in_array((int)$content_id, $array_check)){
				continue;
			}elseif (get_post_status((int)$content_id) != 'publish'){
				continue;
			}elseif (get_field('articolo_importato', (int)$content_id) == 1){
				continue;
			}elseif (get_field('native', (int)$content_id) == 1){
                continue;
            }else{
				$array_check[] = (int)$content_id;
				$count++;
			}
			$post_object = get_post((int)$content_id);
			?>
		<li>
			<p class="widget-text">
				<a href="<?php echo get_permalink($post_object); ?>"><?php echo $post_object->post_title; ?></a>
			</p>
		</li>
		<?php endforeach; ?>
	</ol>
</div>
<!-- END GROUP MINOR -->
<?php endif; ?>
