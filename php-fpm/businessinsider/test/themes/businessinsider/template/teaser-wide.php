<?php
global $class;
?>
<!-- TEASER WIDE -->
<div class="teaser-wide teaser-wide-<?php echo $class; ?>">
    <div class="teaser teaser-regular">
		<?php if(has_post_thumbnail()): ?>
            <a href="<?php the_permalink(); ?>">
                <p class="image">
					<?php echo image_lazy(get_the_ID(), 'teaser_regular') ?>
                </p>
            </a>
		<?php endif; ?>
	    <?php $native = get_field('native');
	    if($native){
		    ?><div class="native-label">Contenuto sponsorizzato</div>
	    <?php } ?>

        <a href="<?php the_permalink(); ?>">
            <h4 class="teaser-title-thinner"><?php the_title(); ?></h4>
        </a>
        <p class="teaser-text">
            <a href="<?php the_permalink(); ?>"></a>
            <span class="time-published">
			<?php
			$nascondi_autore = get_field("nascondi_autore", $post->ID);
			$firma = strip_tags(get_field("firma", $post->ID),"<b><i><a>");
			if(trim($firma) != ""){
				echo '<span class="author-sign">'.$firma.'</span>';
			}
			if(!$nascondi_autore) { ?>
                <span class="author-link"><?php the_author_posts_link(); ?></span>
			<?php } ?>
			<span class="orario-pubblicazione"><?php echo orario($post, 'teaser'); ?></span>
			<?php $numeroVisualizzazioni = numeroVisualizzazioni($post, 'hot'); ?>
				<?php if($numeroVisualizzazioni): ?>
					<?php echo $numeroVisualizzazioni; ?>
				<?php endif; ?>
		</span>
        </p>
    </div>
</div>
<!-- FINE TEASER WIDE -->