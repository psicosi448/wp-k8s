<!-- TEASER SMALL BOX -->
<div class="teaser teaser-small">
	<?php if(has_post_thumbnail()): ?>
	<div class="image col">
		<a href="<?php the_permalink(); ?>">
			<?php echo image_lazy(get_the_ID(), 'teaser_small_box') ?>
		</a>
	</div>
	<?php endif; ?>
	<div class="text col">
		<a href="<?php the_permalink(); ?>">
			<h4 class="teaser-title"><?php the_title(); ?></h4>
		</a>
		<span class="time-published">
			<?php
			$nascondi_autore = get_field("nascondi_autore", $post->ID);
			$firma = strip_tags(get_field("firma", $post->ID),"<b><i><a>");
			if($firma){
				echo '<span class="author-sign">'.$firma.'</span>';
			}
			if(!$nascondi_autore) {
				the_author_posts_link();
			}
			?>
			<?php echo orario($post); ?>
			<?php $numeroVisualizzazioni = numeroVisualizzazioni($post, 'break-temp'); ?>
			<?php if($numeroVisualizzazioni): ?>
				<?php echo $numeroVisualizzazioni; ?>
			<?php endif; ?>
		</span>
	</div>
</div>