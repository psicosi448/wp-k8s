<div class="article-teaser">
	<a href="<?php the_permalink(); ?>">
		<div class="teaser-content">
			<?php if(has_post_thumbnail()): ?>
			<div class="teaser-image">
				<?php echo image_lazy(get_the_ID(), 'teaser_regular', 'menu'); ?>
			</div>
			<?php endif; ?>
			<div class="teaser-text">
				<p><?php the_title(); ?></p>
			</div>
		</div>
	</a>
</div>