<div class="teaser teaser-small teaser-native">
	<div class="native-label">Contenuto sponsorizzato</div>
	<br>
	<?php if(has_post_thumbnail()): ?>
		<div class="image col">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail('teaser_small_box'); ?>
			</a>
		</div>
	<?php endif; ?>
	<div class="text col">
		<a href="<?php the_permalink(); ?>">
			<h4 class="teaser-title"><?php the_title(); ?></h4>
		</a>
		<?php $brand = get_field('brand'); ?>
		<a class="native-link" href="<?php echo get_term_link($brand, 'brand'); ?>">Sponsor Content <?php echo $brand->name; ?></a>
	</div>
</div>