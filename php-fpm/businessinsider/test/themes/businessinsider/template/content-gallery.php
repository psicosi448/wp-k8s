<?php
/*
  * Contenuto per il post di tipo gallery
  * */
?>


<?php
/* microdati */
if(has_post_thumbnail()){
	$url = wp_get_attachment_image_src(get_post_thumbnail_id(), "single");
	if ($url) {
		$image = '<div class="media-image" itemprop="image"  itemscope itemtype="https://schema.org/ImageObject" >';
		$image .= '<meta itemprop="url" content="' . $url[0] . '">';
		$image .= '<meta itemprop="width" content="' . $url[1] . '">';
		$image .= '<meta itemprop="height" content="' . $url[2] . '">';
		$image .= "</div>";
		echo $image;
	}
}

global $middle_check, $bottom_check;
	$middle_check = false;
	$bottom_check = false;
	//Verifico se siano stai inserti media
	$c=0;
	if( have_rows('media_gallery') ):
		// Ciclo su tutti i capi
		while ( have_rows('media_gallery') ) : the_row();
			if( get_row_layout() == 'embed' ):
				echo '<div class="media-image">';
					the_sub_field('codice_embed');
				echo '</div>';
				echo add_tag_adv_media();
			elseif ( get_row_layout() == 'immagine' ):
				$id = get_sub_field('immagine');
				$caption = get_sub_field('didascalia');
				// lazy alle gallery oltre la prima
				if($c)
					echo formatImage($id, 'large', $caption, "", false, true);
				else
					echo formatImage($id, 'large', $caption);
				echo add_tag_adv_media();
			elseif( get_row_layout() == 'blocco_testo'):
				the_sub_field('area_di_testo');
			endif;
			$c++;
		endwhile;
		add_tag_adv_media($last = 1);

	endif;

	?>
<?php the_content(); ?>
<div id='gedi-newsletter-2'> </div>

<div id="taboola-below-article-thumbnails"></div>
    <div id="taboola-below-article-thumbnails-2nd"></div>

<?php
/*

<script type="text/javascript">
	window._taboola = window._taboola || [];
	_taboola.push({
		mode: 'organic-thumbnails-b',
		container: 'taboola-below-article-thumbnails',
		placement: 'Below Article Thumbnails',
		target_type: 'mix'
	});
</script>
<script type="text/javascript">
	window._taboola = window._taboola || [];
	_taboola.push({
		mode: 'thumbnails-b',
		container: 'taboola-below-article-thumbnails-2nd',
		placement: 'Below Article Thumbnails 2nd',
		target_type: 'mix'
	});
</script>
*/
?>