<div class="article-small col-sm-4 col-md-3">
	<a href="<?php the_permalink(); ?>">
		<?php if(has_post_thumbnail()): ?>
			<?php the_post_thumbnail('single-result', ['class' => 'article-image'])?>
		<?php endif;  ?>
		<div class="article-small-text">
			<h4><?php the_title(); ?></h4>
		</div>
	</a>
</div>