<!-- TEASER SMALL  NATIVE -->
<div class="teaser teaser-small teaser-small-native">
	<div class="result-list">
		<div class="teaser-image">
			<?php if(has_post_thumbnail()): ?>
				<a href="<?php the_permalink(); ?>">
					<?php echo image_lazy(get_the_ID(), 'teaser_regular') ?>
				</a>
			<?php endif;  ?>
		</div>
		<div class="teaser-content">
			<div class="teaser-text">
				<div class="native-label">Contenuto sponsorizzato</div>
				<a href="<?php the_permalink(); ?>">
					<h4 class="teaser-title small"><?php the_title(); ?></h4>
				</a>
				<!--<a class="native-link" href="url-dettaglio">Sponsor Content BI Studios</a>-->
			</div>
            <?php if(is_category() || is_tag() || is_tax()): ?>
            <div class="article-author">
                <ul>
                    <li>
						<?php echo orario($post, 'teaser'); ?>
                    </li>
                </ul>
            </div>
            <?php endif; ?>
		</div>
	</div>
</div>
<!-- FINE TEASER SMALL NATIVE -->