<?php
global $acf_key;
?><!-- MAIN -->
<main style="min-height: 100vh;">
	<div class="quicklook-wrapper wrapper">
		<div class="wrapper">
			<div class="group-primary">
				<div>
					<?php
					global $posts;
					// controllo se è stata inserita una immagine di apertura del brand
					$logo = get_field('logo', $acf_key);
					$link_logo = get_field('link_logo', $acf_key);


					//					var_dump($logo["url"]);
					if($logo){
						?>
						<div id="sponsor_logo_container">
							<?php if($link_logo){ echo '<a href="'.$link_logo.'">'; } ?>
							<img src="<?php echo $logo["url"]; ?>">
							<?php if($link_logo){ echo '</a>'; } ?>
						</div>

						<?php
					}
					$apertura_manuale = get_field('apertura_manuale', $acf_key);
					if($apertura_manuale){
						$titolo = get_field('titolo_manuale', $acf_key);
						$immagine_obj = get_field('immagine_manuale', $acf_key);
						if($immagine_obj)
							$immagine = "<img src='".$immagine_obj["sizes"]["strillo_grande"]."' />";
						$link_obj = get_field('link_manuale', $acf_key);
						if($link_obj){
							$link = get_permalink($link_obj[0]->ID);
							$exclude = $link_obj[0]->ID;
						}

					}else{
						$titolo = $posts[0]->post_title;
						$immagine = image_lazy($posts[0]->ID, "strillo_grande");
						$link = get_permalink($posts[0]->ID);
						$exclude = $posts[0]->ID;
					}

					?>
					<a href="<?php echo $link ?>" class=" teaser-image-overlay" data-quicklook="">
						<h1 class="title"><?php echo $titolo; ?></h1>
						<?php if($immagine) echo $immagine; ?>
					</a>
					<div>
						<?php
						/*
                <span class="time-published">
                    <img alt="" class="icon icon-small hide-mobile-down" src="<?php echo get_stylesheet_directory_uri() . '/assets/img/blue-clock-icon.svg'; ?>">
                    <img alt="" class="icon icon-small hide-tablet-up" src="<?php echo get_stylesheet_directory_uri() . '/assets/img/blue-clock-icon.svg'; ?>">
                    <span>4 HOURS</span>
                </span>
						*/
						?>
					</div>
				</div>

				<?php if(have_posts()): ?>
					<div id="feed">
						<div id="adv-x22"><script>try { MNZ_RICH('x22'); } catch(e) { }</script></div>
						<?php $count = 1; while (have_posts()): the_post(); $post_not_in[] = get_the_ID();
							if(get_the_ID() == $exclude){
								$count ++;
								continue;

							}
							?>
							<?php $native = get_field('native'); ?>
							<?php /*
							if($native){ ?>
								<?php get_template_part('template/teaser', 'small_native'); ?>
							<?php }else { ?>
								<?php get_template_part('template/teaser', 'small'); ?>
							<?php }  */ ?>
							<?php get_template_part('template/teaser', 'small'); ?>
							<?php if($count == 2): ?>
								<div id="adv-Middle1-dev"><script>try { MNZ_RICH('Middle1-dev'); } catch(e) { }</script></div>
							<?php endif; $count++; ?>
						<?php endwhile;?>
						<?php global $wp_query;
						if($wp_query->max_num_pages > 1):  ?>
							<input type="button" class="btn btn-big btn-attend" id="load-more" data-page="2" value="Carica altri" data-url="<?php bloginfo('url')?>" data-action="load-more" data-number="10" data-type="brand" data-id="<?php echo $wp_query->get_queried_object()->term_id; ?>" data-container="feed" />
						<?php endif; ?>
					</div>
				<?php endif;  ?>

			</div>


			<div class="group-minor group-minor-sidebar">
				<?php get_sidebar(); ?>
			</div>


		</div>


	</div>
</main>
<!-- END MAIN --><?php
