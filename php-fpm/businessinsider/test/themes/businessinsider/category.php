<?php
get_header(); $post_not_in = [];?>

<!-- MAIN -->
<main style="min-height: 100vh;">
	<div class="quicklook-wrapper wrapper">
		<div class="wrapper wrapper-one-col-sidebar">
			<div class="wrapper ">
				<div class="wrapper wrapper-two-col-sidebar">
					<div class="half-left">
						<div class="group-secondary">
							<?php if(have_posts()):
								$counter = 0;
								?>
							<div id="feed">
								<?php $count = 1; while (have_posts()): the_post(); $post_not_in[] = get_the_ID();
									$counter ++;
									?>
									<?php $native = get_field('native'); ?>
									<?php if($native): ?>
										<?php get_template_part('template/teaser', 'small_native'); ?>
									<?php else: ?>
										<?php get_template_part('template/teaser', 'small'); ?>
									<?php endif; ?>
									<?php if($count == 2): ?>
										<div id="adv-Middle1-dev"><script>try { MNZ_RICH('Middle1-dev'); } catch(e) { }</script></div>
									<?php endif; $count++;

									if($counter == 3){
										?>
										<div id="adv-x22"><script>try { MNZ_RICH('x22'); } catch(e) { }</script></div>
										<?php
									}
									?>
								<?php endwhile;?>
								<?php global $wp_query;
								if($wp_query->max_num_pages > 1):  ?>
									<input type="button" class="btn btn-big btn-attend" id="load-more" data-page="2" value="Carica altri" data-url="<?php bloginfo('url')?>" data-action="load-more" data-number="10" data-type="category" data-id="<?php echo $wp_query->get_queried_object()->term_id; ?>" data-container="feed" />
								<?php endif; ?>
							</div>
							<?php endif;  ?>
							<div id="spinner-small" class="spinner small hide">
								<div class="bounce1"></div>
								<div class="bounce2"></div>
								<div class="bounce3"></div>
							</div>
						</div>
						<?php
						remove_filter('pre_get_posts', 'bi_home_pre_get_post');
						$args = [
							'posts_per_page'    => 10,
							'cat'               => get_queried_object_id(),
							'meta_key'          => 'click_server',
							'orderby'           => 'meta_value_num',
							'post_status'       => 'publish',
						    'post__not_in'      => $post_not_in,
							'meta_query' => [
								'relation' => 'AND',
								[
									'key' => 'articolo_importato',
									'value' => 1,
									'compare' => '!=',
								],
								[
									'key' => 'native',
									'value' => 1,
									'compare' => '!=',
								]
							]
							,
							'date_query' => array(
								array(
									'column' => 'post_date',
									'after' => RELATED_WEEKS_AGO_LIST.' week ago',
								)
							),
						];
						$most_read = new WP_Query($args);


						if($most_read->have_posts()): ?>
							<div class="group-minor">
								<?php while ($most_read->have_posts()): $most_read->the_post();?>
									<?php get_template_part('template/teaser', 'small_box'); ?>
								<?php endwhile; ?>
							</div>
						<?php endif; wp_reset_query();
						add_filter('pre_get_posts', 'bi_home_pre_get_post');?>
					</div>
					<div class="group-minor group-minor-sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<!-- END MAIN -->
<?php get_footer(); ?>