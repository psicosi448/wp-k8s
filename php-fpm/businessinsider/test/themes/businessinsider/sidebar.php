<?php   get_template_part('template/financiallounge'); ?>

<?php  if( is_archive() || is_single()): ?>
	<div id="adv-Middle1"><script>try { MNZ_RICH('Middle1'); } catch(e) { }</script></div>
<?php endif; ?>
<div id="adv-x21">
    <script> try { MNZ_RICH('x21'); } catch(e) { }</script>
</div>

<?php get_template_part('template/taboola', 'first'); ?>


<?php
global $post;
if(!is_native($post)): ?>
<?php $strilli_native = get_field('strilli_native', 'option'); ?>
<?php if($strilli_native): ?>
	<?php foreach($strilli_native as $post): setup_postdata($post); ?>
		<?php get_template_part('template/native', 'teaser'); ?>
	<?php endforeach; wp_reset_postdata(); ?>
<?php endif; ?>
<?php endif; //Fine Verifica Native ?>
<?php //if(!is_native($post)): ?>
<?php //get_template_part('template/piu', 'letti'); ?>
<?php //endif; //Fine verifica Native ?>


<div id="adv-Middle2"><script>try { MNZ_RICH('Middle2'); } catch(e) { }</script></div>
<?php get_template_part('template/taboola', 'second'); ?>

<div id="adv-Middle3"><script>try { MNZ_RICH('Middle3'); } catch(e) { }</script></div>
