<?php get_header(); ?>
<main style="min-height: 100vh;">
	<div class="container-fluid">
		<div>
			<h2>Ooops!</h2>
			<div>
				<h3 class="error-header">Qualcosa è andato storto.<br />(404 pagina non trovata)</h3>
				<h3>&nbsp;</h3>
			</div>
		</div>

		<?php
		$args = [
			'post_type' => 'post',
			'posts_per_page' => 20,
		];
		$query_404 = new WP_Query($args);
		?>
		<?php if($query_404->have_posts()): ?>
		<div id="feed">
			<h3 class="latest-articles">Ultimi Articoli</h3>
			<?php while($query_404->have_posts()): $query_404->the_post(); ?>
				<?php get_template_part('template/content', 'small'); ?>
			<?php endwhile; wp_reset_query(); ?>
		</div>
		<?php endif; ?>
		<div style="clear: both;"></div>
		<?php global $wp_query;
		if($wp_query->max_num_pages > 1):  ?>
			<input type="button" class="btn btn-big btn-attend" id="load-more" data-page="2" value="Carica altri" data-url="<?php bloginfo('url')?>" data-action="load-more" data-number="4" data-type="404" data-id="404" data-container="feed" />
		<?php endif; ?>
	</div>

	<div id="spinner-small" class="spinner small hide">
		<div class="bounce1"></div>
		<div class="bounce2"></div>
		<div class="bounce3"></div>
	</div>

</main>
<?php get_footer(); ?>
