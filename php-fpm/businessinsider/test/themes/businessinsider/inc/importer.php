<?php
//wp_schedule_event(time(), 'hourly', 'financial_hourly_event');

//register_activation_hook(__FILE__, 'financial_import_activation');

add_action( 'wp', 'financial_import_activation' , 100000);

function financial_import_activation() {
    if (! wp_next_scheduled ( 'financial_hourly_event' )) {
        wp_schedule_event(time(), 'hourly', 'financial_hourly_event');

    }
}

add_action('financial_hourly_event', 'financial_do_this_hourly', 1000);

function financial_do_this_hourly() {

    import_financial();
}


add_action( 'admin_menu', 'import_plugin_menu' );

/** Step 1. */
function import_plugin_menu() {
	add_options_page( 'Import Financial', 'Import Financial', 'manage_options', 'import-financial', 'bi_import_financial_options' );
}


function bi_import_financial_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap">';
	echo '<p>Import Financial</p>';
    $schedule = wp_get_schedule( 'financial_hourly_event' );
    var_dump($schedule);
    import_financial();
	echo '</div>';
}

function import_financial()
{

    $response = wp_remote_get('http://www.financialounge.com/rss_businessinsider.xml');
    if (is_array($response)) {
        $header = $response['headers']; // array of http header lines
        $body = $response['body']; // use the content
        $xml = simplexml_load_string($body, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        $c = 0;
        foreach ($array["channel"]["item"] as $item) {
            inside_debugme("Processo " . $item["title"]);
            // controllo se il post non è stato importato (su check postmeta orig_link)
            $args = array(
                'meta_key' => 'orig_link',
                'meta_value' => $item["link"],
                'meta_compare' => '=',
                'fields' => 'all',
                'post_status' => array('publish', 'future', 'draft'),
            );
            $posts = get_posts($args);
            if ($posts) {
                // esiste, concludo il processo
                inside_debugme("Import " . $import . " parziale: concluso dopo " . $c . " elementi");
                return false;
            } else {
                // se non è stato importato creo il post
                $my_post = array(
                    'post_title' => wp_strip_all_tags($item["title"]),
                    'post_content' => strip_tags($item["description"], "<b><strong><i><em><a><br><p><span>"),
                    'post_status' => "publish",
                    'post_date' => date_i18n("Y-m-d H:i:s", strtotime($item["pubDate"])),
                );
                $post_id = wp_insert_post($my_post);
                inside_debugme("Importo  " . $item["title"] . " con ID " . $post_id);
                if (!$post_id) {
                    inside_debugme("errore nel creare il post con titolo " . $item["title"]);
                }

                add_post_meta($post_id, "orig_link", $item["link"]);
                add_post_meta($post_id, "feed_import", 1);
                wp_set_object_terms($post_id, "Featured", "category");
                wp_set_object_terms($post_id, array("FinanciaLounge"), "brand");

                // native
                update_field("field_57f7655617dc6", 1, $post_id);

                // abilita link
                update_field("field_57fb5b5800222", 1, $post_id);

                // sflaggo articolo importato
                update_field("field_57e93b846f4a6", 0, $post_id);

                // nascondi autore
                update_field("field_5851262fff353", 1, $post_id);

                update_field("field_580f5db96ede7", "si", $post_id);

                // recupero l'immagine e la creo localmente
                $image = $item["enclosure"]["@attributes"]["url"];
                inside_debugme("Importo immagine " . $image);
                // aggiungo il caption iStock
                $attachment_id_photo = inside_bi_add_attachment_to_post($image, wp_strip_all_tags($item["title"]), "iStock", $post_id);
                /* ciclo su tutte le immagini associate al post e metto la prima come featured */
                $media = get_attached_media('image', $post_id);
                foreach ($media as $image) {
                    update_post_meta($post_id, '_thumbnail_id', $image->ID);
//                set_post_thumbnail( $post_id, $image->ID );
                    break;
                }
                /*
                if($attachment_id_photo){
                        set_post_thumbnail( $post_id, $attachment_id_photo );
                        debugme( "Importata immagine ".$src." con ID ".$attachment_id_photo);
                }
                */


                // associo il media come featured
                //           $idimg = bi_get_attachment_id_from_src($src);
                //set_post_thumbnail( $post_id, $idimg );

            }

            $c++;
        }
    }

    echo("Import " . $import . " completo: concluso");
}
/*
function bi_get_attachment_id_from_src ($image_src) {
  global $wpdb;
  $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
  $id = $wpdb->get_var($query);
  return $id;
  }*/

function inside_debugme($string){
    echo "<p>";
    echo $string;
    echo "</p>";
    echo "\n";
}




function inside_bi_add_attachment_to_post($media_url, $media_description = '', $media_credit = '', $post_id, $media_type = ''){

    $file = $media_url;
    $filename = basename($file);

    if (strpos($filename, '.jpeg') === false && strpos($filename, '.png') === false && strpos($filename, '.jpg') === false) {
        if($media_type != ''){
            if($media_type == 'image/png'){
                $filename = $filename . '.jpg';
                $file_path = wp_upload_dir();
                $img = imagecreatefrompng($file);
                imagejpeg($img, $file_path['basedir'] . '/'. $filename, 90);
                imagedestroy($img);
                $file_to_import = file_get_contents($file_path['basedir'] . '/'. $filename);
            }elseif ($media_type == 'image/jpeg'){
                $file_to_import = file_get_contents($file);
                $filename = $filename . '.jpeg';
            }
        }else{
            $file_to_import = file_get_contents($file);
            $filename = $filename . '.jpeg';
        }
    }elseif(strpos($filename, '.png') !== false){
        $filename = str_replace('.png', '', $filename);
        $filename = $filename . '.jpeg';
        $file_path = wp_upload_dir();
        $img = imagecreatefrompng($file);
        imagejpeg($img, $file_path['basedir'] . '/'. $filename, 90);
        imagedestroy($img);
        $file_to_import = file_get_contents($file_path['basedir'] . '/'. $filename);
    }else{
        $file_to_import = file_get_contents($file);

    }

    $upload_file = wp_upload_bits($filename, null, $file_to_import);
    if (!$upload_file['error']) {
        $wp_filetype = wp_check_filetype($filename, null );
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_parent' => $post_id,
            'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
            'post_content' => $media_description,
            'post_excerpt' => $media_credit,
            'post_status' => 'inherit'
        );
        $attachment_id_photo = wp_insert_attachment( $attachment, $upload_file['file'], $post_id );
        if (!is_wp_error($attachment_id_photo)) {
            require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            $attachment_data = wp_generate_attachment_metadata( $attachment_id_photo, $upload_file['file'] );
            wp_update_attachment_metadata( $attachment_id_photo,  $attachment_data );
        }
        if(isset($file_path) && !empty($file_path)){
            if(file_exists($file_path['basedir'] . '/'. $filename)){
                unlink($file_path['basedir'] . '/'. $filename);
            }
        }
        return (int)$attachment_id_photo;
    }else{
        if(isset($file_path) && !empty($file_path)){
            if(file_exists($file_path['basedir'] . '/'. $filename)){
                unlink($file_path['basedir'] . '/'. $filename);
            }
        }
        return false;
    }


}