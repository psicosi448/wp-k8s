<?php

if( function_exists('acf_add_local_field_group') ):

	// recupero le categorie

	$categories = get_categories( array(
		'orderby' => 'name',
		'order'   => 'ASC'
	) );

	foreach ( $categories as $category ) {

		$fields[] = array (
			'key' => 'field_5b056a15'.$category->term_id,
			'label' => $category->name,
			'name' => 'codice_theoutplay_'.$category->term_id,
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'new_lines' => '',
		);
	}

	acf_add_local_field_group(array (
		'key' => 'group_5b0569fdde691',
		'title' => 'TheOutPlay',
		'fields' => $fields,
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-theoutplay',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));


endif;


if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'group_5b39e23f86209',
		'title' => 'TheOutPlay',
		'fields' => array (
			array (
				'key' => 'field_5b39e24eba4a8',
				'label' => 'Configurazione TheOutPlay',
				'name' => 'disable_theoutplay',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => 'Disabilita l\'erogazione di TheOutPlay su questo contenuto',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'side',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

endif;