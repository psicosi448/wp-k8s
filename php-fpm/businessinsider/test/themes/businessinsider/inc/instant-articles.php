<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 20/10/17
 * Time: 11.48
 */

use Facebook\InstantArticles\Elements\Analytics;
use Facebook\InstantArticles\Elements\Ad;
use Facebook\InstantArticles\Elements\Footer;
use Facebook\InstantArticles\Elements\Paragraph;

add_filter( 'instant_articles_transformed_element', 'bi_addAdSectionCallback' , 0);
function  bi_addAdSectionCallback( $instant_article ) {
    if ( $instant_article !== null ) {
        $canonical = $instant_article->getCanonicalURL();
        $header = $instant_article->getHeader();
        $header->addAd(
            Ad::create()->withSource('http://www.repstatic.it/cless/channel/instantarticle/adv-page-300x250.html?adref='.$canonical)
        );


       $instant_article->withFooter(
	     	Footer::create()->addCredit(
		        Paragraph::create()->appendText("Copyright © 1999-2018 GEDI Digital S.r.l.")
            )->withCopyright("Tutti i diritti riservati")
        );

    }
    return $instant_article;
}



add_action("instant_articles_after_transform_post","bi_instant_articles_after_transform_post");
function bi_instant_articles_after_transform_post($ia_post){
    $ia_post->instant_article->addMetaProperty('fb:use_automatic_ad_placement', 'true');
    $post_id = $ia_post->get_the_id();
    $canonical = get_permalink($post_id);
    $post = get_post($post_id);
/*
    $advertorial = Ad::create()->withSource('http://www.repstatic.it/cless/channel/instantarticle/adv-page-300x250.html?adref='.$canonical);
    $ia_post->instant_article->addChild( $advertorial);
*/
    $chartbeat = "";
    $chartbeat .= "<script type='text/javascript'>";
    $chartbeat .= " var _sf_async_config = {};";
    $chartbeat .= "_sf_async_config.uid = 62281;";
    $chartbeat .= "_sf_async_config.domain = '".str_replace("https://", "", get_bloginfo("url"))."';";
    $chartbeat .= "_sf_async_config.title = '".$post->post_title."';";
    //$precontent .= "_sf_async_config.sections = '/repubblica/esteri';";
    $chartbeat .= "_sf_async_config.useCanonical = true;";
    $chartbeat .= "window._sf_endpt = (new Date()).getTime();";
    $chartbeat .= "</script>";
    $chartbeat .= "<script defer src=\"//static.chartbeat.com/js/chartbeat_fia.js\"></script>";

    $analitics = Analytics::create();
    $analitics->withHTML( $chartbeat );
    $ia_post->instant_article->addChild( $analitics );
	$canonical = str_replace("https://", "http://", $canonical);
	$category = get_the_category($post_id);
	$wt_section ="";
	if($category)
		$wt_section = "/business insider/".$category[0]->slug;
    $wt = "https://scripts.kataweb.it/ws/wt/getWTConfCommon.php?pageHref=".$canonical."&wt_section=".urlencode($wt_section);
    $analitics = Analytics::create();
    $analitics->withSource( $wt );
    $ia_post->instant_article->addChild( $analitics );

}


add_filter("instant_articles_should_submit_post", "bi_instant_articles_should_submit_post",10, 2);
function bi_instant_articles_should_submit_post($should_show, $IA_object){
    if($should_show){
        $ia_feed = get_field("ia_feed",$IA_object->get_the_id() );
        if($ia_feed)
            return true;

    }
    return false;
}


/**
 * campo acf per il metabox
 */
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array (
        'key' => 'group_4938151023987',
        'title' => 'Instant Article',
        'fields' => array (
            array (
                'key' => 'field_49381563fd0b1',
                'label' => 'Inserisci l\'articolo su Instant Article',
                'name' => 'ia_feed',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => 'Seleziona per inserirlo nel feed',
                'default_value' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 1,
        'position' => 'side',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;


function bi_instant_articles_authors( $authors, $post_id) {

	$nascondi_autore = get_field("nascondi_autore", $post_id);
	$firma = strip_tags(get_field("firma", $post_id),"<b><i><a>");
	if($nascondi_autore){
		if($firma){
			$authors[0]->display_name = $firma;
		}else{
			$authors = array();
		}
	}
	return $authors;

}
add_filter('instant_articles_authors', 'bi_instant_articles_authors', 10, 2);


if(is_feed(INSTANT_ARTICLES_SLUG)){
	remove_filter( 'get_avatar', 'rocket_lazyload_images', PHP_INT_MAX );
	remove_filter( 'the_content', 'rocket_lazyload_images', PHP_INT_MAX );
	remove_filter( 'widget_text', 'rocket_lazyload_images', PHP_INT_MAX );
	remove_filter( 'post_thumbnail_html', 'rocket_lazyload_images', PHP_INT_MAX );
}