<?php
if( function_exists('acf_add_options_page') ) {
	// add parent
	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Impostazioni Business Insider',
		'menu_title' 	=> 'Impostazioni Business Insider',
		'redirect' 		=> false
	));
	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Impostazioni Homepage',
		'menu_title' 	=> 'Homepage',
		'parent_slug' 	=> $parent['menu_slug'],
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Gestione Sidebar',
		'menu_title' 	=> 'Sidebar',
		'parent_slug' 	=> $parent['menu_slug'],
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Rss Setting',
		'menu_title' 	=> 'Rss',
		'parent_slug' 	=> $parent['menu_slug'],
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'TheOutPlay',
		'menu_title' 	=> 'TheOutPlay',
		'parent_slug' 	=> $parent['menu_slug'],
	));

}


function native_realtionship_filter( $args, $field, $post_id ) {

	// only show children of the current post being edited

	$args['meta_query'] = [
		[
			'key'       => 'native',
		    'value'     => 1,
		    'compare'   => '='
		]
	];
	// return
	return $args;

}
// filter for a specific field based on it's name
add_filter('acf/fields/relationship/query/name=strilli_native', 'native_realtionship_filter', 10, 3);



function apertura_realtionship_filter( $args, $field, $post_id ) {

	// only show children of the current post being edited

	$args['post_status'] = 'publish';
	$args['order'] = 'DESC';
	$args['orderby'] = 'date';
	// return
	return $args;

}
// filter for a specific field based on it's name
add_filter('acf/fields/post_object/query/name=articolo_apertura', 'apertura_realtionship_filter', 10, 3);


function riaperture_realtionship_filter( $args, $field, $post_id ) {

	// only show children of the current post being edited

	$args['post_status'] = 'publish';
	$args['order'] = 'DESC';
	$args['orderby'] = 'date';
	// return
	return $args;

}
// filter for a specific field based on it's name
add_filter('acf/fields/relationship/query/name=ri-aperture', 'riaperture_realtionship_filter', 10, 3);


if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array (
        'key' => 'group_57f765504d02e',
        'title' => 'Articoli',
        'fields' => array (
            array (
                'key' => 'field_57fb56fef8645',
                'label' => 'Brand',
                'name' => 'brand',
                'type' => 'taxonomy',
                'instructions' => '',
                'required' => 0,
                /*'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_57f7655617dc6',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),*/
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'taxonomy' => 'brand',
                'field_type' => 'select',
                'allow_null' => 1,
                'add_term' => 1,
                'save_terms' => 1,
                'load_terms' => 1,
                'return_format' => 'object',
                'multiple' => 0,
            ),
            array (
                'key' => 'field_57f7655617dc6',
                'label' => 'Native',
                'name' => 'native',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
            ),
            array (
                'key' => 'field_57fb5b5800222',
                'label' => 'Abilita la visualizzazione del link a bigpage',
                'name' => 'link_bigpage',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'default_value' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_57f7655617dc6',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            /*
            array (
                'key' => 'field_57fb5b5800302',
                'label' => 'Nome Prodotto',
                'name' => 'nome_prodotto',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_57f7655617dc6',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_57fb5b6700303',
                'label' => 'Anno',
                'name' => 'anno',
                'type' => 'number',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_57f7655617dc6',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => 2015,
                'max' => '',
                'step' => '',
            ),
            */
            array (
                'key' => 'field_5851262fff353',
                'label' => 'Nascondi Autore',
                'name' => 'nascondi_autore',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
            ),
            array (
                'key' => 'field_58342dc6160fc',
                'label' => 'Firma Autore',
                'name' => 'firma_autore',
                'type' => 'user',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_5851262fff353',
                            'operator' => '!=',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'role' => '',
                'allow_null' => 1,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_58512645ff354',
                'label' => 'Firma',
                'name' => 'firma',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_57e93b741ab40',
        'title' => 'Articoli Feed',
        'fields' => array (
            array (
                'key' => 'field_57e93b846f4a6',
                'label' => 'Articolo importato',
                'name' => 'articolo_importato',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
            ),
            array (
                'key' => 'field_57e93b9a6f4a7',
                'label' => 'Permalink Originale',
                'name' => 'permalink_originale',
                'type' => 'url',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_57e93b846f4a6',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
            ),
            array (
                'key' => 'field_57e93bae6f4a8',
                'label' => 'Dati Articolo Originale',
                'name' => 'dati_articolo_originale',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_57e93b846f4a6',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
        'local' => 'php',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_57fe0f803fbbc',
        'title' => 'Articoli Gallery',
        'fields' => array (
            array (
                'key' => 'field_57fe0e8c7ba8a',
                'label' => 'Media Gallery',
                'name' => 'media_gallery',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'button_label' => 'Aggiungi Riga',
                'min' => '',
                'max' => '',
                'layouts' => array (
                    array (
                        'key' => '57fe0e9e50540',
                        'name' => 'immagine',
                        'label' => 'Immagine',
                        'display' => 'block',
                        'sub_fields' => array (
                            array (
                                'key' => 'field_57fe0eb97ba8b',
                                'label' => 'Immagine',
                                'name' => 'immagine',
                                'type' => 'image',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array (
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'return_format' => 'id',
                                'preview_size' => 'thumbnail',
                                'library' => 'all',
                                'min_width' => '',
                                'min_height' => '',
                                'min_size' => '',
                                'max_width' => '',
                                'max_height' => '',
                                'max_size' => '',
                                'mime_types' => '',
                            ),
                            array (
                                'key' => 'field_57fe163ea87dd',
                                'label' => 'Didascalia',
                                'name' => 'didascalia',
                                'type' => 'textarea',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array (
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'maxlength' => '',
                                'rows' => '',
                                'new_lines' => 'wpautop',
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    array (
                        'key' => '57fe0f3f7ba8e',
                        'name' => 'embed',
                        'label' => 'Embed',
                        'display' => 'block',
                        'sub_fields' => array (
                            array (
                                'key' => 'field_57fe0f4a7ba8f',
                                'label' => 'Codice Embed',
                                'name' => 'codice_embed',
                                'type' => 'textarea',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array (
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'maxlength' => '',
                                'rows' => '',
                                'new_lines' => '',
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    array (
                        'key' => '57fe16f725c95',
                        'name' => 'blocco_testo',
                        'label' => 'Blocco Testo',
                        'display' => 'block',
                        'sub_fields' => array (
                            array (
                                'key' => 'field_57fe170925c96',
                                'label' => 'Area di testo',
                                'name' => 'area_di_testo',
                                'type' => 'wysiwyg',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array (
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'tabs' => 'all',
                                'toolbar' => 'basic',
                                'media_upload' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                ),
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_format',
                    'operator' => '==',
                    'value' => 'gallery',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
        'local' => 'php',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_588f2548d8975',
        'title' => 'Commenti',
        'fields' => array (
            array (
                'key' => 'field_588f254cf4cf6',
                'label' => 'Abilita commenti',
                'name' => 'abilita_commenti',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'side',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array (
            0 => 'comments',
        ),
        'active' => 1,
        'description' => '',
        'local' => 'php',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_57e8eacda2bf9',
        'title' => 'Gestione Feed Rss',
        'fields' => array (
            array (
                'key' => 'field_57e8ead6e1337',
                'label' => 'Gestione Feed Business Insider',
                'name' => 'gestione_feed_business_insider',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => '',
                'max' => '',
                'layout' => 'table',
                'button_label' => 'Aggiungi Riga',
                'sub_fields' => array (
                    array (
                        'key' => 'field_57e8eaeae1338',
                        'label' => 'Url Feed',
                        'name' => 'url_feed',
                        'type' => 'url',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                    ),
                    array (
                        'key' => 'field_57e8eaf9e1339',
                        'label' => 'Format',
                        'name' => 'categoria_associata',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array (
                            'standard' => 'Articolo',
                            'gallery' => 'Gallery',
                            'video' => 'Video',
                        ),
                        'default_value' => array (
                        ),
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'ajax' => 0,
                        'return_format' => 'value',
                        'placeholder' => '',
                    ),
                    array (
                        'key' => 'field_57e8ee600897a',
                        'label' => 'Label',
                        'name' => 'label',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                ),
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-rss',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
        'local' => 'php',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_580f5daf42d0e',
        'title' => 'Gestione Immagine In Evidenza',
        'fields' => array (
            array (
                'key' => 'field_580f5db96ede7',
                'label' => 'Mostrare Immagine in Evidenza?',
                'name' => 'mostrare_immagine_in_evidenza',
                'type' => 'radio',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array (
                    'si' => 'Si',
                    'no' => 'No',
                ),
                'allow_null' => 0,
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'no',
                'layout' => 'horizontal',
                'return_format' => 'value',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'acf_after_title',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_57ff9caa6df87',
        'title' => 'Gestione Menu',
        'fields' => array (
            array (
                'key' => 'field_57ff9cb340c67',
                'label' => 'Categorie',
                'name' => 'categorie',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => '',
                'max' => '',
                'layout' => 'table',
                'button_label' => 'Aggiungi Categoria',
                'sub_fields' => array (
                    array (
                        'key' => 'field_57ff9cc240c68',
                        'label' => 'Categoria',
                        'name' => 'categoria',
                        'type' => 'taxonomy',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'taxonomy' => 'category',
                        'field_type' => 'select',
                        'allow_null' => 0,
                        'add_term' => 1,
                        'save_terms' => 0,
                        'load_terms' => 0,
                        'return_format' => 'object',
                        'multiple' => 0,
                    ),
                ),
            ),
            array (
                'key' => 'field_57ffa40feb858',
                'label' => 'Pagine',
                'name' => 'pagine',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array (
                    0 => 'page',
                ),
                'taxonomy' => array (
                ),
                'filters' => '',
                'elements' => '',
                'min' => '',
                'max' => '',
                'return_format' => 'object',
            ),
            array (
                'key' => 'field_57ffa45aac451',
                'label' => 'Pagina Autore',
                'name' => 'pagina_autore',
                'type' => 'post_object',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array (
                    0 => 'page',
                ),
                'taxonomy' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'object',
                'ui' => 1,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-impostazioni-business-insider',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
        'local' => 'php',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_5808c89f41647',
        'title' => 'Gestione Sidebar',
        'fields' => array (
            array (
                'key' => 'field_5800d275a273a',
                'label' => 'Strilli Native',
                'name' => 'strilli_native',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array (
                    0 => 'post',
                ),
                'taxonomy' => array (
                ),
                'filters' => array (
                    0 => 'search',
                ),
                'elements' => array (
                    0 => 'featured_image',
                ),
                'min' => '',
                'max' => '',
                'return_format' => 'object',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-sidaber',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
        'local' => 'php',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_57fce34b3d4f0',
        'title' => 'Impostazioni Click Server',
        'fields' => array (
            array (
                'key' => 'field_57fce357be676',
                'label' => 'Referrer Click Server',
                'name' => 'referrer_click_server',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_5808c7bc14517',
                'label' => 'Chiave Click Server',
                'name' => 'chiave_click_server',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_5808c7d114518',
                'label' => 'Soglia Click Server',
                'name' => 'soglia_click_server',
                'type' => 'number',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => '',
                'max' => '',
                'step' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-impostazioni-business-insider',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
        'local' => 'php',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_57f76f1419137',
        'title' => 'Impostazioni Hompage',
        'fields' => array (
            array (
                'key' => 'field_57f770db35dc3',
                'label' => 'Articolo Apertura',
                'name' => 'articolo_apertura',
                'type' => 'post_object',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array (
                ),
                'taxonomy' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'object',
                'ui' => 1,
            ),
            array (
                'key' => 'field_57f7797435dc4',
                'label' => 'Immagine Articolo Apertura',
                'name' => 'immagine_articolo_apertura',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array (
                'key' => 'field_57f7798835dc5',
                'label' => 'Ri-Aperture',
                'name' => 'ri-aperture',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array (
                ),
                'taxonomy' => array (
                ),
                'filters' => array (
                    0 => 'search',
                    1 => 'post_type',
                    2 => 'taxonomy',
                ),
                'elements' => '',
                'min' => '',
                'max' => '',
                'return_format' => 'object',
            ),
            array (
                'key' => 'field_57f779efa35d4',
                'label' => 'Numero blocchi aperture',
                'name' => 'numero_blocchi_aperture',
                'type' => 'number',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => 1,
                'max' => '',
                'step' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-homepage',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
        'local' => 'php',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_57ffa4869d085',
        'title' => 'Social Link',
        'fields' => array (
            array (
                'key' => 'field_57ffa48d3e1be',
                'label' => 'Facebook',
                'name' => 'facebook',
                'type' => 'url',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
            ),
            array (
                'key' => 'field_57ffa49cfcb41',
                'label' => 'Twitter',
                'name' => 'twitter',
                'type' => 'url',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
            ),
	        array (
		        'key' => 'field_44ffa49cfcb41',
		        'label' => 'Linkedin',
		        'name' => 'linkedin',
		        'type' => 'url',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => 0,
		        'wrapper' => array (
			        'width' => '',
			        'class' => '',
			        'id' => '',
		        ),
		        'default_value' => '',
		        'placeholder' => '',
	        ),
	        array (
		        'key' => 'field_34ffa49cfcb41',
		        'label' => 'Youtube',
		        'name' => 'youtube',
		        'type' => 'url',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => 0,
		        'wrapper' => array (
			        'width' => '',
			        'class' => '',
			        'id' => '',
		        ),
		        'default_value' => '',
		        'placeholder' => '',
	        ),
	        array (
		        'key' => 'field_35ffa49cfcb41',
		        'label' => 'Flipboard',
		        'name' => 'flipboard',
		        'type' => 'url',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => 0,
		        'wrapper' => array (
			        'width' => '',
			        'class' => '',
			        'id' => '',
		        ),
		        'default_value' => '',
		        'placeholder' => '',
	        ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-impostazioni-business-insider',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
        'local' => 'php',
    ));

endif;

/**
 * bigpage
 */
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array (
        'key' => 'group_58bd25391bd1d',
        'title' => 'BigPage',
        'fields' => array (
            array (
                'key' => 'field_58bd254a65102',
                'label' => 'Logo',
                'name' => 'logo',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'full',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
	        array (
		        'key' => 'field_486d2ddad232b',
		        'label' => 'Url Link Logo',
		        'name' => 'link_logo',
		        'type' => 'url',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => 0,
		        'wrapper' => array (
			        'width' => '',
			        'class' => '',
			        'id' => '',
		        ),
		        'message' => '',
		        'default_value' => '    ',
	        ),
	        array (
		        'key' => 'field_73bd2ddae342b',
		        'label' => 'Template Wide',
		        'name' => 'wide',
		        'type' => 'true_false',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => 0,
		        'wrapper' => array (
			        'width' => '',
			        'class' => '',
			        'id' => '',
		        ),
		        'message' => '',
		        'default_value' => 0,
	        ),
	        array (
                'key' => 'field_58bd2ddae342b',
                'label' => 'Apertura manuale',
                'name' => 'apertura_manuale',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
            ),
            array (
                'key' => 'field_58bd2ddad232b',
                'label' => 'Tag',
                'name' => 'tag_native',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => '    ',
            ),
            array (
                'key' => 'field_58bd2deee342c',
                'label' => 'Titolo Manuale',
                'name' => 'titolo_manuale',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_58bd2ddae342b',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_58bd2e02e342d',
                'label' => 'Immagine manuale',
                'name' => 'immagine_manuale',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_58bd2ddae342b',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array (
                'key' => 'field_58bd2e0ce342e',
                'label' => 'Articolo da linkare',
                'name' => 'link_manuale',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_58bd2ddae342b',
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array ('post'),
                'taxonomy' => array (
                ),
                'filters' => array (
                    0 => 'search',
                    1 => 'taxonomy',
                ),
                'elements' => '',
                'min' => '',
                'max' => 1,
                'return_format' => 'object',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => 'brand',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;

/** campi utente **/

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array (
        'key' => 'group_591ac6095af90',
        'title' => 'Dati Profilo Pubblico',
        'fields' => array (
            array (
                'key' => 'field_591ac6109b85b',
                'label' => 'Immagine utente',
                'name' => 'immagine_utente',
                'type' => 'image',
                'instructions' => 'Sarà croppata in 300 x 389 pixel',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'user_profile',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array (
                'key' => 'field_33332645ff354',
                'label' => 'Bio Pubblica',
                'name' => 'bio',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
            ),
            array (
                'key' => 'field_591d55720913b',
                'label' => 'Twitter',
                'name' => 'twitter',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_591d557a0913c',
                'label' => 'Mail Pubblica',
                'name' => 'mail_pubblica',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_591d55860913d',
                'label' => 'Job',
                'name' => 'job',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => 'wpautop',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'user_form',
                    'operator' => '==',
                    'value' => 'all',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;


if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array (
        'key' => 'group_5938151023987',
        'title' => 'Msn Feed',
        'fields' => array (
            array (
                'key' => 'field_59381563fd0b1',
                'label' => 'Inserisci l\'articolo nel Feed Msn',
                'name' => 'msn_feed',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => 'Seleziona per inserirlo nel feed',
                'default_value' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 1,
        'position' => 'side',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;