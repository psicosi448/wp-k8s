/**
 * Created by matteobarale on 22/09/16.
 */
jQuery(document).ready(function($) {

    jQuery('select#rssFeed').on('change', function () {
        if (jQuery('#save_post_contaier').length > 0){
           jQuery('#save_post_contaier').remove();
        }


        jQuery('#rssLoader').show();

        //var url = jQuery(this).attr('value');
        //var format = jQuery(this).attr('data-cat');

        var url = jQuery(this).find(':selected').attr('value')
        var format = jQuery(this).find(':selected').attr('data-cat')

        var data = {
            'format': format,
            'action': 'bi_get_feed',
            'url_feed': url
        };

        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        jQuery.post(ajaxurl, data, function(response) {

            jQuery('#rssResult').append(response);
            jQuery('#rssLoader').hide();
            run_save();
            //alert('Got this from the server: ' + response);
        });
    })

});

function run_save(){
    jQuery('a.save_post').on('click', function (e) {
        //alert('test');
        e.preventDefault();
        var cat = jQuery( "select#rssFeed option:selected" ).attr('data-cat');

        var DivBlock = '<div id="divBlock" ' +
            'style="position: fixed;' +
            'top:0;bottom:0;' +
            'right:0;' +
            'left:0;' +
            'width:100%;' +
            'height:100%;' +
            'display:block;' +
            'background-color: rgba(0,0,0,0.8);' +
            'z-index:9999;' +
            'opacity:1;">' +
            '<div id="divBlockText" style="' +
            'position: absolute;' +
            'width: 400px;' +
            'height: 50px;' +
            'color: white;' +
            'font-size:18px;'+
            'top: 50%;' +
            'left: 50%;' +
            'margin-left: -50px; ' +
            'margin-top: -25px;">Creazione post in corso. Attendere Prego...</div>' +
            '</div>';


        jQuery('body').append(DivBlock);
        jQuery(this).addClass('disabled');
        //jQuery('#rssLoader').show();
        var Hash = jQuery(this).attr('data-transient');
        var data = {
            'action': 'save_feed_item',
            'hash': Hash,
            'categoria' : cat
        };
        var button = jQuery(this);
        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            data: data,
            dataType: "json",
            context: this,
            success: function (data) {
                console.log(data);
                if(data.stato == 'successo'){
                    jQuery(this).remove();
                    jQuery('#' + Hash).attr('href', data.url).show();
                    jQuery('#divBlockText').text(data.tipo).delay(2000).queue(function(){
                        jQuery(this).fadeOut();
                        jQuery('#divBlock').remove();
                    });
                }else{

                    jQuery('#divBlockText').text(data.tipo).delay(2000).queue(function(){
                        if(typeof data.url !== undefined){
                            jQuery(this).remove();
                            jQuery('#' + Hash).attr('href', data.url).show();
                        }
                        jQuery(this).fadeOut();
                        jQuery('#divBlock').remove();
                    });
                }
                //alert(data);
            },
        });
    })
}