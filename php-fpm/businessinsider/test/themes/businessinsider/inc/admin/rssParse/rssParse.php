<?php

add_action( 'admin_menu', 'bi_rss_menu' );
function bi_rss_menu() {
	$page = add_menu_page( 'Gestione Rss', 'Gestione Rss', 'edit_posts', 'bi-rss', 'bi_rss' );
	add_action( 'load-' . $page, 'bi_load_admin_js' );
}

function bi_rss() {
	if ( !current_user_can( 'edit_posts' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	echo '<div class="wrap">';
	echo '<div id="icon-tool"class="icon32"></div><h1>Gestione Rss</h1>';
	echo '<div class="description">Descrizione Funzionalità</div>';
	echo '<div class="wrap">';
	if(have_rows('gestione_feed_business_insider', 'option')):
		echo '<table class="widefat">';
		echo '<tbody>';
		echo '<tr>';
        echo '<td>Selezione il feed</td>';
        echo '<td>';
		echo '<select id="rssFeed">';
        echo '<option> -- Seleziona Feed -- </option>';
		while(have_rows('gestione_feed_business_insider', 'option')): the_row();
			$categoria = get_sub_field('categoria_associata');
			$label = get_sub_field('label');
			if($label){
				$etichetta = $label . ' - ' . $categoria;
			}else{
				$etichetta = $categoria;
			}

			echo '<option value="' . get_sub_field('url_feed') . '" data-cat="' . $categoria. '">' . $etichetta . '</option>';
		endwhile;
		echo '</select>';
		echo '</td>';
        echo '</tr>';
	    echo '</tbody>';
		echo '</table>';
	endif;
	echo '</div>';
	echo '<div id="rssResult" class="warp">
<div id="rssLoader" style="margin-top: 60px; text-align: center; width: 100%; display: none">
	<img src="'. admin_url() . '/images/spinner.gif" />
</div>
</div>';
	echo '</div>';
}

function bi_load_admin_js(){
	// Unfortunately we can't just enqueue our scripts here - it's too early. So register against the proper action hook to do it
	add_action( 'admin_enqueue_scripts', 'bi_rss_admin_js' );

}

function bi_rss_admin_js(){
	// Isn't it nice to use dependencies and the already registered core js files?
	wp_enqueue_script( 'bi-rss', get_stylesheet_directory_uri() . '/inc/admin/rssParse/js/rssParse.js', array( 'jquery-ui-core', 'jquery-ui-tabs' ) );
}


add_action( 'wp_ajax_bi_get_feed', 'bi_get_feed' );
function bi_get_feed() {
	if(isset($_POST['url_feed']) && !empty($_POST['url_feed'])){
		$xml = file_get_contents($_POST['url_feed']);
	}
	if(!isset($xml)) return;
	$xmlDoc = simplexml_load_string($xml, 'SimpleXMLElement',LIBXML_NOCDATA);
	$ns = $xmlDoc->getNamespaces(true);

	$channel_title = $xmlDoc->title;
	$channel_link = $xmlDoc->link;
	$channel_desc = $xmlDoc->subtitle;
//output elements from "<channel>"
	echo '<div id="save_post_contaier">';
	echo '<div class="warp">';
	echo("<p><a href='" . $channel_link
	     . "'>" . $channel_title . "</a>");
	echo("<br>");
	echo($channel_desc . "</p>");
	echo '</div>';
	echo '<div class="warp">';
	echo '<table id="save_post" class="wp-list-table widefat fixed striped">';
//get and output "<item>" elements
	echo '<tbody class="the-list">';
	echo '<thead>';
	echo '<tr>';

	echo '<th scope="col" id="title" class="manage-column column-title column-primary"><span>Titolo e descrizione</span></th>';
	echo '<th scope="col" id="" class=""><span>Tipologia</span></th>';
	echo '<th scope="col" id="" class=""><span>Azioni</span></th>';
	echo '</tr>';
	echo '</thead>';


	foreach ($xmlDoc->entry as $item){

		echo '<tr>';
		$item_title = $item->title;
		$item_link = $item->id;

		//media
		$media = $item->children($ns["media"]);
     //   var_dump($media->attributes());
     //   exit();
		$array_item = json_decode(json_encode($item));
        $array_item->link = (string)$item_link;
		if(!empty($media)) {
            $array_item->media[] = [ 'url' => (string) $media->attributes()->url];

		}
		//Summary Update
		$summary = $item->summary;
		if(isset($summary)){
			$array_item->summary = (string)$summary;
		}else{
			$array_item->summary = '';
		}
		if(isset($item->updated)){
			$array_item->updated = (string)$item->updated;
		}else{
			$array_item->updated = '';
		}

		//Creator
		$creator = $item->author;
		if(isset($creator->name)){
			$array_item->creator = (string)$creator->name;
		}else{
			$array_item->creator = '';
		}
/*
		echo "<pre>";
		print_r($array_item);
        echo "</pre>";
*/
		$id_transient = hash('md5', $item_link);
		if(get_transient( $id_transient )){
			delete_transient($id_transient);
		}
		set_transient($id_transient, json_encode($array_item), 60*60*12);


		echo "<td class=\"title column-title has-row-actions column-primary page-title\" style='width: 50%'><a href='" . $item_link
		     . "'>" . $item_title . "</a>";
		if(isset($summary) && !empty($summary)){
			echo  "<p><strong>Sommario:</strong>"  . (string)$summary . "</p>";
		}
		echo "</td>";

        $etichetta = "<label for=\"post-format-0\" class=\"post-format-icon post-format-".$_POST["format"]."\">".$_POST["format"]."</label>";

        /*
        $categorie = explode(', ', $item->category);
        if(in_array('Features', $categorie)){
            $etichetta = "<label for=\"post-format-gallery\" class=\"post-format-icon post-format-gallery\">Galleria</label>";
        }else{
            $etichetta = "<label for=\"post-format-0\" class=\"post-format-icon post-format-standard\">Articolo</label>";
        }
*/

		echo "<td class='check-column' style=\"width:1.2em%;\">" . $etichetta ."</td>";
		echo '<td style=\'width: 30%\'><a class="button button-primary save_post" href="#" data-transient="' . $id_transient . '">Crea Post</a>
			<a id="' . $id_transient . '" class="button button-secondary show_post" style="display:none;" href="#" target="_blank">Visualizza Post</a></td>';
		//echo '<td>' . var_dump($array_item) . '</td>';
		echo '</tr>';
	}
	echo '</tbody>';
	echo '</table>';
	echo '</div>';
	echo '</div>';
	wp_die(); // this is required to terminate immediately and return a proper response
}

add_action( 'wp_ajax_save_feed_item', 'save_feed_item' );
function save_feed_item() {
	//ini_set('max_execution_time', 300);
	if(isset($_POST['hash'])){
		$post_data = get_transient($_POST['hash']);
		if($post_data){
			$post_data = json_decode($post_data);
			$args = array(
				'meta_query' => array(
					array(
						'key' => 'permalink_originale',
						'value' => "$post_data->link",
						'compare' => '=',
					)
				)
			);
			$query = new WP_Query($args);
			if ( $query->have_posts() ) {
				while ($query->have_posts()) {
					$query->the_post();
					$array_errore = [
						'stato' => 'errore',
					    'tipo'  => 'Articolo esistente',
					    'url'   => admin_url('post.php?post='. get_the_ID() . '&action=edit')
					];
					wp_reset_query();
					echo json_encode($array_errore);
					wp_die();
				}
			}

			//echo $_POST['hash'];
			$slug = explode('/', $post_data->link);
			$slug = end($slug);

			//Recupero o creo l'utente

//print_r($post_data);
//			$mail_author = explode(' ', $post_data->author->name);
//			$mail_author = hash('md5', $mail_author[0]) . '@it.businessinsider.com';

            $mail_author = $post_data->author->email;

			$user = get_user_by( 'email', $mail_author );

            if(!$user){
                $user = get_user_by( 'login', $post_data->creator );
            }

			if($user){
				$user_id_import = $user->ID;
			}else {
				$userdata = array(
					'user_login' => $post_data->creator,
					'user_email' => $mail_author,
					'user_pass'  => time(),  // When creating an user, `user_pass` is expected.
					'role'       => 'bi_contributor'
				);

				$user_id_import = wp_insert_user( $userdata );

				//On success
				if ( is_wp_error( $user_id_import ) ) {
					$array_errore = [
						'stato' => 'errore',
						'tipo'  => $post_data->creator." - ".$mail_author." - ".$user_id_import->get_error_message(),
						'url'   => ''
					];
					wp_send_json( $array_errore );
				}
			}

			// print_r($post_data);
			$my_post = array(
				'post_title'    => wp_strip_all_tags( $post_data->title ),
				'post_content'  => $post_data->content,
				'post_excerpt'  => $post_data->summary,
				'post_name'     => $slug,
				'post_status'   => 'draft',
				'post_type'     => 'post',
//				'post_category' => array($_POST['categoria']),
			    'post_author'   => $user_id_import
			);

		//	print_r($my_post);
			$post_id = wp_insert_post( $my_post );
          //  print_r($post_id);

            set_post_format($post_id, $_POST['categoria']);
      //      if($_POST['categoria'] == "gallery")
//                $gallery_id = [];

          //  print_r($post_data);
			foreach($post_data->media as $key => $media){

				if (!isset($media_check)) {
					$media_check = [];
				}
		//		error_log(print_r($media, true));
				$attachment_id_photo = add_attachment_to_post($media->url, "", "", $post_id, $media->type);
				//Se è un post di tipo gallery mi registro gli id per valorizzare il custom field successivamente
				$media_check[] = ['url' => $media->url, 'id_photo' => $attachment_id_photo];
				//echo var_dump($attachment_id_photo);
			/*	if(isset($gallery_id)){
					$gallery_id[$key]['id'] = (int)$attachment_id_photo;
//					$gallery_id[$key]['descrizione'] = $media->description;
//                    $gallery_id[$key]['credit'] = $media->credit;

                }
			*/
				if($attachment_id_photo){
					if(!isset($array_image)) $array_image = [];
					$array_image[$attachment_id_photo] = $media->url;
					set_post_thumbnail( $post_id, $attachment_id_photo );
				}
			}

			//Aggiungo i post meta
			//Lo definisco come articlo imporato

			update_field('articolo_importato', 1, $post_id);

			//Se essite un array di immagini creo il custom field
	/*		if(isset($gallery_id)){
				update_field('field_57fe0e8c7ba8a', '', $post_id);
				foreach ($gallery_id as $key => $immagine){
					$gallery_order[$key] = "immagine";
					update_post_meta($post_id,  'media_gallery_' . $key . '_immagine', $immagine['id']);
					update_post_meta($post_id,  '_media_gallery_' . $key . '_immagine', 'field_57fe0eb97ba8b' );
					//add_sub_row( array('media_gallery', 1, 'didascalia'), '', $post_id );

					//update_post_meta($post_id,  'media_gallery_' . $key . '_didascalia', $immagine['descrizione'] );
					//update_post_meta($post_id,  '_media_gallery_' . $key . '_didascalia', 'field_57fe163ea87dd' );

                 //   update_post_meta($post_id,  'media_gallery_' . $key . '_didascalia', $immagine['credit'] );
                  //  update_post_meta($post_id,  '_media_gallery_' . $key . '_didascalia', 'field_57fe163ea87dd' );

                }
				update_post_meta($post_id, 'media_gallery', $gallery_order);

			}
*/
			//Salvo il permailink originale
			update_field('permalink_originale', $post_data->link, $post_id);
			update_field('dati_articolo_originale', json_encode($post_data), $post_id);

			preg_match_all('/<img[^>]+>/i', $post_data->content, $matches);

			foreach ($matches[0] as $match){
				preg_match_all( '@src="([^"]+)"@' ,  $match, $img );

				$img_url = $img[1][0];

				//creo i valori per creare una seconda tipologia di controllo sull'id
				$img_url2 = str_replace('http://', '', $img_url);
				$img_url2 = str_replace('https://', '', $img_url2);
				$img_url2 = explode('/', $img_url2);
				$img_url2 = $img_url2[2];
				if(strpos($img_url2, '-') !== false){
					$img_url2 = explode('-', $img_url2);
					$img_url2 = $img_url2[0];
				}
				$to_import = true;
				if (isset($media_check) && !empty($media_check)) {
					foreach ( $media_check as $key => $media_to_check ) {

						$media_to_check2 = str_replace('http://', '', $media_to_check['url']);
						$media_to_check2 = str_replace('https://', '', $media_to_check2);
						$media_to_check2 = explode('/', $media_to_check2);
						$media_to_check2 = $media_to_check2[2];
						if(strpos($media_to_check2, '-') !== false){
							$media_to_check2 = explode('-', $media_to_check2);
							$media_to_check2 = $media_to_check2[0];
						}
						$pos = strpos($img_url, $media_to_check['url']);
						if ($pos !== false) {
							$post_data->content = img_replace($media_to_check['id_photo'], $post_data->content, $img_url);
							unset($media_check[$key]);
							$to_import = false;
							break;
						}elseif ($img_url2 == $media_to_check2){
							$post_data->content = img_replace($media_to_check['id_photo'], $post_data->content, $img_url);
							unset($media_check[$key]);
							$to_import = false;
							break;
						}
					}
					if($to_import){
						$attachment_id = add_attachment_to_post($img_url, '', '', $post_id);
						if($attachment_id){
							$post_data->content = img_replace($attachment_id, $post_data->content, $img_url);
						}
					}
					if(empty($media_check)){
						unset($media_check);
					}
				}else{
					$attachment_id = add_attachment_to_post($img_url, '', '', $post_id);
					if($attachment_id){
						$post_data->content = img_replace($attachment_id, $post_data->content, $img_url);
					}
				}

			}
			$my_post = array(
				'ID'           => $post_id,
				'post_content' => $post_data->content,
			);


			wp_update_post( $my_post );
			if($post_id){
				$array_successo = [
					'stato' => 'successo',
					'tipo'  => 'Articolo creato con successo',
					'url'   => admin_url('post.php?post='. $post_id . '&action=edit')
				];

				echo json_encode($array_successo);
			}else{
				$array_errore = [
					'stato' => 'errore',
					'tipo'  => 'Errore nella Creazione Si prega di riporvare',
					//'url'   => admin_url('post.php?post='. $post_id . '&action=edit')
				];

				echo json_encode($array_errore);
			}
			wp_die();
		}
	}

}


function add_attachment_to_post($media_url, $media_description = '', $media_credit = '', $post_id, $media_type = ''){

	$file = $media_url;
	$filename = basename($file);

	if (strpos($filename, '.jpeg') === false && strpos($filename, '.png') === false && strpos($filename, '.jpg') === false) {
		if($media_type != ''){
			if($media_type == 'image/png'){
				$filename = $filename . '.jpg';
				$file_path = wp_upload_dir();
				$img = imagecreatefrompng($file);
				imagejpeg($img, $file_path['basedir'] . '/'. $filename, 90);
				imagedestroy($img);
				$file_to_import = file_get_contents($file_path['basedir'] . '/'. $filename);
			}elseif ($media_type == 'image/jpeg'){
				$file_to_import = file_get_contents($file);
				$filename = $filename . '.jpeg';
			}
		}else{
			$file_to_import = file_get_contents($file);
			$filename = $filename . '.jpeg';
		}
	}elseif(strpos($filename, '.png') !== false){
		$filename = str_replace('.png', '', $filename);
		$filename = $filename . '.jpeg';
		$file_path = wp_upload_dir();
		$img = imagecreatefrompng($file);
		imagejpeg($img, $file_path['basedir'] . '/'. $filename, 90);
		imagedestroy($img);
		$file_to_import = file_get_contents($file_path['basedir'] . '/'. $filename);
	}else{
		$file_to_import = file_get_contents($file);

	}

	$upload_file = wp_upload_bits($filename, null, $file_to_import);
	if (!$upload_file['error']) {
		$wp_filetype = wp_check_filetype($filename, null );
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_parent' => $post_id,
			'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
			'post_content' => $media_description,
			'post_status' => 'inherit'
		);
		$attachment_id_photo = wp_insert_attachment( $attachment, $upload_file['file'], $post_id );
		if (!is_wp_error($attachment_id_photo)) {
			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			$attachment_data = wp_generate_attachment_metadata( $attachment_id_photo, $upload_file['file'] );
			wp_update_attachment_metadata( $attachment_id_photo,  $attachment_data );
		}
		if(isset($file_path) && !empty($file_path)){
			if(file_exists($file_path['basedir'] . '/'. $filename)){
				unlink($file_path['basedir'] . '/'. $filename);
			}
		}
		return (int)$attachment_id_photo;
	}else{
		if(isset($file_path) && !empty($file_path)){
			if(file_exists($file_path['basedir'] . '/'. $filename)){
				unlink($file_path['basedir'] . '/'. $filename);
			}
		}
		return false;
	}


}

function img_replace($attachment_id, $content, $old_url){
	$new_url = wp_get_attachment_image_src( (int)$attachment_id, 'single' );
	if($new_url)
	    return str_replace($old_url,$new_url[0], $content);
	else
        return ($content);
}