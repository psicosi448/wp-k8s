<?php
function bi_post_custom_fields() {
	remove_meta_box( 'tagsdiv-brand' , 'post' , 'normal' );
}
add_action( 'admin_menu' , 'bi_post_custom_fields' );

function bi_admin_scripts( $hook ) {
	global $post;
	if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
		wp_register_script(
			'bi-backend',
			get_template_directory_uri() . '/assets/js/backend.js',
			array( 'jquery' )
		);
		wp_enqueue_script( 'bi-backend' );
	}
}
add_action( 'admin_enqueue_scripts', 'bi_admin_scripts', 10, 1 );



add_role('bi_contributor', 'BI Contributor', array(
	'read' => false, // True allows that capability
	'edit_posts' => false,
	'delete_posts' => false, // Use false to explicitly deny
));

function bi_modifica_firma( $post_id ) {
	if ( wp_is_post_revision( $post_id ) )
		return;

    if( empty($_POST['acf']) ) {
        return;
    }
  //  $firma_autore = get_field('field_58342dc6160fc', $post_id);

    $firma_autore = $_POST['acf']['field_58342dc6160fc'];

//var_dump($firma_autore);
//    exit;

	if($firma_autore){
		if($firma_autore != $_POST['post_author']){

			$my_post = array(
				'ID'           => $post_id,
				'post_author'   => $firma_autore,
			);
			wp_update_post( $my_post );;
		}
	}
}
add_action('acf/save_post', 'bi_modifica_firma', 20);


add_action('save_post', 'bi_parse_caption', 20);

function bi_parse_caption($post_id){
    if ( wp_is_post_revision( $post_id ) )
		return;

    $content = $_POST["post_content"];
    $content = stripslashes($content);
    if ( has_shortcode( $content, 'caption' ) ) {
        preg_match_all('/\[caption([^\]]+)\]([^\[]+)\[\/caption/', $content, $match);
        foreach($match[2] as $me){
            $caption = strip_tags($me);
            $xpath = new DOMXPath(@DOMDocument::loadHTML($me));
            $src = $xpath->evaluate("string(//img/@src)");
            $attach_id = bi_get_attachment_id($src);
            if($attach_id){

                $attach = get_post($attach_id);
                if($attach->post_excerpt == ""){
                    $arg["ID"]=$attach_id;
                    $arg["post_excerpt"]=$caption;
                    wp_update_post($arg);
                }


                //    wp_update_attachment_metadata($attach_id, array("caption" => $caption));
/*
                $nome_fotografo = get_post_meta($attach_id, 'be_photographer_name', true);
                if(trim($nome_fotografo) == "")
                    update_post_meta( $attach_id, 'be_photographer_name', $caption );
*/
            }else{
                // se ho inserito una immagine croppata ho una url differente, la cerco
                $array_img = explode("-", $src);
                // prendo l'ultimo elemento della stringa
                $last = end($array_img);
                $extension = strstr($last, '.');
                $orig_src = str_replace("-".$last, $extension, $src);
                $attach_id = bi_get_attachment_id($orig_src);

                if($attach_id) {

                    $attach = get_post($attach_id);
                    if($attach->post_excerpt == ""){
                        $arg["ID"]=$attach_id;
                        $arg["post_excerpt"]=$caption;
                        wp_update_post($arg);
                    }
                    //                    wp_update_attachment_metadata($attach_id, array("caption" => $caption));
                    /*
                    $nome_fotografo = get_post_meta($attach_id, 'be_photographer_name', true);


                    if (trim($nome_fotografo) == ""){
                        update_post_meta($attach_id, 'be_photographer_name', $caption);

                    }
*/
                }
             }
        }
    }
}


function bi_get_attachment_id( $url ) {
    $post_id = attachment_url_to_postid( $url );

    if ( ! $post_id ){
        $dir = wp_upload_dir();
        $path = $url;
        if ( 0 === strpos( $path, $dir['baseurl'] . '/' ) ) {
            $path = substr( $path, strlen( $dir['baseurl'] . '/' ) );
        }

        if ( preg_match( '/^(.*)(\-\d*x\d*)(\.\w{1,})/i', $path, $matches ) ){
            $url = $dir['baseurl'] . '/' . $matches[1] . $matches[3];
            $post_id = attachment_url_to_postid( $url );
        }
    }

    return (int) $post_id;
}