<?php
/** amp hooks */
add_action( 'amp_post_template_head', 'bi_amp_post_template_add_schemaorg_metadata' , 0);
function bi_amp_post_template_add_schemaorg_metadata( $amp_template ) {
    // rimuovo l'hook nativo per riscriverlo con i miei dati
    remove_action( 'amp_post_template_head', 'amp_post_template_add_schemaorg_metadata');

    $metadata = $amp_template->get( 'metadata' );
    if ( empty( $metadata ) ) {
        return;
    }
    $metadata["publisher"]["name"] = "Business Insider Italia";
    $metadata["publisher"]["logo"] = array(
        "@type" => "ImageObject",
        "url" => "https://it.businessinsider.com/wp-content/themes/businessinsider/assets/img/logo_smld.jpg",
        "width" => "368",
        "height" => "60");
    ?>
    <script type="application/ld+json"><?php echo json_encode( $metadata ); ?></script>
    <?php
}


//add_action('amp_post_template_css','bi_amp_add_custom_css', 11);
function bi_amp_add_custom_css() {
    $amp_css = "https://www.repstatic.it/cless-mobile/main/amp/2016-v1/css/businessinsider/detail.css";
  //  $amp_css = "https://www.repstatic.it/cless-mobile/main/amp/2016-v1/css/nazionale/detail.css";
    $result = wp_cache_get( 'amp_repstatic_css' );
    if ( false === $result ) {
        $response = wp_remote_get( $amp_css );
        if( is_array($response) ) {
            $header = $response['headers']; // array of http header lines
            if($header["content-type"] == "text/css"){
                $body = $response['body']; // use the content
                $result = $body;
            }
        }
        wp_cache_set( 'amp_repstatic_css', $result );
    }
    if($result)
        echo $result;
}