<?php
/**
 * Feed per newsstand
 * */

/* Add the feed. */
function newsstand_rss_init(){
    add_feed('newsstand', 'newsstand_rss');
}
add_action('init', 'newsstand_rss_init');

/* Filter the type, this hook wil set the correct HTTP header for Content-type. */
function newsstand_rss_content_type( $content_type, $type ) {
    if ( 'newsstand' === $type ) {
        return feed_content_type( 'rss2' );
    }
    return $content_type;
}
add_filter( 'feed_content_type', 'newsstand_rss_content_type', 10, 2 );

function newsstand_rss() {
    get_template_part('rss', 'newsstand');
}
