<?php
/**
 * Feed per msn
 * */


/* Add the feed. */
function msn_rss_init(){
    add_feed('msn_feed', 'msn_rss');
}
add_action('init', 'msn_rss_init');

/* Filter the type, this hook wil set the correct HTTP header for Content-type. */
function msn_rss_content_type( $content_type, $type ) {
    if ( 'msn_feed' === $type ) {
        return feed_content_type( 'rss2' );
    }
    return $content_type;
}
add_filter( 'feed_content_type', 'msn_rss_content_type', 10, 2 );

/* Show the RSS Feed on domain.com/?feed=my_custom_feed or domain.com/feed/my_custom_feed. */
function msn_rss() {
    get_template_part('rss', 'msn');
}
 ?>