<?php


function social_share($post, $class){

	$social_share ='<div class="' . $class . '">';
	$social_share .='	<div class="whatsapp-icon">';
	$social_share .='		<a href="whatsapp://send?text=' . rawurlencode($post->post_title . ' ' . get_permalink($post)). '">';
	$social_share .='			<img src="'. get_stylesheet_directory_uri() . '/assets/img/whatsapp-icon.svg" alt="">';
	$social_share .='		</a>';
	$social_share .='	</div>';
	$social_share .='	<div class="facebook-icon">';
	$social_share .='		<a href="https://www.facebook.com/sharer/sharer.php?u=' . rawurldecode(get_permalink($post)). '&t=' . urlencode($post->post_title) . '" target="_blank">';
	$social_share .='			<img src="'. get_stylesheet_directory_uri() . '/assets/img/facebook-icon.svg" alt="">';
	$social_share .='		</a>';
	$social_share .='	</div>';
	$social_share .='	<div class="linkedin-icon">';
	$social_share .='		<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=' . rawurlencode(get_permalink($post)). '&title=' . urlencode($post->post_title) . '">';
	$social_share .='			<img src="'. get_stylesheet_directory_uri() . '/assets/img/linkedin-icon.svg" alt="">';
	$social_share .='		</a>';
	$social_share .='	</div>';
	$social_share .='	<div class="twitter-icon">';
	$social_share .='		<a href="https://twitter.com/share?via=bi_italia&text=' . urlencode($post->post_title) . '&url=' . get_permalink($post). '">';
	$social_share .='			<img src="'. get_stylesheet_directory_uri() . '/assets/img/twitter-icon.svg" alt="">';
	$social_share .='		</a>';
	$social_share .='	</div>';
	$social_share .='	<div class="mail-icon">';
	$social_share .='		<a href="mailto:?subject=' . urlencode($post->post_title) . '&body=' . urlencode($post->post_excerpt) . ' -' . get_permalink($post). '">';
	$social_share .='			<img src="'. get_stylesheet_directory_uri() . '/assets/img/mail-icon.svg" alt="">';
	$social_share .='		</a>';
	$social_share .='	</div>';
	if(get_field("abilita_commenti")) {
		$social_share .='	<div class="comment-icon">';
		$social_share .='		<a href="'. get_permalink($post) .'#commenta">';
		$social_share .='			<img src="'. get_stylesheet_directory_uri() . '/assets/img/comment-icon.svg" alt="">';
		$social_share .='		</a>';
		$social_share .='	</div>';
	}
	$social_share .='</div>';

	return $social_share;
}

function formatImage($id, $size = 'single', $caption = '', $credit = '', $microdati = false, $lazy = false){
	$url = wp_get_attachment_image_src( $id, $size );
	if($url){
		if($microdati)
			$image =' <div class="media-image" itemprop="image"  itemscope itemtype="https://schema.org/ImageObject" >';
		else
			$image =' <div class="media-image" >';


		if($lazy){
			$image .= '<img
			src="'. get_stylesheet_directory_uri(). '/assets/img/pixel.jpg"
			data-original="' . $url[0] . '"
	        class="attachment-teaser_regular size-teaser_regular wp-post-image lazy ' . $add_class . '"
	        >';
			$img .= '<noscript><img src="'. $url[0] . '" width="' . $url[1] . '" heigh="'. $url[2] . '"></noscript>';

		} else{
			$image .='<img src="' . $url[0] . '">';
		}





		if($microdati){
			$image .='<meta itemprop="url" content="'.$url[0].'">';
			$image .='<meta itemprop="width" content="'.$url[1].'">';
			$image .='<meta itemprop="height" content="'.$url[2].'">';
		}
		$image .=' <dl>';
		$image .='<dt></dt>';
		$media =  get_post((int)$id);
		if($caption != ''):
			$image .='<dd>';
			$image .= $caption;
			$image .='</dd>';
		elseif(isset($media->post_excerpt) && !empty($media->post_excerpt)):
			$image .='<dd>';
			$image .= $media->post_excerpt;
			$image .='</dd>';
		endif;
		$image .=' <dt id="image-source"></dt>';
		$credit_name = get_post_meta($id, 'be_photographer_name', true);;
		$credit_url = get_post_meta($id, 'be_photographer_url', true);
		if($credit_name):
			$image .=' <dd>';
			if($credit_url):
				$image .=' <a href="' . $credit_url . '">';
			endif;
			$image .= $credit_name;
			if($credit_url):
				$image .='</a>';
			endif;
			$image .=' </dd>';
		endif;
		$image .=' 	</dl>';
		$image .=' 	</div>';
		return $image;
	}else{
		return false;
	}
}

add_filter( 'img_caption_shortcode', 'bi_caption_shortcode', 10, 3 );

function bi_caption_shortcode( $empty, $attr, $content ){

	if(isset($attr['id']) && !empty($attr['id'])){
		$id = explode('_', $attr['id']);
		$id = $id[1];
	}
	if(isset($attr['caption']) && !empty($attr['caption'])){
		$caption = $attr['caption'];
	}else{
		$caption = '';
	}
	$img = formatImage($id, 'large', $caption);
	if($img){
		return $img;
	}
}


function biPostTag($id){
	$posttags = get_the_tags($id);
	if($posttags):
		$tagsDiv = '<div class="article-tags">';
		foreach ($posttags as $tag):
            if($tag->slug != "native")
    			$tagsDiv .= '<a href="' . get_term_link($tag) . '" class="tag">' . $tag->name . '</a>';
		endforeach;
		$tagsDiv .= '</div>';
		return $tagsDiv;
	else:
		return false;
	endif;
}


function numeroVisualizzazioni($post, $class = ''){

    // controllo se ho il dato in memcache
    $numero_click = wp_cache_get( 'click_server_'.$post->ID );
    //$numero_click = false;
    if ( false === $numero_click ) {

        // recupero il dato dal click server e aggiorno il postmeta
        $chiave = get_option('options_chiave_click_server');
        $category = get_the_category($post->ID);
        #try{
        #    $urltocall = 'http://click.kataweb.it/ws/' . $chiave . '/content_' . $category[0]->slug . '_' . $post->ID;
        #    $query = file_get_contents($urltocall);
        #   // echo "<pre>".$urltocall."</pre>";

        #}catch (Exception $ex){
        #    error_log( $ex );
        #    return false;
        #}
        if(isset($query) && $query != false){
            $query = simplexml_load_string($query);
            if($query){
                $numero_click = (int)$query->sinceever0->content->attributes()->click;
                update_post_meta($post->ID, 'click_server', $numero_click);
                wp_cache_set( 'click_server_'.$post->ID, $numero_click );
            }
        }

    }
    // echo "<p>NUMERO CLICK: ".$numero_click."</p>";
    if(!$numero_click)
        return false;

    //    $numero_click = get_post_meta($post->ID, 'click_server', true);
    $min_set = get_field('soglia_click_server', 'option');
    if($min_set == false){
        $min_set = 10;
    }

	if($numero_click && $numero_click > $min_set ){

		switch ($numero_click):
			case $numero_click > 1 && $numero_click <= 399:
				$class_icon = 'cool';
				break;
			case $numero_click > 399 && $numero_click <= 999:
				$class_icon = 'warm';
				break;
			case $numero_click > 999 && $numero_click <= 9999:
				$class_icon = 'hot';
				break;
			case $numero_click > 9999:
				$class_icon = 'very-hot';
				break;
		endswitch;

		$count = '<span class="temp ' . $class. '">';
		$count .= '<span class="temp-color ' . $class_icon .'">';
		$count .= '<img alt="" src="' . get_stylesheet_directory_uri() . '/assets/img/' . $class_icon .'.svg" />';
		$count .= '</span>';
		$count .= '<span class="page-count">' . $numero_click . '</span>';
		$count .= '</span>';
		return $count;
	}else{
		return false;
	}

}

function orario($post_search, $type = '', $date = ''){

	if($date != ''){
		$post_date = $date;
	}else{
		$post_date = $post_search->post_date;
	}

	$time1 = new DateTime($post_date); // string date
	$time2 = new DateTime();
	$interval = date_diff($time1, $time2);

	if(($interval->d > 0 ) || ($interval->m > 0 )){

		$tempo = get_the_time( 'j/n/Y g:i:s A', $post_search );;

	}else{
		if( $interval->h > 0 ){
			if($interval->h > 1){
				$time_string = 'ORE';
			}else{
				$time_string = 'ORA';
			}
			$tempo = $interval->h . ' ' . $time_string;
		}else{
			if($interval->i > 0){
				if($interval->i > 1){
					$time_string = 'MINUTI';
				}else{
					$time_string = 'MINUTO';
				}
				$tempo = $interval->i . ' ' . $time_string;
			}else{
				if($interval->s > 1){
					$time_string = 'SECONDI';
				}else{
					$time_string = 'SECONDO';
				}
				$tempo = $interval->s . ' ' . $time_string;
			}
		}
	}


	if($type == 'strillo'){
		$orario = '<img alt="" class="icon icon-small hide-mobile-down" src="' .get_stylesheet_directory_uri() . '/assets/img/white-clock-icon.svg" >';
		$orario .= '<img alt="" class="icon icon-small hide-tablet-up" src="' . get_stylesheet_directory_uri() . '/assets/img/blue-clock-icon.svg" >';
		$orario .= '<span>' . $tempo .'</span>';
	}elseif($type == 'home'){
		$orario = '<img alt="" class="icon icon-small" src="' . get_stylesheet_directory_uri() . '/assets/img/white-clock-icon.svg" >';
		$orario .= ' ' . $tempo;
	}else{
		$orario = '<img alt="" class="icon icon-small" src="' . get_stylesheet_directory_uri() . '/assets/img/blue-clock-icon.svg" >';
		$orario .= ' ' . $tempo;
	}


	return $orario;
}


add_filter( 'the_content', 'bi_adv_content' );

/**
 * Parsing del content per aggiungere la pubblicità dopo x Paragrafi
 * @param $content
 *
 * @return mixed|string
 */
function bi_adv_content( $content ) {
	if(is_single()){
		global $post;

		if (get_post_format($post) == 'gallery'):
			return $content;
		else:


			// recupero la categoria del post
            $cats = wp_get_post_categories($post->ID);
		    $cat = $cats[0];
            $outplay_html = get_field("codice_theoutplay_".$cat, "option");
			$disable_outplay = get_field("disable_theoutplay", $post->ID);

			$content = do_shortcode($content);

			if(preg_match_all("/<iframe[^>]*src=[\"|']([^'\"]+)[\"|'][^>]*>/i", $content, $matches )) {

				foreach($matches[1] as $match) {
					$content = str_replace($match, str_replace('&', '&amp;', $match), $content);
				}
			}

			$dom = new DOMDocument();
			$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
			$new_content = '';
			$middle_check = 0;
			$bottom_check = 0;
			$outplay_check = 0;
			$advhook_check = 0;

			if($dom->getElementsByTagName('body')->item(0)->childNodes->length == 1):
				foreach($dom->getElementsByTagName('body')->item(0)->childNodes as $key => $element){

					$newnode = $element->parentNode->insertBefore( $dom->createElement( 'div' ), $element->nextSibling );
					$newnode->setAttribute( "id", "adv-Middle1-dev" );
					$script = $dom->createElement( 'script' );
					$script = $newnode->appendChild( $script );
					$script = $script->appendChild( $dom->createTextNode( "try { MNZ_RICH(\"Middle1-dev\"); } catch(e) { }" ) );
					$newnode = $element->parentNode->insertBefore( $dom->createElement( 'div' ), $script->nextSibling );
					$newnode->setAttribute( "id", "adv-Bottom" );
					$script = $dom->createElement( 'script' );
					$script = $newnode->appendChild( $script );
					$script->appendChild( $dom->createTextNode( "try { MNZ_RICH(\"Bottom\");} catch(e) { }" ) );
					break;
				}
			else:
				foreach($dom->getElementsByTagName('body')->item(0)->childNodes as $key => $element) {
                    if (get_class($element) != 'DOMElement') continue;
					if($element->tagName === null && ($element->nodeValue === null || trim($element->nodeValue) == '')) continue;
					if($element->getAttribute('id') == 'adv-Middle1-dev') continue;
					if($element->getAttribute('id') == 'adv-Bottom') continue;
					if($element->getAttribute('id') == 'outplay') continue;

					if ( $middle_check == 0 ) {
						$newnode = $element->parentNode->insertBefore( $dom->createElement( 'div' ), $element->nextSibling );
						$newnode->setAttribute( "id", "adv-Middle1-dev" );
						$script = $dom->createElement( 'script' );
						$script = $newnode->appendChild( $script );
						$script->appendChild( $dom->createTextNode( "try { MNZ_RICH(\"Middle1-dev\");} catch(e) { }" ) );
						$middle_check = 1;
					} elseif ( $bottom_check == 0 ) {

						$newnode = $element->parentNode->insertBefore( $dom->createElement( 'div' ), $element->nextSibling );
						$newnode->setAttribute( "id", "adv-Bottom" );
						$script = $dom->createElement( 'script' );
						$script = $newnode->appendChild( $script );
						$script->appendChild( $dom->createTextNode( "try { MNZ_RICH(\"Bottom\");} catch(e) { }" ) );
						$bottom_check = 1;
					}elseif ( $outplay_check == 0 ) {
					    if(!$disable_outplay){
						    if(!is_amp_endpoint()){
							    $newnode = $element->parentNode->insertBefore( $out = $dom->createElement( 'div' ), $element->nextSibling );
							    $newnode->setAttribute( "id", "outplay" );
							    appendHTML($out, "&nbsp;".$outplay_html);

						    }
					    }
						$outplay_check = 1;
					}elseif ( $advhook_check == 0 ) {
							if(!is_amp_endpoint()){
								$newnode = $element->parentNode->insertBefore( $out = $dom->createElement( 'div' ), $element->nextSibling );
								$newnode->setAttribute( "id", "advHook-x21" );
							}
						$advhook_check = 1;
					}

				}
			endif;
		endif;

		//$content = mb_convert_encoding($dom->saveHTML(), "UTF-8", "ASCII");
		//$content = mb_detect_encoding($content);
		$content = $dom->saveHTML();
	}

	return $content;
}

function appendHTML(DOMNode $parent, $source) {
	$tmpDoc = new DOMDocument();
	$tmpDoc->loadHTML($source);
	foreach ($tmpDoc->getElementsByTagName('body')->item(0)->childNodes as $node) {
		$node = $parent->ownerDocument->importNode($node, true);
		$parent->appendChild($node);
	}
}


function add_tag_adv_media($last = 0){
	global $middle_check, $bottom_check;
	if($middle_check == false){
		$middle_check = true;
		return '<div id="adv-Middle1-dev"><script>try { MNZ_RICH("Middle1-dev"); } catch(e) { }</script></div>';
	}elseif ($bottom_check == false){
		$bottom_check = true;
		return '<div id="adv-Bottom"><script>try { MNZ_RICH("Bottom"); } catch(e) { }</script></div>';
	}
	if($last == 1){
		$adv = '';
		if($middle_check == false){
			$adv .= '<div id="adv-Middle1-dev"><script>try { MNZ_RICH("Middle1-dev"); } catch(e) { }</script></div>';
		}
		if ($bottom_check == false){
			$adv .= '<div id="adv-Bottom"><script>try { MNZ_RICH("Bottom"); } catch(e) { }</script></div>';
		}
	}

}

function getElementByClass(&$parentNode, $tagName, $className, $offset = 0) {
	$response = false;

	$childNodeList = $parentNode->getElementsByTagName($tagName);
	$tagCount = 0;
	for ($i = 0; $i < $childNodeList->length; $i++) {
		$temp = $childNodeList->item($i);
		if (stripos($temp->getAttribute('class'), $className) !== false) {
			if ($tagCount == $offset) {
				$response = $temp;
				break;
			}

			$tagCount++;
		}
	}

	return $response;
}

function bi_native_single_class($classes){
	if(is_single()){
		global $post;
		if(get_field('native', $post->ID)){
			$classes[] = 'is-native';
			$classes[] = 'bigpage';
		}
	}
	return $classes;
}

add_filter('body_class', 'bi_native_single_class');


function search_result_setup_post($post_search){
	$id = explode(':',$post_search->str[0]);
	$id = end($id);
			$nascondi_autore = get_field("nascondi_autore", $id);
			$firma = strip_tags(get_field("firma", $id),"<b><i><a>");
	$post=get_post($id);
	setup_postdata($post);
	if($post){
		?>
		<div class="teaser teaser-small">
			<div class="result-list">
				<div class="teaser-image">
					<?php if(has_post_thumbnail($id)): ?>
						<a href="<?php echo get_permalink($post) ?>">
							<?php echo get_the_post_thumbnail($id, 'teaser_regular'); ?>
						</a>
					<?php endif;  ?>
				</div>
				<div class="teaser-content">
					<div class="teaser-text">
						<?php if(get_field('native', $id) == 1): ?>
							<div class="native-label">Contenuto sponsorizzato</div>
						<?php endif; ?>
						<a href="<?php echo get_permalink($post); //$post_search->str[7]; ?>">
							<h4 class="teaser-title small"><?php echo $post->post_title; ?></h4>
						</a>
					</div>
					<div class="article-author">
						<ul>
							<li><?php
								if($firma){
									echo '<span class="author-sign">'.$firma.'</span>';
								}
								if(!$nascondi_autore){
								?>
								<a href="<?php echo get_author_posts_url( $post->post_author ); ?>"><?php the_author(); ?></a>

							<?php

							}
							?></li>
							<?php
							/*
							?>
							<li>
								<?php echo orario($post, 'teaser', get_the_date( '', (int)$id )); ?>
							</li>
							<?php
							*/
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- FINE TEASER SMALL -->
		<?php

	}
}

function image_lazy($idpost, $size, $add_class = ''){
	$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $idpost ), $size );
	//$srcset = wp_get_attachment_image_srcset(get_post_thumbnail_id( $idpost )); srcset="'. $srcset . '
	$img = '<img
			src="'. get_stylesheet_directory_uri(). '/assets/img/pixel.jpg"
			data-original="' . $thumbnail_src[0] . '"
	        class="attachment-teaser_regular size-teaser_regular wp-post-image lazy ' . $add_class . '"
	        >';
	$img .= '<noscript><img src="'. $thumbnail_src[0] . '" width="' . $thumbnail_src[1] . '" heigh="'. $thumbnail_src[2] . '"></noscript>';
	return $img;
}
