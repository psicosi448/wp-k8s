<?php
/*
|--------------------------------------------------------------------------
| Funzioni Header
|--------------------------------------------------------------------------
|
| Di seguito tutte le funzioni per la gestione della header dei meta
| Style, etc.
*/

/**
 * Aggiungo gli stile al frontend
 */

function bi_enqueue_style() {

	wp_register_style(
		'business', //style name
		get_template_directory_uri() . '/assets/css/main.css', // Url css
		'', // Dipendenze
        '1.1', // 1.1 Versione
		'screen' // CSS media type
	);
	wp_enqueue_style( 'business' );
	wp_register_style(
		'business-print', //style name
		get_template_directory_uri() . '/assets/css/print.css', // Url css
		'', // Dipendenze
		'1.0', // Versione
		'print' // CSS media type
	);
	wp_enqueue_style( 'business-print' );
	wp_register_style(
		'custom', //style name
		get_template_directory_uri() . '/assets/css/custom.css', // Url css
		'', // Dipendenze
		'2.3', // Versione
		'screen' // CSS media type
	);
	wp_enqueue_style( 'custom' );
}
add_action( 'wp_enqueue_scripts', 'bi_enqueue_style' );

/**
 * Aggiungo porzioni di codice al wp_head
 */
function bi_wp_head() {
    if(is_home()){
        echo '<meta http-equiv="Refresh" content="300; URL=/?refresh_ce" />';
    }
	echo '<link href="'. get_template_directory_uri() . '/assets/img/favicon.ico" rel="shortcut icon" />' . PHP_EOL;
}

add_action( 'wp_head', 'bi_wp_head' );


function bi_meta_tag_single(){
	if(is_single()):
		global $post;
		//Date
		$publish_date = date('Y-m-d', strtotime($post->post_date)) . 'T'. date('h:i:s', strtotime($post->post_date)) ;
		$modified_date = date('Y-m-d', strtotime($post->post_modified)) . 'T'. date('h:i:s', strtotime($post->post_modified)) ;
		$format = get_post_format($post);
		if($format == 'video'){
			$name_time = 'og:published_time';
		}else{
			$name_time = 'article:published_time';
		}
		?>
		<meta name="<?php echo $name_time; ?>" content="<?php echo $publish_date ?>" />
		<?php
	endif;
}
add_action('wp_head', 'bi_meta_tag_single');

function webtrekk_nielsen(){
	$wt_section = '';
	if(is_home()):
		$pageType = 'homepage';
		$wt_section = 'homepage';
		$pageHref = get_bloginfo('url');
	elseif (is_archive()):
		$pageType = 'homepage_sezione';
		if(is_category()):
			$wt_section = get_queried_object()->slug;
			$pageHref = get_category_link(get_queried_object());
		elseif (is_author()):
			$pageHref = get_author_posts_url( get_queried_object_id());
		elseif (is_tag()):
			$pageHref = get_tag_link(get_queried_object_id());
		elseif (is_tax()):
			$pageHref = get_term_link(get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		endif;
	elseif (is_search()):
		$pageType = 'lista_ricerca';
		$pageHref = get_bloginfo('url') . '/?s=';
		//Todo Aggiungere categoria
		$pageSearchKeyword = get_search_query();
	elseif (is_single()):
		global $post;
		$format = get_post_format($post);
		if($format == 'gallery'){
			$pageType = 'dettaglio_foto';
		}elseif ($format == 'video'){
			$pageType = 'dettaglio_video';
		}else{
			$pageType = 'dettaglio_content';
			$contentElement = 'content';
			$contentGalleryCount =  "0";
			if(has_shortcode($post->post_content, 'tvzap-video')){
				$pattern = '\[(\[?)(tvzap\-video)(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)';
				preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches );
				$contentVideoCount = count($matches[0]);
			}
		}
		$pageHref = get_permalink($post);
		$category = get_the_category($post->ID);
		$wt_section = $category[0]->slug;
		$tags = wp_get_post_tags($post->ID, ['fields' => 'names']);
		if($tags && !empty($tags)){
			$varTags = implode(',', $tags);
		}
	endif; ?>

	<script type="text/javascript">
		var pageType = "<?php echo $pageType; ?>";
		var pageHref = "<?php echo $pageHref; ?>";
		<?php if(isset($pageSearchKeyword) && !empty($pageSearchKeyword)): ?>
		var pageSearchKeyword="<?php echo $pageSearchKeyword; ?>";<?php endif; ?>
		<?php if(isset($contentGalleryCount)): ?>
		var contentGalleryCount = <?php echo $contentGalleryCount; ?>;
		<?php endif; ?>
		<?php if(isset($contentVideoCount)): ?>
		var contentVideoCount = <?php echo $contentVideoCount; ?>;
		<?php endif; ?>
		<?php if(isset($varTags)): ?>
		var tags = "<?php echo $varTags; ?>";<?php endif; ?>
		<?php if($_GET["r"] != ""): ?>

		var wt_track_ref_override = 'redirect';<?php endif; ?>
		<?php
		if(is_singular()){
			$articolo_importato = get_field("articolo_importato");
			if($articolo_importato){ ?>

		var wt_content_language = 'en';
		<?php
			}
		}
		?>

		var __wt_section = '/business insider/<?php echo $wt_section; ?>';
	</script>

	<?php
}
add_action('wp_head', 'webtrekk_nielsen', 1);

function bi_ad_setup(){
	$ad_setup = '//oasjs.kataweb.it/adsetup_pcmp.js';
    ?>
    <esi:include src="http://www.repstatic.it/cless/common/stable/js/script/tlh/traffic_light_handler.html" />

    <?php
	if(is_tax("brand")){
		$queried_object = get_queried_object();
		$taxonomy = $queried_object->taxonomy;
		$term_id = $queried_object->term_id;
		$term_name = $queried_object->name;

		$ad_setup_native = get_field('tag_native', $taxonomy."_".$term_id);
		if(!$ad_setup_native)
			$ad_setup_native = $term_name;
		$metatag = urldecode($ad_setup_native.",bigpage");
		$ad_setup .= '?tags=' . rawurlencode($ad_setup_native).",bigpage";
	}

    if(is_tag()){
	    $queried_object = get_queried_object();
	    $term_slug = $queried_object->slug;
	    $ad_setup .= '?tags='.$term_slug;
    }

	if(is_single()){
		global $post;
		//Recupero tutti i termni di categoria e tassonomie
		$categorie = wp_get_post_categories($post->ID, ['fields' => 'names']);
		$tags = wp_get_post_tags($post->ID, ['fields' => 'names']);
		if(!empty($tags)){
			$categorie = array_merge($categorie, $tags);
		}
		$categorie = implode(',', array_map('rawurlencode', $categorie));



		//Verifico se il post è di tipo native
		if(is_native($post)){
			//Recupero i valori
			// brand
			$brand = get_field('field_57fb56fef8645', $post->ID);
			// tag native
            if($brand){
                $ad_setup_native = get_field('field_58bd2ddad232b', $brand);
                //$nome_prodotto = get_field('nome_prodotto', $post->ID);
                //$anno = get_field('anno', $post->ID);
                //$ad_setup_native = rawurlencode($brand . '-' . $nome_prodotto . $anno);
                //Aggiungo ai termini anche il native
                if($ad_setup_native)
                    $categorie = rawurlencode($ad_setup_native) . ',' . $categorie;

            }
		}else{
			//Recupero i valori del brand
			$brand = get_field('field_57fb56fef8645', $post->ID);

			if($brand){
				$ad_setup_native = get_field('field_58bd2ddad232b', $brand);
				$categorie = rawurlencode($ad_setup_native) . ',' . $categorie;
			}
		}/*
		if(is_native($post)){
			$categorie = $categorie . ',native';
		}*/
		if(get_post_format($post) == 'gallery'){
			$categorie = $categorie . ',photo';
		}
		$ad_setup .= '?tags=' . $categorie;
        $metatag = urldecode($categorie);
	}
	echo '<script async onload="try { kw_tlh_ready(); } catch(e) {}" onerror="try { asr_error() } catch(e) {}"   src="' . $ad_setup . '"></script>'."\n";
    echo '<meta name="tags" content="' . $metatag . '" />'."\n";
}
add_action('wp_head', 'bi_ad_setup', 1);


function bi_taboola_head(){ ?>
	<?php if(is_home()): ?>
	<script type="text/javascript">
		window._taboola = window._taboola || [];
		_taboola.push({home:'auto'});
		!function (e, f, u, i) {
			if (!document.getElementById(i)){
				e.async = 1;
				e.src = u;
				e.id = i;
				f.parentNode.insertBefore(e, f);
			}
		}(document.createElement('script'),
			document.getElementsByTagName('script')[0],
			'//cdn.taboola.com/libtrc/gruppoespresso-businessinsider/loader.js',
			'tb_loader_script');
	</script>
	<?php elseif (is_archive()): ?>
		<script type="text/javascript">
			window._taboola = window._taboola || [];
			_taboola.push({category:'auto'});
			!function (e, f, u, i) {
				if (!document.getElementById(i)){
					e.async = 1;
					e.src = u;
					e.id = i;
					f.parentNode.insertBefore(e, f);
				}
			}(document.createElement('script'),
				document.getElementsByTagName('script')[0],
				'//cdn.taboola.com/libtrc/gruppoespresso-businessinsider/loader.js',
				'tb_loader_script');
		</script>
	<?php elseif (is_single()): ?>
		<script type="text/javascript">
			window._taboola = window._taboola || [];
			_taboola.push({article:'auto'});
			!function (e, f, u, i) {
				if (!document.getElementById(i)){
					e.async = 1;
					e.src = u;
					e.id = i;
					f.parentNode.insertBefore(e, f);
				}
			}(document.createElement('script'),
				document.getElementsByTagName('script')[0],
				'//cdn.taboola.com/libtrc/gruppoespresso-businessinsider/loader.js',
				'tb_loader_script');
		</script>
	<?php endif; ?>
		<?php }
add_action('wp_head', 'bi_taboola_head');

/*
|--------------------------------------------------------------------------
| Funzioni Footer
|--------------------------------------------------------------------------
|
| Di seguito tutte le funzioni per la gestione del Footer ad esempio script.
*/

// Register Script
function bi_enqueue_script() {
	wp_register_script('modernizr', get_stylesheet_directory_uri() . '/assets/js/vendor/modernizr-2.6.2.min.js', '', '', true);
	wp_enqueue_script('modernizr');
	wp_register_script('respond', get_stylesheet_directory_uri() . '/assets/js/vendor/respond.min.js', '', '', true);
	wp_enqueue_script('respond');
	wp_register_script('main', get_stylesheet_directory_uri() . '/assets/js/main.js', '', '', true);
	wp_enqueue_script('main');
	wp_register_script('lazyload', get_stylesheet_directory_uri() . '/assets/js/lazyload.js', '', '1.9', true);
	wp_enqueue_script('lazyload');
	wp_register_script('custom', get_stylesheet_directory_uri() . '/assets/js/custom.js', '', '2.4', true);
	wp_enqueue_script('custom');
}
add_action( 'wp_enqueue_scripts', 'bi_enqueue_script' );

function bi_minify(){
	$base = get_bloginfo('url');
	$repstatic = strpos($base, "test.") !== FALSE ? "test.repstatic.it" : "www.repstatic.it";
	wp_register_script('minify', "//".$repstatic."/minify/sites/businessinsider/it/common.cache.php?name=js", '', '2.4', true);
	wp_enqueue_script('minify');
}
//add_action('wp_enqueue_scripts', 'bi_minify');

function bi_taboola_footer(){?>
	<script type="text/javascript">
		window._taboola = window._taboola || [];
		_taboola.push({flush: true});
	</script>
<?php }
//add_action('wp_footer', 'bi_taboola_footer');


function bi_repstatic(){
	$repstatic = file_get_contents('https://www.repstatic.it/cless/common/stable/include/wt/it.businessinsider.com/wt.html');
	echo $repstatic;
}
add_action('wp_footer', 'bi_repstatic');


function bi_js_clickserver() {
	if(is_single()):
		global $post;
		if(!is_attachment()):
			$category = get_the_category($post->ID);
			$category_slug = $category[0]->slug;
			$referrer = get_field('referrer_click_server', 'option');
			if($referrer): ?>
				<script type="text/javascript">
					var clickid = "content_<?php echo $category_slug . '_' .$post->ID; ?>";
					var clickurl = '//click.kataweb.it/void.js';
					var clickreferrer = '<?php echo $referrer; ?>/postview';
				</script>
				<?php
			endif; //Referrer
		endif;
	endif; //Is Single
}
add_action( 'wp_footer', 'bi_js_clickserver' );
