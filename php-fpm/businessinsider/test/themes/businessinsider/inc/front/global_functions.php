<?php
add_action('wp', 'bi_load_more');

function bi_load_more(){
    global $class;
	if(isset($_GET['action']) && $_GET['action'] == 'load-more') {
		if((isset($_GET['page']) && !empty($_GET['page'])) && (isset($_GET['number']) && !empty($_GET['number']))){

					$args = [
				'posts_per_page' => $_GET['number'],
			    'paged' => $_GET['page'],
                'post_status' => "publish",
                'suppress_filters' => true,
            ];

			if(isset($_GET['type']) && !empty($_GET['type'])){
				switch ($_GET['type']){
					case 'category':
						if(isset($_GET['id']) && !empty($_GET['id'])):
							$args['cat'] = (int)$_GET['id'];
						endif;
						break;
                    case 'brand':
					case 'widebrand':
                        if(isset($_GET['id']) && !empty($_GET['id'])):
                            $args['tax_query'] =  array(array(
                                'taxonomy' => "brand",
                                'field' => 'id',
                                'terms' => array($_GET['id']),
                                'operator' => 'IN') );
                        endif;
                        break;
					case 'tag':
						if(isset($_GET['id']) && !empty($_GET['id'])):
							$args['tag_id'] = (int)$_GET['id'];
						endif;
						break;
					case 'author':
						if(isset($_GET['id']) && !empty($_GET['id'])):
							$args['author'] = $_GET['id'];
						endif;
						break;
					case 'home':
						$riaperture = get_option('options_ri-aperture', 'option');
						if($riaperture){
							foreach ($riaperture as $post_exclude){
								$post__not_in[] = $post_exclude;
							}
						}
						$post_apertura = get_option('options_articolo_apertura', 'option');
						if($post_apertura){
							$post__not_in[] = $post_apertura;
						}
						if(!empty($post__not_in)){
							$args['post__not_in'] = $post__not_in;
						}
						$meta_query =
							[
								'relation' => 'AND',
							    [
								    'key' => 'native',
								    'value' => 1,
								    'compare' => '!=',
							    ]
							];
						break;
				}
			}
            if(($_GET['type'] != "brand") || ($_GET['type'] != "widebrand")){
                $args['meta_query'] =  [
                    [
                        'key' => 'articolo_importato',
                        'value' => 1,
                        'compare' => '!=',
                    ]
                ];

            }
			if(isset($meta_query) && !empty($meta_query)){
				$args['meta_query'] = array_merge($args['meta_query'], $meta_query);
			}
//print_r($args);
//echo "-------------";
			$more_post = new WP_Query($args);
         //   var_dump($more_post);
			if($more_post->have_posts()){
				ob_start();
				$c=0;
				while($more_post->have_posts()){
					$more_post->the_post();
					$c++;
					if(isset($_GET['type']) && !empty($_GET['type'])){
						switch ($_GET['type']){
							case 'category':
							    if($_GET['id'] == 6)
								    get_template_part('template/teaser', 'small_box');
							    else
							        get_template_part('template/teaser', 'small');
								break;
							case 'tag':
								$native = get_field('native');
								if($native)
								    get_template_part('template/teaser', 'small_native');
								else
									get_template_part('template/teaser', 'small');
								break;
                            case 'brand':
	                            get_template_part('template/teaser', 'small');
                                break;
							case 'widebrand':

								if($_GET['page']%2){
								    $class="left";
									if($c%2) $class="right";
								}else{
									$class="right";
									if($c%2) $class="left";
                                }
	                            get_template_part('template/teaser', 'wide');
                                break;
							case 'author':
								get_template_part('template/teaser', 'small');
								break;
							case 'home':
								get_template_part('template/teaser', 'small');
								break;
							case '404':
								get_template_part('template/content', 'small');
								break;

						}
					}
				}
				$result = ob_get_contents();
				ob_end_clean();
				if($more_post->max_num_pages <= $_GET['page']){
					$status = 'successnopost';
				}else{
					$status = 'success';
				}
				$array_return = [
					'status' => $status,
					'content' => $result
				];
			}else{
				$array_return = [
					'status' => 'nopost'
				];
			}
			wp_send_json($array_return);
		}
		wp_die();
	}elseif(isset($_GET['action']) && $_GET['action'] == 'load-more-search'){
		$search_results = bi_search_result(10, $_GET['page']);
		ob_start();
		foreach($search_results->result->doc as $post_search):
			search_result_setup_post($post_search);
		endforeach;
		$result = ob_get_contents();
		ob_end_clean();
		$array_return = [
			'status' => 'success',
			'content' => $result
		];
		wp_send_json($array_return);
	}
}


function bi_brand_init() {
	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       =>  'Brands',
		'singular_name'              =>  'Brand',
		'search_items'               =>  'Cerca Brand',
		'popular_items'              =>  'Brands Popolari',
		'all_items'                  =>  'Tutti i Brand',
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => 'Modifica Brand',
		'update_item'                => 'Aggiorna Brand',
		'add_new_item'               => 'Aggiungi un nuovo brand',
		'new_item_name'              => 'Nuovo Nome Brand',
		'separate_items_with_commas' => 'Separa Brand da virgola',
		'add_or_remove_items'        => 'Aggiungi o rimuovi Brands',
		'choose_from_most_used'      => 'Scegli tra i brand utilizzati',
		'not_found'                  => 'Nessun Brand trovato.',
		'menu_name'                  => 'Brand',
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'brand' ),
	);

	register_taxonomy( 'brand', 'post', $args );
}
add_action( 'after_setup_theme', 'bi_brand_init' );


function bi_feed_var( $query_vars ) {
	$query_vars[] = 'bifeed';
	return $query_vars;
}
add_action( 'query_vars', 'bi_feed_var' );

/**
 * Recognize feed and parameters and initialie actions accordingly.
 */
function bi_custom_feed( $query ) {
	if ( $query->is_feed() && $query->get( 'bifeed' ) == true) {
		$meta_query = array(
			array(
				'key'=>'articolo_importato',
				'value'=>'1',
				'compare'=>'=',
			),
		);
		$query->set('meta_query',$meta_query);
	}
	return $query;
}
add_filter( 'pre_get_posts', 'bi_custom_feed' );


function generate_click_server(){
	if(isset($_GET['click_server'])){
		$args = [
			'posts_per_page' => -1,
			'post_status' => 'publish'
		];

		$click_query = new WP_Query($args);

		if($click_query->have_posts()):
			while ($click_query->have_posts()): $click_query->the_post();
				$category = get_the_category(get_the_ID());
				$chiave = get_option('options_chiave_click_server');
				#try{
				#	$query = file_get_contents('http://click.kataweb.it/ws/' . $chiave . '/content_' . $category[0]->slug . '_' . get_the_ID() );
				#}catch (Exception $ex){
				#	error_log( $ex );
				#}

				if(isset($query) && $query != false){
					$query = simplexml_load_string($query);
					$click = (int)$query->sinceever0->content->attributes()->click;
					update_post_meta(get_the_ID(), 'click_server', $click);
				}
			endwhile;
		endif;
		exit;
	}
}

//add_action('template_redirect', 'generate_click_server');

function bi_home_pre_get_post($query){
	if(is_home() && !isset($_GET["number"])){
		//Imposto il numero di post per pagina in base a quanti blocchi
		$posts_per_page = 8;
		$numero_blocchi = get_option('options_numero_blocchi_aperture');
		if($numero_blocchi){
			$posts_per_page = (int)$posts_per_page * (int)$numero_blocchi;
		}
		$query->set('posts_per_page', $posts_per_page );

		//escludo i post gia mostrati
		$post__not_in = [];
		$riaperture = get_option('options_ri-aperture', 'option');
		if(!empty($riaperture) && (is_array($riaperture))){
			foreach ($riaperture as $post_exclude){
				$post__not_in[] = $post_exclude;
			}
		}
		$post_apertura = get_option('options_articolo_apertura', 'option');
		if($post_apertura){
			$post__not_in[] = $post_apertura;
		}
		if(!empty($post__not_in)){
			$query->set('post__not_in', $post__not_in );
		}
	}
	return $query;
}
add_filter('pre_get_posts', 'bi_home_pre_get_post');


function is_native($post_id){
	if(is_array($post_id)){
		$post_id = $post_id->ID;
	}
	$native = get_field('native', $post_id);
	if($native){
		return true;
	}else{
		return false;
	}
}


function xml_attribute($object, $attribute)
{
	if(isset($object[$attribute]))
		return (string) $object[$attribute];
}


add_action( 'pre_get_posts', 'bi_exclude_import_post' );
function bi_exclude_import_post( $query ) {
	if ( ! is_admin() && $query->is_main_query() && !is_single() && !is_page()){

		$meta_query = [
			'meta_query', [
				'relation' => 'AND',
				[
					'key' => 'articolo_importato',
					'value' => 1,
					'compare' => '!=',
				]
			]
		];
		if(is_home() || (is_category() && (!is_category("featured"))) || is_search() || is_author()){
			$meta_query['meta_query'][] = [
				'key' => 'native',
				'value' => 1,
				'compare' => '!=',
			];
		}

        if(is_category("featured")){
            $meta_query['meta_query'][] = [
                'key' => 'native',
                'value' => 1,
                'compare' => '==',
            ];
        }


        $query->set( 'meta_query', $meta_query);
	}
}


add_filter('template_redirect', 'bi_404_override' );
function bi_404_override() {
	global $wp_query;
	if($wp_query->is_404==true){
		if(isset($_GET['IR']) && $_GET['IR'] == 'T' ){
			global $wp;
            $source = $_GET['r'];
            if($source == "DE")
                $sourcesite = "http://www.businessinsider.de";
            else if($source == "UK")
                $sourcesite = "https://www.businessinsider.com/international";
            else
                $sourcesite = "http://www.businessinsider.com";

			$current_url = home_url( $wp->request );
			$redirect_url = str_replace(get_bloginfo('url'), $sourcesite, $current_url);
			$redirect_url = $redirect_url . '?IR=T';
			wp_redirect($redirect_url, 302);
			exit;
		}
	}
}


add_filter( 'body_class', 'bi_body_class');

function bi_body_class($classes){
	if(is_tag()){
		$key_class = array_search('tag', $classes);
		if($key_class){
			unset($classes[$key_class]);
		}
	}
	return $classes;
}


add_filter('aioseop_canonical_url','bi_change_canonical_url', 10, 1);
function bi_change_canonical_url( $url ){
    global $post;
    // verifico se è settato un link dall'import
    $permalink_originale = get_field('permalink_originale',  $post->ID);
    if( $permalink_originale != ""){
        return $permalink_originale;
    }
    return $url;
}


add_action( 'rss2_item', 'bi_add_abstract');
function bi_add_abstract(){
    global $post;
    if($_GET["type"] == "cust"){
	    echo "\t<dc:abstract>";
	    $content = $post->post_content;
	    $content = apply_filters("the_content", $content);
	    $content = trim(strip_tags($content));
	    $content = str_replace("\n", "", $content);
	    $content = substr($content, 0, 450)."...";

	    echo $content;
	    echo "</dc:abstract>\n";

	    if( has_post_thumbnail( $post->ID )) {
		    $thumb_ID = get_post_thumbnail_id($post->ID);
		    $details = wp_get_attachment_image_src($thumb_ID, 'large');
		    if (is_array($details)) {
			    echo "\t".'<enclosure url="'.$details[0].'" type="image/jpeg"/>'."\n";
		    }
	    }
    }
}

/**
 * AMP
 */
add_filter('do_rocket_lazyload', 'disable_lazyload_on_amp');
function disable_lazyload_on_amp( $status )
{
    if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) {
        return false;
    }
    return $status;
}


add_filter('do_rocket_lazyload', 'disable_lazyload_on_ia');
function disable_lazyload_on_ia( $status )
{
    if (is_feed("instant-articles")) {
        return false;
    }
    return $status;
}


add_action( 'amp_post_template_css', 'isa_amp_css_styles_date' );

function isa_amp_css_styles_date( $amp_template ) {
    ?>
    .amp-wp-article-header .amp-wp-posted-on.amp-wp-meta:first-of-type{
    text-align:right
    }
    <?php
}
