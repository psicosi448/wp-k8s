<?php
if(class_exists("WP_CLI")){
    function getfeed_command( $args ) {
        $import = $args[0];
        if(isset($import)){
            switch ($import){
                case "financiallounge":
                    $response = wp_remote_get( 'http://www.financialounge.com/rss_businessinsider.xml' );
                    if( is_array($response) ) {
                        $header = $response['headers']; // array of http header lines
                        $body = $response['body']; // use the content
                        $xml = simplexml_load_string($body, "SimpleXMLElement", LIBXML_NOCDATA);
                        $json = json_encode($xml);
                        $array = json_decode($json,TRUE);
                        $c=0;
                        foreach ($array["channel"]["item"] as $item){
                            WP_CLI::line( "Processo ".$item["title"] );
                            // controllo se il post non è stato importato (su check postmeta orig_link)
                            $args = array(
                                'meta_key'     => 'orig_link',
                                'meta_value'   => $item["link"],
                                'meta_compare' => '=',
                                'fields'       => 'all',
                                'post_status' => array('publish', 'future', 'draft'),
                            );
                            $posts = get_posts( $args );
                            if($posts){
                                // esiste, concludo il processo
                                WP_CLI::success("Import ".$import." parziale: concluso dopo ".$c." elementi");
                                return false;
                            }else{
                                // se non è stato importato creo il post
                                $my_post = array(
                                    'post_title' => wp_strip_all_tags($item["title"]),
                                    'post_content' => $item["description"],
                                    'post_status' => "publish",
                                    'post_date' => date_i18n("Y-m-d H:i:s", strtotime($item["pubDate"])),
                                );
                                $post_id = wp_insert_post($my_post);
                                WP_CLI::line( "Importo  ".$item["title"]."con ID ".$post_id);
                                if (!$post_id) {
                                    WP_CLI::error("errore nel creare il post con titolo ".$item["title"]);
                                }

                                add_post_meta($post_id, "orig_link", $item["link"]);
                                add_post_meta($post_id, "feed_import", 1);
                                wp_set_object_terms($post_id, "Financial Lounge", "brand");
                                wp_set_object_terms($post_id, "Featured", "category");

                                // native
                                update_field("field_57f7655617dc6", 1, $post_id);

                                // abilita link
                                update_field("field_57fb5b5800222", 1, $post_id);

                                // sflaggo articolo importato
                                update_field("field_57e93b846f4a6", 0, $post_id);

                                update_field("field_580f5db96ede7", "si", $post_id);

                                // recupero l'immagine e la creo localmente
                                $image = $item["enclosure"]["@attributes"]["url"];
                                WP_CLI::line( "Importo immagine ".$image );
                                // aggiungo il caption iStock
                                $src = media_sideload_image( $image,$post_id, "iStock", "src");
                                WP_CLI::line( "Importata immagine ".$src);
                                // associo il media come featured
                                $idimg = bi_cli_get_attachment_id($src);
                                set_post_thumbnail( $post_id, $idimg );

                            }

                        $c++;
                        }

                    }

                    break;
                default:
                    WP_CLI::error("import non previsto");
                    break;
            }
        }

        WP_CLI::success("Import ".$import." completo: concluso");
    }
    WP_CLI::add_command( 'getfeed', 'getfeed_command' );
}


if(!function_exists("bi_cli_get_attachment_id")) {

    function bi_cli_get_attachment_id($url, $image = false)
    {

        if (!is_string($url)) {
            return false;
        }

        $attachment_id = 0;

        $dir = wp_upload_dir();

        if (false !== strpos($url, $dir['baseurl'] . '/')) { // Is URL in uploads directory?

            $file = basename($url);

            $query_args = array(
                'post_type' => 'attachment',
                'post_status' => 'inherit',
                'fields' => 'ids',
                'meta_query' => array(
                    array(
                        'value' => $file,
                        'compare' => 'LIKE',
                        'key' => '_wp_attachment_metadata',
                    ),
                )
            );

            $query = new WP_Query($query_args);

            if ($query->have_posts()) {

                foreach ($query->posts as $post_id) {

                    $meta = wp_get_attachment_metadata($post_id);

                    $original_file = basename($meta['file']);
                    $cropped_image_files = wp_list_pluck($meta['sizes'], 'file');

                    if ($original_file === $file || in_array($file, $cropped_image_files)) {
                        $attachment_id = $post_id;
                        break;
                    }

                }

            }

        }

        return $attachment_id;
    }
}