<?php
/*
Plugin Name: SSO Authentication
Version: 2.2.20

latest changes:

- gestiti login sso non social 
- creazione utente transazionale (merge)
- send password change email false
- default role null
- creazione utente transazionale
- disabilitato wp_nonce per il logout
- aggiornamento sso_user
- wpId
- aggiunto forward

Plugin URI: http://www.kataweb.it
Description: Authenticate users to the Kataweb SSO system
Author: Emanuele Sabellico
Author URI: http://www.kataweb.it
*/

require_once(__DIR__.'/config.php');
require_once(ABSPATH . 'wp-admin/admin-functions.php');
require_once(ABSPATH . WPINC . '/registration.php');

if (! class_exists('SSOAuthenticationPlugin')) {

    class SSOAuthenticationPlugin {

        function SSOAuthenticationPlugin() {
       	
           add_action('wp_authenticate', array(&$this, 'authenticate'), 10, 2);
           $wp_head_priority = 1000;
           $site_conf = self::getSiteConf();

            if(!empty($site_conf) && !empty($site_conf["WP_HEAD_PRIORITY"])){
            	$wp_head_priority = $site_conf["WP_HEAD_PRIORITY"];
            }
            add_action('wp_head', array(&$this, 'head'), $wp_head_priority);
            add_action('init',  array(&$this, 'init'));
            add_action('pre_user_query', array(&$this, 'pre_user_query'));
            add_action('wp_ajax_kw_userdata', array(&$this, 'userdata'));
            add_action('wp_ajax_nopriv_kw_userdata', array(&$this, 'userdata'));
            add_action('manage_users_custom_column', array(&$this, 'manage_users_custom_column'), 15, 3);
            
            add_filter('auth_cookie_expiration', array(&$this, 'auth_cookie_expiration'));
            add_filter('logout_url', array(&$this, 'logout_url'));
            add_filter('login_redirect', array(&$this, 'login_redirect'), 1000000, 3 );
            add_filter('manage_users_columns', array(&$this, 'manage_users_columns'), 15, 1);
            add_filter('wp_mail', array(&$this, 'wp_mail'));
            add_filter('send_password_change_email', '__return_false');
        }

        function wp_mail($vars){
        	$to = $vars["to"];
        	
        	if(!empty($to)){
        		
        		$pos = strpos($to, "noreply.kataweb.it");
	        	
        		if ($pos !== false) {
	        		
        			$user = get_user_by('email', $to);
		        	
		        	if(!empty($user)){
			        	
		        		$realmail = get_user_meta($user->ID, "sso_email", true);
			        	
			        	if($realmail){
			        		$vars["to"] = $realmail;
			        	}
		        	}
        		}
        	}
        	
        	return $vars;
        }
        
        function pre_user_query($user_search){

		    global $wpdb;
		    if (!isset($_GET['s'])) return;

		    $user_search->query_from = "FROM {$wpdb->users} INNER JOIN {$wpdb->usermeta} ON {$wpdb->users}.ID={$wpdb->usermeta}.user_id";
		    $user_search->query_where = "WHERE 1=1 AND ({$wpdb->usermeta}.meta_value LIKE '%" . $_GET['s'] . "%' OR user_login LIKE '%" . $_GET['s'] . "%' ".
		    							"OR user_nicename LIKE '%" . $_GET['s'] . "%')";
        }
        
        function manage_users_custom_column($value, $column_name, $id){     	
        	
        	if( $column_name == 'sso_email' ) {
        		return get_user_meta($id, 'sso_email', true);
        	}
        	if( $column_name == 'sso_user' ) {
        		return get_user_meta($id, 'sso_user', true);
        	}        	
        }
        
        function manage_users_columns($defaults){
			
        	if(!empty($defaults['email'])){
        		unset($defaults['email']);
        	}
        	
        	$defaults1 = array();
        	foreach($defaults as $k => $v){
        		if($k == "role"){
        			$defaults1['sso_user'] = __('User SSO', 'user-column');
        			$defaults1['sso_email'] = __('Mail SSO', 'user-column'); 
        		}        		
        		$defaults1[$k] = $v;
        	}

        	return $defaults1;        	       	        	
        }
        
        function get_blogname($blog_id){
        	if(!function_exists("get_blog_details")) return "";
        	
        	$current_blog_details = get_blog_details( array( 'blog_id' => $blog_id ) );
        	$primary_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
        	
        	$primary_domain = $primary_blog_details->domain;
        	$primary_path = $primary_blog_details->path;
        	
        	$current_domain = $current_blog_details->domain;
        	$current_path = $current_blog_details->path;
        	
        	if($primary_path != $current_path){
        		$blogname = substr($current_path , strlen($primary_path), strlen($current_path) - strlen($primary_path) - 1);       		
        	} else if($primary_domain != $current_domain){
        		$blogname = substr($current_domain, 0, strlen($current_domain) - strlen($primary_domain) - 1);        		
        	}
        	
        	return $blogname;
        }

        function userdata(){
        	 $social = self::getSocial();
        	 header("Content-Type: application/json");
			 header("Expires: Tue, 31 Mar 1981 05:00:00 GMT");
			 header("Cache-Control: private, no-cache, no-store, must-revalidate, pre-check=0, post-check=0");

			 $blog_id = get_current_blog_id();
			 $blog_name = $this->get_blogname($blog_id);
			 
        	 $userdata = array(
        	 	"logoutUrl" => html_entity_decode($this->real_logout_url())
        	 	,"loggedIn" => is_user_logged_in()
        	 	,"isAdmin" => current_user_can('administrator')
        	 	,"wpId" => get_current_user_id()
        	 );
        	 if(isset($blog_name)){
        	 	$userdata["blogName"] = $blog_name;
        	 }
        	 if(!isset($social["WORDPRESS_LOGIN"]) || $social["WORDPRESS_LOGIN"]){
        	 	   $userdata["doLogin"] = true;
        	 }
        	 $userdata = apply_filters('kw_userdata_filter', $userdata);
			 echo json_encode($userdata);
        	 die();
        }

        function init(){
        	if(isset($_GET["action"]) && $_GET["action"]=="logout"){
        		$_GET["_wpnonce"] = wp_create_nonce("log-out");
        		$_REQUEST["_wpnonce"] = $_GET["_wpnonce"];
        	}
        	
        }
        
        function head(){

        	$social = self::getSocial();

            if(!empty($social)){
                $BASEURL = $social["BASEURL"];
                echo "<!-- SOCIAL START -->";
                if(!empty($social["JQUERY"])) echo '<script type="text/javascript" src="/social/common/js/jquery-'.$social["JQUERY"].'.min.js"></script>';

                if (isset($social["ECHO_FILE"]) && ($social["ECHO_FILE"] === true)) {
                    echo '<link rel="stylesheet" href="/social/common/css/social_shadowbox.css" type="text/css" media="all" />';
                    echo '<script type="text/javascript" src="/social/common/js/jquery.plugin/jquery.shadowbox.js"></script>';
                    echo '<script type="text/javascript" src="/social/common/js/gelesocial.cache.php"></script>';
                    echo '<script type="text/javascript" src="/social/common/js/gelesocial.wordpress.cache.php"></script>';
                    echo '<script type="text/javascript" src="'.$BASEURL.'/js/renderSocial.cache.php"></script>';
                }

                $userdata = array(
                "loginUrl" => html_entity_decode(wp_login_url())
                ,"blogUrl" => get_bloginfo('url')
                ,"templateUrl" => get_bloginfo('template_url')
                ,"cookieHash" => COOKIEHASH);
                if(!isset($social["WORDPRESS_LOGIN"]) || $social["WORDPRESS_LOGIN"]){
        	 	   $userdata["doLogin"] = true;
        	 	}
        	 	if(isset($social["USERDATA_NOTLOGGED"]) && $social["USERDATA_NOTLOGGED"]){
        	 		$userdata["userdataNotLogged"] = true;
        	 	}
        	 	

        	 	echo '<script type="text/javascript">
                if(!window.wordpress) window.wordpress = {};
                if(window.jQuery) window.jQuery.extend(true, window.wordpress, '.json_encode($userdata).');
                </script>';
                $this->print_fb();
                echo "<!-- SOCIAL END -->";
            }
        }

        function auth_cookie_expiration($time, $user=null, $remember=null){
        	$site_conf = self::getSiteConf();
            if(!empty($site_conf) && !empty($site_conf["COOKIE_EXPIRE"])) return $site_conf["COOKIE_EXPIRE"];
            else return $time;
        }

        function real_logout_url($redirect = '') {
	        $args = array( 'action' => 'logout' );
	        if ( !empty($redirect) ) {
		        $args['redirect_to'] = urlencode( $redirect );
	        }

	        $logout_url = add_query_arg($args, site_url('wp-login.php', 'login'));
	        $logout_url = wp_nonce_url( $logout_url, 'log-out' );

	        return $logout_url;
        }


        function print_fb() {

        	$social = self::getSocial();
            if(!empty($social)){
                if(!empty($social["FB_APP_ID"])) echo "<meta property=\"fb:app_id\" content=\"" .$social["FB_APP_ID"]."\" />\n";
            }
        }

        function logout_url($logout_url, $redirect=0){

        	$social = self::getSocial();
        	$site_conf = self::getSiteConf();
        	$backoffice_auth = self::isBackofficeAuth();
        	
            if(empty($redirect)) $logout_url = $this->real_logout_url(get_site_url());
            
            $is_super_admin = is_super_admin() && empty($site_conf["SSO_ADMIN_LOGIN"]);
            
            if($is_super_admin) return $logout_url;
            else {

	            if(!empty($social)){
	            	return self::getSocialBaseUrl()."/logout.php?fromGigya=true&bckurl=".urlencode(html_entity_decode($logout_url));
	            }
	            else {
	            	$op = "logout";
	            	if($backoffice_auth){
	            		$op = "bo_logout";
	            	}
	            	return self::getSSOUrl()."&op=".$op."&backurl=".urlencode(html_entity_decode($logout_url));
	            }

            }
        }

        function login_redirect($redirect_to, $request, $user){

        	return  self::getDefaultRedirectUrl();
        }

        function wp_set_auth_cookie_patched($user_id, $remember = false, $secure = '') {
			if ( $remember ) {
				$expiration = $expire = time() + apply_filters('auth_cookie_expiration', 1209600, $user_id, $remember);
			} else {
				$expiration = time() + apply_filters('auth_cookie_expiration', 172800, $user_id, $remember);
				$expire = 0;
			}

			if ( '' === $secure )
				$secure = is_ssl();

			$secure = apply_filters('secure_auth_cookie', $secure, $user_id);
			$secure_logged_in_cookie = apply_filters('secure_logged_in_cookie', false, $user_id, $secure);

			if ( $secure ) {
				$auth_cookie_name = SECURE_AUTH_COOKIE;
				$scheme = 'secure_auth';
			} else {
				$auth_cookie_name = AUTH_COOKIE;
				$scheme = 'auth';
			}

			$auth_cookie = wp_generate_auth_cookie($user_id, $expiration, $scheme);
			$logged_in_cookie = wp_generate_auth_cookie($user_id, $expiration, 'logged_in');

			do_action('set_auth_cookie', $auth_cookie, $expire, $expiration, $user_id, $scheme);
			do_action('set_logged_in_cookie', $logged_in_cookie, $expire, $expiration, $user_id, 'logged_in');

			setcookie($auth_cookie_name, $auth_cookie, $expire, PLUGINS_COOKIE_PATH, COOKIE_DOMAIN, $secure, true);
			setcookie($auth_cookie_name, $auth_cookie, $expire, ADMIN_COOKIE_PATH, COOKIE_DOMAIN, $secure, true);
			setcookie(LOGGED_IN_COOKIE, $logged_in_cookie, $expire, COOKIEPATH, COOKIE_DOMAIN, $secure_logged_in_cookie, false);
			if ( COOKIEPATH != SITECOOKIEPATH )
				setcookie(LOGGED_IN_COOKIE, $logged_in_cookie, $expire, SITECOOKIEPATH, COOKIE_DOMAIN, $secure_logged_in_cookie, true);
		 }


         function authenticate($username, $password) {

        	$social = self::getSocial();

            $is_admin = !empty($_GET["admin"]);
            $redirect_to = !empty($_GET["redirect_to"]);
            $reauth = !empty($_GET["reauth"]);
            $ssid = !empty($_GET["SSID"]);
            $forward = !empty($_GET["forward"]);
            $login_url = self::getLoginUrl();
            $backoffice_auth = self::isBackofficeAuth();
            $logged = is_user_logged_in();
            $wordpress_login = !isset($social) || !isset($social["WORDPRESS_LOGIN"]) || $social["WORDPRESS_LOGIN"] !== false;
            $social_login = !empty($social);

           	$redirect = self::getDefaultRedirectUrl();
           	if($redirect_to){
           		$redirect = $_GET["redirect_to"];
           	}

           	if($reauth && !$wordpress_login && stripos($redirect, '/wp-admin') !== false){
           		$redirect = get_bloginfo('url');
           	}

           	$default_behavior = false;
           	if(((!$logged || $reauth) && $is_admin) || $_SERVER['REQUEST_METHOD'] == "POST" || empty($login_url)){
           		//comportamento di wordpress
           		$default_behavior = true;
           	}


           	if(!$default_behavior){
           		
	            if($ssid){

					$op = "auth";
					if($backoffice_auth){
						$op = "bo_auth";
					}
	            	$ret = $this->checkAuthentication($op);
	            	
	            	if( $ret['statauth'] == 1 ) {
	            		//loggo l'utente
	            		$this->wp_set_auth_cookie_patched($ret['ID'], true);
						if($forward){
							$redirect .= strpos($redirect, "?") !== false ? "&" : "?";
							$redirect .= "SSID=".urlencode($_GET["SSID"]);
						}
	            	} else {
	            		
	            		die("Autenticazione non riuscita: dati mancanti");
	            		
	            	}


	            } else if(!$logged || $reauth){

	                	$backurl = $redirect;

	                	if($wordpress_login){
	                		$backurl = wp_login_url($backurl);
	                	}

			            if($social_login){
			            	$backurl = self::getSocialBaseUrl()."/loader.php?mClose=2&backUrl=".urlencode($backurl);
			            }
						
			            if($backoffice_auth){
			            	$sso = self::getSSOUrl();
			            	$redirect = $sso."&op=bo_login&backurl=".urlencode($backurl);
			            			 
			            } else {
                    		$redirect = $login_url."?backurl=".urlencode($backurl)."&optbackurl=".urlencode($backurl)."&ssoOnly=".(empty($social) ? "true" : "false");
			            }

	            }

	            wp_redirect($redirect);
	            die();
	            
           	}
         }

        function checkAuthentication($op = "auth"){

            $ret  = Array("statauth"=>2);
            $is_multisite = function_exists('is_multisite') && is_multisite();
            $is_subdomain_install = function_exists('is_subdomain_install') && is_subdomain_install();
            
            if(!empty($_GET['SSID'])){// K: verifica se esiste il ssid

                $SSID = $_GET['SSID'];//prendo il SSID
                //decripti il token
                $xmlResponse = $this->ssoDecryptSSID($SSID, $op);

                $sso_user = null;
                //parso la risposta per estrarre i dati
                if(strpos($xmlResponse, 'xml') !== false) {
                    $xml = simplexml_load_string($xmlResponse);

                    $firstname = (string)$xml->firstname;
                    $lastname = (string)$xml->lastname;
                    $nickname = (string)$xml->nickname;
                    $thumbnailURL = $xml->thumbnailURL;
                    $photoURL = $xml->photoURL;
                    $blogname = $xml->blogname;

                    if((empty($nickname) || stristr($nickname, "@")) && !empty($firstname) && !empty($lastname)){
                        $nickname = $firstname." ".$lastname;
                    }
                    
                    if(!empty($nickname)) $nickname = wp_specialchars($nickname);
                    if(empty($firstname)) $firstname = "";
                    if(empty($lastname)) $lastname = "";

                    $role = null;
                    $site_conf = self::getSiteConf();
                    if(!empty($site_conf) && !empty($site_conf["INITIAL_ROLE"])){
                    	$role =  $site_conf["INITIAL_ROLE"];
                    }    
                    $allow_wordpress_changes = false;             
                    if(!empty($site_conf) && !empty($site_conf["ALLOW_WORDPRESS_CHANGES"])){
                    	
                    	$allow_wordpress_changes = true;
                    
                    }
                    
                    $sso_user = $xml->user;
                    if(empty($sso_user) && !empty($xml->hashId)) $sso_user = $xml->hashId;
                    
                    $ret['user_login']  = (string) sha1($sso_user);
                    $ret['nickname'] = $nickname;
                    $ret['first_name']  = $firstname;
                    $ret['last_name']   = $lastname;
                    $ret['user_email']  = (string)$xml->email;
                    $ret['first_name']  = htmlentities(utf8_decode(html_entity_decode($ret['first_name'], ENT_QUOTES)));
                    $ret['last_name']   = htmlentities(utf8_decode(html_entity_decode($ret['last_name'], ENT_QUOTES)));
                    $ret['first_name']  = str_replace(" ", "&nbsp;", $ret['first_name']);
                    $ret['last_name']   = str_replace(" ", "&nbsp;", $ret['last_name']);
                    $ret['user_login']  = wp_specialchars(strtolower( $ret['user_login']));
                    $ret['user_email']  = wp_specialchars(strtolower( $ret['user_email']));
                    if(!$allow_wordpress_changes) $ret['display_name']  = $ret['nickname'];
                    $ret['user_nicename'] = $ret['user_login'];

                   	$encoded_nick = preg_replace('/\s+/', '-', strtolower($nickname));
                    $encoded_nick = rawurlencode($encoded_nick);

                    // test stessa email
                    //$ret['user_email'] = wp_specialchars(strtolower("emasab@gmail.com"));
                }

                if ($sso_user and !empty($ret['user_login']) and !empty($ret['user_email'])){//la userid decriptata dal cookie nn e vuota

                    $sso_user1 = wp_specialchars($sso_user);
                    $username = $ret['user_login'];
					$new_user = false;
					$email = $ret['user_email'];
					$ret['user_email'] = $ret['user_login']."@noreply.kataweb.it";
					
					global $wpdb;
					$user_lock = $wpdb->users."_login_".$username;
					try {
						
						$wpdb->query('START TRANSACTION');
						$wpdb->query($wpdb->prepare("SELECT GET_LOCK(%s, 30);", $user_lock));
	                    
	                    //creazione/update utente
	                    if(($ID = username_exists($username))){
	
	                    	$ret['ID'] = $ID;
	                    	$user_id = $ID;
	                    	update_user_meta($user_id, "sso_user", $sso_user1);
	                    	update_user_meta($user_id, "sso_email", $email);
	
	                    } else {
	
	                    	$new_user = true;
	                        $ret['user_pass'] = wp_generate_password();
	                        $ret['role'] = $role;
	                        if($allow_wordpress_changes) $ret['display_name']  = $ret['nickname'];
	                        
	                        $user_id = wp_insert_user($ret);
	                        add_user_meta($user_id, "sso_user", $sso_user1, true);
	                        add_user_meta($user_id, "sso_email", $email, false);
	                        $ret['ID'] = $user_id;
	
	                    }
	
	                    $ret['user_nicename'] = $encoded_nick."-".$ret['ID'];
	                    unset($ret['user_pass']);
	                    $user_id = wp_update_user($ret);
	
	                    if(!empty($thumbnailURL) && !empty($photoURL)){
	                    	update_user_meta($user_id, "sso_thumbnailURL", wp_specialchars($thumbnailURL));
	                    	update_user_meta($user_id, "sso_photoURL", wp_specialchars($photoURL));
	                    }
	
	                    $ret["statauth"]=1;
	
	                    //assegnazione ruolo utente
	                    if(!is_super_admin($username)){
	
	                        $blog_id = get_current_blog_id();
	
	                        if(empty($ID)){
	
		                        if(function_exists('add_user_to_blog')){
		                        	add_user_to_blog($blog_id, $user_id, $role);
		                        }
	
		                    } else {
	
		                    	$user_blogs = get_blogs_of_user( $ID );
		                    	$is_user = FALSE;
		                    	foreach ($user_blogs AS $user_blog) {
		                    		if($user_blog->userblog_id == $blog_id){
		                    			$is_user = TRUE;
		                    			break;
		                    		}
		                    	}
	
		                    	if(!$is_user && function_exists('add_user_to_blog')){
		                    		add_user_to_blog($blog_id, $ID, $role);
		                    	}
	
		                    }
		                    
		                    // TEST
		                    /*if(empty($blogname)){
		                    	$blogname = $nickname;	                    	
		                    }*/
		                    
		                    if($is_multisite){
		                    
			                    $create_blogs = isset($site_conf["CREATE_BLOGS"]) && $site_conf["CREATE_BLOGS"] == true;
			                    if($create_blogs && !empty($blogname)){
			                    	
			                    	$current_site_domain = get_current_site()->domain;
			                    	
			                    	$path = "/";
			                    	if(!$is_subdomain_install){
			                    		$domain = $current_site_domain;
			                    		$path .= $blogname;
			                    	} else {
				                    	$domain = $blogname.".".$current_site_domain;
			                    	}
			                    	
			                    	
			                    	if(!domain_exists($domain, $path)){
			                    		
			                    		$new_blog_id = wpmu_create_blog($domain, $path, $blogname, $user_id);
			                    		
			                    	}
			                    	
			                    }
		                    
		                    }
	                    }
	
	                    try {
		                    if($new_user) do_action("kw_user_register", $ret, $xml);
		                    else do_action("kw_profile_update", $ret, $xml);
	                    } catch (Exception $e){}
	                    
	                    $wpdb->query('COMMIT');
                    
					} catch(Exception $e){
						
						$wpdb->query('ROLLBACK');
						throw $e;
						
					}  
					$wpdb->query($wpdb->prepare("SELECT RELEASE_LOCK(%s);", $user_lock));
                    

                    return $ret;
                } else {
                    return $ret;
                }
            }
        }

        function ssoDecryptSSID($SSID, $op="auth"){

            $sso = self::getSSOUrl()."&op=".$op."&SSID=".$SSID."&f=xml&noGigyaLogin=true";
            if(function_exists("curl_init")){
            	
            	$ch = curl_init("$sso");
            	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            	$xmlResponse = curl_exec($ch);
            	curl_close($ch);
            
            } else {
            	
            	$xmlResponse = file_get_contents($sso);
            	
            }
            return $xmlResponse;
        }

        private static function checkSubdomain($url, $var){
			foreach($var as $k => $v){
				
				if($k[strlen($k) - 1] == "/"){
					$k = substr($k,0,strlen($k) - 1);
				}
				
				$q = preg_quote($k, '/');
		
				$protocol = '://';
				$protocol = preg_quote($protocol, '/');
				$protocol = preg_quote($protocol, '/');
		
				$q = preg_replace('/http(s)?'.$protocol.'/', '/^$0([a-z\-0-9]+?\.)?', $q);
				$q = preg_replace('/$/', '$/', $q);
				
				if(preg_match($q, $url)){
					return $v;
				}
			}
			return null;    	
        }
        
        private static function checkSubpath($url, $var){
        	foreach($var as $k => $v){
        		if($k[strlen($k) - 1] == "/"){

        			$q = substr($k,0,strlen($k) - 1);
        			$q = preg_quote($q, '/');
        
	        		$q = "/".$q."(\/[^\/]+)?$/";
	        
	        		if(preg_match($q, $url)){
	        			return $v;
	        		}
        		}
        	}
        	return null;
        }        
        
        public static function getSocial(){
        	$ret = null;
        	$site_name = get_bloginfo('url');
        	$is_subdomain_install = function_exists('is_subdomain_install') && is_subdomain_install();
        	
        	if(isset(SSOPluginConfig::$SOCIAL) && isset(SSOPluginConfig::$SOCIAL[$site_name])) $ret = SSOPluginConfig::$SOCIAL[$site_name];
        	else if(isset(SSOPluginConfig::$SOCIAL)){
	        	
        		if($is_subdomain_install){
        			$ret = self::checkSubdomain($site_name, SSOPluginConfig::$SOCIAL);
        		} else {
        			$ret = self::checkSubpath($site_name, SSOPluginConfig::$SOCIAL);			
        		}
        	}
        	
        	return $ret;
        } 

        public static function getSiteConf(){
        	$ret = null;
        	$site_name = get_bloginfo('url');
        	$is_subdomain_install = function_exists('is_subdomain_install') && is_subdomain_install();
        	
        	if(isset(SSOPluginConfig::$SITE_CONF) && isset(SSOPluginConfig::$SITE_CONF[$site_name])) $ret = SSOPluginConfig::$SITE_CONF[$site_name];
        	else if(isset(SSOPluginConfig::$SITE_CONF)){

        		if($is_subdomain_install){
        			$ret = self::checkSubdomain($site_name, SSOPluginConfig::$SITE_CONF);
        		} else {
        			$ret = self::checkSubpath($site_name, SSOPluginConfig::$SITE_CONF);
        		}
        	
        	}
        	return $ret;
        }        
        
        public static function getSocialBaseUrl(){
        	$social = self::getSocial();
        	if(empty($social)) return null;
        	else {
        		$blogurl = get_bloginfo('url');
        		$blogurl = array_slice(explode("/", $blogurl), 0, 3);
        		$host = implode("/", $blogurl);
        		return $host.$social["BASEURL"];
        	}
        }  

        public static function getSSOUrl(){
        	$site_conf = self::getSiteConf();
        	if(empty($site_conf)) return null;
        	return "https://".SSOPluginConfig::$SSO_DOMAIN."/login/SSO?service=".$site_conf["SSO_SERVICE"];
        } 

        public static function isBackofficeAuth(){
        	$site_conf = self::getSiteConf();
        	return !empty($site_conf) && !empty($site_conf["BACKOFFICE_AUTH"]) && $site_conf["BACKOFFICE_AUTH"];
       	}

        public static function getLoginUrl(){
        	$site_conf = self::getSiteConf();
        	if(empty($site_conf)) return null;
        	$ret = "https://".SSOPluginConfig::$SSO_DOMAIN."/registrazione/".$site_conf["SSO_FOLDER"]."/";
        	if(isset($_GET["action"]) && $_GET["action"]=="register" || 
        			isset($_GET["registration"]) && $_GET["registration"]=="disabled"){
        		$ret.="privato.jsp";
        	} else $ret.="login.jsp";
        	
        	if(!empty($site_conf["SSO_LOGIN_PATH"])){
        		$ret = "https://".SSOPluginConfig::$SSO_DOMAIN.$site_conf["SSO_LOGIN_PATH"];
        	}
        	return $ret;
        }      
        
        public static function getDefaultRedirectUrl(){
        	$ret = get_admin_url();
        	return $ret;
        }     

    }
}

$sso_authentication_plugin = new SSOAuthenticationPlugin();
?>
