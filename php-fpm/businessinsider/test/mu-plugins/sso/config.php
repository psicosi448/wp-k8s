<?php

// DEFINIRE IL TIPO DI AMBIENTE
//define('SSO_ENVIRONMENT', 'test');
define('SSO_ENVIRONMENT', 'prod');

/**
 Il plugin può essere utilizzato in tre modi:

 1) Login su Social e Wordpress. Bisogna inserire SSOPluginConfig::$SOCIAL[get_bloginfo('url')]
 2) Login solo su Wordpress. Non inserire (commentare) SSOPluginConfig::$SOCIAL[get_bloginfo('url')].
 3) Login solo su Social. Non inserire (commentare) RenderSocialConfig::$FORWARD_SSID
    ed impostare SSOPluginConfig::$SOCIAL[get_bloginfo('url')]["WORDPRESS_LOGIN"] = false

 n.b.: RenderSocialConfig::$FORWARD_SSID si trova nella conf.php del social
*/

class SSOPluginConfig {

    public static $SSO_DOMAIN = null; // da definire

    public static $SITE_CONF = [
               "https://it.businessinsider.com" => [
               		"SSO_SERVICE" => "business-insider"
					,"SSO_FOLDER" => "/"
					,"COOKIE_EXPIRE" => 31536000 // un anno
					,"WP_HEAD_PRIORITY" => 1000
                    ,"BACKOFFICE_AUTH" => true
                    ,"SSO_ADMIN_LOGIN" => true
                    ,"INITIAL_ROLE" => "editor"
               ]
    ];
   
}

if(isset($GLOBALS['KW_SSO_CONF']) && empty(SSOPluginConfig::$SITE_CONF["FORCE"])){
	SSOPluginConfig::$SITE_CONF = $GLOBALS['KW_SSO_CONF'];
}


if(SSO_ENVIRONMENT == "test"){


    SSOPluginConfig::$SSO_DOMAIN = "login.kataweb.it";

} else {


    SSOPluginConfig::$SSO_DOMAIN = "login.kataweb.it";

}
?>
