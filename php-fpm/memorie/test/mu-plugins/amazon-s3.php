<?php
add_filter('as3cf_object_meta', 'disable_s3_caching');
function disable_s3_caching($args) {

	if (array_key_exists('CacheControl', $args))
		unset($args['CacheControl']);

	if (array_key_exists('Expires', $args))
		unset($args['Expires']);

	return $args;
}
