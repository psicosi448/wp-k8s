<?php

// DEFINIRE IL TIPO DI AMBIENTE
// define('SSO_ENVIRONMENT', 'test');
define('SSO_ENVIRONMENT', 'prod');

/**
 Il plugin può essere utilizzato in tre modi:
 
 1) Login su Social e Wordpress. Bisogna inserire SSOPluginConfig::$SOCIAL[get_bloginfo('url')]
 2) Login solo su Wordpress. Non inserire (commentare) SSOPluginConfig::$SOCIAL[get_bloginfo('url')].
 3) Login solo su Social. Non inserire (commentare) RenderSocialConfig::$FORWARD_SSID
 ed impostare SSOPluginConfig::$SOCIAL[get_bloginfo('url')]["WORDPRESS_LOGIN"] = false
 
 n.b.: RenderSocialConfig::$FORWARD_SSID si trova nella conf.php del social
 */

class SSOPluginConfig {
    
    public static $SSO_DOMAIN = null; // da definire
    
    public static $SITE_CONF = array(
        
        "https://test-www.memorie.it" => array(
            "SSO_SERVICE" => "memorie"
            ,"SSO_FOLDER" => "memorie"
            ,"COOKIE_EXPIRE" => 31536000 // un anno
            ,"WP_HEAD_PRIORITY" => 1000
            ,"INITIAL_ROLE" => "subscriber"
            ,"FUNNEL_LOGIN_URL" => "https://test-www.memorie.it/area-personale/funnel/login.jsp"
        ),
        "https://www.memorie.it" => array(
            "SSO_SERVICE" => "memorie"
            ,"SSO_FOLDER" => "memorie"
            ,"COOKIE_EXPIRE" => 31536000 // un anno
            ,"WP_HEAD_PRIORITY" => 1000
            ,"INITIAL_ROLE" => "subscriber"
            ,"FUNNEL_LOGIN_URL" => "https://www.memorie.it/area-personale/funnel/login.jsp"
        )
    );
    
    public static $SOCIAL = array(
        "https://test-www.memorie.it" => array(
            "BASEURL" => "/social/sites/memorie/www"
            ,"FB_APP_ID" => "790583588133491"
            ,"FB_APP_DOMAIN" => 'test-memorie'
            ,"ECHO_FILE" => false
            //,"JQUERY" => "1.5.2"
            //,"WORDPRESS_LOGIN" => false
        ),
        "https://www.memorie.it" => array(
            "BASEURL" => "/social/sites/memorie/www"
            ,"FB_APP_ID" => "790583588133491"
            ,"FB_APP_DOMAIN" => 'memorie.it'
            ,"ECHO_FILE" => false
            //,"JQUERY" => "1.5.2"
            //,"WORDPRESS_LOGIN" => false
        )
    );
}


if(SSO_ENVIRONMENT == "test"){
    
    
    SSOPluginConfig::$SSO_DOMAIN = "test-login.kataweb.it";
    
} else {
    
    
    SSOPluginConfig::$SSO_DOMAIN = "login.kataweb.it";
    
}
?>
