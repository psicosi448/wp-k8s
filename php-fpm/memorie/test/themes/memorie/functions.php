<?php
require get_template_directory() . '/inc/define.php';
require get_template_directory() . '/inc/cpt.php';
require get_template_directory() . '/inc/acf.php';
require get_template_directory() . '/inc/actions.php';
require get_template_directory() . '/inc/comment.php';
require get_template_directory() . '/inc/solr.php';

/**
 * Enqueue scripts and styles.
 */
function memorie_scripts() {

	wp_enqueue_style( 'swiper', get_template_directory_uri()."/css/swiper.min.css" );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri()."/css/bootstrap.min.css" );
    wp_enqueue_style( 'template-style', get_template_directory_uri()."/css/style.css" );
    wp_enqueue_style( 'jquery-bootstrap', get_template_directory_uri()."/css/jquery.smartmenus.bootstrap.css" );

    wp_enqueue_style( 'memorie-font', "https://fonts.googleapis.com/css?family=Open+Sans|PT+Serif|Parisienne&display=swap" );
    //wp_enqueue_style( 'memorie-awesome', "https://use.fontawesome.com/releases/v5.6.3/css/all.css" );

	wp_enqueue_style( 'main-style', get_stylesheet_uri() );




//    wp_enqueue_script( 'memorie-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', array(), false, true );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array("jquery"), false, true );
    wp_enqueue_script( 'sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array("jquery"), 1, true );
    wp_enqueue_script( 'search', get_template_directory_uri() . '/js/search.js', array("jquery"), 1, true );
    wp_enqueue_script( 'sticky-header', get_template_directory_uri() . '/js/sticky-header.js',array("jquery"), 1, true );
    wp_enqueue_script( 'smartmenus', get_template_directory_uri() . '/js/jquery.smartmenus.min.js', array("jquery"), 1, true );
    wp_enqueue_script( 'smartmenus-bootstrap', get_template_directory_uri() . '/js/jquery.smartmenus.bootstrap.min.js', array("jquery", "bootstrap"), false, true );

   // if(is_singular()){
	    wp_enqueue_script( 'swiper-slider',  get_template_directory_uri() .'/js/swiper.min.js', array("jquery"), false, true );
   // }

	wp_enqueue_script( 'gelesocial-cache', home_url().'/social/common/js/gelesocial.cache.php', array("jquery"), 1, true );
	wp_enqueue_script( 'gelesocial-wordpress', home_url().'/social/common/js/gelesocial.wordpress.cache.php', array("jquery"), 12, true );
	wp_enqueue_script( 'gs-plugins', '//www.repstatic.it/cless/common/stable/js/script/gs/gs-plugins.js', array("jquery"), 1, true );
	wp_enqueue_script( 'rendersocial', home_url().'/social/sites/memorie/www/js/renderSocial.cache.php', array("jquery"), 27, true );

}
add_action( 'wp_enqueue_scripts', 'memorie_scripts' );



function memorie_post_thumbnails() {
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'menus' );



}
add_action( 'after_setup_theme', 'memorie_post_thumbnails' );

function memorie_new_menu() {
    register_nav_menus(
        array(
            'header' => __( 'Main menu' )
        )
    );
}
add_action( 'init', 'memorie_new_menu' );


add_image_size( 'defunto-small', 360, 400, true );
add_image_size( 'defunto-square', 325, 325, true );
add_image_size( 'defunto-comment', 400, 350, true );
