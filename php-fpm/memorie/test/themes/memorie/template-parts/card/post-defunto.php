<?php
global $post;

$images =  get_field("fotografie");
//print_r($images);
if($images && $images[0]["immagine"]){
	$image = $images[0]["immagine"]["sizes"]["defunto-square"];
	//$image = get_the_post_thumbnail_url($post, "defunto-small");
}else{
	$image = PLACEHOLDER_DEFUNTO;
}



$anni = get_field("anni");
$luogo_di_nascita = get_field("luogo_di_nascita");
if(!get_field("nato-italia"))
	$luogo_di_nascita = get_field("stato_di_nascita");

$data_di_nascita = get_field("data_di_nascita");

$luogo_di_morte = get_field("luogo_di_morte");
if(!get_field("morto-italia"))
	$luogo_di_morte = get_field("stato_di_morte");

$data_di_morte = get_field("data_di_morte");
?>
<div class="memorie-block"><!-- memorie block -->
    <div class="well-default">
        <div class="row">
            <div class="col-md-4 memorie-img">
                <a href="<?php the_permalink(); ?>" title="memorie Image">
                    <img src="<?php echo $image; ?>" class="img-responsive img-circle" alt="<?php echo esc_attr($post->post_title); ?>">
                </a>
            </div>
            <div class="col-md-8">
                <h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr($post->post_title); ?>"><?php echo $post->post_title; ?></a></h2>
                <div class="memorie-meta">

					<?php // if($anni  || $luogo_di_nascita || $luogo_di_morte || $data_di_morte || $data_di_nascita) echo " ( "; ?>
					<?php  if($anni){ ?><span class="memorie-meta--age"><?php echo $anni; ?> anni</span><?php }
					if($anni && ($luogo_di_nascita || $luogo_di_morte || $data_di_morte || $data_di_nascita)) echo ", ";
					if($luogo_di_nascita){ ?><span id="memorie-meta--location-birth"><?php echo $luogo_di_nascita["label"] ?></span><?php }
					if($data_di_nascita){ ?><span id="memorie-meta--date-birth"><?php echo $data_di_nascita ?></span><?php }
					if(($luogo_di_nascita  || $data_di_nascita) && ($luogo_di_morte || $data_di_morte)) echo " - ";
					if($luogo_di_morte){ ?><span id="memorie-meta--location-last"><?php echo $luogo_di_morte["label"] ?></span><?php }
					if($data_di_morte){ ?><span id="memorie-meta--date-last"><?php echo $data_di_morte ?></span><?php }  ?>
					<?php // if($anni  || $luogo_di_nascita || $luogo_di_morte || $data_di_morte || $data_di_nascita) echo " ) "; ?>

                </div>
	            <p>
				<?php
//				$sommario =  trim(get_field('sommario_necrologio')." ".get_field('testo_necrologio'));
				$sommario =  trim(get_field('sommario_necrologio'));
				if($sommario == "")
					$sommario = trim(get_field('testo_necrologio'));

				$excerpt_length = 30;
				$excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;<a href="'.get_permalink().'">leggi di più</a>]' );
				echo wp_trim_words( $sommario, $excerpt_length, $excerpt_more );

				//the_excerpt();
				?>
	            </p>
                <a href="<?php the_permalink(); ?>" class="btn btn-primary">lascia una tua memoria</a> </div>
        </div>
    </div>
</div>

