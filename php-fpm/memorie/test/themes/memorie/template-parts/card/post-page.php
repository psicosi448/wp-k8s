<?php
global $post;
?>
<div class="memorie-block"><!-- memorie block -->
    <div class="well-default">
        <div class="row">
            <div class="col-md-12">
                <h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr($post->post_title); ?>"><?php the_title(); ?></a></h2>
                <?php the_excerpt(); ?>
                <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e("Read more"); ?></a> </div>
        </div>
    </div>
</div>

