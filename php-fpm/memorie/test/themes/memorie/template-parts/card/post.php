<?php
global $post;

$image = get_the_post_thumbnail_url($post,'defunto-square');

?>
<div class="memorie-block"><!-- memorie block -->
    <div class="well-default">
        <div class="row">
            <div class="col-md-3 memorie-img">
                <?php
                if($image) {
	                ?>
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo $image; ?>" class="img-responsive img-circle"
                             alt="<?php echo esc_attr( $post->post_title ); ?>">
                    </a>
	                <?php
                }
                ?>
            </div>
            <div class="col-md-9">
                <h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr($post->post_title); ?>"><?php the_title(); ?></a></h2>
                <?php the_excerpt(); ?>
                <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e("Read more"); ?></a> </div>
        </div>
    </div>
</div>

