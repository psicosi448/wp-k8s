<div class="row">

	<?php
	global $allpages;
	$total = $allpages;

	$request = remove_query_arg( 'pagesearch' );

	$home_root = parse_url( home_url() );
	$home_root = ( isset( $home_root['path'] ) ) ? $home_root['path'] : '';
	$home_root = preg_quote( $home_root, '|' );

	$request = preg_replace( '|^' . $home_root . '|i', '', $request );
	$request = preg_replace( '|^/+|', '', $request );

	$qs_regex = '|\?.*?$|';
	preg_match( $qs_regex, $request, $qs_match );

	if ( ! empty( $qs_match[0] ) ) {
		$query_string = $qs_match[0];
		$request      = preg_replace( $qs_regex, '', $request );
	} else {
		$query_string = '';
	}

	$pages = paginate_links( array(
		'base' => home_url($query_string."&pagesearch=%#%"),
		'format' => '?pagesearch=%#%',
		'current' => max( 1, $_GET['pagesearch'] ),
		'total' => $total,
		'type'  => 'array',
		'prev_text'          => __( '<span aria-hidden="true"><</span>' ),
		'next_text'          => __( '<span aria-hidden="true">></span>' ),
	) );
	if( is_array( $pages ) ) {
		echo '<div class="col-md-12 st-pagination"><ul class="pagination">';
		foreach ( $pages as $page ) {
			echo "<li>$page</li>";
		}
		echo '</ul></div>';
	}
	?>
</div><?php
