<div class="well-default memorie-search">
    <div class="row">
        <div class="col-md-12">
            <form class="memorie-form row">
                <div class="col-md-12 _flex-align">
                    <input type="text" name="s" class="form-control" placeholder="Nome Cognome" value="<?php echo sanitize_text_field($_GET["s"]); ?>" >
                    <button type="submit" class="btn btn-primary _icon-search">
                        <svg>
                            <use xlink:href="#icon-search"></use>
                            <symbol id="icon-search" viewBox="0 0 16 16">
                                <g fill="none" fill-rule="evenodd" stroke="#ffffff">
                                    <path stroke-linecap="round" stroke-width="2" d="M15 15l-4.053-4.053" />
                                    <circle cx="6.5" cy="6.5" r="5.5" stroke-width="2" />
                                </g>
                            </symbol>
                        </svg>
                    </button>
                </div>
                <?php

                ?>
                <div class="col-md-12 text-right _adv-search">
                    <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#advancedSearch" aria-expanded="false" aria-controls="advancedSearch">
                        <i class="_custom-arrow _custom-arrow-down"></i>
                        Raffina la ricerca
                    </button>
                </div>
                <div class="collapse" id="advancedSearch">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="xxx">Luogo di nascita</label>
                            <input name="luogo_di_nascita" value="<?php echo sanitize_text_field($_GET["luogo_di_nascita"]); ?>" type="text" class="form-control" id="xxx" placeholder="Luogo di nascita">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="yyy">Luogo di morte</label>
                            <input name="luogo_di_morte"  value="<?php echo sanitize_text_field($_GET["luogo_di_morte"]); ?>" type="text" class="form-control" id="yyy" placeholder="Luogo di morte">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="aaa">Data di nascita</label>
                            <input name="data_di_nascita" type="date"  value="<?php echo sanitize_text_field($_GET["data_di_nascita"]); ?>" class="form-control" id="aaa" placeholder="Data di nascita">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="bbb">Data di morte</label>
                            <input name="data_di_morte" type="date" value="<?php echo sanitize_text_field($_GET["data_di_morte"]); ?>" class="form-control" id="bbb" placeholder="Data di morte">
                        </div>
                    </div>
                    <div class="form-row text-center">
                        <button type="submit" class="btn btn-primary">Cerca</button>
                    </div>
                </div>
	            <?php

	            ?>

            </form>
        </div>
    </div>
</div>
<?php
