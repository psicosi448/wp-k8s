<div class="row">
	<?php
    global $wp_query;
	$big = 999999999; // need an unlikely integer

    $total = $wp_query->max_num_pages;

    $pages = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $total,
		'type'  => 'array',
		'prev_text'          => __( '<span aria-hidden="true"><</span>' ),
		'next_text'          => __( '<span aria-hidden="true">></span>' ),
	) );
	if( is_array( $pages ) ) {
		echo '<div class="col-md-12 st-pagination"><ul class="pagination">';
		foreach ( $pages as $page ) {
			echo "<li>$page</li>";
		}
		echo '</ul></div>';
	}
	?>
</div><?php
