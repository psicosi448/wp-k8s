<?php get_header();  ?>




    <div class="main-container"><!-- main container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<?php
					if(have_posts()) {
					while ( have_posts() ) {
					the_post();

					$anni = get_field("anni");
					$luogo_di_nascita = get_field("luogo_di_nascita");
					if(!get_field("nato-italia"))
						$luogo_di_nascita = get_field("stato_di_nascita");

					$data_di_nascita = get_field("data_di_nascita");
					$luogo_di_morte = get_field("luogo_di_morte");
					if(!get_field("morto-italia"))
						$luogo_di_morte = get_field("stato_di_morte");

					$data_di_morte = get_field("data_di_morte");

					$images =  get_field("fotografie");

					?>
                    <div class="memorie-block _detail-content"><!-- memorie block -->
                        <div class="well-default">
                            <div class="row">
                                <div class="col-md-4 memorie-img"><!-- memorie img -->

                                    <div class="swiper-container _personal-slide"><!-- slider -->
                                        <div class="swiper-wrapper">
											<?php
                                            $ci=0;
                                            if(count($images)) {
                                                foreach ($images as $image) {
                                                    if($image["immagine"]["sizes"]["defunto-square"]) {
                                                        $ci++;
                                                        ?>
                                                        <div class="swiper-slide">
                                                            <img class="img-responsive img-circle"
                                                                 src="<?php echo $image["immagine"]["sizes"]["defunto-square"]; ?>"/>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                            if($ci == 0) {
                                                ?>
                                                <div class="swiper-slide">
                                                    <img class="img-responsive img-circle"
                                                         src="<?php echo PLACEHOLDER_DEFUNTO; ?>"/>
                                                </div>
                                                <?php
                                            }
											?>
                                        </div>
										<?php
										if(count($images) > 1) {
											?>
                                            <div class="swiper-pagination"></div>
                                            <div class="swiper-button-prev"></div>
                                            <div class="swiper-button-next"></div>
											<?php
										}  ?>
                                    </div><!-- / slider -->


                                    <?php
                                    $array_full = array();
                                    $siti_personali = get_field("siti_personali");
                                    $altri_necrologi = get_field("altri_necrologi");
                                    $link_gedi = get_field("link_gedi");
                                    $array_link = array_merge($siti_personali, $altri_necrologi, $link_gedi);
                                     foreach ($array_link as $link){
                                         if($link["link"])
                                             $array_full[]=$link;
                                     }

                            if($array_full) {
	                            ?>
                                <div class="memorie-meta _memorie-web">
                                    <div class="_memorie-web--title">
                                        Ricordi on line:
                                    </div>
                                    <ul>
                                        <?php foreach ($array_full as $item ) { ?>
                                            <li id="_memorie-web-<?php echo sanitize_html_class($item["nome"]); ?>"><a href="<?php echo $item["link"];  ?>" target="_blank"><?php
                                                    if(trim($item["nome"])) echo $item["nome"]; else echo $item["link"];
                                                    ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
	                            <?php
                            }
                                ?>


                                </div><!-- / memorie img -->

                                <div class="col-md-8">
                                    <h2><?php echo $post->post_title; ?></h2>

                                    <div class="memorie-meta">
		                                <?php
		                                if($anni){ ?><span class="memorie-meta--age"><?php echo $anni; ?> anni</span><?php
		                                }
		                                if($anni && ($luogo_di_nascita || $luogo_di_morte || $data_di_morte || $data_di_nascita)) echo ", ";
		                                if($luogo_di_nascita){ ?><span id="memorie-meta--location-birth"><?php echo $luogo_di_nascita["label"] ?></span><?php }
		                                if($data_di_nascita){ ?><span id="memorie-meta--date-birth"><?php echo $data_di_nascita ?></span><?php }
		                                if(($luogo_di_nascita  || $data_di_nascita) && ($luogo_di_morte || $data_di_morte)) echo " - ";
		                                if($luogo_di_morte){ ?><span id="memorie-meta--location-last"><?php echo $luogo_di_morte["label"] ?></span><?php }
		                                if($data_di_morte){ ?><span id="memorie-meta--date-last"><?php echo $data_di_morte ?></span><?php }  ?>
                                    </div>

                                    <h3 class="memorie-subtitle"><?php echo get_field("sommario_necrologio"); ?></h3>
									<?php the_content(); ?>

                                    <?php
                                    $firma_redazionale = get_field("testata", "user_".$post->post_author);
                                    $firmato_da = get_field("firmato_da");

                                    if($firma_redazionale || $firmato_da){ ?>
                                        <div class="memorie-meta _memorie-firma">
                                            <?php
                                            if($firmato_da)
                                                echo $firmato_da;
                                            else
                                                echo $firma_redazionale;
                                            ?>
                                        </div>
                                    <?php }  ?>

                                </div>

                                <div class="col-md-12">
                                    <div class="_my-message-share">
                                        <!--
                                    <span class="_my-message-icon _my-message-icon-heart">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/heart.svg">
                                    </span>
                                    //-->
                                        <?php echo memorie_share_button(get_permalink()); ?>
                                        <span class="_my-message-alert"><a href="mailto:<?php echo EMAIL_ABUSE; ?>?subject=memorie.it:%20segnalazione%20contenuto&body=Contenuto%20inappropriato:%20<?php echo urlencode(get_permalink($post)); ?>">Il contenuto è inappropriato?</a></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
					?>
                </div>
				<?php
				}
				}else{
					get_template_part( "template-parts/content-none" );
				}
				?>
            </div>
        </div>
    </div>
    <!-- /.main container -->

<?php get_footer();