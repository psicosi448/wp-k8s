<?php get_header(); ?>

	<div class="main-container"><!-- main container -->
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<?php
					get_template_part("template-parts/search");
					?>
                    <?php
                    if(have_posts()) {
	                    while ( have_posts() ){
	                        the_post();
		                    get_template_part( 'template-parts/card/post', get_post_type() );
	                    }
	                    get_template_part( "template-parts/pagination" );
                    }else{
	                    get_template_part( "template-parts/content-none" );
                    }
					?>
				</div>
				<div class="col-md-4">
					<?php
					get_sidebar();
					?>
				</div>
			</div>
		</div>
	</div>

<?php  get_footer();