<?php get_header(); ?>


    <div class="main-container"><!-- main container -->
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php
                    get_template_part("template-parts/search");
                    ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'template-parts/card/post', get_post_type() ); ?>
                    <?php endwhile; ?>

                    <?php
                    get_template_part("template-parts/pagination");
                    ?>
                </div>
                <div class="col-md-4">
                    <?php
                    get_sidebar();
                    ?>
                </div>
            </div>
        </div>
    </div>





<?php  get_footer();
