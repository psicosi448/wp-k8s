<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php wp_head(); ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body <?php body_class(); ?>>
<?php get_template_part("template-parts/header-top"); ?>




<div class="page-header"><!-- page header -->
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo home_url(); ?>">
	                <?php get_template_part("template-parts/logo"); ?>
                </a>

            </div>
            <h1 class="page-title">
	            <?php echo get_bloginfo("title"); ?>
                <span><?php echo get_bloginfo("description"); ?></span>
            </h1>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="navigation ">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <a class="navbar-brand _sticky-only" href="<?php echo home_url(); ?>">
	                        <?php get_template_part("template-parts/logo"); ?>
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
	                    <?php
	                    $usermenu = "<ul class=\"nav navbar-nav\" id=\"account-profile\"></ul>";
	                    wp_nav_menu(
		                    array(
			                    'theme_location' => 'header',
			                    'container_class' => 'collapse navbar-collapse',
			                    'container_id' => 'main-navigation',
			                    'menu_class'      => 'nav navbar-nav',
			                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>'.$usermenu.'',
		                    )
	                    );
	                    ?>

                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.page header -->