<?php get_header();
global $allpages;
?>

	<div class="main-container"><!-- main container -->
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<?php
					get_template_part("template-parts/search");
					?>
                    <?php

					$elemperpage=20;

					if($_REQUEST['pagesearch'])
						$page=$_REQUEST['pagesearch'];
					else
						$page=1;

					$query = memorie_search_results($elemperpage, $page);

					// calcolo il totale per valutare quante pagine ci sono
					$totelem = $query["numFound"];

					// calcolo il numero di pagine
					$allpages=ceil($totelem/$elemperpage);

					$results = $query["results"];

					$cp=0;
					foreach ($results as $result){
					    $post_id = $result;
					    $post = get_post($post_id);
					    if($post){
						    setup_postdata($post);
						    get_template_part( 'template-parts/card/post', get_post_type() );
						    $cp++;
                        }else{
					        if($_GET["debug"])
    					        print_r($post);
                        }
                    }

					if($cp == 0){
					    echo "Nessuna memoria trovata con i parametri di ricerca utilizzati. ";
                    }else{
						get_template_part( "template-parts/pagination", "search" );
                    }
  					?>
				</div>
				<div class="col-md-4">
					<?php
					get_sidebar();
					?>
				</div>
			</div>
		</div>
	</div>

<?php  get_footer();