<?php
global $exclude_cta;
if(!$exclude_cta) {
	?>
	<div class="row">
		<div class="col-md-12">
			<a href="<?php

		//	if ( is_user_logged_in() ) {
				echo home_url( "segnala" );
		//	} else {
		//		echo wp_login_url( home_url( "segnala" ) );
		//	}

			?>" class="btn btn-primary btn-block">SEGNALA LA SCOMPARSA<br>DI UNA PERSONA CARA</a>
		</div>
	</div>
	<?php
}
	?>
<div class="row">
    <div class="col-md-12 widget-recent-posts"><!-- widget recent posts -->
        <div class="widget"><!-- widget -->
            <h3 class="widget-title">Memorie recenti</h3>
            <?php
            $recent_comments = get_comments( array(
	            'number'      => 8, // number of comments to retrieve.
	            'status'      => 'approve', // we only want approved comments.
	            'post_status' => 'publish', // limit to published comments.
	            'post_type' => 'defunto',
            ) );

            if ( $recent_comments ) {
	            foreach ( (array) $recent_comments as $comment ) {

		            // sample output - do something useful here
//		            echo '<a href="' . esc_url( get_comment_link( $comment ) ) . '">' . get_the_title( $comment->comment_post_ID ) . '</a>';

		            $firma = get_field("firma", $comment);

		            ?>
                    <div class="row memorie-post-block">
                        <div class="col-md-12">
                            <div class="memorie-post-desc"><!-- recent post desc -->
                                <span class="memorie-post-date"><?php echo get_comment_date("d/m/Y", $comment->comment_ID) ?></span>
                                <h3 class="memorie-post-title"><span class="memorie-post-from"><a href="<?php echo esc_url( get_comment_link( $comment ) ); ?>"><?php echo $firma; ?></a></span>
                                    in ricordo di <span class="memorie-post-to"><a href="<?php echo esc_url( get_permalink( $comment->comment_post_ID ) ); ?>"><?php echo get_the_title( $comment->comment_post_ID ) ?></a></span></h3>
                                <q class="memorie-post-text"><?php
	                                $excerpt_length = 30;
	                                $excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
	                                echo wp_trim_words( $comment->comment_content, $excerpt_length, $excerpt_more );
                                    ?></q>
                            </div>
                            <!-- /.recent post desc -->
                        </div>
                        <!-- /.recent post block -->
                    </div>
		            <?php

	            }
            }
            ?>
        </div>
    </div>
</div>
<?php
