<!-- debug inside comments //-->
<?php
/**
 * The template file for displaying the comments and comment form for the
 * Twenty Twenty theme.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
*/
if ( post_password_required() ) {
	return;
}


if ( $comments ) {
	$comments_number = absint( get_comments_number() );

	if ( comments_open()) {

	    $action = "#memoria-form-container";
//	    if(!is_user_logged_in())
//	        $action = wp_login_url(get_permalink()."#commentform");
		?>

        <div class="row">
            <div class="col-md-12 _my-message-cta">
                <a href="<?php echo $action; ?>" class="btn btn-primary">lascia una tua memoria</a>
            </div>
        </div>
		<?php
	}
        ?>

    <div class="comments" id="comments">
			<?php
			wp_list_comments('type=comment&callback=comment_memorie');

			$comment_pagination = paginate_comments_links(
				array(
					'echo'      => false,
					'end_size'  => 0,
					'mid_size'  => 0,
					'type'  => 'array',
					'prev_text'          => __( '<span aria-hidden="true"><</span>' ),
					'next_text'          => __( '<span aria-hidden="true">></span>' ),
				)
			);

			if( is_array( $comment_pagination ) ) {
				echo '<div class="col-md-12 st-pagination"><ul class="pagination">';
				foreach ( $pages as $page ) {
					echo "<li>$page</li>";
				}
				echo '</ul></div>';
			}
			?>

	</div><!-- comments -->

	<?php
}

if ( comments_open()) {

	if ( $comments ) {
		echo '<hr class="styled-separator is-style-wide" aria-hidden="true" />';
	}

	?>
    <div class="memorie-block" id="memoria-form-container"><!-- memorie block -->
        <h3>Lascia una tua memoria</h3>
        <p>Questo è lo spazio per condividere un messaggio e rendere omaggio alla memoria della persona cara.<br>
            Se lo desideri, puoi anche caricare una fotografia.<br>
            <span class="hide-logged">
                Per lasciare un messaggio è necessario essere registrati al sito. Se non sei ancora registrato, <a href="<?php echo wp_login_url(get_permalink()); ?>">clicca qui</a>. </span></p>
        <div class="well-default hide-not-logged">
            <div class="row">
                <div class="col-md-12" id="memorie-comment-form">

<?php

	comment_form(
		array(
		        'title_reply' => 'La tua memoria',
            'class_submit' => "btn btn-primary",
            'label_submit' => "Pubblica la tua memoria",
            'format' => 'html5',
            'logged_in_as' => '',
            'fields' => array(),
            'comment_field' => '<textarea id="comment" name="comment" maxlength="65525" required="required"></textarea>'
		)
	);

	?>
                </div>
            </div>
        </div>
    </div>

                    <?php

}
