<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<!-- /.main container -->
<div class="section-space footer"><!-- footer -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 footer-block">
                <div class="ft-logo ft-logo--gedi">
                    <!-- logo GEDI Digitial -->
                    <a href="https://www.gedispa.it/attivita/digitale.html">
                        <svg width="82px" height="36px" viewBox="0 0 494 259" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="Group" fill-rule="nonzero">
                                    <path d="M106.3,104.9 L106.3,92.7 L113.8,92.7 C118,92.7 119.4,91.3 119.4,86.1 L119.4,21.9 C119.4,16.7 118,15.3 113.8,15.3 L106.3,15.3 L106.3,3.1 L193,3.1 L193,42.5 L181.3,42.5 L181.3,35 C181.3,21.9 175.2,15.8 165.8,15.8 L149.4,15.8 C145.2,15.8 143.8,17.2 143.8,22.4 L143.8,47.2 L152.7,47.2 C156.9,47.2 158.8,43.9 158.8,38.8 L158.8,29.9 L169.6,29.9 L169.6,78.7 L158.8,78.7 L158.8,67.4 C158.8,62.2 156.5,59.9 152.2,59.9 L143.3,59.9 L143.3,86.6 C143.3,91.8 144.7,93.2 148.9,93.2 L163.4,93.2 C176.1,93.2 182.2,88.5 182.2,71.2 L182.2,64.6 L194.4,64.6 L194.4,105.9 L106.3,105.9 L106.3,104.9 L106.3,104.9 Z" id="Path" fill="#D62B1C"></path>
                                    <path d="M245.1,3.6 C261,3.6 271.4,6.9 279.8,13 C292,22.4 299.5,37.4 299.5,54.3 C299.5,71.2 292,86.2 279.8,95.6 C271.8,101.7 261,105 245.1,105 L213.7,105 L213.7,3.1 L245.1,3.1 L245.1,3.6 Z M272.7,32.2 C267.1,25.2 259.1,20.9 246.9,20.9 L232.8,20.9 L232.8,87.5 L247.3,87.5 C259,87.5 267.5,83.3 273.1,76.2 C277.8,70.6 280.6,62.6 280.6,54.2 C280.2,45.8 277.4,37.8 272.7,32.2" id="Shape" fill="#FFFFFF"></path>
                                    <rect id="Rectangle" fill="#FFFFFF" x="315.4" y="3.6" width="19.2" height="101.7"></rect>
                                    <path d="M20,53.8 C20,73.5 35,89 54.7,89 C64.1,89 71.6,85.7 77.2,81 L77.2,60.8 L53.3,60.8 L53.3,43.5 L96.4,43.5 L96.4,87.1 C87.5,98.8 72.5,106.8 53.7,106.8 C22.3,106.8 0.3,83.4 0.3,53.8 C0.3,24.3 22.3,0.8 53.7,0.8 C68.7,0.8 81.4,6.4 90.7,15.3 L78,28.9 C71.9,22.8 63.5,18.6 53.6,18.6 C34.1,18.6 20,34.6 20,53.8" id="Path" fill="#FFFFFF"></path>
                                    <path d="M64.6,247.9 C56.2,254 45.8,257.7 29.4,257.7 L0.3,257.7 L0.3,155.5 L29.4,155.5 C45.3,155.5 56.1,158.8 64.6,165.3 C77.3,174.7 84.8,189.7 84.8,206.6 C84.7,223.5 77.2,238.5 64.6,247.9 M60.4,172.4 C52.9,166.8 43.5,164 29.5,164 L9.3,164 L9.3,249.3 L29.5,249.3 C43.6,249.3 53.4,246.5 60.4,240.9 C70.2,232.9 76.3,220.3 76.3,206.7 C76.3,193 70.2,180.4 60.4,172.4" id="Shape" fill="#FFFFFF"></path>
                                    <rect id="Rectangle" fill="#FFFFFF" x="103" y="155.5" width="8.9" height="102.2"></rect>
                                    <path d="M213.2,205.2 L179.9,205.2 L179.9,196.8 L222.1,196.8 L222.1,239.9 C213.2,251.2 198.7,258.7 182.7,258.7 C152.2,258.7 130.2,236.2 130.2,206.2 C130.2,176.7 151.8,153.7 181.3,153.7 C195.8,153.7 209.4,159.3 218.3,169.2 L212.2,175.3 C205.2,167.3 193.9,162.2 181.3,162.2 C156.9,162.2 139.1,181.9 139.1,206.3 C139.1,231.1 156.9,250.4 182.2,250.4 C194.4,250.4 205.6,245.2 213.1,237.7 L213.2,205.2 L213.2,205.2 Z" id="Path" fill="#FFFFFF"></path>
                                    <rect id="Rectangle" fill="#FFFFFF" x="244.6" y="155.5" width="8.9" height="102.2"></rect>
                                    <polygon id="Path" fill="#FFFFFF" points="336 155.5 336 163.9 307 163.9 307 257.7 298.5 257.7 298.5 163.9 269.5 163.9 269.5 155.5"></polygon>
                                    <path d="M379.6,155.5 L423.7,257.7 L414.8,257.7 L401.2,226.8 L351.5,226.8 L337.9,257.7 L329,257.7 L373.1,155.5 L379.6,155.5 Z M355.2,218.3 L397.4,218.3 L376.3,169.1 L355.2,218.3 Z" id="Shape" fill="#FFFFFF"></path>
                                    <polygon id="Path" fill="#FFFFFF" points="446.2 249.3 494 249.3 494 257.7 437.3 257.7 437.3 155.5 446.2 155.5"></polygon>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
                <p>
                    <a href="https://www.gedispa.it/attivita/digitale.html">GEDI DIGITAL Srl</a> - Via Cristoforo Colombo n. 90 00147 Roma - Codice Fiscale e Partita Iva n. 06979891006 <br>Direzione e coordinamento di GEDI Gruppo Editoriale S.p.A
                    - <a id="kw-privacy-link" href="https://login.kataweb.it/static/privacy/">Privacy</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- /.footer -->

<?php wp_footer();

?>

    <script>

	    <?php



	    echo 'var ajaxurl = "' . admin_url('admin-ajax.php') . '";';

        if(is_page_template("page-templates/add-defunto.php")){ ?>


        jQuery("#acf-field_5e820a7c90e56").change(
            function() {
                var stringa = jQuery("#acf-field_5e820a7c90e56").val() + " " + jQuery("#acf-field_5e820a8b90e57").val();
                checkAjaxItemExist(stringa);
            }
        );
        jQuery("#acf-field_5e820a8b90e57").change(
            function() {
                var stringa = jQuery("#acf-field_5e820a7c90e56").val() + " " + jQuery("#acf-field_5e820a8b90e57").val();
                checkAjaxItemExist(stringa);
            }
        );

        function checkAjaxItemExist(stringa){
            jQuery(".memorie-alert").remove();
            var data = {
                action: 'defunto_exists',
                cerca: stringa
            };

            jQuery.post(ajaxurl, data, function(response) {
                if(response){

                    jQuery(".acf-field-5e820e666c0d2").before("<div class='memorie-alert'>" + response + "</div>");

                }
            });

        }

        <?php }


	    if(is_singular("defunto")){
	        /*
	        ?>

        var data = {
            action: 'is_user_logged_in'
        };

        jQuery.post(ajaxurl, data, function(response) {
            if(response == 'true') {
                // user is logged in, do your stuff here
                $(".hide-logged").hide();
            } else {
                // user is not logged in, show login form here
                $(".hide-not-logged").hide();
            }
        });
	        <?php
	        */
	         ?>
        var data = {
            action: 'comment_form',
            id: '<?php echo $post->ID; ?>'
        };


        var cookie = document.cookie.indexOf('wordpress_logged_in') !== -1;
        if(cookie){
            jQuery.post(ajaxurl, data, function(response) {
                if(response) {
                    // user is logged in, do your stuff here
                    jQuery("#memorie-comment-form").html(response);

                    // altra chiamata ajax per generare il nonce
                    var datanonce = {
                        action: 'comment_form_nonce',
                        id: '<?php echo $post->ID; ?>'
                    };

                    jQuery.post(ajaxurl, datanonce, function(responsenonce) {
                        acf.data["nonce"]=responsenonce;
                    });
                }
            });
        }else{
            var response =   '<?php echo 'Devi essere autenticato per poter lasciare una memoria.<br><br> <a class="btn btn-primary" href="'.wp_login_url(get_permalink($post->ID)).'">Accedi</a>'; ?>';
            jQuery("#memorie-comment-form").html(response);
        }




        var mySwiper = new Swiper ('._personal-slide', {
            autoHeight: true, //enable auto height
            spaceBetween: 30,
            effect: 'fade',
            speed: 1000,
            autoplay: {
                delay: 7000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
	    <?php
	    }
	    ?>

        var mySwiper = new Swiper ('._gedi-gnn-logo', {
            initialSlide: 0,
            autoHeight: false,
            direction: 'horizontal',
            autoplayStopOnLast: true, // loop false also

            effect: 'slide',
            centeredSlides: true,
            slidesOffsetBefore: 1,
            grabCursor: true,

            // ----
            loop: true,
            autoplay: {delay: 1, },
            freeMode: true,
            speed: 10000,

            breakpoints: {
                240: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                    speed: 7000,
                },
                420: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
                640: {
                    slidesPerView: 4,
                    spaceBetween: 20,
                },
                768: {
                    spaceBetween: 40,
                    slidesPerView:5,
                },
                1024: {
                    spaceBetween: 40,
                    slidesPerView:6,
                },
                1200: {
                    spaceBetween: 40,
                    slidesPerView:8,
                },
            },
        })


        <?php if(is_search()){ ?>
        jQuery('#advancedSearch').collapse();
        <?php } ?>
    </script>

</body>
</html>
