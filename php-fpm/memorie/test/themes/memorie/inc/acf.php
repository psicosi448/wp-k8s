<?php
global $array_comuni, $array_stati;


add_action('acf/save_post', 'save_post_type_post', 20); // fires after ACF
function save_post_type_post($post_id) {
    $post_type = get_post_type($post_id);
    if ($post_type == 'defunto') {

        $post_title = get_field('nome', $post_id). " " .get_field('cognome', $post_id);
	    $post_excerpt = get_field('sommario_necrologio', $post_id);
	    $post_content = get_field('testo_necrologio', $post_id);
        $post_name = sanitize_title($post_title);
        $post = array(
            'ID' => $post_id,
            'post_name' => $post_name,
            'post_title' => $post_title,
            'post_content' => $post_content,
	        'post_excerpt' => $post_excerpt
        );
        wp_update_post($post);

    }else if($post_type == 'ricordo'){

        // associo il ricordo come parent del defunto e creo un titolo

        $defunto = get_field('defunto', $post_id);

        $post_title =   get_field('firma', $post_id). " per " .$defunto->post_title;
        $post_name = sanitize_title($post_title);

        $post = array(
            'ID' => $post_id,
            'post_name' => $post_name,
            'post_title' => $post_title,
            'post_parent' => $defunto->ID,
        );
        wp_update_post($post);


    }

}


add_filter('acf/load_value/name=defunto', 'memorie_ricordo_acf_load_value', 10, 3);
function memorie_ricordo_acf_load_value( $field ) {
    global $post;
    if(is_singular("defunto")){
        $field = $post;
    }else if (isset($_GET['defunto'])) {
        $defunto = $_GET['defunto'];
        $field = $defunto;
    }
    return $field;
}



if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5e820a7384ee6',
        'title' => 'Cambi Defunto',
        'fields' => array(
            array(
                'key' => 'field_5e820a7c90e56',
                'label' => 'Nome',
                'name' => 'nome',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '33',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5e820a8b90e57',
                'label' => 'Cognome',
                'name' => 'cognome',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '33',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5e820c8f6c0cf',
                'label' => 'Anni',
                'name' => 'anni',
                'type' => 'number',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '33',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => '',
                'max' => '',
                'step' => '',
            ),
	        array(
		        'key' => 'field_5e820e666c0d2',
		        'label' => 'Sommario',
		        'name' => 'sommario_necrologio',
		        'type' => 'textarea',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => 0,
		        'wrapper' => array(
			        'width' => '',
			        'class' => '',
			        'id' => '',
		        ),
		        'default_value' => '',
		        'placeholder' => '',
		        'prepend' => '',
		        'append' => '',
		        'maxlength' => '',
	        ),
	        array(
		        'key' => 'field_5e820e6b6c0d3',
		        'label' => 'Testo',
		        'name' => 'testo_necrologio',
		        'type' => 'wysiwyg',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => 0,
		        'wrapper' => array(
			        'width' => '',
			        'class' => '',
			        'id' => '',
		        ),
		        'default_value' => '',
		        'tabs' => 'all',
		        'toolbar' => 'basic',
		        'media_upload' => 1,
		        'delay' => 0,
	        ),
	        array(
		        'key' => 'field_5e8db44c4cd55',
		        'label' => 'Nato in Italia',
		        'name' => 'nato-italia',
		        'type' => 'true_false',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => 0,
		        'wrapper' => array(
			        'width' => '',
			        'class' => '',
			        'id' => '',
		        ),
		        'message' => '',
		        'default_value' => 1,
		        'ui' => 1,
		        'ui_on_text' => '',
		        'ui_off_text' => '',
	        ),
            array(
                'key' => 'field_5e820efcf35c1',
                'label' => 'Luogo di Nascita',
                'name' => 'luogo_di_nascita',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
	                array(
		                array(
			                'field' => 'field_5e8db44c4cd55',
			                'operator' => '==',
			                'value' => '1',
		                ),
	                ),
                ),
                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => $array_comuni,
                'default_value' => array(
                ),
                'allow_null' => true,
                'multiple' => 0,
                'ui' => 1,
                'return_format' => 'array',
                'ajax' => 1,
                'placeholder' => '',
            ),
	        array(
		        'key' => 'field_67820efcf35c1',
		        'label' => 'Stato di Nascita',
		        'name' => 'stato_di_nascita',
		        'type' => 'select',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => array(
			        array(
				        array(
					        'field' => 'field_5e8db44c4cd55',
					        'operator' => '!=',
					        'value' => '1',
				        ),
			        ),
		        ),
		        'wrapper' => array(
			        'width' => '50',
			        'class' => '',
			        'id' => '',
		        ),
		        'choices' => $array_stati,
		        'default_value' => array(
		        ),
		        'allow_null' => true,
		        'multiple' => 0,
		        'ui' => 1,
		        'return_format' => 'array',
		        'ajax' => 1,
		        'placeholder' => '',
	        ),
            array(
                'key' => 'field_5e820f56f35c2',
                'label' => 'Data di Nascita',
                'name' => 'data_di_nascita',
                'type' => 'date_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'display_format' => 'd/m/Y',
                'return_format' => 'd/m/Y',
                'first_day' => 1,
            ),
	        array(
		        'key' => 'field_5e8d54356cd55',
		        'label' => 'Morto in Italia',
		        'name' => 'morto-italia',
		        'type' => 'true_false',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => 0,
		        'wrapper' => array(
			        'width' => '',
			        'class' => '',
			        'id' => '',
		        ),
		        'message' => '',
		        'default_value' => 1,
		        'ui' => 1,
		        'ui_on_text' => '',
		        'ui_off_text' => '',
	        ),
            array(
                'key' => 'field_5e82142886528',
                'label' => 'Luogo di Morte',
                'name' => 'luogo_di_morte',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
	                array(
		                array(
			                'field' => 'field_5e8d54356cd55',
			                'operator' => '==',
			                'value' => '1',
		                ),
	                ),
                ),                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => $array_comuni,
                'default_value' => array(
                ),
                'allow_null' => true,
                'multiple' => 0,
                'ui' => 1,
                'return_format' => 'array',
                'ajax' => 1,
                'placeholder' => '',
            ),
	        array(
		        'key' => 'field_5e82165656528',
		        'label' => 'Stato di Morte',
		        'name' => 'stato_di_morte',
		        'type' => 'select',
		        'instructions' => '',
		        'required' => 0,
		        'conditional_logic' => array(
			        array(
				        array(
					        'field' => 'field_5e8d54356cd55',
					        'operator' => '!=',
					        'value' => '1',
				        ),
			        ),
		        ),                'wrapper' => array(
		        'width' => '50',
		        'class' => '',
		        'id' => '',
	        ),
		        'choices' => $array_stati,
		        'default_value' => array(
		        ),
		        'allow_null' => true,
		        'multiple' => 0,
		        'ui' => 1,
		        'return_format' => 'array',
		        'ajax' => 1,
		        'placeholder' => '',
	        ),
            array(
                'key' => 'field_5e82141286527',
                'label' => 'Data di Morte',
                'name' => 'data_di_morte',
                'type' => 'date_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'display_format' => 'd/m/Y',
                'return_format' => 'd/m/Y',
                'first_day' => 1,
            ),
            array(
                'key' => 'field_5e8214550c8cb',
                'label' => 'Siti personali',
                'name' => 'siti_personali',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 1,
                'max' => 0,
                'layout' => 'table',
                'button_label' => 'Aggiungi un nuovo indirizzo',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5e8214770c8cc',
                        'label' => 'Nome',
                        'name' => 'nome',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5e8214830c8cd',
                        'label' => 'Link',
                        'name' => 'link',
                        'type' => 'url',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                    ),
                ),
            ),
            array(
                'key' => 'field_5e821538071c4',
                'label' => 'Altri Necrologi',
                'name' => 'altri_necrologi',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 1,
                'max' => 0,
                'layout' => 'table',
                'button_label' => 'Aggiungi un nuovo indirizzo',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5e821538071c5',
                        'label' => 'Nome',
                        'name' => 'nome',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5e821538071c6',
                        'label' => 'Link',
                        'name' => 'link',
                        'type' => 'url',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                    ),
                ),
            ),
            array(
                'key' => 'field_5e82155c55aba',
                'label' => 'Fotografie',
                'name' => 'fotografie',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 1,
                'max' => 4,
                'layout' => 'table',
                'button_label' => 'Aggiungi una nuova immagine',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5e821637aec8a',
                        'label' => 'Immagine',
                        'name' => 'immagine',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'medium',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '1.5',
                        'mime_types' => '',
                    ),
                ),
            ),
            array(
                'key' => 'field_5e8216bdd997d',
                'label' => 'Firmato da',
                'name' => 'firmato_da',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array(
	        array(
		        array(
			        'param' => 'post_type',
			        'operator' => '==',
			        'value' => 'defunto',
		        ),
		        array(
			        'param' => 'current_user',
			        'operator' => '==',
			        'value' => 'logged_in',
		        ),
	        ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

    acf_add_local_field_group(array(
        'key' => 'group_5e8217815a586',
        'title' => 'Campi Editoriali',
        'fields' => array(
            array(
                'key' => 'field_5e821810a9e28',
                'label' => 'Link siti del gruppo',
                'name' => 'link_gedi',
                'type' => 'repeater',
                'instructions' => 'link a eventuali articoli fatti sui nostri giornali/siti',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 1,
                'max' => 0,
                'layout' => 'table',
                'button_label' => 'Aggiungi un nuovo indirizzo',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5e821830a9e29',
                        'label' => 'Nome',
                        'name' => 'nome',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5e821849a9e2a',
                        'label' => 'Link',
                        'name' => 'link',
                        'type' => 'url',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                    ),
                ),
            ),/*
            array(
                'key' => 'field_5e82198ab5e9a',
                'label' => 'Firma Redazionale',
                'name' => 'firma_redazionale',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => $array_testate,
                'default_value' => array(
                ),
                'allow_null' => true,
                'multiple' => 0,
                'ui' => 1,
                'return_format' => 'value',
                'ajax' => 1,
                'placeholder' => '',
            ),*/
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'defunto',
                ),
                array(
                    'param' => 'current_user_role',
                    'operator' => '==',
                    'value' => 'administrator',
                ),
            ),
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'defunto',
                ),
                array(
                    'param' => 'current_user_role',
                    'operator' => '==',
                    'value' => 'editor',
                ),
            ),
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'defunto',
                ),
                array(
                    'param' => 'current_user_role',
                    'operator' => '==',
                    'value' => 'author',
                ),
            ),
        ),
        'menu_order' => 10,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;


/**
 * ricordi
 */
/*
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5e825d2b55ffd',
        'title' => 'Campi Ricordo',
        'fields' => array(
            array(
                    'key' => 'field_5e825d37f87cf',
                'label' => 'Defunto a cui fa riferimento il ricordo',
                'name' => 'defunto',
                'type' => 'post_object',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array(
                    0 => 'defunto',
                ),
                'taxonomy' => '',
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'object',
                'ui' => 1,
            ),
            array(
                'key' => 'field_5e8260b7b0655',
                'label' => 'Immagine',
                'name' => 'immagine',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'medium',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => 10,
                'mime_types' => '',
            ),
            array(
                'key' => 'field_5e82655805963',
                'label' => 'Firma',
                'name' => 'firma',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'ricordo',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;

*/


if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5e84b267a26b3',
        'title' => 'Commenti',
        'fields' => array(
	        array(
		        'key' => 'field_5e84b34a75bf2',
		        'label' => 'Firma',
		        'name' => 'firma',
		        'type' => 'text',
		        'instructions' => '',
		        'required' => 1,
		        'conditional_logic' => 0,
		        'wrapper' => array(
			        'width' => '',
			        'class' => 'acf-col-6',
			        'id' => '',
		        ),
		        'default_value' => '',
		        'placeholder' => '',
		        'prepend' => '',
		        'append' => '',
		        'maxlength' => '',
	        ),
            array(
                'key' => 'field_5e84b26e40976',
                'label' => 'Immagine',
                'name' => 'immagine',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => 'acf-col-6',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'medium',
                'library' => 'uploadedTo',
                'min_width' => 200,
                'min_height' => '',
                'min_size' => '',
                'max_width' => 800,
                'max_height' => '',
                'max_size' => '1.5',
                'mime_types' => 'gif,jpg,jpeg,png',
            ),

        ),
        'location' => array(
            array(
                array(
                    'param' => 'comment',
                    'operator' => '==',
                    'value' => 'defunto',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;

/**
* testata utente
 **/

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5e8dc7d2a5455',
		'title' => 'Testata',
		'fields' => array(
			array(
				'key' => 'field_5e8dc7dabb046',
				'label' => 'Testata',
				'name' => 'testata',
				'type' => 'text',
				'instructions' => 'Digita la testata, che verrà inserita in coda alla firma dei contenuti inseriti dall\'utente',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'current_user_role',
					'operator' => '==',
					'value' => 'administrator',
				),
				array(
					'param' => 'user_form',
					'operator' => '==',
					'value' => 'edit',
				),
			),
			array(
				array(
					'param' => 'current_user_role',
					'operator' => '==',
					'value' => 'editor',
				),
				array(
					'param' => 'user_form',
					'operator' => '==',
					'value' => 'edit',
				),
			),
			array(
				array(
					'param' => 'current_user_role',
					'operator' => '==',
					'value' => 'author',
				),
				array(
					'param' => 'user_form',
					'operator' => '==',
					'value' => 'edit',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;