<?php

/**
 * listo post type defunto in home e archivi
 * @param $query
 */
function memorie_set_defunto_archive( $query ) {
    if ( !is_admin() && $query->is_main_query() && (is_front_page() || is_archive() ) ) {
        $query->set( 'post_type', 'defunto' );
    }
}
add_action( 'pre_get_posts', 'memorie_set_defunto_archive' );

/**
 * aggiungo defunto in colonna
 * @param $actions
 * @param $post
 * @return array
 */
function modify_list_row_actions( $actions, $post ) {
    // Check for your post type.
    if ( $post->post_type == "defunto" ) {

        // Build your links URL.
        $url = admin_url( 'post-new.php?post_type=ricordo&defunto=' . $post->ID );
         $actions = array_merge( $actions, array(
                'ricordo' => sprintf( '<a href="%1$s">%2$s</a>',
                    esc_url( $url ),
                    'Crea ricordo'
                )
            ) );
    }
    return $actions;
}
add_filter( 'post_row_actions', 'modify_list_row_actions', 10, 2 );


/**
 * Register Custom Post Type
 */
function memorie_post_type() {

	$labels = array(
		'name'                  => _x( 'Defunti', 'Post Type General Name', 'memorie' ),
		'singular_name'         => _x( 'Defunto', 'Post Type Singular Name', 'memorie' ),
		'menu_name'             => __( 'Defunti', 'memorie' ),
		'name_admin_bar'        => __( 'Defunti', 'memorie' ),
		'all_items'             => __( 'Tutti i Defunti', 'memorie' ),
		'add_new_item'          => __( 'Aggiungi Defunto', 'memorie' ),
		'add_new'               => __( 'Aggiungi ', 'memorie' ),
		'new_item'              => __( 'Nuovo Defunto', 'memorie' ),
		'edit_item'             => __( 'Modifica Defunto', 'memorie' ),
		'update_item'           => __( 'Aggiorna Defunto', 'memorie' ),
		'view_item'             => __( 'Vedi Defunto', 'memorie' ),
		'view_items'            => __( 'Vedi Defunti', 'memorie' ),
		'search_items'          => __( 'Cerca Defunto', 'memorie' ),
	
	);
	$args = array(
		'label'                 => __( 'Defunto', 'memorie' ),
		'description'           => __( 'Defunto', 'memorie' ),
		'labels'                => $labels,
		'supports'              => array( 'author', 'comments'),
		'rewrite'               => array("slug"=>"ricordo"),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-money',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'defunto', $args );

/*

    $labels = array(
        'name'                  => _x( 'Ricordi', 'Post Type General Name', 'memorie' ),
        'singular_name'         => _x( 'Ricordo', 'Post Type Singular Name', 'memorie' ),
        'menu_name'             => __( 'Ricordi', 'memorie' ),
        'name_admin_bar'        => __( 'Ricordi', 'memorie' ),
        'all_items'             => __( 'Tutti i Ricordi', 'memorie' ),
        'add_new_item'          => __( 'Aggiungi Ricordo', 'memorie' ),
        'add_new'               => __( 'Aggiungi ', 'memorie' ),
        'new_item'              => __( 'Nuovo Ricordo', 'memorie' ),
        'edit_item'             => __( 'Modifica Ricordo', 'memorie' ),
        'update_item'           => __( 'Aggiorna Ricordo', 'memorie' ),
        'view_item'             => __( 'Vedi Ricordo', 'memorie' ),
        'view_items'            => __( 'Vedi Ricordi', 'memorie' ),
        'search_items'          => __( 'Cerca Ricordo', 'memorie' ),
    );
    $args = array(
        'label'                 => __( 'Ricordo', 'memorie' ),
        'description'           => __( 'Ricordo', 'memorie' ),
        'labels'                => $labels,
        'supports'              => array('editor'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-status',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
    );
    register_post_type( 'ricordo', $args );
*/

}
add_action( 'init', 'memorie_post_type', 0 );


add_filter('post_class', 'memorie_set_row_post_class', 10,3);
function memorie_set_row_post_class($classes, $class, $post_id){
	if (!is_admin()) { //make sure we are in the dashboard
		return $classes;
	}
	$post = get_post($post_id);
	$user_meta=get_userdata($post->post_author);
	$user_roles=$user_meta->roles; //array of roles the user is part of.

	foreach ($user_roles as $user_role){
		$classes[] = "role-".$user_role;
	}

	// Return the array
	return $classes;
}
