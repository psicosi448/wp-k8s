<?php



function memorie_search_results($max = 20, $page = 1){
	$qry = get_search_query();
    //$qry = html_entity_decode($qry);
    //$qry = htmlentities($qry);
//    $qry = stripslashes($_GET['s']);

	$skip=$max * ($page -1);
	// rimuovo i doppi spazi
	//	while (strpos($qry,'  ') !== false)
	//		$qry = str_replace('  ',' ',$qry);

	$search_options['q']='(';


	if($qry == ""){
		$search_options['q'].='( *:* )';
	}else{
		$search_options['q'].='( (nome_defunto:*'.$qry.'*) OR  (cognome_defunto:*'.$qry.'*) ) ';

	}


	if($_GET["luogo_di_nascita"])
		$search_options['q'].=' AND (luogo_di_nascita:*'.preg_quote($_GET["luogo_di_nascita"]).'*) ';

	if($_GET["luogo_di_morte"])
		$search_options['q'].=' AND (luogo_di_morte:*'.preg_quote($_GET["luogo_di_morte"]).'*) ';



	if($_GET["data_di_nascita"]){
		$arrdata = explode("-", $_GET["data_di_nascita"]);
		$search_options['q'].=' AND (data_di_nascita:['.$arrdata["0"]."-".$arrdata["1"]."-".$arrdata["2"].'T00:00:00Z TO '.$arrdata["0"]."-".$arrdata["1"]."-".$arrdata["2"].'T23:59:59Z]) ';
	}

	if($_GET["data_di_morte"]){
		$arrdata = explode("-", $_GET["data_di_morte"]);
		$search_options['q'].=' AND (data_di_morte:['.$arrdata["0"]."-".$arrdata["1"]."-".$arrdata["2"].'T00:00:00Z TO '.$arrdata["0"]."-".$arrdata["1"]."-".$arrdata["2"].'T23:59:59Z]) ';
	}

	$search_options['q'].=')';



	//$search_options['q']='*:*';

	$search_options['rows']=$max;
	$search_options['start']=$skip;
	//$search_options['sort']="timestamp desc";
	//	$search_options['sort']="pubdate desc";

	$query_url = MEMORIE_SOLR_URL.'?'.http_build_query($search_options);
	//	$query_url ="http://test-solrcloud.am.kataweb.it:8888/solr/memorie/select?q=2318";

	if($_GET["debug"]) {
		echo $query_url;
	}
	$response = wp_remote_get( $query_url );
	$response_code = wp_remote_retrieve_response_code( $response );
	if($response_code  == 200) {
		$xmlresult = wp_remote_retrieve_body( $response );
		$xml = simplexml_load_string( $xmlresult );

		if($_GET["debug"]) {
			echo "<hr><pre>";
			print_r($xmlresult);
			echo "</pre><hr>";
		}
		$numFound = $xml->result->attributes()->numFound;

		foreach ( $xml->result->doc as $doc ) {
			foreach ( $doc as $docitem ) {
				$attr =  (string)$docitem->attributes()->name[0];
				if ( $attr == "id" ) {
					$arrid       = explode( ":", $docitem );
					$idpost      = $arrid[2];

					if($_GET["debug"]) {
						echo "<hr>Attr:".$attr;
						echo $idpost."<br>";
						echo "<hr>";
					}
					$ret['id'][] = $idpost;
				}

			}
		}
		$retu["numFound"] = (string)$numFound;
		$retu["results"]  = $ret["id"];

	}else{
		if($_GET["debug"]){
			echo "<pre>";
			print_r($response);
			echo "</pre>";
		}
		$retu["numFound"] = 0;
		$retu["results"]  = array();
	}

	if($_GET["debug"]) {
		echo "<pre>";
		print_r($retu);
		echo "</pre>";

	}
	return $retu;
}
