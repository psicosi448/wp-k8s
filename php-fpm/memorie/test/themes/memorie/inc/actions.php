<?php

/**
 * Disable the emoji's
 */
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

        $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }

    return $urls;
}

/**
 * ajax per commento
 */
function ajax_get_comment_form() {
    $id = trim($_REQUEST["id"]);
    if(!is_user_logged_in()){
        echo "Devi essere autenticato per poter lasciare una memoria.<br><br><a class='btn btn-primary' href='".wp_login_url(get_permalink($id))."'>Accedi</a>";
	    die();
    }

    if($id){
	    global $post;
	    $post = get_post($id);

        comment_form(
            array(
                'title_reply' => 'La tua memoria',
                'class_submit' => "btn btn-primary",
                'label_submit' => "Pubblica la tua memoria",
                'format' => 'html5',
                'logged_in_as' => '',
                'fields' => array(),
                'comment_field' => '<textarea id="comment" name="comment" maxlength="65525" required="required"></textarea>'
            ),
            $id
        );
    }
    die();
}
add_action('wp_ajax_comment_form', 'ajax_get_comment_form');
add_action('wp_ajax_nopriv_comment_form', 'ajax_get_comment_form');



/**
 * ajax per nonce commento
 */
function ajax_get_comment_form_nonce() {

    echo wp_create_nonce("acf_nonce");
    die();
}
add_action('wp_ajax_comment_form_nonce', 'ajax_get_comment_form_nonce');
add_action('wp_ajax_nopriv_comment_form_nonce', 'ajax_get_comment_form_nonce');





add_action('admin_init', 'redirect_subscriber_users' );
/**
 * Redirect non-admin users to home page
 *
 * This function is attached to the ‘admin_init’ action hook.
 */
function redirect_subscriber_users() {
	if ( ! current_user_can( 'edit_posts' ) &&  ! wp_doing_ajax() ) {
		wp_redirect( home_url() );
		exit;
	}
}

if ( ! current_user_can( 'edit_posts' ) ) { show_admin_bar( false ); }




/**
 * ajax per controllo utente esistente
 */
function ajax_check_defunto_exists() {
    $ricerca = trim($_REQUEST["cerca"]);
	$found_post = post_exists( $ricerca,'','','defunto');
    if($found_post){
        echo "Esiste già una memoria dedicata a  ".$ricerca.".  <a target='_BLANK' href='".get_permalink($found_post)."'>Verifica che non si tratti della stessa persona</a>";

    }
	die();
}
add_action('wp_ajax_defunto_exists', 'ajax_check_defunto_exists');
add_action('wp_ajax_nopriv_defunto_exists', 'ajax_check_defunto_exists');




/**
 * ajax per controllo utente loggato
 */
function ajax_check_user_logged_in() {
    echo is_user_logged_in()?'true':'false';
    die();
}
add_action('wp_ajax_is_user_logged_in', 'ajax_check_user_logged_in');
add_action('wp_ajax_nopriv_is_user_logged_in', 'ajax_check_user_logged_in');

// blocco il salvataggio del commento per utenti non loggati

add_action("pre_comment_on_post", "memorie_check_comment");
function memorie_check_comment(){
    if(!is_user_logged_in())
        wp_die('Utente non loggato');
}

/**
 * webtrekk params
 */
function webtrekk_nielsen(){
	$wt_section = '';
	if(is_home() || is_front_page()):
		$pageType = 'homepage';
		$wt_section = 'homepage';
		$pageHref = get_bloginfo('url');
	elseif (is_archive()):
		$pageType = 'homepage_sezione';
		if(is_category()):
			$wt_section = get_queried_object()->slug;
			$pageHref = get_category_link(get_queried_object());
		elseif (is_author()):
			$pageHref = get_author_posts_url( get_queried_object_id());
		elseif (is_tag()):
			$pageHref = get_tag_link(get_queried_object_id());
		elseif (is_tax()):
			$pageHref = get_term_link(get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		endif;
	elseif (is_search()):
		$pageType = 'lista_ricerca';
		$pageHref = get_bloginfo('url') . '/?s=';
		$pageSearchKeyword = get_search_query();
	elseif (is_single()):
		global $post;
		$pageType = 'dettaglio_content';
		$pageHref = get_permalink($post);
		$category = get_the_category($post->ID);
		if($category)
			$wt_section = $category[0]->slug;
		$tags = wp_get_post_tags($post->ID, ['fields' => 'names']);
		if($tags && !empty($tags)){
			$varTags = implode(',', $tags);
		}
	endif; ?>

	<script type="text/javascript">
        var pageType = "<?php echo $pageType; ?>";
        var pageHref = "<?php echo $pageHref; ?>";
		<?php if(isset($pageSearchKeyword) && !empty($pageSearchKeyword)): ?>
        var pageSearchKeyword="<?php echo $pageSearchKeyword; ?>";<?php endif; ?>
		<?php if(isset($varTags)): ?>        var tags = "<?php echo $varTags; ?>";<?php endif; ?>
        var __wt_section = '/memorie/<?php echo $wt_section; ?>';
	</script>

	<?php
}
add_action('wp_head', 'webtrekk_nielsen', 1);


/**
 * adsetup
 */
function bi_ad_setup(){
	$ad_setup = '//oasjs.kataweb.it/adsetup_pcmp.js';
	?><esi:include src="http://www.repstatic.it/cless/common/stable/js/script/tlh/traffic_light_handler.html" />
	<?php
	echo '<script async onload="try { kw_tlh_ready(); } catch(e) {}" onerror="try { asr_error() } catch(e) {}" src="' . $ad_setup . '"></script>'."\n\n";

}
add_action('wp_head', 'bi_ad_setup', 1);

/**
 * webtrek & nielsen footer
 */
function bi_repstatic_nielsen(){ ?>

<esi:include src="http://www.repstatic.it/cless/common/stable/include/wt/wt.html" />
<esi:include src="http://www.repstatic.it/cless/common/stable/include/nielsen/nielsen.html" />
	<?php
}
add_action('wp_footer', 'bi_repstatic_nielsen', 100);


/**
 * funzione per gestire i dati di share
 * @param $url
 * @param $containerID
 *
 * @return string
 */
function memorie_share_data($url, $containerID){
	$data = array(
			"containerID" => $containerID
    	    ,"gsUrl" => $url
    	    ,"gsOrderButtons" => "gs-customtpl-whatsapp,gs-customtpl-facebook,gs-customtpl-twitter"
    	    ,"gsVia" => "memorie.it"
    	    ,"gsButtons" => array(
    	        array("provider" => "gs-customtpl-facebook", "action"=>"share")
    	        ,array("provider" => "gs-customtpl-twitter")
    	        ,array("provider" => "gs-customtpl-whatsapp")
    	    )
	);
	return htmlspecialchars(json_encode($data));
}


function memorie_share_button($url){
	$containerID = "share-balloon-".uniqid();
	return '<div class="share-button gs-social-popup">
                <div class="gs-social-popup-trigger" data-sharebuttons="'.memorie_share_data($url, $containerID).'">
                    <span class="_my-message-icon"><img src="'.get_template_directory_uri().'/img/share.svg"></span>
                </div>
                <div id="'.$containerID.'" class="share-balloon gs-social-popup-balloon"></div>
            </div>';
}

function memoria_admin_style() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin.css');
}
add_action('admin_enqueue_scripts', 'memoria_admin_style');