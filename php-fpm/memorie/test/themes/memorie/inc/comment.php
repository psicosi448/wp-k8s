<?php
/**
 * commenti
 */

/*
add_filter( 'comments_open', 'memorie_comments_open', 10, 2 );

function memorie_comments_open( $open, $post_id ) {

    $post = get_post( $post_id );

    if ( 'defunto' == $post->post_type )
        $open = true;

    return $open;
}
*/

function comment_memorie($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>

	<div class="memorie-block " id="comment-<?php comment_ID(); ?>"><!-- memorie block -->
		<div class="well-default _my-message">
			<div class="row">
				<div class="col-md-9 _my-message-name">
					<h3><?php echo get_field("firma", $comment); ?> <?php edit_comment_link(__('(modifica)'),' &nbsp; <small>','</small>') ?></h3>

				</div>
				<div class="col-md-3 _my-message-date">
					<span><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></span>
					<!--<span class="_my-message-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/push-pin.svg"></span> //-->

				</div>
				<div class="col-md-12">
					<div class="_my-message-image">
						<?php
						$immagine = get_field("immagine", $comment);
						if($immagine){ ?>
							<img src="<?php echo $immagine["sizes"]["defunto-comment"] ?>">
						<?php }  ?>
					</div>
					<?php if ($comment->comment_approved == '0') : ?>
						<em><?php _e('Your comment is awaiting moderation.') ?></em>
						<br />
					<?php endif; ?>
                    <?php echo wpautop(get_comment_text($comment)); ?>

				</div>
				<div class="col-md-12">
					<div class="_my-message-share">
					<!--
						<span class="_my-message-icon _my-message-icon-heart"><img src="<?php echo get_template_directory_uri(); ?>/img/heart.svg"></span>
						//-->
						<?php echo memorie_share_button(get_comment_link($comment)); ?>
                        <span class="_my-message-alert"><a href="mailto:<?php echo EMAIL_ABUSE; ?>?subject=memorie.it:%20segnalazione%20contenuto&body=Contenuto%20inappropriato:%20<?php echo urlencode(get_comment_link($comment)); ?>">Il contenuto è inappropriato?</a></span>

                    </div>
				</div>
			</div>
		</div>
	</div>
	<?php
}

