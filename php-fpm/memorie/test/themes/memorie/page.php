<?php get_header();  ?>




    <div class="main-container"><!-- main container -->
        <div class="container">
            <div class="row">
                <div class="col-md-8">
					<?php
					if(have_posts()) {
						while ( have_posts() ) {
							the_post();


							?>

                            <div class="memorie-block _about-content"><!-- memorie block -->
                                <div class="well-default">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2><?php the_title(); ?></h2>
											<?php
											the_content();
											?>
                                        </div>
                                    </div>
                                </div>
                            </div>

							<?php
						}
					}else{
						get_template_part( "template-parts/content-none" );
					}
					?>
                </div>
                <div class="col-md-4">
					<?php
					get_sidebar();
					?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.main container -->

<?php get_footer();