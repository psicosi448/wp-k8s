<?php /* Template Name: Redirect pagina utente */
if(is_user_logged_in()){
	$current_user_id = get_current_user_id();
	wp_redirect(get_author_posts_url($current_user_id));
}else{
	wp_redirect(home_url());
}
?>
