<?php /* Template Name: Aggiungi Defunto */ ?>

<?php


get_header();
acf_form_head();

?>

<div class="main-container"><!-- main container -->
    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <div class="well-default">
                    <div class="row">
                        <div class="col-md-12">

				<?php
				if(have_posts()) {
					while ( have_posts() ){
						the_post();

						?>
                <h1><?php the_title(); ?></h1>
                <?php
						if(is_user_logged_in()){
							acf_form(array(
								'post_id'		=> 'new_post',
								'post_title'	=> false,
								'post_content'	=> false,
								'field_groups' => array('group_5e820a7384ee6'),
								'new_post'		=> array(
									'post_type'		=> 'defunto',
									'post_status'	=> 'draft'
								),
                                'return'		=> home_url('grazie-della-segnalazione'),
								'submit_value'	=> 'Segnala il defunto'
							));

						}else{
						    echo "Devi essere autenticato per accedere a questa pagina. <br><br><a  class='btn btn-primary' href='".wp_login_url(get_permalink())."'>Accedi</a>";
                        }


					}
				}else{
					get_template_part( "template-parts/content-none" );
				}
				?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
				<?php
                global $exclude_cta;
				$exclude_cta = true;
				get_sidebar();
				?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
